// standard C++ headers
#include <algorithm>
#include <bitset>

// ROOT generic
#include <TSystem.h>
#include <TTree.h>
#include <TTreeFormula.h>
#include <TH1F.h>
#include <TFile.h>

// event loop
#include <EventLoop/Job.h>
//#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

// my code
#include <DileptonST/errorcheck.h>
#include <DileptonST/DileptonNTUPMaker.h>
#include <DileptonST/HemisphereCalculation.h>
#include <DileptonST/RazorCalculation.h>
#include <DileptonST/GetTruthBosonP4.h>

// EDM
#include <xAODRootAccess/Init.h>
#include <xAODRootAccess/TEvent.h>
#include "xAODRootAccess/TStore.h"
#include <xAODBase/IParticleHelpers.h>
#include <PATInterfaces/SystematicVariation.h>
#include <xAODJet/JetAuxContainer.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthEventContainer.h>
#include <xAODTruth/TruthEvent.h>
#include <xAODMissingET/MissingETAuxContainer.h>
#include <xAODCutFlow/CutBookkeeper.h>
#include <xAODCutFlow/CutBookkeeperContainer.h>
#include "CxxUtils/make_unique.h"

// tools
//#include "PileupReweighting/PileupReweightingTool.h"
#include <xAODEgamma/EgammaxAODHelpers.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>
#include "FourMomUtils/xAODP4Helpers.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODRootAccess/tools/TFileAccessTracer.h"
// #include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h" // TODO!!! NOAM

// utils
#include <DileptonST/Common.h>

// this is needed to distribute the algorithm to the workers
ClassImp(DileptonNTUPMaker)


DileptonNTUPMaker :: DileptonNTUPMaker() :
   m_grl(nullptr),
   // m_electronIsolationSFTool(""), // TODO!!! NOAM
   m_objTool(nullptr),
   m_determinedDerivation(kFALSE),
   m_isEXOT5(kFALSE),
   m_histoEventCount(nullptr)
{
   // Here you put any code for the base initialization of variables,
   // e.g. initialize all pointers to 0.  Note that you should only put
   // the most basic initialization here, since this method will be
   // called on both the submission and the worker node.  Most of your
   // initialization code will go into histInitialize() and
   // initialize().
   //
   gSystem->Unsetenv("ROOT_TTREECACHE_SIZE"); // bug workaround for memory consumption


   // temporary fix to avoid Base,2.4.21 issues (https://its.cern.ch/jira/browse/ATLASG-809)
   xAOD::TFileAccessTracer::enableDataSubmission(kFALSE);
}



EL::StatusCode DileptonNTUPMaker :: setupJob(EL::Job& job)
{
   // Here you put code that sets up the job on the submission object
   // so that it is ready to work with your algorithm, e.g. you can
   // request the D3PDReader service or add output files.  Any code you
   // put here could instead also go into the submission script.  The
   // sole advantage of putting it here is that it gets automatically
   // activated/deactivated when you add/remove the algorithm from your
   // job, which may or may not be of value to you.

   job.useXAOD();

   xAOD::Init("DileptonNTUPMaker").ignore();   // call before opening first file

   // add output stream (i.e. files in the data-XXX directory)
   EL::OutputStream output("minitrees");
   job.outputAdd(output);

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode DileptonNTUPMaker :: histInitialize()
{
   // Here you do everything that needs to be done at the very
   // beginning on each worker node, e.g. create histograms and output
   // trees.  This method gets called before any input files are
   // connected.
   Info("histInitialize", "Initializing cut flow histogram");

   TString cf_name("SignalRegion");

   Info("histInitialize", "Name will be %s", cf_name.Data());
   m_cutFlow = Analysis::CutFlowTool(cf_name);
   m_cutFlow.addCut("processed");
   m_cutFlow.addCut("GRL");
   m_cutFlow.addCut("cleaning");
   //m_cutFlow.addCut("vertex");
   //m_cutFlow.addCut("MET_cleaning");

   Info("histInitialize", "Initializing event count histogram (derivation-proof)");
   m_histoEventCount = new TH1F("histoEventCount", "event count (derivation-proof, only MC weight if any)", 100, 0, 100);
   m_histoEventCount->Fill("initial_weighted", 0);
   m_histoEventCount->Fill("initial_raw", 0);

   wk()->addOutput(m_histoEventCount);

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode DileptonNTUPMaker :: fileExecute()
{
   // Here you do everything that needs to be done exactly once for every
   // single file, e.g. collect a list of all lumi-blocks processed

   const char *APP_NAME = "DileptonNTUPMaker::fileExecute";

   xAOD::TEvent *event = wk()->xaodEvent();

   //
   // read original number of processed events, if any
   // from https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisMetadata
   //
   //

   TTree *MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
   if (!MetaData) {
      Error(APP_NAME, "MetaData not found!");
      return EL::StatusCode::FAILURE;
   }
   MetaData->LoadTree(0);

   const Bool_t isDerivation = !MetaData->GetBranch("StreamAOD");
   const Bool_t shouldDoThis = (wk()->metaData()->castString("isData") == "NO"); // m_isMC not filled yet

   Info(APP_NAME, "is this MC = %d (used to decide if reading CutBookkeeperContainer or not)", (Int_t)shouldDoThis);

   //get sum of event weights in derivation skims
   if (isDerivation && shouldDoThis) {

      // set pointers & retrieve the bookkeeper container
      const xAOD::CutBookkeeperContainer* bookkeepers = nullptr;
      if (!event->retrieveMetaInput(bookkeepers, "CutBookkeepers").isSuccess()) {
         Error(APP_NAME, "Failed to retrieve CutBookkeepers from MetaData");
         return EL::StatusCode::FAILURE;
      }
      const xAOD::CutBookkeeper* event_bookkeeper = nullptr;

      // find the max cycle where input stream is StreamAOD and the name is AllExecutedEvents
      int maxCycle = -1;
      for (auto cbk : *bookkeepers) {
         if (cbk->inputStream() == "StreamAOD" && cbk->name() == "AllExecutedEvents" && cbk->cycle() > maxCycle) {
            maxCycle = cbk->cycle();
            event_bookkeeper = cbk;
         }
      }
      // if the right & proper bookkeeper is found, read info
      if (event_bookkeeper) {
         m_histoEventCount->Fill("initial_weighted", event_bookkeeper->sumOfEventWeights());
         m_histoEventCount->Fill("initial_raw", event_bookkeeper->nAcceptedEvents());
      }

   } // derivation

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode DileptonNTUPMaker :: changeInput(bool firstFile)
{
   // Here you do everything you need to do when we change input files,
   // e.g. resetting branch addresses on trees.  If you are using
   // D3PDReader or a similar service this method is not needed.

   if(false) std::cout << firstFile << std::endl;

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode DileptonNTUPMaker :: initialize()
{
   // Here you do everything that you need to do after the first input
   // file has been connected and before the first event is processed,
   // e.g. create additional histograms based on which variables are
   // available in the input files.  You can also create all of your
   // histograms and trees in here, but be aware that this method
   // doesn't get called if no events are processed.  So any objects
   // you create here won't be available in the output if you have no
   // input events.

   const char *APP_NAME = "DileptonNTUPMaker::initialize";
   m_isMC = (wk()->metaData()->castString("isData") == "NO");
   isAFII = (wk()->metaData()->castString("isAFII") == "YES");
   TString workdirlocal = gSystem->ExpandPathName("$TestArea");
   TString workdirgrid  = gSystem->ExpandPathName("$WorkDir_DIR");
   TString filespath    = (workdirlocal.Contains("/afs/")) ? workdirlocal+"/DileptonST/" : workdirgrid+"/src/DileptonST/";

   Info(APP_NAME, "Called with");
   Info(APP_NAME, "    - isData = %d", (Int_t)!m_isMC);
   Info(APP_NAME, "    - isAFII = %d", (Int_t)isAFII);
   Info(APP_NAME, "    - doIgnorePhotons = %d", (Int_t)doIgnorePhotons);
   Info(APP_NAME, "    - doSystematics = %d", (Int_t)doSystematics);
   Info(APP_NAME, "    - doAllHists = %d", (Int_t)doAllHists);
   Info(APP_NAME, "    - doSystTrees = %d", (Int_t)doSystTrees);
   Info(APP_NAME, "    - dirtyJets = %d", (Int_t)dirtyJets);
   Info(APP_NAME, "    - doPreOR = %d", (Int_t)doPreOR);
   Info(APP_NAME, "    - ptSkim = %f MeV (%f MeV for systematics)", ptSkim, ptSkimForSyst);
   Info(APP_NAME, "    - metSkim = %f MeV (%f MeV for systematics)", metSkim, metSkimForSyst);
   Info(APP_NAME, "    - phptSkim = %f MeV", phptSkim);
   Info(APP_NAME, "    - configFile = " + configFile);
   Info(APP_NAME, "    - workdirlocal = " + workdirlocal);
   Info(APP_NAME, "    - workdirgrid = " + workdirgrid);
   Info(APP_NAME, "    - filespath = " + filespath);

   if (configFile != "SUSYTools_Dilepton.config" && configFile != "SUSYTools_MonoB.config" && configFile != "SUSYTools_DileptonnfigCutflow.config") {
      Error(APP_NAME, "Unrecognised configFile name");
      return EL::StatusCode::FAILURE;
   }

   xAOD::TEvent *event = wk()->xaodEvent();


   ////////////////////////
   // Counters
   ////////////////////////
   m_eventCounter = 0;

   // as a check, let's see the number of events in our xAOD
   Info(APP_NAME, "Number of events = %lli", event->getEntries());  // print long long int


   ////////////////////////
   // GRL
   ////////////////////////
   if(!m_isMC)
   {
      m_grl.reset(new GoodRunsListSelectionTool("GoodRunsListSelectionTool"));
      std::vector<std::string> vecStringGRL;
      //vecStringGRL.push_back(gSystem->ExpandPathName("$TestArea/DileptonST/data/grl/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml")); // 2015 dataset
      vecStringGRL.push_back((filespath+"data/grl/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml").Data()); // 2015 dataset
      //vecStringGRL.push_back(gSystem->ExpandPathName("$TestArea/DileptonST/data/grl/data16_13TeV.periodAllYear_DetStatus-v88-pro20-21_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml"));
      vecStringGRL.push_back((filespath+"data/grl/data16_13TeV.periodAllYear_DetStatus-v88-pro20-21_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml").Data());
      CHECK(m_grl->setProperty("GoodRunsListVec", vecStringGRL));
      CHECK(m_grl->setProperty("PassThrough", kFALSE));   // if true (default) will ignore results of GRL and just select all events
      if (!m_grl->initialize().isSuccess()) {
         Error(APP_NAME, "Failed to properly initialize the GRL tool");
         return EL::StatusCode::FAILURE;
      }
   }

   ////////////////////////
   // SUSYTools
   ////////////////////////

   m_objTool.reset(new ST::SUSYObjDef_xAOD("SUSYObjDef_xAOD"));
   m_objTool->msg().setLevel(MSG::ERROR);

   if (!m_isMC) CHECK(m_objTool->setProperty("DataSource", ST::ISUSYObjDef_xAODTool::Data));
   else if (isAFII) CHECK(m_objTool->setProperty("DataSource", ST::ISUSYObjDef_xAODTool::AtlfastII));
   else CHECK(m_objTool->setProperty("DataSource", ST::ISUSYObjDef_xAODTool::FullSim));


   if (m_isMC) {
      Int_t showerType = m_objTool->getMCShowerType(wk()->metaData()->castString("sample_name"));
      CHECK(m_objTool->setProperty("ShowerType", (Int_t)showerType));
      Info(APP_NAME, "Sample Name = " + (TString)wk()->metaData()->castString("sample_name"));
      Info(APP_NAME, "Shower Type = %d", (Int_t)showerType);
   }

   // configure objects
   //TString configFilePath = gSystem->ExpandPathName("$TestArea/DileptonST/data/config/" + configFile);
   CHECK(m_objTool->setProperty("ConfigFile", (filespath+"data/config/"+configFile).Data()));

   // configure pileup reweighting
   std::vector<std::string> prw_conf;
   //prw_conf.push_back(gSystem->ExpandPathName("$TestArea/DileptonST/data/pileup/mc15c_v2_defaults.NotRecommended.prw.root"));
   prw_conf.push_back((filespath+"data/pileup/mc15c_v2_defaults.NotRecommended.prw.root").Data());
   //prw_conf.push_back(gSystem->ExpandPathName("$TestArea/DileptonST/data/pileup/merged_prw_mc15c_Mar22.root"));
   prw_conf.push_back((filespath+"data/pileup/merged_prw_mc15c_Mar22.root").Data());

   std::vector<std::string> prw_lumicalc;
   //prw_lumicalc.push_back(gSystem->ExpandPathName("$TestArea/DileptonST/data/pileup/ilumicalc_histograms_None_276262-284484_OflLumi-13TeV-005.root"));// 2015 dataset
   prw_lumicalc.push_back((filespath+"data/pileup/ilumicalc_histograms_None_276262-284484_OflLumi-13TeV-005.root").Data());// 2015 dataset
   //prw_lumicalc.push_back(gSystem->ExpandPathName("$TestArea/DileptonST/data/pileup/ilumicalc_histograms_None_297730-311481_OflLumi-13TeV-008.root"));// 2016 dataset
   prw_lumicalc.push_back((filespath+"data/pileup/ilumicalc_histograms_None_297730-311481_OflLumi-13TeV-008.root").Data());// 2016 dataset

   //CHECK(m_objTool->setProperty("PRWDefaultChannel", 410000));
   CHECK(m_objTool->setProperty("PRWConfigFiles", prw_conf));
   CHECK(m_objTool->setProperty("PRWLumiCalcFiles", prw_lumicalc));


   if (doIgnorePhotons) {
      CHECK(m_objTool->setBoolProperty("DoPhotonOR", false));
      Info(APP_NAME, "Excluded photons in the overlap removal");
   } else {
      CHECK(m_objTool->setBoolProperty("DoPhotonOR", true));
      Info(APP_NAME, "Included photons in the overlap removal");
   }

   if (m_objTool->initialize() != StatusCode::SUCCESS) {
      Error(APP_NAME, "Failed to properly initialize the SUSYObjDef_xAOD tool (initialize() stage)");
      return EL::StatusCode::FAILURE;
   }


   // Initialize the Photon Selector Tool
   m_photonTightIsEMSelector.reset(new AsgPhotonIsEMSelector("PhotonTightIsEMSelector"));
   CHECK(m_photonTightIsEMSelector->setProperty("isEMMask", egammaPID::PhotonTight));
   CHECK(m_photonTightIsEMSelector->setProperty("ConfigFile", "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMTightSelectorCutDefs.conf"));
   CHECK(m_photonTightIsEMSelector->initialize());

   // electron, muon and photon isolation tools. do this here to allow advance studies. presumably will someday be provided by SUSYTools.
   m_leptonIsolationLooseTrackOnly.reset(new CP::IsolationSelectionTool("electronLooseTrackOnly"));
   m_leptonIsolationLoose.reset(new CP::IsolationSelectionTool("electronLoose"));
   m_leptonIsolationGradient.reset(new CP::IsolationSelectionTool("electronGradient"));
   m_leptonIsolationGradientLoose.reset(new CP::IsolationSelectionTool("electronGradientLoose"));
   m_leptonIsolationFixedCutTight.reset(new CP::IsolationSelectionTool("electronFixedCutTight"));
   m_leptonIsolationFixedCutTightTrackOnly.reset(new CP::IsolationSelectionTool("electronFixedCutTightTrackOnly"));
   m_photonIsolationFixedCutTightCaloOnly.reset(new CP::IsolationSelectionTool("photonFixedCutTightCaloOnly"));
   m_photonIsolationFixedCutTight.reset(new CP::IsolationSelectionTool("photonFixedCutTight"));
   m_photonIsolationFixedCutLoose.reset(new CP::IsolationSelectionTool("photonFixedCutLoose"));

   CHECK(m_leptonIsolationLooseTrackOnly->setProperty("ElectronWP", "LooseTrackOnly"));
   CHECK(m_leptonIsolationLoose->setProperty("ElectronWP", "Loose"));
   CHECK(m_leptonIsolationGradient->setProperty("ElectronWP", "Gradient"));
   CHECK(m_leptonIsolationGradientLoose->setProperty("ElectronWP", "GradientLoose"));
   CHECK(m_leptonIsolationFixedCutTight->setProperty("ElectronWP", "FixedCutTight"));
   CHECK(m_leptonIsolationFixedCutTightTrackOnly->setProperty("ElectronWP", "FixedCutTightTrackOnly"));

   CHECK(m_leptonIsolationLooseTrackOnly->setProperty("MuonWP", "LooseTrackOnly"));
   CHECK(m_leptonIsolationLoose->setProperty("MuonWP", "Loose"));
   CHECK(m_leptonIsolationGradient->setProperty("MuonWP", "Gradient"));
   CHECK(m_leptonIsolationGradientLoose->setProperty("MuonWP", "GradientLoose"));
// CHECK(m_leptonIsolationFixedCutTight->setProperty("MuonWP", "FixedCutTight")); // does not exist for muons
   CHECK(m_leptonIsolationFixedCutTightTrackOnly->setProperty("MuonWP", "FixedCutTightTrackOnly"));

   CHECK(m_photonIsolationFixedCutTightCaloOnly->setProperty("PhotonWP", "FixedCutTightCaloOnly"));
   CHECK(m_photonIsolationFixedCutTight->setProperty("PhotonWP", "FixedCutTight"));
   CHECK(m_photonIsolationFixedCutLoose->setProperty("PhotonWP", "FixedCutLoose"));

   CHECK(m_leptonIsolationLooseTrackOnly->initialize());
   CHECK(m_leptonIsolationLoose->initialize());
   CHECK(m_leptonIsolationGradient->initialize());
   CHECK(m_leptonIsolationGradientLoose->initialize());
   CHECK(m_leptonIsolationFixedCutTight->initialize());
   CHECK(m_leptonIsolationFixedCutTightTrackOnly->initialize());
   CHECK(m_photonIsolationFixedCutTightCaloOnly->initialize());
   CHECK(m_photonIsolationFixedCutTight->initialize());
   CHECK(m_photonIsolationFixedCutLoose->initialize());

   // TODO!!! NOAM
   /*
   Info(APP_NAME, "Setting electron isolation tool (used for tight isolation SF)");
   if (!m_electronIsolationSFTool.isUserConfigured()) {
      SET_DUAL_TOOL(m_electronIsolationSFTool, AsgElectronEfficiencyCorrectionTool, "AsgElectronEfficiencyCorrectionTool_iso_TightLLH_FixedCutTightTrackOnly");
      CHECK(m_electronIsolationSFTool.setProperty("IdKey", "Tight")); // for some reason, remove LLH from the name
      CHECK(m_electronIsolationSFTool.setProperty("IsoKey", "FixedCutTight"));
      // if (!isData()) {
      //   ATH_CHECK (m_electronIsolationSFTool.setProperty("ForceDataType", (int) data_type) );
      // }
      CHECK(m_electronIsolationSFTool.setProperty("CorrelationModel", "TOTAL"));
      CHECK(m_electronIsolationSFTool.initialize());
   }
   */


   Info(APP_NAME, "Setting tau tool properties");

   m_tauLoose =
      CxxUtils::make_unique<TauAnalysisTools::TauSelectionTool>("TauSelectionToolLoose");

   m_tauMedium =
      CxxUtils::make_unique<TauAnalysisTools::TauSelectionTool>("TauSelectionToolMedium");

   m_tauTight =
      CxxUtils::make_unique<TauAnalysisTools::TauSelectionTool>("TauSelectionToolTight");

   CHECK(m_tauLoose->setProperty("JetIDWP", static_cast<int>(TauAnalysisTools::JETIDBDTLOOSE)));
   CHECK(m_tauMedium->setProperty("JetIDWP", static_cast<int>(TauAnalysisTools::JETIDBDTMEDIUM)));
   CHECK(m_tauTight->setProperty("JetIDWP", static_cast<int>(TauAnalysisTools::JETIDBDTTIGHT)));

   Info(APP_NAME, "Initializing tau tool");
   //if(kFALSE) // TODO !!! need to remove this - NOAM
   //{
   	CHECK(m_tauLoose->initialize());
   	CHECK(m_tauMedium->initialize());
	CHECK(m_tauTight->initialize());
   //}
	
   Info(APP_NAME, "Initializing VJetsUncertaintyTool");
   m_vjetsuncEW = CxxUtils::make_unique<DileptonST::VJetsUncertaintyTool>();
   //m_vjetsuncEW->setInputFileName(gSystem->ExpandPathName("$TestArea/DileptonST/data/vjetsunc/corrections_vjets.root"));
   m_vjetsuncEW->setInputFileName((filespath+"data/vjetsunc/corrections_vjets.root").Data());
   m_vjetsuncEW->applyEWCorrection(kTRUE);
   m_vjetsuncEW->applyQCDCorrection(kFALSE); // NNLO not ready yet
   m_vjetsuncEW->initialize();

   ////////////////////////
   // Systematics
   ////////////////////////
   m_sysList.clear();

   if (!doSystematics || !m_isMC) {
      // no systematics if asked, or for data
      ST::SystInfo nominal;
      nominal.affectsKinematics = kFALSE;
      nominal.affectsWeights = kFALSE;
      nominal.affectsType = ST::Unknown;
      m_sysList.push_back(nominal);
   } else {
      auto fullSystList = m_objTool->getSystInfoList();

      std::vector<TString> forbidden;
      if (configFile == "SUSYTools_MonoB.config")  forbidden = {"EG_", "EL_EFF_ID", "EL_EFF_Iso", "EL_EFF_Reco", "JET_eta", "JET_Rtrk", "MUONS", "MUON_EFF_S", "MUON_ISO", "MUON_TTVA", "PH_", "TAUS"};// "extrapolation" };
      else forbidden = { "TAUS_"};

      for (auto syst : fullSystList) {
         const TString thisSyst = syst.systset.name();
         Bool_t keepThis(kTRUE);
         for (auto badWord : forbidden) {
            if (thisSyst.Contains(badWord)) {
               Info(APP_NAME, "Skip to run the \"%s\" systematic", thisSyst.Data());
               keepThis = kFALSE;
               break;
            }
         } // loop over forbidden systematics

         if (keepThis) m_sysList.push_back(syst);
      }
   }



   ////////////////////////
   // Output tree
   ////////////////////////
   m_trees.clear();
   m_hists.clear();
   m_pure_hists.clear();
   m_cand.clear();

   TFile *thisFile = wk()->getOutputFile("minitrees");

   const std::vector<TString> trigList = {
      // 2015 menu
      "HLT_e28_tight_iloose",
      "HLT_e60_medium",
      "HLT_mu26_imedium",
      "HLT_mu50",
      
      "HLT_2e17_loose",
      "HLT_2mu14",
      "HLT_e24_lhmedium_iloose_L1EM20VH",
      "HLT_e24_lhmedium_L1EM20VH",
      "HLT_e24_lhmedium_L1EM18VH",
      "HLT_e24_lhtight_iloose",
      "HLT_e60_lhmedium",
      "HLT_e120_lhloose",
      
      "HLT_mu24_ivarmedium",
      "HLT_mu24_imedium",
      "HLT_mu26_ivarmedium",
      "HLT_mu26_imedium",
      "HLT_mu50",
      "HLT_e24_lhtight_nod0_ivarloose",
      "HLT_e26_lhtight_ivarloose",
      "HLT_e26_lhtight_nod0_ivarloose",
      "HLT_e60_lhmedium_nod0",
      "HLT_e120_lhloose_nod0",
      "HLT_e140_lhloose_nod0",
      //"HLT_g140_loose",
      //"HLT_g160_loose",
      //"HLT_g120_loose",

      //Unprescaled 2015
      "HLT_mu20_iloose_L1MU15",
      "HLT_mu40",
      "HLT_2mu10",
      // "HLT_e24_lhmedium_L1EM20VH",
      // "HLT_e60_lhmedium",
      // "HLT_e120_lhloose", //in MJ list
      "HLT_2e12_lhloose_L12EM10VH",
      "HLT_e17_lhloose_2e9_lhloose",

      //Unprescaled 2016
      "HLT_e24_lhmedium_L1EM20VHI",
      "HLT_e24_lhmedium_nod0_ivarloose",
      "HLT_e24_lhmedium_nod0_L1EM20VH",
      "HLT_2e12_lhvloose_nod0_L12EM10VH",
      "HLT_e17_lhloose_nod0_2e9_lhloose_nod0",
      "HLT_2e17_lhloose",
      "HLT_2e17_lhvloose",
      "HLT_2e17_lhvloose_nod0",
      "HLT_mu20_ivarloose_L1MU15",
      "HLT_mu24_iloose_L1MU15",
      "HLT_mu24_ivarloose_L1MU15",
      "HLT_mu24_iloose",
      "HLT_mu24_ivarloose",
      // "HLT_mu20_iloose_L1MU15",
      // "HLT_2mu10",
      "HLT_mu20_mu8noL1",
      "HLT_mu20_2mu4noL1",
      "HLT_mu22_mu8noL1",
      // "HLT_mu24_ivarmedium",
      // "HLT_mu26_ivarmedium",
      // "HLT_e24_lhtight_nod0_ivarloose",
      // "HLT_e26_lhtight_ivarloose",
      // "HLT_e26_lhtight_nod0_ivarloose",
      // "HLT_e60_lhmedium_nod0",
      // "HLT_e120_lhloose_nod0",
   };

   // loop over systematics to run on
   for (auto systInfo : m_sysList) {

      // create one TTree per systematic variation, to which a separate "candidate" object is attached
      // we use trimmed tree for systematics, full tree for nominal
      const TString systName = systInfo.systset.name();
      const Bool_t isNominal = (systName == "");
      const TString treeName = (!isNominal) ? ("syst_" + systName).ReplaceAll(" ", "_") : "nominal";
      TString treeTitle = systName;

      /*
      if (systName.Contains("TAUS_")) {
         Info(APP_NAME, "Skip to run the \"%s\ systematic", systName.Data());
         continue;
      }
      */

      if (doSystTrees || isNominal) {
         Info(APP_NAME, "Creating TTree named \"%s\" for systematic named \"%s\"", treeName.Data(), systName.Data());
         if (doIgnorePhotons) treeTitle += "_IgnorePhotons";
         m_trees[systName] = new TTree(treeName, treeTitle);
         m_trees[systName]->SetDirectory(thisFile);
      }
      // set trigger list
      m_cand[systName].evt.setTriggers(trigList);

      // set Scale Factor systematics
      if (m_isMC && isNominal) {
         // automatically get relevant systematics from SUSYTools
         for (auto syst : m_objTool->getSystInfoList()) {
            const TString thisSyst = syst.systset.name();
            if (thisSyst.Contains("EL_EFF")) m_cand[systName].evt.evsf_baseline_syst_EL[thisSyst] = 0.;
            if (thisSyst.Contains("MUON_EFF")) m_cand[systName].evt.evsf_baseline_syst_MU[thisSyst] = 0.;
            if (thisSyst.Contains("PH_EFF")) m_cand[systName].evt.evsf_baseline_syst_PH[thisSyst] = 0.;
         }

         // manual VJetsUncertaintyTool systematics
         std::vector<TString> listOfVjetsWeights;
         for (auto syst : m_vjetsuncEW->getAllVariationNames()) {
            const TString thisSyst = "VJetsEW_" + syst; // final name will be "weight_VJets_blabla" (see OutputEvent.cxx)
            listOfVjetsWeights.push_back(thisSyst);
         }
         m_cand[systName].evt.setWeights(listOfVjetsWeights);
      }

      m_cand[systName].met["met_nomuon_tst"].setName("met_nomuon_tst");
      if (isNominal)m_cand[systName].met["met_nomuon_cst"].setName("met_nomuon_cst");
      m_cand[systName].met["met_wmuon_tst"].setName("met_wmuon_tst");
      if (isNominal)m_cand[systName].met["met_wmuon_cst"].setName("met_wmuon_cst");
      m_cand[systName].met["met_noelectron_tst"].setName("met_noelectron_tst");
      if (isNominal)m_cand[systName].met["met_noelectron_cst"].setName("met_noelectron_cst");
      m_cand[systName].met["met_muonterm_tst"].setName("met_muonterm_tst");
      if (isNominal) m_cand[systName].met["met_muonterm_cst"].setName("met_muonterm_cst");
      m_cand[systName].met["met_track"].setName("met_track");
      if (isNominal)m_cand[systName].met["met_truth"].setName("met_truth");
      m_cand[systName].met["met_softerm_tst"].setName("met_softerm_tst");
      if (isNominal)m_cand[systName].met["met_softerm_cst"].setName("met_softerm_cst");
      m_cand[systName].met["met_nophoton_tst"].setName("met_nophoton_tst");
      if (isNominal)m_cand[systName].met["met_nophoton_cst"].setName("met_nophoton_cst");
      m_cand[systName].met["met_nophcalib_nomuon_tst"].setName("met_nophcalib_nomuon_tst");
      if (isNominal)m_cand[systName].met["met_nophcalib_nomuon_cst"].setName("met_nophcalib_nomuon_cst");
      m_cand[systName].met["met_nophcalib_wmuon_tst"].setName("met_nophcalib_wmuon_tst");
      if (isNominal)m_cand[systName].met["met_nophcalib_wmuon_cst"].setName("met_nophcalib_wmuon_cst");
      if (isNominal) {
         m_cand[systName].met["met_jetterm"].setName("met_jetterm");
         m_cand[systName].met["met_muonterm"].setName("met_muonterm");
         m_cand[systName].met["met_eleterm"].setName("met_eleterm");
         m_cand[systName].met["met_phterm"].setName("met_phterm");
      }


      m_cand[systName].mu["mu"].setName("mu");
      m_cand[systName].el["el"].setName("el");
      m_cand[systName].mu["mu_baseline"].setName("mu_baseline");
      m_cand[systName].el["el_baseline"].setName("el_baseline");
      m_cand[systName].mu["mu_preor"].setName("mu_preor");
      m_cand[systName].el["el_preor"].setName("el_preor");
      m_cand[systName].jet["jet"].setName("jet");
      m_cand[systName].jet["jet_preor"].setName("jet_preor");
      m_cand[systName].ph["ph"].setName("ph");
      m_cand[systName].ph["ph_baseline"].setName("ph_baseline");
      m_cand[systName].ph["ph_preor"].setName("ph_preor");
      m_cand[systName].hem["hem"].setName("hem");
      m_cand[systName].tau["tau"].setName("tau");

      // decide trim vs full
      m_cand[systName].setDoTrim(!isNominal);

      // don't save baseline objects in ttree
      m_cand[systName].mu["mu_baseline"].setWrite(false);
      m_cand[systName].el["el_baseline"].setWrite(false);
      m_cand[systName].ph["ph_baseline"].setWrite(false);

      // for debugging purpose

      m_cand[systName].jet["jet_preor"].setWrite(doPreOR);
      m_cand[systName].mu["mu_preor"].setWrite(doPreOR);
      m_cand[systName].el["el_preor"].setWrite(doPreOR);
      m_cand[systName].ph["ph_preor"].setWrite(doPreOR);

      // attach to TTree
      m_cand[systName].attachToTree(m_trees[systName]);

      if (m_isMC && doSystematics && !doSystTrees) {
         defineAllSystHists(m_hists,      systName, treeName, "");
         defineAllSystHists(m_pure_hists, systName, treeName, "noPURW");

         // manual VJetsUncertaintyTool systematics
         if (isNominal) {
            for (auto syst : m_vjetsuncEW->getAllVariationNames()) {
               const TString thisSyst = (syst == "Nominal") ? "VJetsEWnominal" : ("syst_VJetsEW_" + syst);
               defineAllSystHists(m_hists,      thisSyst, thisSyst, "");
               defineAllSystHists(m_pure_hists, thisSyst, thisSyst, "noPURW");
            }
         } // nominal
      }

   }

   if(m_isMC)
   {
      /// LPXKfactorTool
      m_kFTool = new LPXKfactorTool("LPXKfactorTool");
      CHECK( m_kFTool->setProperty("isMC15", true) );  // to be generalized!
      CHECK( m_kFTool->initialize() );
   }

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode DileptonNTUPMaker :: execute()
{
   // Here you do everything that needs to be done on every single
   // events, e.g. read input variables, apply cuts, and fill
   // histograms and trees.  This is where most of your actual analysis
   // code will go.
   //
   const char *APP_NAME = "DileptonNTUPMaker::execute";
   m_nominalDone = kFALSE; // initialise flag keeping track of calls to getObjects

   // xAOD::TEvent *event = wk()->xaodEvent();

   if ((m_eventCounter % 100) == 0) Info(APP_NAME, "Event number = %i", m_eventCounter);
   m_eventCounter++;


   for (auto systInfo : m_sysList) {
      const TString systName = systInfo.systset.name();
      analyzeEvent(systInfo, m_cand[systName]);
   } // loop on systematic variations

   return EL::StatusCode::SUCCESS;
}

Bool_t DileptonNTUPMaker :: PassedCut(Bool_t ispassed, DileptonCuts::CutID thiscut,  DileptonCuts::CutID& lastcut)
{
   if(!ispassed) return false;
   lastcut = thiscut;
   return true;
}

EL::StatusCode DileptonNTUPMaker :: analyzeEvent(ST::SystInfo &systInfo, Analysis::OutputEntry &cand)
{
   const char *APP_NAME = "DileptonNTUPMaker::analyzeEvent";
   const TString systName = systInfo.systset.name();

   xAOD::TEvent *event = wk()->xaodEvent();

   // Needed to calculate SFs
   CHECK(m_objTool->ApplyPRWTool());

   // event selection using a given systematic variation
   // event analysis, from object getting to saving-on-disk, is performed here
   if (m_nominalDone) m_content_current = m_content_nominal;
   Analysis::ContentHolder &content = (m_nominalDone) ? m_content_current : m_content_nominal;

   const Bool_t syst_affectsElectrons = ST::testAffectsObject(xAOD::Type::Electron, systInfo.affectsType);
   const Bool_t syst_affectsMuons     = ST::testAffectsObject(xAOD::Type::Muon,     systInfo.affectsType);
   const Bool_t syst_affectsJets      = ST::testAffectsObject(xAOD::Type::Jet,      systInfo.affectsType);
   const Bool_t syst_affectsPhotons   = ST::testAffectsObject(xAOD::Type::Photon,   systInfo.affectsType);
   const Bool_t syst_affectsBTag      = ST::testAffectsObject(xAOD::Type::BTag,     systInfo.affectsType);
   const bool syst_affectsTaus        = ST::testAffectsObject(xAOD::Type::Tau, systInfo.affectsType);

   const CP::SystematicSet& sys = systInfo.systset;
   if (m_objTool->applySystematicVariation(sys) != CP::SystematicCode::Ok) {
      Error(APP_NAME, "Cannot configure SUSYTools for systematic var. %s", sys.name().c_str());
      return EL::StatusCode::FAILURE;
   }

   // CHECK(m_electronIsolationSFTool->applySystematicVariation(sys)); // TODO!!! NOAM

   content.doMuons = (syst_affectsMuons || !m_nominalDone);
   content.doElectrons = (syst_affectsElectrons || !m_nominalDone);
   content.doJets = (syst_affectsJets || syst_affectsBTag || !m_nominalDone);
   content.ownJets = (m_nominalDone); // nominal jets are registered to TStore
   content.doPhotons = !doIgnorePhotons && (syst_affectsPhotons || !m_nominalDone);
   content.doOverlapRemoval = kTRUE; // always do
   content.doMET = kTRUE; // always do
   content.doTaus = (syst_affectsTaus || !m_nominalDone);


   DileptonCuts::CutID last;
   if (getLastCutPassed(systInfo, content, DileptonNTUPMaker::SR, last) != EL::StatusCode::SUCCESS) {
      Error(APP_NAME, "getLastCutPassed failed");
      return EL::StatusCode::FAILURE;
   }

   // fill cutflow
   if (!m_nominalDone) m_cutFlow.addCutCounter(last, (m_isMC) ? content.eventInfo->mcEventWeight() : 1.0); // TODO: add also pileup


   /// LPXKfactorTool
   CHECK( m_kFTool->execute() );
   //double w_event_kF = content.eventInfo->auxdata< double >( "KfactorWeight" );
   //std::cout << "w_event_kF = " << w_event_kF << std::endl;


   // save syst trees only at high pt, met
   //const Double_t ptSkimToUse = (!m_nominalDone) ? ptSkim : ptSkimForSyst;
   //const Double_t metSkimToUse = (!m_nominalDone) ? metSkim : metSkimForSyst;
   //const Double_t phptSkimToUse = (!m_nominalDone) ? phptSkim : phptSkimForSyst;
   //const DileptonCuts::CutID lastSkimToUse = (!m_nominalDone) ? DileptonCuts::vertex : DileptonCuts::MET_cleaning;

   //const DileptonCuts::CutID lastSkimToUse = DileptonCuts::MET_cleaning;
   const DileptonCuts::CutID lastSkimToUse = DileptonCuts::cleaning;

	/*
   //Dilepton skimming
   Bool_t saveMe = (last >= lastSkimToUse
                    && content.goodJets.size() > 0
                    && content.goodJets[0]->pt() > ptSkimToUse
                    && (m_met_nomuon_to_use.Mod() > metSkimToUse || m_met_noelectron_to_use.Mod() > metSkimToUse || ((m_met_nophoton_to_use.Mod() > metSkimToUse || (content.goodPhotons.size() > 0 && content.goodPhotons[0]->pt() > phptSkimToUse)) && !doIgnorePhotons))
                   ); // can't be earlier, otherwise objects have not been loaded yet!
	*/

  // Dilepton skimming
   Bool_t saveMe = (last >= lastSkimToUse); // can't be earlier, otherwise objects have not been loaded yet!


   if (configFile == "SUSYTools_MonoB.config") {
      /////////////////////////////
      // selected jets
      // + compute b-jet multiplicity
      ////////////////////////////
      //need for mono-b skimming

      /*
       static SG::AuxElement::Accessor<char> tmp_acc_bjet("bjet");
       UInt_t skimNBJet(0);
       if (content.goodJets.size() > 0) {
          for (auto thisJet : content.goodJets) {
             if (tmp_acc_bjet(*thisJet))  skimNBJet++;
          }
       }
      */

	/*
      //Mono-b skimming
      saveMe = (last >= lastSkimToUse
                && content.goodJets.size() > 0
                && content.goodJets.size() <= 3
                //&& skimNBJet > 0
                && content.goodJets[0]->pt() > ptSkimToUse
                && (m_met_nomuon_to_use.Mod() > metSkimToUse || m_met_noelectron_to_use.Mod() > metSkimToUse)
                //&& (m_met_wmuon_to_use.Mod() > metSkimToUse || (content.goodMuons.size() == 2 ||  content.goodElectrons.size() == 2))
               );
	*/

      //Mono-b skimming
      saveMe = (last >= lastSkimToUse);
   }

   if (saveMe) {
      cand.reset();

      //////////////////
      // trigger
      //////////////////
      for (auto &kv : cand.evt.trigger) {
         kv.second = m_objTool->IsTrigPassed(kv.first.Data());
      }
      cand.evt.trigger_pass = cand.evt.trigger["HLT_xe70"] || // standard 2015
                              cand.evt.trigger["HLT_xe80_tc_lcw_L1XE50"] || cand.evt.trigger["HLT_xe90_mht_L1XE50"] || cand.evt.trigger["HLT_xe100_mht_L1XE50"] || cand.evt.trigger["HLT_xe110_mht_L1XE50"] || cand.evt.trigger["HLT_xe130_mht_L1XE50"] ||//standard 2016
                              cand.evt.trigger["HLT_e24_lhmedium_L1EM20VH"] || cand.evt.trigger["HLT_e60_lhmedium"] || cand.evt.trigger["HLT_e120_lhloose"] || // for CR2e 2015
                              cand.evt.trigger["HLT_e26_lhtight_nod0_ivarloose"] || cand.evt.trigger["HLT_e60_lhmedium_nod0"] || cand.evt.trigger["HLT_e140_lhloose_nod0"]; // for CR2e

      if (!doIgnorePhotons) cand.evt.trigger_pass |= cand.evt.trigger["HLT_g140_loose"] || // 2015
                                                        cand.evt.trigger["HLT_g140_loose"] || cand.evt.trigger["HLT_g160_loose"]; // for CR1ph



      //Mono-b trigger matching
      if (configFile == "SUSYTools_MonoB.config") {
         Float_t elPt = 30000, muPt = 30000;
         Int_t tmp_year =  m_objTool->treatAsYear();

         if (content.goodElectrons.size() > 0) {
            if (tmp_year == 2015 && (m_objTool->IsTrigMatched(content.goodElectrons[0], "trigger_HLT_e24_lhmedium_L1EM20VH")) && content.goodElectrons[0]->pt() > elPt) cand.evt.trigger_matched_electron = 1;
            if (tmp_year == 2016 && (m_objTool->IsTrigMatched(content.goodElectrons[0], "HLT_e60_lhmedium_nod0 || HLT_e60_lhmedium || HLT_e26_lhtight_nod0_ivarloose")) && content.goodElectrons[0]->pt() > elPt) cand.evt.trigger_matched_electron = 1;
         }

         if (content.goodMuons.size() > 0) {
            if (tmp_year == 2015 && (m_objTool->IsTrigMatched(content.goodMuons[0], "HLT_mu26_imedium || HLT_mu40")) && content.goodMuons[0]->pt() > muPt) cand.evt.trigger_matched_muon = 1;
            if (tmp_year == 2016 && (m_objTool->IsTrigMatched(content.goodMuons[0], "HLT_mu26_imedium || HLT_mu40 || HLT_mu26_ivarmedium")) && content.goodMuons[0]->pt() > muPt) cand.evt.trigger_matched_muon = 1;
         }
      }



      // TODO: re-implement trigger matching
      /*
      bool passingTrig;
      for (auto thisElectron : content.goodElectrons) {
        for (auto &kv : cand.evt.trigger) {

                 passingTrig = m_objTool->IsTrigMatched(thisElectron, kv.first.Data());
                 if (passingTrig) {
            if (kv.first == "HLT_e60_lhmedium") cand.evt.trigger_matched_HLT_e60_lhmedium = 1;
            else if (kv.first == "HLT_e120_lhloose") cand.evt.trigger_matched_HLT_e120_lhloose = 1;
            else if (kv.first == "HLT_e24_lhmedium_L1EM18VH") cand.evt.trigger_matched_HLT_e24_lhmedium_L1EM18VH = 1;
            else if (kv.first == "HLT_e24_lhmedium_L1EM20VH") cand.evt.trigger_matched_HLT_e24_lhmedium_L1EM20VH = 1;
          }
        }
      }*/


      //////////////////
      // event variables
      //////////////////

      cand.evt.run = (m_isMC) ? content.eventInfo->mcChannelNumber() : content.eventInfo->runNumber();
      cand.evt.event = (ULong64_t)content.eventInfo->eventNumber(); // TODO: makes sure it means 'unsigned long long'
      cand.evt.lbn = content.eventInfo->lumiBlock();
      cand.evt.bcid = content.eventInfo->bcid();
      cand.evt.hfor = -9999;
      cand.evt.last = last;
      cand.evt.year = (m_isMC) ? m_objTool->treatAsYear() : 0;
      //cand.evt.selected = (last >= DileptonCuts::MET_cleaning);
      cand.evt.selected = (last >= DileptonCuts::cleaning);
      cand.evt.bb_decision = -9999;
      cand.evt.n_vx = content.vertices->size(); // absolute number of PV's (i.e. no track cut)
      cand.evt.n_jet = content.goodJets.size(); // after OR
      cand.evt.n_jet_preor = content.jets->size(); // before OR
      cand.evt.n_mu_preor = content.muons->size();
      cand.evt.n_el_preor = content.electrons->size();
      cand.evt.n_ph_preor = content.photons->size();
      cand.evt.n_el = content.goodElectrons.size();
      cand.evt.n_el_baseline = m_n_el_baseline;
      cand.evt.n_mu = content.goodMuons.size();
      cand.evt.n_allmu_bad = m_n_allmu_bad;
      cand.evt.n_mu_baseline = m_n_mu_baseline;
      cand.evt.n_mu_baseline_bad = m_n_mu_baseline_bad;
      cand.evt.n_ph = content.goodPhotons.size();
      cand.evt.n_ph_baseline = m_n_ph_baseline;
      cand.evt.flag_bib = content.eventInfo->eventFlags(xAOD::EventInfo::Background) & (1 << 20);
      cand.evt.flag_bib_raw = content.eventInfo->eventFlags(xAOD::EventInfo::Background);
      cand.evt.flag_sct = (content.eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error);
      cand.evt.flag_core = (content.eventInfo->eventFlags(xAOD::EventInfo::Core) & 0x40000);
      cand.evt.passTSTCleaning = -999;// REMOVED in SUSYTools-00-08-09 (Int_t)m_objTool->passTSTCleaning(*content.met_tst_nomuon);
      cand.evt.averageIntPerXing = content.eventInfo->averageInteractionsPerCrossing();
      cand.evt.corAverageIntPerXing = m_objTool->GetCorrectedAverageInteractionsPerCrossing();
      cand.evt.mu_SF_tot = (m_isMC) ?  m_objTool->GetTotalMuonSF(content.goodMuons, true, true, "HLT_mu26_imedium_OR_HLT_mu50") : 1.0;


      if (m_isMC) {
         cand.evt.kF_weight = content.eventInfo->auxdata< double >( "KfactorWeight" );
         cand.evt.xsec      = m_kFTool->getMCCrossSection();
         cand.evt.geneff    = m_kFTool->getMCFilterEfficiency();
         cand.evt.mconly_weight = content.eventInfo->mcEventWeight();
         cand.evt.mconly_weights = content.eventInfo->mcEventWeights();
         cand.evt.pu_weight = 1.0;
         cand.evt.pu_weight *= content.eventInfo->mcEventWeight();
         cand.evt.pu_weight *= m_objTool->GetPileupWeight();
         cand.evt.pu_hash = m_objTool->GetPileupWeightHash();
         cand.evt.sh22_weight = m_objTool->getSherpaVjetsNjetsWeight();
         // add sherpa 2.2 additional weight for V+jets
         if ((cand.evt.run >= 363331 && cand.evt.run <= 363354) // Wtaunu
             || (cand.evt.run >= 363436 && cand.evt.run <= 363483) // Wmunu && Wenu
             || (cand.evt.run >= 363388 && cand.evt.run <= 363411) // Zee
             || (cand.evt.run >= 363364 && cand.evt.run <= 363387) // Zmumu
             || (cand.evt.run >= 363102 && cand.evt.run <= 363122) // Ztautau
             || (cand.evt.run >= 363412 && cand.evt.run <= 363435) // Znunu
            ) {
            cand.evt.pu_weight *= cand.evt.sh22_weight;
            cand.evt.mconly_weight *= cand.evt.sh22_weight;
         }
         cand.evt.btag_weight = m_objTool->BtagSF(&content.goodJets);
         cand.evt.jvt_weight = 1.; // m_objTool->GetTotalJetSF(content.jets, false, true); // btag, jvt // TODO!!! need to uncomment when TruthJets is available on input - NOAM
         //cand.evt.jvt_all_weight = m_objTool->GetTotalJetSF(&content.allJets, false, true); // btag, jvt
      }


      /////////////////////////
      // pdf, truth variables
      /////////////////////////
      if (m_isMC) {
         const xAOD::TruthEventContainer *truthE(nullptr);
         if (!event->retrieve(truthE, "TruthEvents").isSuccess()) {
            Error(APP_NAME, "Failed to retrieve Truth container");
         } else {
            try {
               xAOD::TruthEventContainer::const_iterator truthE_itr = truthE->begin();
               (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_id1, xAOD::TruthEvent::PDGID1);
               (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_id2, xAOD::TruthEvent::PDGID2);
               (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_x1, xAOD::TruthEvent::X1);
               (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_x2, xAOD::TruthEvent::X2);
               (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_pdf1, xAOD::TruthEvent::PDF1);
               (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_pdf2, xAOD::TruthEvent::PDF2);
               (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_scale, xAOD::TruthEvent::Q);
            } catch (SG::ExcBadAuxVar) {
               // ignore this variable, when unavailable
               // (happens often, see
               // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MC15aKnownIssues
               // )
            }
         }

         //sherpa 2.2.1 weight
         if (truthE->size() > 0) {
            const xAOD::TruthEvent *truthEvent = (*truthE)[0];

            std::vector<float> variationweights = truthEvent->weights();
            std::vector<float> tmp_weights;

            //std::cout << " Variation weights size " <<  variationweights.size() << std::endl;

            if (variationweights.size() >= 11) {
               for (int i = 0; i < 11; i++)tmp_weights.push_back(variationweights.at(i));
            }
            //cout << tmp_weights.size()<<endl;
            cand.evt.newSH_weight = tmp_weights;

            variationweights.clear();
            tmp_weights.clear();
         }

         //truth W from top
         const xAOD::TruthParticleContainer *truthParticle(nullptr);
         CHECK(event->retrieve(truthParticle, "TruthParticles"));
         vector <Int_t> tmp_truth_W_decay;

         for (const auto& particle : *truthParticle) {
            if (fabs(particle->pdgId()) == 6) { //it's a top
               Int_t tmp_W = isTrueW(particle);
               if (tmp_W >= 0) tmp_truth_W_decay.push_back(tmp_W);
            }
         }
         cand.evt.truth_W_decay = tmp_truth_W_decay;
         cand.evt.n_truthTop = tmp_truth_W_decay.size();


         if (configFile == "SUSYTools_MonoB.config") {
            //Fill met truth of the event (veto on inclusive top will be applied in NtupleMaker)
            static SG::AuxElement::Accessor<float> acc_GenFiltMET("GenFiltMET");
            if (acc_GenFiltMET(*content.eventInfo))
               cand.evt.GenFiltMet = acc_GenFiltMET(*content.eventInfo);
         } else  cand.evt.GenFiltMet = -9999;

         
         // truth jet information
         // leading two jets are saved; this is needed for example for QCD MC, where
         // events with ((pt1+pt2)/2)/pt1 < 1.4 are ignored, in order to avoid counting
         // pileup jets as leading jets when combining different slices of leading jet pt
         const xAOD::JetContainer *truthJets(nullptr);
         static Bool_t failedLookingFor(kFALSE); // trick to avoid infinite RuntimeWarning's for EXOT5
         if (!failedLookingFor) {
            if (!event->retrieve(truthJets, "AntiKt4TruthJets").isSuccess()) {
               Error(APP_NAME, "Failed to access Truth Jets container; not attempting again, truth_jet* variables will be empty");
               failedLookingFor = kTRUE;
            } else {
               //number of truth jets with pt > 20 GeV and OR
               // loop over all truth jets passing basic cuts
               Int_t nTruthJets(0);
               const xAOD::TruthParticleContainer* truthP(0);
               event->retrieve(truthP, "TruthParticles") ;
               for (const auto& truthJ_itr : *truthJets) {
                  if (truthJ_itr->pt() > 20000. && fabs(truthJ_itr->eta()) < 2.8) {
                     float minDR2 = 9999999.;
                     // for each jet, loop over all electrons and muons passing basic cuts to check if the jets should be counted or not
                     for (const auto& truthP_itr : *truthP) {
                        if (truthP_itr->pt() > 25000. && fabs(truthP_itr->eta()) < 2.5 && truthP_itr->status() == 1 && (abs(truthP_itr->pdgId()) == 11 || abs(truthP_itr->pdgId()) == 13)) {
                           if (xAOD::P4Helpers::isInDeltaR(*truthJ_itr, *truthP_itr, 0.2, true)) {
                              float dR2 = xAOD::P4Helpers::deltaR2(truthJ_itr, truthP_itr, true);
                              if (dR2 < minDR2) {
                                 minDR2 = dR2;
                                 // stop if we already know the jet shouldn't be counted
                                 if (minDR2 < 0.2 * 0.2) break;
                              }
                           }
                        }
                     }
                     if (minDR2 > 0.2 * 0.2) nTruthJets++;
                  }
               }
               cand.evt.n_jet_truth = nTruthJets;
               if (truthJets->size() > 0) {
                  cand.evt.truth_jet1_pt = (*truthJets)[0]->p4().Pt();
                  cand.evt.truth_jet1_eta = (*truthJets)[0]->p4().Eta();
                  cand.evt.truth_jet1_phi = (*truthJets)[0]->p4().Phi();
                  cand.evt.truth_jet1_m = (*truthJets)[0]->p4().M();
                  if (truthJets->size() > 1) {
                     cand.evt.truth_jet2_pt = (*truthJets)[1]->p4().Pt();
                     cand.evt.truth_jet2_eta = (*truthJets)[1]->p4().Eta();
                     cand.evt.truth_jet2_phi = (*truthJets)[1]->p4().Phi();
                     cand.evt.truth_jet2_m = (*truthJets)[1]->p4().M();
                  }
               }
            }
         } // already found AntiKt4TruthJets, or never failed yet
       

         // save truth information of the leading good photon
         if (content.goodPhotons.size() > 0) {
            const xAOD::Photon *leadPhoton = content.goodPhotons.at(0);
            const xAOD::TruthParticle *truth_ph = xAOD::TruthHelpers::getTruthParticle(*leadPhoton);
            if (truth_ph) {
               //std::cout << "Index good photons: " << count_good << "  pdgId: " <<  truth_ph->pdgId()
               //          << "  Pt: " << truth_ph->pt() << "  Phi: " << truth_ph->phi() << "  Eta: " << truth_ph->eta()
               //          << "  origin: " << xAOD::TruthHelpers::getParticleTruthOrigin(*thisPhoton)  << " type: " << xAOD::TruthHelpers::getParticleTruthType(*thisPhoton) << std::endl;
               // fill variables
               cand.evt.truth_ph1_pt = truth_ph->pt();
               cand.evt.truth_ph1_eta = truth_ph->eta();
               cand.evt.truth_ph1_phi = truth_ph->phi();
               cand.evt.truth_ph1_type = xAOD::TruthHelpers::getParticleTruthType(*leadPhoton);

            }
         } // end goodPhoton size



         // save truth information for the W/Z boson, and for truth muons
         const int mcid = content.eventInfo->mcChannelNumber();
         const xAOD::TruthParticleContainer *truthParticles(nullptr);
         const xAOD::TruthParticleContainer *truthMuons = nullptr;
         const xAOD::TruthParticleContainer *truthElectrons = nullptr;
         //const xAOD::TruthParticleContainer *truthPhotons = nullptr;
         CHECK(event->retrieve(truthParticles, "TruthParticles"));
         if (!m_determinedDerivation) {
            Info(APP_NAME, "Determining derivation type");
            //m_isEXOT5 = event->retrieve(truthMuons, "EXOT5TruthMuons").isSuccess();
            m_isEXOT5 = kFALSE; // TODO !!! NOAM
            m_determinedDerivation = kTRUE;
            Info(APP_NAME, "Is it EXOT5? (will trigger access to dedicated truth electron and muon containers): %s", (m_isEXOT5) ? "YES" : "NO");
         }
         const TString mu_container = (m_isEXOT5) ? "EXOT5TruthMuons" : "TruthMuons";
         //const TString mu_container = (m_isEXOT5) ? "EXOT5TruthMuons" : "MuonTruthParticles";
         if (!event->retrieve(truthMuons, mu_container.Data()).isSuccess()) {    // retrieve arguments: container type, container key
            Error(APP_NAME, "Failed to retrieve "+mu_container+" container");
            //return EL::StatusCode::FAILURE; //TODO!!! need to release that commet in principle - NOAM
         }
         if (!m_determinedDerivation) {
            Error(APP_NAME, "Wrong logic: is/isn't EXOT5 should have been determined in the muon part!");
         }
         const TString el_container = (m_isEXOT5) ? "EXOT5TruthElectrons" : "TruthElectrons";
         //const TString el_container = (m_isEXOT5) ? "EXOT5TruthElectrons" : "egammaTruthParticles";
         if (!event->retrieve(truthElectrons, el_container.Data()).isSuccess()) {    // retrieve arguments: container type, container key
            Error(APP_NAME, "Failed to retrieve "+el_container+" container");
            //return EL::StatusCode::FAILURE; //TODO!!! need to release that commet in principle - NOAM
         }
         //const TString ph_container = "TruthPhotons";
         //if (!event->retrieve(truthPhotons, ph_container.Data()).isSuccess()) {    // retrieve arguments: container type, container key
         //   Error(APP_NAME, "Failed to retrieve Photons container");
         //   return EL::StatusCode::FAILURE;
         //}


	 if(truthMuons!=nullptr && truthElectrons!=nullptr){
         // needed to study fakes
         for (const auto& part : *truthMuons) {
            cand.evt.truth_mu_pt.push_back(part->pt());
            cand.evt.truth_mu_eta.push_back(part->eta());
            cand.evt.truth_mu_phi.push_back(part->phi());
            cand.evt.truth_mu_m.push_back(part->m());
            cand.evt.truth_mu_status.push_back(part->status());
         }


         if (isSherpaVJets(mcid)) {
            //const TLorentzVector truth_V = DileptonST::getTruthBosonP4(truthParticles, truthElectrons, truthMuons, truthPhotons);
            const TLorentzVector truth_V = DileptonST::getTruthBosonP4(truthParticles, truthElectrons, truthMuons, truthParticles); // EXOT5 does not have TruthPhotons container

            cand.evt.truth_V_pt = truth_V.Pt();
            cand.evt.truth_V_eta = truth_V.Eta();
            cand.evt.truth_V_phi = truth_V.Phi();
            cand.evt.truth_V_m = truth_V.M();
            for (auto &kv : cand.evt.weights) {
               TString VJetsVariation = TString(kv.first).ReplaceAll("VJetsEW_", "");
               kv.second = m_vjetsuncEW->getCorrection(cand.evt.run, cand.evt.truth_V_pt / 1000., VJetsVariation); // NOTE: tool takes GeV!
            }
         } else { // not a Sherpa V+jets sample
            for (auto &kv : cand.evt.weights) {
               kv.second = 1.0; // set VJetsEW weights to 1 for non-VJets samples
            }
         }
	} // end if(truthMuons!=nullptr && truthElectrons!=nullptr)
      } // isMC

      //////////////////
      // composite variables
      //////////////////
      TVector2 the_met(m_met_nomuon_to_use);

      if (content.goodMuons.size() > 0) {
         const xAOD::Muon *thisMuon = content.goodMuons[0];
         cand.evt.munu_mT = TMath::Sqrt(2.0 * thisMuon->pt() * m_met_wmuon_to_use.Mod() * (1 - TMath::Cos(thisMuon->p4().Vect().XYvector().DeltaPhi(m_met_wmuon_to_use))));
         if (content.goodMuons.size() > 1) {
            TLorentzVector v_2mu = thisMuon->p4() + content.goodMuons[1]->p4();
            cand.evt.mumu_pt = v_2mu.Pt();
            cand.evt.mumu_eta = v_2mu.Eta();
            cand.evt.mumu_phi = v_2mu.Phi();
            cand.evt.mumu_m = v_2mu.M();
         } // at least two vetoed muons
      } // at least a vetoed muon
      if (content.goodJets.size() > 1) {
         TLorentzVector v_2jet = content.goodJets[0]->p4() + content.goodJets[1]->p4();
         cand.evt.jj_m = v_2jet.M();

         std::vector<TLorentzVector> hemi = RazorAnalysis::GetHemi(content.goodJets);
         cand.hem["hem"].add(hemi[0]);
         cand.hem["hem"].add(hemi[1]);

         RazorAnalysis::RazorVariables raz = RazorAnalysis::GetRazorVariables(hemi[0], hemi[1], the_met);
         cand.evt.shatR = raz.shatR;
         cand.evt.gaminvR = raz.gaminvR;
         cand.evt.gaminvRp1 = raz.gaminvRp1;
         cand.evt.dphi_BETA_R = raz.dphi_BETA_R;
         cand.evt.dphi_J1_J2_R = raz.dphi_J1_J2_R;
         cand.evt.gamma_Rp1 = raz.gamma_Rp1;
         cand.evt.costhetaR = raz.costhetaR;
         cand.evt.dphi_R_Rp1 = raz.dphi_R_Rp1;
         cand.evt.mdeltaR = raz.mdeltaR;
         cand.evt.cosptR = raz.cosptR;
         cand.evt.costhetaRp1 = raz.costhetaRp1;
      }
      if (content.goodElectrons.size() > 0) {
         TVector2 the_electron_met(the_met);
         cand.evt.enu_mT = TMath::Sqrt(2.0 * content.goodElectrons[0]->pt() * m_met_wmuon_to_use.Mod() * (1 - TMath::Cos(content.goodElectrons[0]->p4().Vect().XYvector().DeltaPhi(m_met_wmuon_to_use))));
         if (content.goodElectrons.size() > 1) {
            TLorentzVector v_2el = content.goodElectrons[0]->p4() + content.goodElectrons[1]->p4();
            cand.evt.ee_pt = v_2el.Pt();
            cand.evt.ee_eta = v_2el.Eta();
            cand.evt.ee_phi = v_2el.Phi();
            cand.evt.ee_m = v_2el.M();
         } // at least two vetoed electrons
      } // at least a vetoed electron


      //////////////////
      // MET variables
      // note that we save CST only for nominal
      //////////////////

      cand.met["met_nomuon_tst"].add(*((*content.met_tst_nomuon)["Final"]));
      if (!m_nominalDone) cand.met["met_nomuon_cst"].add(*((*content.met_cst_nomuon)["Final"]));
      cand.met["met_wmuon_tst"].add(*((*content.met_tst_wmuon)["Final"]));
      if (!m_nominalDone) cand.met["met_wmuon_cst"].add(*((*content.met_cst_wmuon)["Final"]));
      cand.met["met_noelectron_tst"].add(*((*content.met_tst_noelectron)["Final"]));
      if (!m_nominalDone) cand.met["met_noelectron_cst"].add(*((*content.met_cst_noelectron)["Final"]));
      if (!m_nominalDone) cand.met["met_muonterm_tst"].add(*((*content.met_tst_wmuon)["Muons"]));
      if (!m_nominalDone) cand.met["met_muonterm_cst"].add(*((*content.met_cst_wmuon)["Muons"]));

      if (!doIgnorePhotons) {
         cand.met["met_nophoton_tst"].add(*((*content.met_tst_nophoton)["Final"]));
         if (!m_nominalDone) cand.met["met_nophoton_cst"].add(*((*content.met_cst_nophoton)["Final"]));
         if (!m_nominalDone) cand.met["met_nophcalib_nomuon_tst"].add(*((*content.met_nophcalib_tst_nomuon)["Final"]));
         if (!m_nominalDone) cand.met["met_nophcalib_wmuon_tst"].add(*((*content.met_nophcalib_tst_wmuon)["Final"]));
      }

      cand.met["met_softerm_tst"].add(*((*content.met_tst_nomuon)["PVSoftTrk"]));
      if (!m_nominalDone) {
         cand.met["met_softerm_cst"].add(*((*content.met_cst_nomuon)["SoftClus"]));
         cand.met["met_jetterm"].add(*((*content.met_tst_wmuon)["RefJet"]));
         cand.met["met_muonterm"].add(*((*content.met_tst_wmuon)["Muons"]));
         cand.met["met_eleterm"].add(*((*content.met_tst_wmuon)["RefEle"]));
         cand.met["met_phterm"].add(*((*content.met_tst_wmuon)["RefGamma"]));
      }
      cand.met["met_track"].add(*((*content.met_track)["Track"]));
      if (m_isMC && !m_nominalDone) cand.met["met_truth"].add(*((*content.met_truth)["NonInt"]));

      //////////////////
      // muons
      //////////////////
      for (auto thisMuon : content.goodMuons) {
         cand.mu["mu"].add(*thisMuon);
      }

      // not saved in ttree
      for (auto thisMuon : content.baselineMuons) {
         cand.mu["mu_baseline"].add(*thisMuon);
      }

      // for debugging purpose
      for (auto thisMuon : * (content.muons)) {
         cand.mu["mu_preor"].add(*thisMuon);
      }

      //////////////////
      // electrons
      //////////////////
      for (auto thisElectron : content.goodElectrons) {
         cand.el["el"].add(*thisElectron);
      }

      // not saved in ttree
      for (auto thisElectron : content.baselineElectrons) {
         cand.el["el_baseline"].add(*thisElectron);
      }

      // for debugging purpose
      for (auto thisElectron : * (content.electrons)) {
         cand.el["el_preor"].add(*thisElectron);
      }

      /////////////////////////////
      // selected jets
      // + compute b-jet multiplicity
      ////////////////////////////
      static SG::AuxElement::Accessor<char> acc_bjet("bjet");
      UInt_t tmpNBJet(0);
      for (auto thisJet : content.goodJets) {
         cand.jet["jet"].add(*thisJet);

         if (acc_bjet(*thisJet))  tmpNBJet++;
      }
      cand.evt.n_bjet = tmpNBJet;

      // for debugging purpose
      for (auto thisJet : * (content.jets)) {
         cand.jet["jet_preor"].add(*thisJet);
      }

      //////////////////
      // photons
      //////////////////
      for (auto thisPhoton : content.goodPhotons) {
         cand.ph["ph"].add(*thisPhoton);
      }

      // not saved in ttree
      for (auto thisPhoton : content.baselinePhotons) {
         cand.ph["ph_baseline"].add(*thisPhoton);
      }

      // for debugging purpose
      for (auto thisPhoton : * (content.photons)) {
         cand.ph["ph_preor"].add(*thisPhoton);
      }

      //////////////////
      // Tau Jets
      //////////////////
      for (auto const & thisTau : content.goodTaus) {

         cand.tau["tau"].add(*thisTau);
      }

      // overall lepton scale factor calculation for baseline leptons
      // (used for veto efficiency corrections)
      // CAUTION: this does _not_ include isolation or trigger SFs!!!
      if (m_isMC && (doSystTrees || !m_nominalDone)) {
         cand.evt.evsf_baseline_nominal_EL = m_objTool->GetTotalElectronSF(content.baselineElectrons, kTRUE, kTRUE, kFALSE, kFALSE, ""); // reco, id, trig, iso, trigName
         cand.evt.evsf_baseline_nominal_MU = m_objTool->GetTotalMuonSF(content.baselineMuons, kTRUE, kFALSE, ""); // reco+id, iso, trigName
         if (!doIgnorePhotons) cand.evt.evsf_baseline_nominal_PH = m_objTool->GetTotalPhotonSF(content.baselinePhotons, kTRUE, kFALSE);  // eff, iso

         for (auto &kv : cand.evt.evsf_baseline_syst_EL) {
            kv.second = m_objTool->GetTotalElectronSFsys(content.baselineElectrons, CP::SystematicSet(kv.first.Data()), kTRUE, kTRUE, kFALSE, kFALSE);
         }
         for (auto &kv : cand.evt.evsf_baseline_syst_MU) {
            kv.second = m_objTool->GetTotalMuonSFsys(content.baselineMuons, CP::SystematicSet(kv.first.Data()), kTRUE, kFALSE, "");
         }
         for (auto &kv : cand.evt.evsf_baseline_syst_PH) {
            if (!doIgnorePhotons) kv.second = m_objTool->GetTotalPhotonSFsys(content.baselinePhotons, CP::SystematicSet(kv.first.Data()), kTRUE, kFALSE);
         }
      }


      if (doSystTrees || !m_nominalDone) cand.save();

      // fill systematic variation histograms (MC)
      if (m_isMC) {
         // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/CentralMC15ProductionList#Treating_high_weights_in_Sherpa
         // Temporary fixing to skip the events with huge mc weights
         // please note that this is NOT propagated back to ntuples (so you need to apply it also offline to have them
         // fully consistent with histograms!)
         const Bool_t setWeightTo1 = (isSherpaVJets(cand.evt.run) && abs(cand.evt.mconly_weight) > 1e2);

         const Float_t histo_pu_weight = (setWeightTo1) ? (cand.evt.pu_weight / cand.evt.mconly_weight) : cand.evt.pu_weight;
         const Float_t histo_mconly_weight = (setWeightTo1) ? 1.0 : cand.evt.mconly_weight;

         for (auto & m1 : m_hists) {
            TString param = m1.first;
            if (m1.second.count(systName) == 1) {
               for (auto & h : m1.second[systName]) {
                  // to avoid the overlap of the electron CRs
                  //if (h.first == CR1e_metnoel    && passRegion(CR1e,    cand)) continue;
                  //if (h.first == CR1e_metnoel150 && passRegion(CR1e150, cand)) continue;
                  //if (passRegion(h.first, cand)) { //TODO!!! NOAM
                     h.second->Fill(histEntryRegion(h.first, cand, param), histo_pu_weight * histWeightRegion(h.first, cand));

                     // manual VJetsUncertaintyTool systematics
                     if (systName == "nominal") {
                        for (auto kv_VJets : cand.evt.weights) {
                           const TString VJetsVariation = kv_VJets.first;
                           const Float_t thisWeight = kv_VJets.second; // weight for this variation
                           m_hists[param][VJetsVariation][h.first]->Fill(histEntryRegion(h.first, cand, param), thisWeight * histo_pu_weight * histWeightRegion(h.first, cand));
                        }
                     } // nominal
                  //} //TODO!!! NOAM
               }
            }
         }

         for (auto & m1 : m_pure_hists) {
            TString param = m1.first;
            if (m1.second.count(systName) == 1) {
               for (auto & h : m1.second[systName]) {
                  // to avoid the overlap of the electron CRs
                  //if (h.first == CR1e_metnoel    && passRegion(CR1e,    cand)) continue;
                  //if (h.first == CR1e_metnoel150 && passRegion(CR1e150, cand)) continue;
                  //if (passRegion(h.first, cand)) { //TODO!!! NOAM
                     h.second->Fill(histEntryRegion(h.first, cand, param), histo_mconly_weight * histWeightRegion(h.first, cand));

                     // manual VJetsUncertaintyTool systematics
                     if (systName == "nominal") {
                        for (auto kv_VJets : cand.evt.weights) {
                           const TString VJetsVariation = kv_VJets.first;
                           const Float_t thisWeight = kv_VJets.second; // weight for this variation
                           m_hists[param][VJetsVariation][h.first]->Fill(histEntryRegion(h.first, cand, param), thisWeight * histo_mconly_weight * histWeightRegion(h.first, cand));
                        }
                     } // nominal
                  //} //TODO!!! NOAM
               }
            }
         }
      } // MC
   } // event to be saved

// clear objects we own (note: objects registered in TStore are NOT owned)

// set read-once flag to true
   m_nominalDone = kTRUE;

   return EL::StatusCode::SUCCESS;
}

Int_t DileptonNTUPMaker :: isTrueW(const xAOD::TruthParticle * particle)
{
   bool isW = kFALSE;
   Int_t W_decay(-1);
   bool doPrint = kFALSE; // VI

   // Find the top children
   if (doPrint) cout << "Found " << particle->nChildren() << " top daughter" << endl;

   for (unsigned int p = 0; p < (unsigned int) particle->nChildren(); ++p) {
      const auto & top_child = particle->decayVtx()->outgoingParticle(p);

      if (doPrint) cout << "pdgId: " << fabs(top_child->pdgId()) << endl;
      if (fabs(top_child->pdgId()) == 24) { //find a W

         unsigned int  nq = 0;
         unsigned int nl = 0;
         unsigned int nnu = 0;

         // Find the W children
         for (unsigned int c = 0; c < (unsigned int) top_child->nChildren(); ++c) {
            const auto & child = top_child->decayVtx()->outgoingParticle(c);

            if (fabs(child->pdgId()) == 24) {
               return -1;
            } else if (fabs(child->pdgId()) > 0 && fabs(child->pdgId()) < 5) {
               if (doPrint)  cout << "Found a quark child of the W, pdgID = " << child->pdgId() << endl;
               ++nq;
            } else if (fabs(child->pdgId()) == 11 || fabs(child->pdgId()) == 13 || fabs(child->pdgId()) == 15) {
               if (doPrint)   cout << "Found a charged lepton child of the W, pdgID = " << child->pdgId() << endl;
               ++nl;
            } else if (fabs(child->pdgId()) == 12 || fabs(child->pdgId()) == 14 || fabs(child->pdgId()) == 16) {
               if (doPrint)   cout << "Found a neutrino child of the W, pdgID = " << child->pdgId() << endl;
               ++nnu;
            }
         }

         if (nq == 2) W_decay = 1;
         if ((nl == 1 && nnu == 1)) W_decay = 0;

         if (nq == 2 || (nl == 1 && nnu == 1)) {
            isW = true;
         }
         if (doPrint) cout << "Found n_quarks = " << nq << ", n_chargedLep = " << nl << ", and n_neutrinos = " << nnu << " children of the W. Is W? " << isW << "  W decay flag =  " << W_decay << endl;
      }
   }
   return W_decay;
}

EL::StatusCode DileptonNTUPMaker :: postExecute()
{
   // Here you do everything that needs to be done after the main event
   // processing.  This is typically very rare, particularly in user
   // code.  It is mainly used in implementing the NTupleSvc.
   return EL::StatusCode::SUCCESS;
}



EL::StatusCode DileptonNTUPMaker :: finalize()
{
   // This method is the mirror image of initialize(), meaning it gets
   // called after the last event has been processed on the worker node
   // and allows you to finish up any objects you created in
   // initialize() before they are written to disk.  This is actually
   // fairly rare, since this happens separately for each worker node.
   // Most of the time you want to do your post-processing on the
   // submission node after all your histogram outputs have been
   // merged.  This is different from histFinalize() in that it only
   // gets called on worker nodes that processed input events.

   const char *APP_NAME = "DileptonNTUPMaker::finalize";

   Info(APP_NAME, "Number of processed events = %i", m_eventCounter);

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode DileptonNTUPMaker :: histFinalize()
{
   // This method is the mirror image of histInitialize(), meaning it
   // gets called after the last event has been processed on the worker
   // node and allows you to finish up any objects you created in
   // histInitialize() before they are written to disk.  This is
   // actually fairly rare, since this happens separately for each
   // worker node.  Most of the time you want to do your
   // post-processing on the submission node after all your histogram
   // outputs have been merged.  This is different from finalize() in
   // that it gets called on all worker nodes regardless of whether
   // they processed input events.
   //

   m_cutFlow.printUnw();
   m_cutFlow.print();

   TH1F *cflow_hist = m_cutFlow.createTH1F();
   TH1F *cflow_hist_unw = m_cutFlow.createTH1Fraw();
   wk()->addOutput(cflow_hist);
   wk()->addOutput(cflow_hist_unw);

   return EL::StatusCode::SUCCESS;
}

EL::StatusCode DileptonNTUPMaker::getObjects(ST::SystInfo & systInfo, Analysis::ContentHolder & content)
{
   const char *APP_NAME = "DileptonNTUPMaker::getObjects";

   (void)systInfo;

   // THIS is the time-expensive part

   xAOD::TEvent *event = wk()->xaodEvent();


   // our isolation decorators, used to fill our tree
   static SG::AuxElement::Decorator<char> dec_pass_noam_highpt("pass_noam_HighPt"); //// TODO!!!
   static SG::AuxElement::Decorator<char> dec_pass_loosetrackonly("pass_loosetrackonly");
   static SG::AuxElement::Decorator<char> dec_pass_loose("pass_loose");
   static SG::AuxElement::Decorator<char> dec_pass_gradient("pass_gradient");
   static SG::AuxElement::Decorator<char> dec_pass_gradientloose("pass_gradientloose");
   static SG::AuxElement::Decorator<char> dec_pass_fixedcuttight("pass_fixedcuttight");
   static SG::AuxElement::Decorator<char> dec_pass_fixedcuttighttrackonly("pass_fixedcuttighttrackonly");
   static SG::AuxElement::Decorator<char> dec_pass_fixedcuttightcaloonly("pass_fixedcuttightcaloonly");
   static SG::AuxElement::Decorator<char> dec_pass_fixedcutloose("pass_fixedcutloose");
   static SG::AuxElement::Decorator<char> dec_ph_isTight("ph_isTight");
   static SG::AuxElement::Decorator<unsigned int> dec_ph_isEM("ph_isEM");
   static SG::AuxElement::Decorator<float> dec_new_d0("new_d0");
   static SG::AuxElement::Decorator<float> dec_new_d0sig("new_d0sig");
   static SG::AuxElement::Decorator<float> dec_new_z0("new_z0");
   static SG::AuxElement::Decorator<float> dec_new_z0sig("new_z0sig");
   static SG::AuxElement::Decorator<float> dec_new_met_nomuon_dphi("new_met_nomuon_dphi");
   static SG::AuxElement::Decorator<float> dec_new_met_wmuon_dphi("new_met_wmuon_dphi");
   static SG::AuxElement::Decorator<float> dec_new_met_noelectron_dphi("new_met_noelectron_dphi");
   static SG::AuxElement::Decorator<float> dec_new_met_nophoton_dphi("new_met_nophoton_dphi");
   static SG::AuxElement::Decorator<float> dec_new_met_nophcalib_nomuon_dphi("new_met_nophcalib_nomuon_dphi");
   static SG::AuxElement::Decorator<float> dec_new_met_nophcalib_wmuon_dphi("new_met_nophcalib_wmuon_dphi");
   static SG::AuxElement::Decorator<float> dec_lep_SF("lep_SF");
   static SG::AuxElement::Decorator<float> dec_lep_iso_SF("lep_iso_SF");
   static SG::AuxElement::Decorator<float> dec_lep_trigger_SF("lep_trigger_SF");
   static SG::AuxElement::Decorator<float> dec_lep_tot_SF("lep_tot_SF");
   static SG::AuxElement::Decorator<float> dec_lep_tot_isotight_SF("lep_tot_isotight_SF");
   static SG::AuxElement::Decorator<float> dec_ph_SF("ph_SF");
   static SG::AuxElement::Decorator<float> dec_ph_iso_SF("ph_iso_SF");

   // Initializing OutputTau decorators:
   static SG::AuxElement::Decorator<int> dec_is_loose("is_loose");
   static SG::AuxElement::Decorator<int> dec_is_medium("is_medium");
   static SG::AuxElement::Decorator<int> dec_is_tight("is_tight");

   // we clear good object vectors (as they depend on overlap removal and they have to be recalculated
   // for each systematic variation), but we do not clear all object vectors (as they don't)
   //
   // NOTE: we have to use the argument to clear, otherwise the xAOD framework will take ownership of the
   // pointers fed to these vectors, with a DISASTER as a result
   content.goodMuons.clear(SG::VIEW_ELEMENTS);
   content.baselineMuons.clear(SG::VIEW_ELEMENTS);
   content.goodElectrons.clear(SG::VIEW_ELEMENTS);
   content.baselineElectrons.clear(SG::VIEW_ELEMENTS);
   content.goodJets.clear(SG::VIEW_ELEMENTS);
   content.goodPhotons.clear(SG::VIEW_ELEMENTS);
   content.baselinePhotons.clear(SG::VIEW_ELEMENTS);
   content.goodTaus.clear(SG::VIEW_ELEMENTS);

   ///////////
   // Muons
   ///////////
   if (content.doMuons) {

      content.allMuons.clear(SG::VIEW_ELEMENTS);
      content.muons = nullptr;
      content.muonsAux = nullptr;
      CHECK(m_objTool->GetMuons(content.muons, content.muonsAux));
      m_n_allmu_bad = 0;

      for (auto thisMuon : * (content.muons)) {
         //check if there is a bad muon in ALL the muon candidates
         static SG::AuxElement::Accessor<char> acc_bad("bad");
         if (acc_bad(*thisMuon)) m_n_allmu_bad++;

         // HighPt
         dec_pass_noam_highpt(*thisMuon) = (int)m_objTool->IsHighPtMuon(*thisMuon);

         // check isolation
         dec_pass_loosetrackonly(*thisMuon) = (m_leptonIsolationLooseTrackOnly->accept(*thisMuon));
         dec_pass_loose(*thisMuon)         = (m_leptonIsolationLoose->accept(*thisMuon));
         dec_pass_gradient(*thisMuon)      = (m_leptonIsolationGradient->accept(*thisMuon));
         dec_pass_gradientloose(*thisMuon) = (m_leptonIsolationGradientLoose->accept(*thisMuon));
         //dec_pass_fixedcuttight(*thisMuon) = (m_leptonIsolationFixedCutTight->accept(*thisMuon)); // does not exist for muons
         dec_pass_fixedcuttighttrackonly(*thisMuon) = (m_leptonIsolationFixedCutTightTrackOnly->accept(*thisMuon));
         // do our own d0 and z0 corrections
         const xAOD::TrackParticle *thisTrack = thisMuon->primaryTrackParticle();
         const xAOD::TrackParticle *thisCBTrack = thisMuon->trackParticle(xAOD::Muon::TrackParticleType::CombinedTrackParticle);
         dec_new_d0(*thisMuon) = thisTrack->d0();
         dec_new_d0sig(*thisMuon) = xAOD::TrackingHelpers::d0significance(thisTrack, content.eventInfo->beamPosSigmaX(), content.eventInfo->beamPosSigmaY(), content.eventInfo->beamPosSigmaXY());
         const xAOD::Vertex* pv = m_objTool->GetPrimVtx();
         const Float_t primvertex_z = pv ? pv->z() : 0;
         dec_new_z0(*thisMuon) = thisTrack->z0() + thisTrack->vz() - primvertex_z;
         //dec_new_z0sig(*thisMuon) = TMath::Sqrt(thisTrack->definingParametersCovMatrix()(1, 1));
         dec_new_z0sig(*thisMuon) = (thisCBTrack && pv) ? fabs((thisCBTrack->z0() + thisCBTrack->vz() - primvertex_z)*sin(thisCBTrack->theta())) : -999.;
/*
//// from Peter
const xAOD::TrackParticle* trk = mu->trackParticle(xAOD::Muon::TrackParticleType::CombinedTrackParticle);
delta_z0 = fabs((trk->z0() + trk->vz() - pri_vtx->z())*sin(trk->theta())); 
if ( delta_z0 < 0.5 ) mu_bcs->SetPassed(MUON_CUTS::Z0);
*/


         // check SFs
         if(m_isMC) {
            //m_objTool->GetSignalMuonSF(*thisMuon,true,false);
            dec_lep_SF(*thisMuon) = m_objTool->GetSignalMuonSF(*thisMuon, true, false);
            dec_lep_iso_SF(*thisMuon) = m_objTool->GetSignalMuonSF(*thisMuon, true, true);
         }

         static SG::AuxElement::Accessor<char> acc_baseline("baseline");
         if(acc_baseline(*thisMuon)==1) {
            content.allMuons.push_back(thisMuon);
         } // signal muon
      } // loop over muons
      content.allMuons.sort(&DileptonST::comparePt); // sort only this (cannot sort my_muons, no need to sort content.goodMuons afterwards)
   }


   ///////////
   // Electrons
   ///////////
   if (content.doElectrons) {
      content.allElectrons.clear(SG::VIEW_ELEMENTS);
      content.electrons = nullptr;
      content.electronsAux = nullptr;

      if(

      CHECK(m_objTool->GetElectrons(content.electrons, content.electronsAux));

      for (auto thisElectron : * (content.electrons)) {

         // check isolation
         dec_pass_loosetrackonly(*thisElectron) = (m_leptonIsolationLooseTrackOnly->accept(*thisElectron));
         dec_pass_loose(*thisElectron)         = (m_leptonIsolationLoose->accept(*thisElectron));
         dec_pass_gradient(*thisElectron)      = (m_leptonIsolationGradient->accept(*thisElectron));
         dec_pass_gradientloose(*thisElectron) = (m_leptonIsolationGradientLoose->accept(*thisElectron));
         dec_pass_fixedcuttight(*thisElectron) = (m_leptonIsolationFixedCutTight->accept(*thisElectron));
         dec_pass_fixedcuttighttrackonly(*thisElectron) = (m_leptonIsolationFixedCutTightTrackOnly->accept(*thisElectron));

         // do our own d0 and z0 corrections
         const xAOD::TrackParticle *thisTrack = thisElectron->trackParticle();
         dec_new_d0(*thisElectron) = thisTrack->d0();
         dec_new_d0sig(*thisElectron) = xAOD::TrackingHelpers::d0significance(thisTrack, content.eventInfo->beamPosSigmaX(), content.eventInfo->beamPosSigmaY(), content.eventInfo->beamPosSigmaXY());
         const xAOD::Vertex* pv = m_objTool->GetPrimVtx();
         const Float_t primvertex_z = pv ? pv->z() : 0;
         dec_new_z0(*thisElectron) = thisTrack->z0() + thisTrack->vz() - primvertex_z;
         //dec_new_z0sig(*thisElectron) = TMath::Sqrt(thisTrack->definingParametersCovMatrix()(1, 1));
         dec_new_z0sig(*thisElectron) = (thisTrack && pv) ? fabs((thisTrack->z0() + thisTrack->vz() - primvertex_z)*sin(thisTrack->theta())) : -9999;

         // check SFs
         if (m_isMC) {
            //m_objTool->GetSignalElecSF(*thisElectron, true, true, false, true);
            dec_lep_SF(*thisElectron) = m_objTool->GetSignalElecSF(*thisElectron, true, true, false, false); // reco, id
            dec_lep_iso_SF(*thisElectron) = m_objTool->GetSignalElecSF(*thisElectron, true, true, false, true); // reco, id, iso
            //dec_lep_trigger_SF(*thisElectron) = m_objTool->GetSignalElecSF(*thisElectron, true, true, true, false, "e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose");
            //const Double_t SF_without_iso = m_objTool->GetSignalElecSF(*thisElectron, true, true, true, false, "singleLepton"); // reco, id, trigger // TODO!!! NOAM
            const Double_t SF_without_iso = 1.; // reco, id, trigger
            dec_lep_trigger_SF(*thisElectron) = SF_without_iso;
            
            //Int_t tmpYear = m_objTool->treatAsYear();

            //dec_lep_tot_SF(*thisElectron) =  m_objTool->GetSignalElecSF(*thisElectron, true, true, true, true, "singleLepton"); // reco, id, trigger, iso // TODO!!! NOAM
            dec_lep_tot_SF(*thisElectron) = 1.; // reco, id, trigger, iso

            // SF_tot_isotight is the same as SF_trigger, multiplied by the TIGHT isolation SF
            Double_t tightiso_SF(1.);
            // CHECK(m_electronIsolationSFTool->getEfficiencyScaleFactor(*thisElectron, tightiso_SF)); // TODO!!! NOAM

            dec_lep_tot_isotight_SF(*thisElectron) =  tightiso_SF * SF_without_iso; // reco, id, trigger, isotight
         }

         static SG::AuxElement::Accessor<char> acc_signal("signal");
         static SG::AuxElement::Accessor<char> acc_baseline("baseline");
         if (acc_baseline(*thisElectron) == 1) {
            content.allElectrons.push_back(thisElectron);
         } // signal electron
      } // loop over electrons
      content.allElectrons.sort(&DileptonST::comparePt);
   }


   ///////////
   // Jets
   ///////////
   if (content.doJets) {
      content.allJets.clear(SG::VIEW_ELEMENTS);

      content.jets = nullptr;
      content.jetsAux = nullptr;
      CHECK(m_objTool->GetJets(content.jets, content.jetsAux));

      for (auto thisJet : * (content.jets)) {
         static SG::AuxElement::Accessor<char> acc_baseline("baseline");
         if (acc_baseline(*thisJet) == 1) {
            content.allJets.push_back(thisJet);
         } // baseline jet
      } // loop over jets

      content.allJets.sort(&DileptonST::comparePt);
   }


   ///////////
   // Photons
   ///////////
   if (content.doPhotons) {
      content.allPhotons.clear(SG::VIEW_ELEMENTS);

      content.photons = nullptr;
      content.photonsAux = nullptr;
      CHECK(m_objTool->GetPhotons(content.photons, content.photonsAux));

      for (auto thisPhoton : * (content.photons)) {
         static SG::AuxElement::Accessor<char> acc_signal("signal");
         static SG::AuxElement::Accessor<char> acc_baseline("baseline");
         if (acc_baseline(*thisPhoton) == 1) {
            // check isolation
            dec_pass_fixedcuttightcaloonly(*thisPhoton) = (m_photonIsolationFixedCutTightCaloOnly->accept(*thisPhoton));
            dec_pass_fixedcuttight(*thisPhoton) = (m_photonIsolationFixedCutTight->accept(*thisPhoton));
            dec_pass_fixedcutloose(*thisPhoton) = (m_photonIsolationFixedCutLoose->accept(*thisPhoton));
            dec_ph_isTight(*thisPhoton) = m_photonTightIsEMSelector->accept(*thisPhoton);
            dec_ph_isEM(*thisPhoton) = (m_photonTightIsEMSelector->IsemValue());
            // check SFs
            if (m_isMC) {
               dec_ph_SF(*thisPhoton)     = m_objTool->GetSignalPhotonSF(*thisPhoton, true, false); // has only reco SF
               dec_ph_iso_SF(*thisPhoton) = m_objTool->GetSignalPhotonSF(*thisPhoton, false, true);
            }

            content.allPhotons.push_back(thisPhoton);
         } // baseline photon
      } // loop over photons

      content.allPhotons.sort(&DileptonST::comparePt);
   }

   //////////////////
   // Tau Jets
   //////////////////
   if (content.doTaus) {

      content.allTaus.clear(SG::VIEW_ELEMENTS);

      content.taus = nullptr;
      content.tausAux = nullptr;
      CHECK(m_objTool->GetTaus(content.taus, content.tausAux));

      for (auto const & thisTau : *content.taus) {

         static SG::AuxElement::Accessor<char> acc_baseline("baseline");

         dec_is_loose(*thisTau) = -999; // TODO: update convention to 0?
         dec_is_medium(*thisTau) = -999;
         dec_is_tight(*thisTau) = -999;

         if (acc_baseline(*thisTau) == 1) {

            // Check BDT working point
            dec_is_loose(*thisTau) = m_tauLoose->accept(*thisTau);
            dec_is_medium(*thisTau) = m_tauMedium->accept(*thisTau);
            dec_is_tight(*thisTau) = m_tauTight->accept(*thisTau);

            content.allTaus.push_back(thisTau);
         }
      }

      content.allTaus.sort(&DileptonST::comparePt);
   }


   ///////////
   // Overlap removal and good object selection
   ///////////
   if (content.doOverlapRemoval) {
      //CHECK(m_objTool->OverlapRemoval(content.electrons, content.muons, content.jets, 0, 0));
      if (!doIgnorePhotons) CHECK(m_objTool->OverlapRemoval(content.electrons, content.muons, content.jets, content.photons));
      else CHECK(m_objTool->OverlapRemoval(content.electrons, content.muons, content.jets));

      // good jet selection
      static SG::AuxElement::Accessor<char> acc_bad("bad");
      static SG::AuxElement::Accessor<char> acc_signal("signal");
      static SG::AuxElement::Accessor<char> acc_passOR("passOR");
      //TODO: dirty jets need to be reconfigured
      m_ht = 0;
      for (auto thisJet : content.allJets) {
         // ask good jets to be SUSY signal jets, i.e.
         //   - baseline (aka considered for overlap removal)
         //   - passOR (i.e. pass overlap removal)
         //   - with pT > 30 GeV, |eta| < 2.8
         // and also
         //   - not bad (jet cleaning)
         if (acc_bad(*thisJet) == 0 && acc_passOR(*thisJet) == 1 &&
             acc_signal(*thisJet) == 1) {
            content.goodJets.push_back(thisJet);
            m_ht += thisJet->pt();
         }

         //         if (dirtyJets) m_objTool->IsBadJet(*thisJet);
      }

      // good muon selection
      m_n_mu_baseline = 0;
      m_n_mu_baseline_bad = 0;
      for (auto thisMuon : content.allMuons) {
         //static SG::AuxElement::Accessor<char> acc_cosmic("cosmic");
         //if(acc_passOR(*thisMuon)==1 && acc_cosmic(*thisMuon)==0) {
         if(acc_passOR(*thisMuon)==1) {
            //if(acc_bad(*thisMuon)==0) {
               m_n_mu_baseline++;
               content.baselineMuons.push_back(thisMuon);
               //if(acc_signal(*thisMuon)==1) content.goodMuons.push_back(thisMuon); // muons for CRs
               content.goodMuons.push_back(thisMuon);
            //}
            //else {
               m_n_mu_baseline_bad++;
            //}
         }
      }

      // good electron selection (i.e. electrons for veto)
      m_n_el_baseline = 0;
      for (auto thisElectron : content.allElectrons) {
         if(acc_passOR(*thisElectron)==1) {
            m_n_el_baseline++;
            content.baselineElectrons.push_back(thisElectron);
            //if(acc_signal(*thisElectron)==1) content.goodElectrons.push_back(thisElectron); // electrons for CRs
            content.goodElectrons.push_back(thisElectron);
         }
      }

      // good photon selection
      m_n_ph_baseline = 0;
      for (auto thisPhoton : content.allPhotons) {
         if (acc_passOR(*thisPhoton) == 1) {
            m_n_ph_baseline++;
            content.baselinePhotons.push_back(thisPhoton);
            if (acc_signal(*thisPhoton) == 1) {
               content.goodPhotons.push_back(thisPhoton);
            }
         }
      }

      // good tau jets selection
      // WARNING: CURRENTLY NO SELECTION IN MADE, THE CODE IS HERE JUST TO SEE
      // IF HISTOGRAMS ARE PRODUCED.
      for (auto const & thisTau : content.allTaus) {
         content.goodTaus.push_back(thisTau);
      }
   }

   if (content.doMET) {
      const TString met_prefix = systInfo.systset.name() + "_";

      xAOD::PhotonContainer* ph_Cont(nullptr);

      if (!doIgnorePhotons) {
         ph_Cont = content.photons;
      }
      //==================
      // MET calculation
      //==================

      ///////////
      // MET, invisible muons, CST soft term
      ///////////
      getMET(content.met_cst_nomuon,
             content.met_cst_nomuonAux,
             content.jets, // use all objects (before OR and after corrections) for MET utility
             content.electrons,
             content.muons,
             ph_Cont,
             kFALSE, // do TST
             kFALSE, // do JVT
             &content.goodMuons // invisible particles
            );

      ///////////
      // MET, invisible muons, TST soft term
      //////////
      // temporary fix for no muon computation
      /*getMET(content.met_tst_nomuon,
             content.met_tst_nomuonAux,
             content.jets, // use all objects (before OR and after corrections) for MET utility
             content.electrons,
             content.muons,
             ph_Cont,
             kTRUE, // do TST
             kTRUE, // do JVT
             &content.goodMuons // invisible particles
            );*/
      getMET(content.met_tst_nomuon,
             content.met_tst_nomuonAux,
             content.jets, // use all objects (before OR and after corrections) for MET utility
             content.electrons,
             content.muons,
             ph_Cont,
             kTRUE, // do TST
             kTRUE, // do JVT
             nullptr // invisible particles
            );
      // create sum of of muon pts
      {
         Float_t px = 0;
         Float_t py = 0;
         for (auto thisMuon : content.goodMuons) {
            px += thisMuon->pt() * TMath::Cos(thisMuon->phi());
            py += thisMuon->pt() * TMath::Sin(thisMuon->phi());
         }
         //TVector2 mu_momenta(px,py);
         const Float_t mpx = (*content.met_tst_nomuon)["Final"]->mpx();
         const Float_t mpy = (*content.met_tst_nomuon)["Final"]->mpy();
         (*content.met_tst_nomuon)["Final"]->setMpx(mpx + px);
         (*content.met_tst_nomuon)["Final"]->setMpy(mpy + py);
      }

      ///////////
      // MET, with muons (real MET), CST soft term
      ///////////
      getMET(content.met_cst_wmuon,
             content.met_cst_wmuonAux,
             content.jets, // use all objects (before OR and after corrections) for MET utility
             content.electrons,
             content.muons,
             ph_Cont,
             kFALSE, // do TST
             kFALSE, // do JVT
             nullptr // invisible particles
            );

      ///////////
      // MET, with muons (real MET), TST soft term
      ///////////
      getMET(content.met_tst_wmuon,
             content.met_tst_wmuonAux,
             content.jets, // use all objects (before OR and after corrections) for MET utility
             content.electrons,
             content.muons,
             ph_Cont,
             kTRUE, // do TST
             kTRUE, // do JVT
             nullptr // invisible particles
            );

      ///////////
      // MET, invisible good electrons, CST soft term
      ///////////
      getMET(content.met_cst_noelectron,
             content.met_cst_noelectronAux,
             content.jets, // use all objects (before OR and after corrections) for MET utility
             content.electrons,
             content.muons,
             ph_Cont,
             kFALSE, // do TST
             kFALSE, // do JVT
             &content.goodElectrons // invisible particles
            );

      ///////////
      // MET, invisible good electrons, TST soft term
      ///////////
      /*getMET(content.met_tst_noelectron,
             content.met_tst_noelectronAux,
             content.jets, // use all objects (before OR and after corrections) for MET utility
             content.electrons,
             content.muons,
             ph_Cont,
             kTRUE, // do TST
             kTRUE, // do JVT
             &content.goodElectrons // invisible particles
            );*/
      getMET(content.met_tst_noelectron,
             content.met_tst_noelectronAux,
             content.jets, // use all objects (before OR and after corrections) for MET utility
             content.electrons,
             content.muons,
             ph_Cont,
             kTRUE, // do TST
             kTRUE, // do JVT
             nullptr // invisible particles
            );
      // create sum of of electron pts
      {
         Float_t px = 0;
         Float_t py = 0;
         for (auto thisElectron : content.goodElectrons) {
            px += thisElectron->pt() * TMath::Cos(thisElectron->phi());
            py += thisElectron->pt() * TMath::Sin(thisElectron->phi());
         }
         //TVector2 mu_momenta(px,py);
         const Float_t mpx = (*content.met_tst_noelectron)["Final"]->mpx();
         const Float_t mpy = (*content.met_tst_noelectron)["Final"]->mpy();
         (*content.met_tst_noelectron)["Final"]->setMpx(mpx + px);
         (*content.met_tst_noelectron)["Final"]->setMpy(mpy + py);
      }

      if (!doIgnorePhotons) {
         ///////////
         // MET, invisible photons, CST soft term
         ///////////

         getMET(content.met_cst_nophoton,
                content.met_cst_nophotonAux,
                content.jets, // use all objects (before OR and after corrections) for MET utility
                content.electrons,
                content.muons,
                ph_Cont,
                kFALSE, // do TST
                kFALSE, // do JVT
                &content.goodPhotons // invisible particles
               );

         ///////////
         // MET, invisible photons, TST soft term
         ///////////
         /*getMET(content.met_tst_nophoton,
                content.met_tst_nophotonAux,
                content.jets, // use all objects (before OR and after corrections) for MET utility
                content.electrons,
                content.muons,
                ph_Cont,
                kTRUE, // do TST
                kTRUE, // do JVT
                &content.goodPhotons // invisible particles
               );*/
         getMET(content.met_tst_nophoton,
                content.met_tst_nophotonAux,
                content.jets, // use all objects (before OR and after corrections) for MET utility
                content.electrons,
                content.muons,
                ph_Cont,
                kTRUE, // do TST
                kTRUE, // do JVT
                nullptr // invisible particles
               );
         // create sum of of electron pts
         {
            Float_t px = 0;
            Float_t py = 0;
            for (auto thisPhoton : content.goodPhotons) {
               px += thisPhoton->pt() * TMath::Cos(thisPhoton->phi());
               py += thisPhoton->pt() * TMath::Sin(thisPhoton->phi());
            }
            //TVector2 mu_momenta(px,py);
            const Float_t mpx = (*content.met_tst_nophoton)["Final"]->mpx();
            const Float_t mpy = (*content.met_tst_nophoton)["Final"]->mpy();
            (*content.met_tst_nophoton)["Final"]->setMpx(mpx + px);
            (*content.met_tst_nophoton)["Final"]->setMpy(mpy + py);
         }



         ///////////
         // MET, invisible muons, TST soft term calculating ignoring the photons
         //////////
         getMET(content.met_nophcalib_tst_nomuon,
                content.met_nophcalib_tst_nomuonAux,
                content.jets, // use all objects (before OR and after corrections) for MET utility
                content.electrons,
                content.muons,
                nullptr,
                kTRUE, // do TST
                kTRUE, // do JVT
                &content.goodMuons // invisible particles
               );


         ///////////
         // MET, with muons (real MET), TST soft term  calculating ignoring the photons
         ///////////
         getMET(content.met_nophcalib_tst_wmuon,
                content.met_nophcalib_tst_wmuonAux,
                content.jets, // use all objects (before OR and after corrections) for MET utility
                content.electrons,
                content.muons,
                nullptr,
                kTRUE, // do TST
                kTRUE, // do JVT
                nullptr // invisible particles
               );

      }

      ///////////
      // MET, from tracks (aka MPT)
      ///////////
      getTrackMET(content.met_track,
                  content.met_trackAux,
                  content.jets, // use all objects (before OR and after corrections) for MET utility
                  content.electrons,
                  content.muons
                 );

      ///////////
      // MET, truth (if available)
      ///////////
      if (m_isMC) {
         content.met_truth = nullptr;

         if (!event->retrieve(content.met_truth, "MET_Truth").isSuccess()) {    // retrieve arguments: container type, container key
            Error(APP_NAME, "Failed to retrieve MET_Truth container");
            return EL::StatusCode::FAILURE;
         }

         xAOD::MissingETContainer::const_iterator met_it = content.met_truth->find("NonInt");

         if (met_it == content.met_truth->end()) {
            Error(APP_NAME, "No NonInt inside truth MET container");
            return EL::StatusCode::FAILURE;
         }
      }

      // MET to be used
      m_met_nomuon_to_use = TVector2((*content.met_tst_nomuon)["Final"]->mpx(), (*content.met_tst_nomuon)["Final"]->mpy());
      m_met_wmuon_to_use =  TVector2((*content.met_tst_wmuon)["Final"]->mpx(), (*content.met_tst_wmuon)["Final"]->mpy());
      m_met_noelectron_to_use = TVector2((*content.met_tst_noelectron)["Final"]->mpx(), (*content.met_tst_noelectron)["Final"]->mpy());
      if (!doIgnorePhotons) {
         m_met_nophcalib_nomuon_to_use = TVector2((*content.met_nophcalib_tst_nomuon)["Final"]->mpx(), (*content.met_nophcalib_tst_nomuon)["Final"]->mpy());
         m_met_nophcalib_wmuon_to_use =  TVector2((*content.met_nophcalib_tst_wmuon)["Final"]->mpx(), (*content.met_nophcalib_tst_wmuon)["Final"]->mpy());
         m_met_nophoton_to_use = TVector2((*content.met_tst_nophoton)["Final"]->mpx(), (*content.met_tst_nophoton)["Final"]->mpy());
      }

      // compute dphi's
      for (auto thisMuon : content.allMuons) {
         dec_new_met_nomuon_dphi(*thisMuon) = thisMuon->p4().Vect().XYvector().DeltaPhi(m_met_nomuon_to_use);
         dec_new_met_wmuon_dphi(*thisMuon) = thisMuon->p4().Vect().XYvector().DeltaPhi(m_met_wmuon_to_use);
         dec_new_met_noelectron_dphi(*thisMuon) = thisMuon->p4().Vect().XYvector().DeltaPhi(m_met_noelectron_to_use);
         if (!doIgnorePhotons) {
            dec_new_met_nophcalib_nomuon_dphi(*thisMuon) = thisMuon->p4().Vect().XYvector().DeltaPhi(m_met_nophcalib_nomuon_to_use);
            dec_new_met_nophcalib_wmuon_dphi(*thisMuon) = thisMuon->p4().Vect().XYvector().DeltaPhi(m_met_nophcalib_wmuon_to_use);
            dec_new_met_nophoton_dphi(*thisMuon) = thisMuon->p4().Vect().XYvector().DeltaPhi(m_met_nophoton_to_use);
         }
      }
      for (auto thisJet : content.allJets) {
         dec_new_met_nomuon_dphi(*thisJet) = thisJet->p4().Vect().XYvector().DeltaPhi(m_met_nomuon_to_use);
         dec_new_met_wmuon_dphi(*thisJet) = thisJet->p4().Vect().XYvector().DeltaPhi(m_met_wmuon_to_use);
         dec_new_met_noelectron_dphi(*thisJet) = thisJet->p4().Vect().XYvector().DeltaPhi(m_met_noelectron_to_use);
         if (!doIgnorePhotons) {
            dec_new_met_nophcalib_nomuon_dphi(*thisJet) = thisJet->p4().Vect().XYvector().DeltaPhi(m_met_nophcalib_nomuon_to_use);
            dec_new_met_nophcalib_wmuon_dphi(*thisJet) = thisJet->p4().Vect().XYvector().DeltaPhi(m_met_nophcalib_wmuon_to_use);
            dec_new_met_nophoton_dphi(*thisJet) = thisJet->p4().Vect().XYvector().DeltaPhi(m_met_nophoton_to_use);
         } else {
            dec_new_met_nophoton_dphi(*thisJet) = -9999; //needed because the variable is always filled
            dec_new_met_nophcalib_nomuon_dphi(*thisJet) = -9999; //needed because the variable is always filled
            dec_new_met_nophcalib_wmuon_dphi(*thisJet) = -9999; //needed because the variable is always filled
         }
      }
      for (auto thisElectron : content.allElectrons) {
         dec_new_met_nomuon_dphi(*thisElectron) = thisElectron->p4().Vect().XYvector().DeltaPhi(m_met_nomuon_to_use);
         dec_new_met_wmuon_dphi(*thisElectron) = thisElectron->p4().Vect().XYvector().DeltaPhi(m_met_wmuon_to_use);
         dec_new_met_noelectron_dphi(*thisElectron) = thisElectron->p4().Vect().XYvector().DeltaPhi(m_met_noelectron_to_use);
         if (!doIgnorePhotons) {
            dec_new_met_nophcalib_nomuon_dphi(*thisElectron) = thisElectron->p4().Vect().XYvector().DeltaPhi(m_met_nophcalib_nomuon_to_use);
            dec_new_met_nophcalib_wmuon_dphi(*thisElectron) = thisElectron->p4().Vect().XYvector().DeltaPhi(m_met_nophcalib_wmuon_to_use);
            dec_new_met_nophoton_dphi(*thisElectron) = thisElectron->p4().Vect().XYvector().DeltaPhi(m_met_nophoton_to_use);
         }
      }
      if (!doIgnorePhotons) for (auto thisPhoton : content.allPhotons) {
            dec_new_met_nomuon_dphi(*thisPhoton) = thisPhoton->p4().Vect().XYvector().DeltaPhi(m_met_nomuon_to_use);
            dec_new_met_wmuon_dphi(*thisPhoton) = thisPhoton->p4().Vect().XYvector().DeltaPhi(m_met_wmuon_to_use);
            dec_new_met_noelectron_dphi(*thisPhoton) = thisPhoton->p4().Vect().XYvector().DeltaPhi(m_met_noelectron_to_use);
            dec_new_met_nophoton_dphi(*thisPhoton) = thisPhoton->p4().Vect().XYvector().DeltaPhi(m_met_nophoton_to_use);
         }
   } // doMET

   return EL::StatusCode::SUCCESS;
}

EL::StatusCode DileptonNTUPMaker::getLastCutPassed(ST::SystInfo & systInfo, Analysis::ContentHolder & content, SelectionCriteria crit, DileptonCuts::CutID & last)
{
   const char *APP_NAME = "DileptonNTUPMaker::getLastCutPassed";

   if(false) std::cout << crit << std::endl;

   xAOD::TEvent *event = wk()->xaodEvent();

   //////
   // retrieve event info (used here to check data vs MC, and everywhere for analysis)
   //////
   content.eventInfo = nullptr;

   if (!event->retrieve(content.eventInfo, "EventInfo").isSuccess()) {
      Error(APP_NAME, "Failed to retrieve event info collection");
      return EL::StatusCode::FAILURE;
   }

   if (content.eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) && !m_isMC) {
      throw std::runtime_error("Found MC when set-up for data");
   } else if (!content.eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) && m_isMC) {
      throw std::runtime_error("Found data when set-up for MC");
   }


   ///////////////////
   // GRL, cleaning
   ///////////////////
   last = DileptonCuts::processed;
   if (!m_isMC) {
      // GRL
      if(!PassedCut(m_grl->passRunLB(*content.eventInfo),DileptonCuts::GRL,last)) return EL::StatusCode::SUCCESS;
   }


   // cleaning
   Bool_t failCleaning = ((content.eventInfo->errorState(xAOD::EventInfo::LAr)  == xAOD::EventInfo::Error) ||
                          (content.eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) ||
                          (content.eventInfo->errorState(xAOD::EventInfo::SCT)  == xAOD::EventInfo::Error && !(configFile == "SUSYTools_DileptonnfigCutflow.config")) ||
                          (content.eventInfo->eventFlags(xAOD::EventInfo::Core) & 0x40000));
   if(!PassedCut(!failCleaning,DileptonCuts::cleaning,last)) return EL::StatusCode::SUCCESS;




   ///////////////////
   // good vertex
   ///////////////////
   Bool_t passesVertex(kFALSE);
   const UInt_t nMinTracks(2);
   content.vertices = nullptr;
   if (!event->retrieve(content.vertices, "PrimaryVertices").isSuccess()) {
      Error(APP_NAME, "Failed to retrieve PrimaryVertices container");
      return EL::StatusCode::FAILURE;
   }
   for (auto thisVertex : *content.vertices) {
      if ((UInt_t)(thisVertex->nTrackParticles()) >= nMinTracks) {
         passesVertex = kTRUE;
         break;
      }
   }
  // if(!PassedCut(passesVertex,DileptonCuts::vertex,last)) return EL::StatusCode::SUCCESS;


   ///////////////////
   // get objects
   ///////////////////
   if (getObjects(systInfo, content) != EL::StatusCode::SUCCESS) {
      Error(APP_NAME, "Error in getObjects");
      return EL::StatusCode::FAILURE;
   }


   ///////////////////
   // MET cleaning
   ///////////////////
   Bool_t passMETcleaning(kTRUE);
   if (!dirtyJets) {
      static SG::AuxElement::Accessor<char> acc_passOR("passOR"); // passOR implies baseline
      static SG::AuxElement::Accessor<char> acc_bad("bad");
      for (auto thisJet : content.allJets) {
         if (acc_passOR(*thisJet) == 1 && acc_bad(*thisJet) == 1)
         {
             passMETcleaning = kFALSE;
             break;
         }
      }
   }
   //if(!PassedCut(passMETcleaning,DileptonCuts::MET_cleaning,last)) return EL::StatusCode::SUCCESS;


   return EL::StatusCode::SUCCESS;
}



EL::StatusCode DileptonNTUPMaker::getMET(std::shared_ptr<xAOD::MissingETContainer> &met, std::shared_ptr<xAOD::MissingETAuxContainer> &metAux, xAOD::JetContainer * jet, xAOD::ElectronContainer * el, xAOD::MuonContainer * mu, xAOD::PhotonContainer * ph, Bool_t doTST, Bool_t doJVT, xAOD::IParticleContainer * invis)
{
   const char *APP_NAME = "DileptonNTUPMaker::getMET";

   met = std::make_shared<xAOD::MissingETContainer>();
   metAux = std::make_shared<xAOD::MissingETAuxContainer>();
   met->setStore(metAux.get());

   CHECK(m_objTool->GetMET(*met,
                           jet, // use all objects (before OR and after corrections) for MET utility
                           el,
                           mu,
                           ph,
                           0, // tau term
                           doTST,
                           doJVT,
                           invis// invisible particles
                          ));

   xAOD::MissingETContainer::const_iterator met_it = met->find("Final");

   if (met_it == met->end()) {
      Error(APP_NAME, "No RefFinal inside MET container");
      return EL::StatusCode::FAILURE;
   }


   return EL::StatusCode::SUCCESS;
}

EL::StatusCode DileptonNTUPMaker::getTrackMET(std::shared_ptr<xAOD::MissingETContainer> &met, std::shared_ptr<xAOD::MissingETAuxContainer> &metAux, xAOD::JetContainer * jet, xAOD::ElectronContainer * el, xAOD::MuonContainer * mu)
{
   const char *APP_NAME = "DileptonNTUPMaker::getTrackMET";

   met = std::make_shared<xAOD::MissingETContainer>();
   metAux = std::make_shared<xAOD::MissingETAuxContainer>();
   met->setStore(metAux.get());

   CHECK(m_objTool->GetTrackMET(*met, // note that we don't need to pass the Aux container here
                                jet, // use all objects (before OR and after corrections) for MET utility
                                el,
                                mu
                               ));
   xAOD::MissingETContainer::const_iterator met_it = met->find("Track");

   if (met_it == met->end()) {
      Error(APP_NAME, "No Track inside MET container");
      return EL::StatusCode::FAILURE;
   }


   return EL::StatusCode::SUCCESS;
}

Bool_t DileptonNTUPMaker::isSherpaVJets(Int_t mcid)
{
   // includes both old and new release, and ordinary and low-mass DY samples
   const Bool_t Zee = ((mcid >= 364114 && mcid <= 364127) || (mcid >= 361468 && mcid <= 361475) || (mcid >= 363388 && mcid <= 363411) || (mcid >= 364204 && mcid <= 364209));
   const Bool_t Zmumu = ((mcid >= 364100 && mcid <= 364113) || (mcid >= 361476 && mcid <= 361483) || (mcid >= 363364 && mcid <= 363387) || (mcid >= 364198 && mcid <= 364203));
   const Bool_t Ztautau = ((mcid >= 364128 && mcid <= 364141) || (mcid >= 363361 && mcid <= 363363) || (mcid >= 361484 && mcid <= 361491) || (mcid >= 363102 && mcid <= 363122) || (mcid >= 364210 && mcid <= 364215));
   const Bool_t Znunu = ((mcid >= 364142 && mcid <= 364155) || (mcid >= 363412 && mcid <= 363435));
   const Bool_t Wenu = ((mcid >= 364170 && mcid <= 364183) || (mcid >= 363460 && mcid <= 363483));
   const Bool_t Wmunu = ((mcid >= 364156 && mcid <= 364169) || (mcid >= 363436 && mcid <= 363459));
   const Bool_t Wtaunu = ((mcid >= 364184 && mcid <= 364197) || (mcid >= 363331 && mcid <= 363354));

   return (Zee || Zmumu || Ztautau || Znunu || Wenu || Wmunu || Wtaunu);
}
