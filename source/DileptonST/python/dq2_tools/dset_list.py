# generate sample lists
# Emma Tolley - Harvard University
# see README

#import pyAMI.client
#import pyAMI.atlas.api
#import dq2.clientapi.DQ2 as theDQ2module

from dset_info import get_dsets_from_query

def get_dsets_from_file(infile):
    intext = open(infile,'r').read().split('\n')

    # trim
    intext = [t for t in intext if len(t) > 0 and '#' not in t]

    return intext

def find_grid_dset(first, mcid, stage, derivation, update_tag, old_name = ''):

    # query with wildcards for dq2
    query = first + '.' + mcid+ '*.' + stage + '.' + derivation + '.*'
    grid_dsets = [str(d) for d in get_dsets_from_query(query).keys()]

    # list of all matching grid datasets that contain new tag
    updated_dset = []
    found_dset = False

    old_matched = False
    if len(old_name) == 0: old_matched = True
    dset_name = ''

    # parse query results, looking for the new tag
    for d in grid_dsets:
        if not old_matched and old_name in d: old_matched = True
        if update_tag in d: updated_dset += [d.split(':')[-1]]
        dset_name = d.split('.')[2]

    if not old_matched:
        print '#', old_name, 'not found on grid'

    if len(updated_dset) == 0:
        print '#',(first + '.' + mcid + '.' + dset_name),'missing! No version with tag', update_tag,'found'
        found_dset = False
    else:
        if len([d for d in updated_dset if '_tid' not in d]) == 0:
            print '#',(first + '.' + mcid + '.' + dset_name),'final dataset not ready'
        updated_dset.sort()
        print updated_dset[0]
        found_dset = True
    return found_dset

def make_list_from_file(input_file, update_tag):
    # list of datasets from file
    dsets = get_dsets_from_file(input_file)

    for dset in dsets:
        (first, mcid, name, stage, derivation, tags) = dset.split('.')
        found_dset = find_grid_dset(first, mcid, stage, derivation, update_tag, dset)
        if not found_dset:
            missing_dsets += [first + '.' + mcid]
    return missing_dsets

def make_list_from_mcids(input_file, update_tag, new_mcids, first = 'mc15_13TeV', stage = 'merge', derivation = 'DAOD_EXOT5'):
    # list of mcids from file
    mcids = [d.split('.')[1] for d in get_dsets_from_file(input_file)]
    mcids += new_mcids
    missing_dsets = []

    for mcid in mcids:
        found_dset = find_grid_dset(first, mcid, stage, derivation, update_tag)
        if not found_dset:
            missing_dsets += [first + '.' + mcid]
    return missing_dsets

if __name__ == '__main__':

    print_missing = False
    print '# use dset_list.py for all your dset listing needs'

    # list of old datasets to be updated
    input_file = '../../data/samples/EXOT5_mc15b_bkg.txt'

    # tag to update to
    update_tag = 'p2666'

    # if all mcids needed are in the input file, can simply use this function
    #make_list_from_file(input_file, update_tag)

    # otherwise, also get info from additional mcids
    new_mcids = []
    new_mcids += [str(i) for i in range(361039,361062+1)] # single photon samples
    new_mcids += [str(i) for i in range(361020,361032+1)] # multijet

    # need to manually specify other tags
    missing_dsets = make_list_from_mcids(input_file, update_tag, new_mcids, first = 'mc15_13TeV', stage = 'merge', derivation = 'DAOD_EXOT5')
    
    if print_missing:
        print '#################'
        gen_tag = 'merge.AOD.*r7676' 
        for dset in missing_dsets:
            query = dset + '.*' + gen_tag 
            grid_dsets = [str(d) for d in get_dsets_from_query(query).keys()]

            # purge _a### tags
            grid_dsets = [d for d in grid_dsets if 'a' not in d.split('.')[5]]
            print 
            if len(grid_dsets) == 0:
                print '#', dset, "missing!"
                continue

            if len(grid_dsets) > 1: print 'vvvvvvvvvvvvvvvvv'
            for g in grid_dsets: print g
            if len(grid_dsets) > 1: print '^^^^^^^^^^^^^^^^^'
