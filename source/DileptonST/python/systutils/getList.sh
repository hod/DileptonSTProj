#!/bin/bash

if [ $# -eq 0 ]
then
  echo "Usage: $0 <run_name>"
else
  LINK_DIR=./links
  TRUTH1_DIR=/n/atlasfs/atlasdata/vippolit/mg/output_jan20_syst_2
  JOBNAME="$1"

  rm -rf ${LINK_DIR}
  mkdir -p ${LINK_DIR}


  for file in ${TRUTH1_DIR}/truth1*
  do
    basename=${file##*/}
    filenoext=${basename%.*}
    samplename=${filenoext/#truth1_/}
#   echo "Sample $samplename found"
    mkdir ${LINK_DIR}/${samplename}
    ln -s $file ${LINK_DIR}/${samplename}
  done

 python DileptonST/python/monojetSubmit.py -a DileptonNTUPMakerTruth -w slurm -d ${LINK_DIR} -n $1
fi
