#ifndef __Analysis_OutputTau__
#define __Analysis_OutputTau__

// EDM's include(s):
#include <xAODTau/TauJet.h>

// Local include(s):
#include <DileptonST/OutputObject.h>

namespace Analysis {

   class OutputTau : public OutputObject {

   private:

      std::vector<Float_t> pt;
      std::vector<Float_t> eta;
      std::vector<Float_t> phi;

      std::vector<Int_t> idtool_pass_loose;
      std::vector<Int_t> idtool_pass_medium;
      std::vector<Int_t> idtool_pass_tight;

      Int_t tau_multiplicity;
      Int_t medium_multiplicity;
      Int_t tight_multiplicity;

   public:

      OutputTau(bool doTrim = false);
      ~OutputTau() {};

      void reset();
      void attachToTree(TTree *tree);
      void add(const xAOD::TauJet &input);
   };
}

#endif // __Analysis_OutputTau__
