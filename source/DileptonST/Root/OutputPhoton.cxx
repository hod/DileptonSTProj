#include "DileptonST/OutputPhoton.h"

#include <TTree.h>
#include <xAODEgamma/EgammaxAODHelpers.h>

Analysis::OutputPhoton::OutputPhoton(Bool_t doTrim) : Analysis::OutputObject::OutputObject(doTrim)
{
   reset();
}

Analysis::OutputPhoton::~OutputPhoton()
{
}

void Analysis::OutputPhoton::reset()
{

   SF.clear();
   SF_iso.clear();
   pt.clear();
   eta.clear();
   phi.clear();
   m.clear();
   ptcone20.clear();
   ptvarcone20.clear();
   etcone20.clear();
   topoetcone20.clear();
   ptcone30.clear();
   ptvarcone30.clear();
   etcone30.clear();
   topoetcone30.clear();
   ptcone40.clear();
   ptvarcone40.clear();
   etcone40.clear();
   topoetcone40.clear();
   OQ.clear();
   Rphi.clear();
   weta2.clear();
   fracs1.clear();
   weta1.clear();
   emaxs1.clear();
   f1.clear();
   Reta.clear();
   wtots1.clear();
   Eratio.clear();
   Rhad.clear();
   Rhad1.clear();
   e277.clear();
   deltae.clear();
   author.clear();
   isConv.clear();
   /*
   time_cl.clear();
   time_maxEcell.clear();
   truth_pt.clear();
   truth_eta.clear();
   truth_phi.clear();
   truth_E.clear();
   truth_matched.clear();
   truth_mothertype.clear();
   truth_status.clear();
   truth_type.clear();
   truth_typebkg.clear();
   truth_origin.clear();
   truth_originbkg.clear();
   */
   isTight.clear();
   isEM.clear();
   isotool_pass_fixedcuttightcaloonly.clear();
   isotool_pass_fixedcuttight.clear();
   isotool_pass_fixedcutloose.clear();
   met_nomuon_dphi.clear();
   met_wmuon_dphi.clear();
   met_nophoton_dphi.clear();

   return;
}

void Analysis::OutputPhoton::attachToTree(TTree *tree)
{
   const TString prefix = name() + "_";

   if (write()) {
      tree->Branch(prefix + "pt", &pt);
      tree->Branch(prefix + "eta", &eta);
      tree->Branch(prefix + "phi", &phi);
      tree->Branch(prefix + "SF", &SF);
      tree->Branch(prefix + "SF_iso", &SF_iso);
      tree->Branch(prefix + "isotool_pass_fixedcuttightcaloonly", &isotool_pass_fixedcuttightcaloonly);
      tree->Branch(prefix + "isotool_pass_fixedcuttight", &isotool_pass_fixedcuttight);
      tree->Branch(prefix + "isotool_pass_fixedcutloose", &isotool_pass_fixedcutloose);

      if (!doTrim()) {

         tree->Branch(prefix + "m", &m);
         tree->Branch(prefix + "ptcone20", &ptcone20);
         tree->Branch(prefix + "ptvarcone20", &ptvarcone20);
         tree->Branch(prefix + "etcone20", &etcone20);
         tree->Branch(prefix + "topoetcone20", &topoetcone20);
         tree->Branch(prefix + "ptcone30", &ptcone30);
         tree->Branch(prefix + "ptvarcone30", &ptvarcone30);
         tree->Branch(prefix + "etcone30", &etcone30);
         tree->Branch(prefix + "topoetcone30", &topoetcone30);
         tree->Branch(prefix + "ptcone40", &ptcone40);
         tree->Branch(prefix + "ptvarcone40", &ptvarcone40);
         tree->Branch(prefix + "etcone40", &etcone40);
         tree->Branch(prefix + "topoetcone40", &topoetcone40);
         tree->Branch(prefix + "isTight", &isTight);
         tree->Branch(prefix + "isEM", &isEM);
         tree->Branch(prefix + "OQ", &OQ);
         tree->Branch(prefix + "Rphi", &Rphi);
         tree->Branch(prefix + "weta2", &weta2);
         tree->Branch(prefix + "fracs1", &fracs1);
         tree->Branch(prefix + "weta1", &weta1);
         tree->Branch(prefix + "emaxs1", &emaxs1);
         tree->Branch(prefix + "f1", &f1);
         tree->Branch(prefix + "Reta", &Reta);
         tree->Branch(prefix + "wtots1", &wtots1);
         tree->Branch(prefix + "Eratio", &Eratio);
         tree->Branch(prefix + "Rhad", &Rhad);
         tree->Branch(prefix + "Rhad1", &Rhad1);
         tree->Branch(prefix + "e277", &e277);
         tree->Branch(prefix + "deltae", &DeltaE);
         tree->Branch(prefix + "author", &author);
         tree->Branch(prefix + "isConv", &isConv);
         /*
              tree->Branch(prefix + "time_cl", &time_cl);
              tree->Branch(prefix + "time_maxEcell", &time_maxEcell);
              tree->Branch(prefix + "truth_pt", &truth_pt);
              tree->Branch(prefix + "truth_eta", &truth_eta);
              tree->Branch(prefix + "truth_phi", &truth_phi);
              tree->Branch(prefix + "truth_E", &truth_E);
              tree->Branch(prefix + "truth_matched", &truth_matched);
              tree->Branch(prefix + "truth_mothertype", &truth_mothertype);
              tree->Branch(prefix + "truth_status", &truth_status);
              tree->Branch(prefix + "truth_type", &truth_type);
              tree->Branch(prefix + "truth_typebkg", &truth_typebkg);
              tree->Branch(prefix + "truth_origin", &truth_origin);
              tree->Branch(prefix + "truth_originbkg", &truth_originbkg);
         */

         //tree->Branch(prefix + "isotool_pass_loose", &isotool_pass_loose);
         tree->Branch(prefix + "met_nomuon_dphi", &met_nomuon_dphi);
         tree->Branch(prefix + "met_wmuon_dphi", &met_wmuon_dphi);
         tree->Branch(prefix + "met_nophoton_dphi", &met_nophoton_dphi);
      }
   }

   return;
}

void Analysis::OutputPhoton::add(const xAOD::Photon &input)
{

   author.push_back(input.author());
   isConv.push_back(xAOD::EgammaHelpers::isConvertedPhoton(&input));
   pt.push_back(input.pt());
   eta.push_back(input.eta());
   phi.push_back(input.phi());
   m.push_back(input.m());

   Rphi.push_back(input.showerShapeValue(xAOD::EgammaParameters::Rphi));
   weta2.push_back(input.showerShapeValue(xAOD::EgammaParameters::weta2));
   fracs1.push_back(input.showerShapeValue(xAOD::EgammaParameters::fracs1));
   weta1.push_back(input.showerShapeValue(xAOD::EgammaParameters::weta1));
   emaxs1.push_back(input.showerShapeValue(xAOD::EgammaParameters::emaxs1));
   f1.push_back(input.showerShapeValue(xAOD::EgammaParameters::f1));
   Reta.push_back(input.showerShapeValue(xAOD::EgammaParameters::Reta));
   wtots1.push_back(input.showerShapeValue(xAOD::EgammaParameters::wtots1));
   Eratio.push_back(input.showerShapeValue(xAOD::EgammaParameters::Eratio));
   Rhad.push_back(input.showerShapeValue(xAOD::EgammaParameters::Rhad));
   Rhad1.push_back(input.showerShapeValue(xAOD::EgammaParameters::Rhad1));
   e277.push_back(input.showerShapeValue(xAOD::EgammaParameters::e277));
   deltae.push_back(input.showerShapeValue(xAOD::EgammaParameters::DeltaE));

   static SG::AuxElement::ConstAccessor< uint32_t > acc_OQ("OQ");
   //   OQ.push_back(input.auxdata< uint32_t >("OQ"));
   OQ.push_back(acc_OQ(input));
   eta2.push_back(input.caloCluster()->etaBE(2));
   /*
   time_cl.push_back(-9999); // TODO: implement or remove
   time_maxEcell.push_back(-9999);  // TODO: implement or remove
   truth_pt.push_back(-9999);  // TODO: implement or remove
   truth_eta.push_back(-9999);  // TODO: implement or remove
   truth_phi.push_back(-9999);  // TODO: implement or remove
   truth_E.push_back(-9999);  // TODO: implement or remove
   */

   // isolation variables
   Float_t tmp_ptcone20(-9999);
   Float_t tmp_ptvarcone20(-9999);
   Float_t tmp_etcone20(-9999);
   Float_t tmp_topoetcone20(-9999);
   Float_t tmp_ptcone30(-9999);
   Float_t tmp_ptvarcone30(-9999);
   Float_t tmp_etcone30(-9999);
   Float_t tmp_topoetcone30(-9999);
   Float_t tmp_ptcone40(-9999);
   Float_t tmp_ptvarcone40(-9999);
   Float_t tmp_etcone40(-9999);
   Float_t tmp_topoetcone40(-9999);
   input.isolationValue(tmp_ptcone20, xAOD::Iso::IsolationType::ptcone20);
   input.isolationValue(tmp_ptvarcone20, xAOD::Iso::IsolationType::ptvarcone20);
   input.isolationValue(tmp_etcone20, xAOD::Iso::IsolationType::etcone20);
   input.isolationValue(tmp_topoetcone20, xAOD::Iso::IsolationType::topoetcone20);
   input.isolationValue(tmp_ptcone30, xAOD::Iso::IsolationType::ptcone30);
   input.isolationValue(tmp_ptvarcone30, xAOD::Iso::IsolationType::ptvarcone30);
   input.isolationValue(tmp_etcone30, xAOD::Iso::IsolationType::etcone30);
   input.isolationValue(tmp_topoetcone30, xAOD::Iso::IsolationType::topoetcone30);
   input.isolationValue(tmp_ptcone40, xAOD::Iso::IsolationType::ptcone40);
   input.isolationValue(tmp_ptvarcone40, xAOD::Iso::IsolationType::ptvarcone40);
   input.isolationValue(tmp_etcone40, xAOD::Iso::IsolationType::etcone40);
   input.isolationValue(tmp_topoetcone40, xAOD::Iso::IsolationType::topoetcone40);
   ptcone20.push_back(tmp_ptcone20);
   ptvarcone20.push_back(tmp_ptvarcone20);
   etcone20.push_back(tmp_etcone20);
   topoetcone20.push_back(tmp_topoetcone20);
   ptcone30.push_back(tmp_ptcone30);
   ptvarcone30.push_back(tmp_ptvarcone30);
   etcone30.push_back(tmp_etcone30);
   topoetcone30.push_back(tmp_topoetcone30);
   ptcone40.push_back(tmp_ptcone40);
   ptvarcone40.push_back(tmp_ptvarcone40);
   etcone40.push_back(tmp_etcone40);
   topoetcone40.push_back(tmp_topoetcone40);

   // decorations we define in our analysis code
   static SG::AuxElement::ConstAccessor<char> acc_pass_fixedcuttightcaloonly("pass_fixedcuttightcaloonly");
   static SG::AuxElement::ConstAccessor<char> acc_pass_fixedcuttight("pass_fixedcuttight");
   static SG::AuxElement::ConstAccessor<char> acc_pass_fixedcutloose("pass_fixedcutloose");
   try {
      isotool_pass_fixedcuttightcaloonly.push_back(acc_pass_fixedcuttightcaloonly(input));
      isotool_pass_fixedcuttight.push_back(acc_pass_fixedcuttight(input));
      isotool_pass_fixedcutloose.push_back(acc_pass_fixedcutloose(input));
   } catch (SG::ExcBadAuxVar) {
      isotool_pass_fixedcuttightcaloonly.push_back(-9999);
      isotool_pass_fixedcuttight.push_back(-9999);
      isotool_pass_fixedcutloose.push_back(-9999);
   }

   static SG::AuxElement::ConstAccessor<char> acc_ph_isTight("ph_isTight");
   try {
      isTight.push_back(acc_ph_isTight(input));
   } catch (SG::ExcBadAuxVar) {
      isTight.push_back(9999);
   }

   static SG::AuxElement::ConstAccessor<unsigned int> acc_ph_isEM("ph_isEM");
   try {
      isEM.push_back(acc_ph_isEM(input));
   } catch (SG::ExcBadAuxVar) {
      isEM.push_back(9999);
   }

   // dphi's
   static SG::AuxElement::ConstAccessor<float> acc_new_met_nomuon_dphi("new_met_nomuon_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_wmuon_dphi("new_met_wmuon_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_nophoton_dphi("new_met_nophoton_dphi");
   try {
      met_nomuon_dphi.push_back(acc_new_met_nomuon_dphi(input));
      met_nophoton_dphi.push_back(acc_new_met_nophoton_dphi(input));
      met_wmuon_dphi.push_back(acc_new_met_wmuon_dphi(input));
   } catch (SG::ExcBadAuxVar) {
      met_nomuon_dphi.push_back(-9999);
      met_wmuon_dphi.push_back(-9999);
      met_nophoton_dphi.push_back(-9999);
   }


   // decorations from SUSYTools
   static SG::AuxElement::ConstAccessor<float> acc_ph_SF("ph_SF");
   static SG::AuxElement::ConstAccessor<float> acc_ph_iso_SF("ph_iso_SF");
   Float_t tmp_ph_SF(-9999);
   Float_t tmp_ph_iso_SF(-9999);
   try {
      tmp_ph_SF = acc_ph_SF(input);
      tmp_ph_iso_SF = acc_ph_iso_SF(input);
   } catch (SG::ExcBadAuxVar) {
      tmp_ph_SF = 1.0;
      tmp_ph_iso_SF = 1.0;
   }
   SF.push_back(tmp_ph_SF);
   SF_iso.push_back(tmp_ph_iso_SF);

   return;
}
