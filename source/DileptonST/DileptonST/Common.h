#ifndef __DileptonST_Common_h__
#define __DileptonST_Common_h__

#include <Rtypes.h>

namespace xAOD {
   class IParticle;
}

namespace DileptonST {
// convenience methods (not class members)
   Bool_t comparePt(const xAOD::IParticle *a, const xAOD::IParticle *b);
}

//#include <AsgTools/AsgTools/AnaToolHandle.h
#define SET_DUAL_TOOL( TOOLHANDLE, TOOLTYPE, TOOLNAME )                \
   ASG_SET_ANA_TOOL_TYPE(TOOLHANDLE, TOOLTYPE);                        \
   TOOLHANDLE.setName(TOOLNAME);

#endif
