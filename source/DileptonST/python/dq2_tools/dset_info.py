# tools to obtain info about datasets
# Valerio Ippolito - Harvard University

#import rucio.client 
#rucioClient = rucio.client.Client() 
#for did in rucioClient.list_dids(scope='data15_13TeV', filters={'name':'data15_13TeV.*.physics_Main.merge.DAOD_EGAM7.*p2425*'}, type='container'): 
#  print did 


import pyAMI.client
import pyAMI.atlas.api
import dq2.clientapi.DQ2 as theDQ2module

def get_dsets_from_query(query):
  theDQ2api = theDQ2module.DQ2()

  searchmetadata = {'name' : query}
  return theDQ2api.listDatasets2(metaDataAttributes=searchmetadata, long=False, all=False, p=None, rpp=None)

def get_all_info(client, dset):
  query_result = pyAMI.atlas.api.get_dataset_info(client, dset)
  the_dict = query_result[0]
  # example for the_dict:
  #OrderedDict([(u'logicalDatasetName', u'mc14_13TeV.167740.Sherpa_CT10_WenuMassiveCBPt0_BFilter.merge.AOD.e2822_s1982_s2008_r5787_r5853'), (u'nFiles', u'100'), (u'totalEvents', u'497000'), (u'totalSize', u'313515964694'), (u'dataType', u'AOD'), (u'prodsysStatus', u'ALL EVENTS AVAILABLE'), (u'ECMEnergy', u'13000'), (u'PDF', u'ct10'), (u'version', u'e2822_s1982_s2008_r5787_r5853'), (u'crossSection', u'18.795'), (u'TransformationPackage', u'19.1.4.1'), (u'datasetNumber', u'167740'), (u'generatorName', u'Sherpa'), (u'geometryVersion', u'ATLAS-R2-2015-01-01-00'), (u'triggerConfig', u'NULL'), (u'conditionsTag', u'OFLCOND-RUN12-SDR-14'), (u'generatorTune', u'CT10'), (u'approx_crossSection', u'1.8795E+01'), (u'approx_GenFiltEff', u'1.7789E-02'), (u'autoConfiguration', u"['everything']"), (u'postInclude', u"['RecJobTransforms/UseFrontier.py']"), (u'added_comment', u'NULL'), (u'keyword', u'e, heavyquark, leptonic, w'), (u'prodsysIdentifier_0', u'1598118'), (u'taskStatus_0', u'FINISHED'), (u'TIDState_0', u'added'), (u'task_lastModified_0', u'2014-10-14 04:14:29')])
  res = {}
  res['totalEvents'] = the_dict['totalEvents']
  res['sqrts_in_GeV'] = the_dict['ECMEnergy'] # GeV
  res['pdf'] = the_dict['PDF']
  res['tags'] = the_dict['version']
  res['xsec_in_nb'] = the_dict['crossSection'] # nb
  res['run'] = the_dict['datasetNumber']
  try:
    res['eff'] = the_dict['GenFiltEff_mean']
  except:
    res['eff'] = the_dict['approx_GenFiltEff']
  res['name'] = the_dict['logicalDatasetName']
  return res


def get_xsec_and_filter(query):
  if 'EVNT' not in query:
    raise RuntimeError('Query %s does not refer to EVNT dataset, AMI results would be WRONG' % query)

  client = pyAMI.client.Client('atlas')

  dset_list_raw = get_dsets_from_query(query)
  dset_list = set(map(lambda x: x.split(':')[-1], dset_list_raw)) # remove duplicates starting with XXX:XXX (new rucio)


  for dset in dset_list:
    info = get_all_info(client, dset)
    #
    # uncomment to get old-style printout (i.e. C++ map)
    #print '// sample {run} is {name}'.format(**info)
    #print 'xsec[{run}] = {xsec_in_nb} * Units::nb * {eff}; // xsec times filter eff'.format(**info)
    #print '//generated_ami[{run}] = {totalEvents}; // total events (WARNING: should be replaced!)'.format(**info)
    #
    xsec_in_pb = float(info['xsec_in_nb'])*1000
    nick = info['name'].split('.')[2] # take only name
    print '{run} {name} {xsec_in_pb} 1.0 {eff} 1.0'.format(run=info['run'], name=nick, eff=info['eff'], xsec_in_pb=xsec_in_pb)

if __name__ == '__main__':
 #import os
 #dsets = filter(lambda x: x.startswith('mc15'), open('%s/data/DileptonST/samples/mc15_sig_xAOD.txt' % os.environ['TestArea']).readlines())
  dsets = [

'mc15_13TeV.301400.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_1_10_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301401.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_1_20_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301402.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_1_50_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301403.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_1_100_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301404.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_1_200_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301405.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_1_300_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301406.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_1_500_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301407.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_1_1000_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301408.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_1_2000_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301409.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_1_10000_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301410.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_10_10_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301411.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_50_10_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301412.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_150_10_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301413.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_500_10_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301414.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_1000_10_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301415.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_10_15_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301416.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_10_50_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301417.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_10_100_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301418.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_50_50_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301419.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_50_95_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301420.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_50_200_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301421.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_50_300_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301422.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_150_200_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301423.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_150_295_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301424.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_150_500_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301425.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_150_1000_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301426.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_500_500_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301427.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_500_995_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301428.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_500_2000_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301429.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_500_10000_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301430.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_1000_1000_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301431.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_1000_1995_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301432.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_1000_10000_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301433.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_10_10000_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301434.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_50_10000_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301435.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_150_10000_gq0p25.evgen.EVNT.e4241',
'mc15_13TeV.301436.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_1_10_g1.evgen.EVNT.e4241',
'mc15_13TeV.301437.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_1_20_g1.evgen.EVNT.e4241',
'mc15_13TeV.301438.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_1_50_g1.evgen.EVNT.e4241',
'mc15_13TeV.301439.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_1_100_g1.evgen.EVNT.e4241',
'mc15_13TeV.301440.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_1_200_g1.evgen.EVNT.e4241',
'mc15_13TeV.301441.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_1_300_g1.evgen.EVNT.e4241',
'mc15_13TeV.301442.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_1_500_g1.evgen.EVNT.e4241',
'mc15_13TeV.301443.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_1_1000_g1.evgen.EVNT.e4241',
'mc15_13TeV.301444.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_1_10000_g1.evgen.EVNT.e4241',
'mc15_13TeV.301445.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_10_10_g1.evgen.EVNT.e4241',
'mc15_13TeV.301446.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_50_10_g1.evgen.EVNT.e4241',
'mc15_13TeV.301447.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_150_10_g1.evgen.EVNT.e4241',
'mc15_13TeV.301448.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_500_10_g1.evgen.EVNT.e4241',
'mc15_13TeV.301449.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_1000_10_g1.evgen.EVNT.e4241',
'mc15_13TeV.301450.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_10_15_g1.evgen.EVNT.e4241',
'mc15_13TeV.301451.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_10_50_g1.evgen.EVNT.e4241',
'mc15_13TeV.301452.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_10_100_g1.evgen.EVNT.e4241',
'mc15_13TeV.301453.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_50_50_g1.evgen.EVNT.e4241',
'mc15_13TeV.301454.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_50_95_g1.evgen.EVNT.e4241',
'mc15_13TeV.301455.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_50_200_g1.evgen.EVNT.e4241',
'mc15_13TeV.301456.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_50_300_g1.evgen.EVNT.e4241',
'mc15_13TeV.301457.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_150_200_g1.evgen.EVNT.e4241',
'mc15_13TeV.301458.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_150_295_g1.evgen.EVNT.e4241',
'mc15_13TeV.301459.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_150_500_g1.evgen.EVNT.e4241',
'mc15_13TeV.301460.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_150_1000_g1.evgen.EVNT.e4241',
'mc15_13TeV.301461.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_500_500_g1.evgen.EVNT.e4241',
'mc15_13TeV.301462.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_500_995_g1.evgen.EVNT.e4241',
'mc15_13TeV.301463.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_500_10000_g1.evgen.EVNT.e4241',
'mc15_13TeV.301464.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_1000_1000_g1.evgen.EVNT.e4241',
'mc15_13TeV.301465.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_1000_10000_g1.evgen.EVNT.e4241',
'mc15_13TeV.301466.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_10_10000_g1.evgen.EVNT.e4241',
'mc15_13TeV.301467.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_50_10000_g1.evgen.EVNT.e4241',
'mc15_13TeV.301468.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_150_10000_g1.evgen.EVNT.e4241',
'mc15_13TeV.301469.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_150_1000_g0p1.evgen.EVNT.e4241',
'mc15_13TeV.301470.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_150_1000_g0p25.evgen.EVNT.e4241',
'mc15_13TeV.301471.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_150_1000_g0p5.evgen.EVNT.e4241',
'mc15_13TeV.301472.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_150_1000_g0p75.evgen.EVNT.e4241',
'mc15_13TeV.301473.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_150_1000_g1.evgen.EVNT.e4241',
'mc15_13TeV.301474.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_150_1000_g1p25.evgen.EVNT.e4241',
'mc15_13TeV.301475.PowhegPythia8EvtGen_A14NNPDF23LO_DMA_150_1000_g1p5.evgen.EVNT.e4241',
'mc15_13TeV.301476.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_50_300_g0p1.evgen.EVNT.e4241',
'mc15_13TeV.301477.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_50_300_g2.evgen.EVNT.e4241',
'mc15_13TeV.301478.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_50_300_g3.evgen.EVNT.e4241',
'mc15_13TeV.301479.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_50_300_g4.evgen.EVNT.e4241',
'mc15_13TeV.301480.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_50_300_g5.evgen.EVNT.e4241',
'mc15_13TeV.301481.PowhegPythia8EvtGen_A14NNPDF23LO_DMP_tloop_50_300_g6.evgen.EVNT.e4241',
# DC14
#'mc14_13TeV.191040.MadGraphPythia_AUET2BMSTW2008LO_D5_400_1000_MET100.evgen.EVNT.e3362',
#'mc14_13TeV.191041.MadGraphPythia_AUET2BMSTW2008LO_D5_400_1000_MET300.evgen.EVNT.e3502',
#'mc14_13TeV.191042.MadGraphPythia_AUET2BMSTW2008LO_D5_400_1000_MET500.evgen.EVNT.e3502',
#'mc14_13TeV.191043.MadGraphPythia_AUET2BMSTW2008LO_D5_50_1000_MET100.evgen.EVNT.e3362',
#'mc14_13TeV.191044.MadGraphPythia_AUET2BMSTW2008LO_D5_50_1000_MET300.evgen.EVNT.e3502',
#'mc14_13TeV.191045.MadGraphPythia_AUET2BMSTW2008LO_D5_50_1000_MET500.evgen.EVNT.e3502',
#'mc14_13TeV.191066.MadGraphPythia_AUET2BMSTW2008LO_dmV_400_5000_W8p_MET500.evgen.EVNT.e3502',
#'mc14_13TeV.191068.MadGraphPythia_AUET2BMSTW2008LO_dmV_400_5000_W8p_MET300.evgen.EVNT.e3502',
#'mc14_13TeV.191067.MadGraphPythia_AUET2BMSTW2008LO_dmV_400_5000_W8p_MET100.evgen.EVNT.3362',
#'mc14_13TeV.191048.MadGraphPythia_AUET2BMSTW2008LO_dmV_400_1000_W8p_MET500.evgen.EVNT.e3502',
#'mc14_13TeV.191050.MadGraphPythia_AUET2BMSTW2008LO_dmV_400_1000_W8p_MET300.evgen.EVNT.e3502',
#'mc14_13TeV.191049.MadGraphPythia_AUET2BMSTW2008LO_dmV_400_1000_W8p_MET100.evgen.EVNT.e3362',
#'mc14_13TeV.191061.MadGraphPythia_AUET2BMSTW2008LO_dmV_400_200_W8p_MET100.evgen.EVNT.e3362',
#'mc14_13TeV.191062.MadGraphPythia_AUET2BMSTW2008LO_dmV_400_200_W8p_MET300.evgen.EVNT.e3502',
#'mc14_13TeV.191063.MadGraphPythia_AUET2BMSTW2008LO_dmV_400_200_W8p_MET500.evgen.EVNT.e3502',
#'mc14_13TeV.191097.MadGraphPythia_AUET2BMSTW2008LO_dmV50_5000_W8p_MET500.evgen.EVNT.e3502',
#'mc14_13TeV.191096.MadGraphPythia_AUET2BMSTW2008LO_dmV50_5000_W8p_MET300.evgen.EVNT.e3502',
#'mc14_13TeV.191095.MadGraphPythia_AUET2BMSTW2008LO_dmV50_5000_W8p_MET100.evgen.EVNT.e3362',
#'mc14_13TeV.191081.MadGraphPythia_AUET2BMSTW2008LO_dmV50_1000_W8p_MET500.evgen.EVNT.e3502',
#'mc14_13TeV.191080.MadGraphPythia_AUET2BMSTW2008LO_dmV50_1000_W8p_MET300.evgen.EVNT.e3502',
#'mc14_13TeV.191079.MadGraphPythia_AUET2BMSTW2008LO_dmV50_1000_W8p_MET100.evgen.EVNT.e3362',
#'mc14_13TeV.191090.MadGraphPythia_AUET2BMSTW2008LO_dmV50_200_W8p_MET100.evgen.EVNT.e3502',
]

  # alternatve: specify a search path as in
  #q = 'mc12_8TeV.*_dmA*NTUP_COMMON*/'


  for dset in dsets:
    get_xsec_and_filter(dset)
