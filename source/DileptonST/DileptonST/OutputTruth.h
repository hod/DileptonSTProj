#ifndef __Analysis_OutputTruth__
#define __Analysis_OutputTruth__

#include <Rtypes.h>

class TTree;

namespace Analysis {
   class OutputTruth {
   public:
      ////
      // NOTE: variables must be sorted with inverse sizeof() criteria
      //
      //   >>> this explains why integers are AT THE BOTTOM <<<
      //
      // NOTE: please do _not_ use for _any_ reason boolean types!
      //
      //   >>> use Int_t instead
      //
      ////
      //////////
      // event variables
      //////////
      ULong64_t pu_hash;
      Float_t averageIntPerXing;
      Float_t pu_weight;
      Float_t mconly_weight;
      std::vector<Float_t> mconly_weights;
      Float_t overlap_weight;
      Float_t pdf_x1;
      Float_t pdf_x2;
      Float_t pdf_pdf1;
      Float_t pdf_pdf2;
      Float_t pdf_scale;
      //////////
      // composite observables (involve >1 objects)
      //////////
      Float_t shatR;
      Float_t gaminvR;
      Float_t gaminvRp1;
      Float_t dphi_BETA_R;
      Float_t dphi_J1_J2_R;
      Float_t gamma_Rp1;
      Float_t costhetaR;
      Float_t dphi_R_Rp1;
      Float_t mdeltaR;
      Float_t cosptR;
      Float_t costhetaRp1;
      Float_t munu_mT;
      Float_t enu_mT;
      Float_t jj_m;
      Float_t mumu_m;
      Float_t mumu_pt;
      Float_t mumu_eta;
      Float_t mumu_phi;
      Float_t ee_m;
      Float_t ee_pt;
      Float_t ee_eta;
      Float_t ee_phi;
      Float_t truth_dm_pt;
      Float_t truth_dm_q;
      Float_t truth_dmjet_q;
      //////////
      // met
      //////////
      Float_t met_truth_et;
      Float_t met_truth_etx;
      Float_t met_truth_ety;
      Float_t met_truth_sumet;
      //////////
      // muons
      //////////
      std::vector<Float_t> mu_charge;
      std::vector<Float_t> mu_pt;
      std::vector<Float_t> mu_eta;
      std::vector<Float_t> mu_phi;
      std::vector<Float_t> mu_m;
      std::vector<Float_t> mu_met_truth_dphi;
      //////////
      // vetoed electrons
      //////////
      std::vector<Float_t> el_charge;
      std::vector<Float_t> el_pt;
      std::vector<Float_t> el_eta;
      std::vector<Float_t> el_phi;
      std::vector<Float_t> el_m;
      std::vector<Float_t> el_met_truth_dphi;
      //////////
      // jets
      //////////
      std::vector<Float_t> jet_pt;
      std::vector<Float_t> jet_eta;
      std::vector<Float_t> jet_phi;
      std::vector<Float_t> jet_m;
      std::vector<Float_t> jet_met_truth_dphi;
      std::vector<Int_t> jet_PartonTruthLabelID;
      std::vector<Int_t> jet_ConeTruthLabelID;
      std::vector<Float_t> jet_TruthLabelDeltaR_B;
      std::vector<Float_t> jet_TruthLabelDeltaR_C;
      std::vector<Float_t> jet_TruthLabelDeltaR_T;
      //////////
      // hemispheres, or mega-jets
      //////////
      Float_t hem1_pt;
      Float_t hem1_eta;
      Float_t hem1_phi;
      Float_t hem1_m;
      //
      Float_t hem2_pt;
      Float_t hem2_eta;
      Float_t hem2_phi;
      Float_t hem2_m;

      //////////
      // SR/CR flags
      //////////
      Int_t last; // referred to SR
      //////////
      // event variables
      //////////
      Int_t run;
      Int_t event;
      Int_t lbn;
      Int_t bcid;
      Int_t n_jet; // with good objects
      Int_t n_el; // with good objects
      Int_t n_mu; // with good objects
      Int_t pdf_id1;
      Int_t pdf_id2;
   public:
      OutputTruth();
      ~OutputTruth();
      void reset();
      void attachToTree(TTree *tree, TString prefix);
   };
}

#endif
