#!/bin/sh

### check the release number here:
### https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisRelease

setupATLAS

mkdir -p source build run

cd source

#asetup AnalysisBase,2.6.4,here
#asetup 21.2,AnalysisBase,latest
#asetup 21.2.4,AnalysisBase
asetup 21.2,AnalysisBase,latest

voms-proxy-init -valid 96:0 -voms atlas
lsetup panda
#lsetup rucio

cd -
