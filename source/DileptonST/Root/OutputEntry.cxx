#include "DileptonST/OutputEntry.h"

#include <TTree.h>
#include <stdexcept>

Analysis::OutputEntry::OutputEntry() : m_attachedTree(nullptr)
{
   reset();
}

Analysis::OutputEntry::~OutputEntry()
{
}

void Analysis::OutputEntry::reset()
{
   evt.reset();

   for (auto &kv : met) kv.second.reset();
   for (auto &kv : mu) kv.second.reset();
   for (auto &kv : el) kv.second.reset();
   for (auto &kv : jet) kv.second.reset();
   for (auto &kv : ph) kv.second.reset();
   for (auto &kv : hem) kv.second.reset();
   for (auto &kv : tau) kv.second.reset();
}

void Analysis::OutputEntry::setDoTrim(Bool_t val)
{
   m_doTrim = val;
}

void Analysis::OutputEntry::attachToTree(TTree *tree)
{
   // first, set trimming if appropriate
   evt.setDoTrim(m_doTrim);
   for (auto &kv : met) kv.second.setDoTrim(m_doTrim);
   for (auto &kv : mu)  kv.second.setDoTrim(m_doTrim);
   for (auto &kv : el)  kv.second.setDoTrim(m_doTrim);
   for (auto &kv : jet) kv.second.setDoTrim(m_doTrim);
   for (auto &kv : ph) kv.second.setDoTrim(m_doTrim);
   for (auto &kv : hem) kv.second.setDoTrim(m_doTrim);
   for (auto &kv : tau) kv.second.setDoTrim(m_doTrim);

   // then, attach to TTree
   if (tree != m_attachedTree) {
      m_attachedTree = tree; // do NOT take ownership

      evt.attachToTree(tree);

      for (auto &kv : met) kv.second.attachToTree(tree);
      for (auto &kv : mu)  kv.second.attachToTree(tree);
      for (auto &kv : el)  kv.second.attachToTree(tree);
      for (auto &kv : jet) kv.second.attachToTree(tree);
      for (auto &kv : ph)  kv.second.attachToTree(tree);
      for (auto &kv : hem) kv.second.attachToTree(tree);
      for (auto &kv : tau) kv.second.attachToTree(tree);
   }
}

void Analysis::OutputEntry::save()
{
   if (!m_attachedTree) throw std::runtime_error("Output candidate not attached to any TTree");
   m_attachedTree->Fill();
}
