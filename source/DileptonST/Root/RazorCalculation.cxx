////////////////////////////////
//
// SUPER-RAZOR VARIABLES CALCULATION
//
// Code written by Christopher Rogan <crogan@cern.ch>, 27-04-14
// Adapted from paper PRD 89, 055020 (http://arxiv.org/abs/1310.4827) written by
// Matthew R. Buckley, Joseph D. Lykken, Christopher Rogan, Maria Spiropulu
//
////////////////////////////////
//
#include <DileptonST/RazorCalculation.h>
//
//
/////////////
//
// LAB frame
//
/////////////

//
// Reconstructed mega-jets and missing transverse energy (from somewhere else...)
// put them in the variables below:

#include <TLorentzVector.h>
#include <TVector2.h>

RazorAnalysis::RazorVariables RazorAnalysis::GetRazorVariables(const TLorentzVector &jet1, const TLorentzVector &jet2, const TVector2 &met)
{
/////////////
//
// LAB frame
//
/////////////

//
// Reconstructed mega-jets and missing transverse energy (from somewhere else...)
// put them in the variables below:
   TLorentzVector J1(jet1), J2(jet2);
   TVector3 MET(met.X(), met.Y(), 0); // empty Z (TODO: rewrite code below to be more efficient and use TVector3 instead of this)
//
//

//
// from these inputs the relevant variables will be calculated - the proposed
// triggers will be based on shatR, gaminvR and cosptR (defined below)
//

   TVector3 vBETA_z = (1. / (J1.E() + J2.E())) * (J1 + J2).Vect();
   vBETA_z.SetX(0.0);
   vBETA_z.SetY(0.0);

//transformation from lab frame to approximate rest frame along beam-axis
   J1.Boost(-vBETA_z);
   J2.Boost(-vBETA_z);

   TVector3 pT_CM = (J1 + J2).Vect() + MET;
   pT_CM.SetZ(0.0); //should be redundant...

   Double_t Minv2 = (J1 + J2).M2() - 4.*J1.M() * J2.M();
   Double_t Einv = sqrt(MET.Mag2() + Minv2);

//****************************************
   Double_t shatR = sqrt(((J1 + J2).E() + Einv) * ((J1 + J2).E() + Einv) - pT_CM.Mag2());

   TVector3 vBETA_R = (1. / sqrt(pT_CM.Mag2() + shatR * shatR)) * pT_CM;

//transformation from lab frame to R frame
   J1.Boost(-vBETA_R);
   J2.Boost(-vBETA_R);

/////////////
//
// R-frame
//
/////////////

//****************************************
   Double_t gaminvR = sqrt(1. - vBETA_R.Mag2());

//****************************************
   Double_t dphi_BETA_R = fabs(((J1 + J2).Vect()).DeltaPhi(vBETA_R));

//****************************************
   Double_t dphi_J1_J2_R = fabs(J1.Vect().DeltaPhi(J2.Vect()));

   TVector3 vBETA_Rp1 = (1. / (J1.E() + J2.E())) * (J1.Vect() - J2.Vect());

//****************************************
   Double_t gamma_Rp1 = 1. / sqrt(1. - vBETA_Rp1.Mag2());

//****************************************
   Double_t gaminvRp1 = sqrt(pow(J1.P() + J2.P(), 2.) - (J1.Vect() - J2.Vect()).Mag2()) / (J1.P() + J2.P());

//****************************************
   Double_t costhetaR = vBETA_Rp1.Unit().Dot(vBETA_R.Unit());

//****************************************
   Double_t dphi_R_Rp1 = fabs(vBETA_R.DeltaPhi(vBETA_Rp1));

//transformation from R frame to R+1 frames
   J1.Boost(-vBETA_Rp1);
   J2.Boost(vBETA_Rp1);

//////////////
//
// R+1-frames
//
//////////////

//****************************************
   Double_t mdeltaR = J1.P() + J2.P();

//****************************************
   Double_t cosptR = pT_CM.Mag() / sqrt(pT_CM.Mag2() + shatR * gaminvRp1 / 2.);

//****************************************
   Double_t costhetaRp1 = vBETA_Rp1.Unit().Dot(J1.Vect().Unit());



// output
   RazorVariables output;
   output.shatR = shatR;
   output.gaminvR = gaminvR;
   output.gaminvRp1 = gaminvRp1;
   output.dphi_BETA_R = dphi_BETA_R;
   output.dphi_J1_J2_R = dphi_J1_J2_R;
   output.gamma_Rp1 = gamma_Rp1;
   output.costhetaR = costhetaR;
   output.dphi_R_Rp1 = dphi_R_Rp1;
   output.mdeltaR = mdeltaR;
   output.cosptR = cosptR;
   output.costhetaRp1 = costhetaRp1;

   return output;
}
