#ifndef __DileptonDC14_Enums_h__
#define __DileptonDC14_Enums_h__

namespace DileptonCuts {
   typedef enum {
      processed,
      GRL,
      cleaning,
      vertex,
      MET_cleaning,
   } CutID;
}

namespace DileptonMuonCuts {
   typedef enum {
      exists = 0,
      family,
      quality,
      eta,
      pt,
      MCP,
      isolated,
   } CutID;
}

namespace DileptonElectronCuts {
   typedef enum {
      exists = 0,
      family,
      isMedium,
      author,
      kinematics,
      OQ,
   } CutID;
}

#endif
