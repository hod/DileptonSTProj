#ifndef DileptonST_DileptonNTUPMaker_H
#define DileptonST_DileptonNTUPMaker_H

#include <memory>

#include <EventLoop/Algorithm.h>
#include <EventLoop/StatusCode.h>
#include <AsgTools/AnaToolHandle.h>

#include <ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h>
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"
#include <GoodRunsLists/GoodRunsListSelectionTool.h>
#include <IsolationSelection/IsolationSelectionTool.h>
#include <TauAnalysisTools/TauSelectionTool.h>
#include <SUSYTools/SUSYObjDef_xAOD.h>

#include <DileptonST/Enums.h>
#include <DileptonST/OutputEntry.h>
#include <DileptonST/CutFlowTool.h>
#include <DileptonST/ContentHolder.h>
#include <DileptonST/VJetsUncertaintyTool.h>

#include "LPXKfactorTool/LPXKfactorTool.h"

class DileptonNTUPMaker : public EL::Algorithm {
public:
   typedef enum {
      SR = 0,
      CR1mu,
      CR1mubveto,
      CR1mubtag,
      CR2mu,
      CR1e,
      CR1e_metnoel,
      CR2e,
      CR1ph,
      VR150 = 9,
      CR1mu150,
      CR2mu150,
      CR1e150,
      CR1e_metnoel150,
      CR2e150
   } SelectionCriteria;

#ifndef __CINT__
   // hide this part from dictionary generation
   std::unique_ptr<GoodRunsListSelectionTool> m_grl; //!

   std::unique_ptr<CP::IsolationSelectionTool> m_leptonIsolationLooseTrackOnly; //!
   std::unique_ptr<CP::IsolationSelectionTool> m_leptonIsolationLoose; //!
   std::unique_ptr<CP::IsolationSelectionTool> m_leptonIsolationGradient; //!
   std::unique_ptr<CP::IsolationSelectionTool> m_leptonIsolationGradientLoose; //!
   std::unique_ptr<CP::IsolationSelectionTool> m_leptonIsolationFixedCutTight; //!
   std::unique_ptr<CP::IsolationSelectionTool> m_leptonIsolationFixedCutTightTrackOnly; //!
   std::unique_ptr<CP::IsolationSelectionTool> m_photonIsolationFixedCutTightCaloOnly; //!
   std::unique_ptr<CP::IsolationSelectionTool> m_photonIsolationFixedCutTight; //!
   std::unique_ptr<CP::IsolationSelectionTool> m_photonIsolationFixedCutLoose; //!
   std::unique_ptr<AsgPhotonIsEMSelector> m_photonTightIsEMSelector; //!
   // asg::AnaToolHandle<IAsgElectronEfficiencyCorrectionTool> m_electronIsolationSFTool; //! // TODO!!! NOAM
   std::unique_ptr<ST::SUSYObjDef_xAOD> m_objTool; //!

   std::unique_ptr<TauAnalysisTools::TauSelectionTool> m_tauLoose; //!
   std::unique_ptr<TauAnalysisTools::TauSelectionTool> m_tauMedium; //!
   std::unique_ptr<TauAnalysisTools::TauSelectionTool> m_tauTight; //!

   std::unique_ptr<DileptonST::VJetsUncertaintyTool> m_vjetsuncEW; //!
#endif // not __CINT__

   // put your configuration variables here as public variables.
   // that way they can be set directly from CINT and python.
public:
   Bool_t doApplyTSTBugFix;
   Bool_t doIgnorePhotons;
   Bool_t doSystTrees;
   Bool_t doSystematics;
   Bool_t doAllHists;
   Bool_t dirtyJets;
   Bool_t doPreOR;
   Bool_t isAFII;
   Double_t ptSkim;
   Double_t ptSkimForSyst;
   Double_t metSkim;
   Double_t metSkimForSyst;
   Double_t phptSkim;
   Double_t phptSkimForSyst;
   TString configFile;

   // variables that don't get filled at submission time should be
   // protected from being send from the submission node to the worker
   // node (done by the //!)
public:
   // things used to access xAOD information
   Bool_t m_determinedDerivation; //!
   Bool_t m_isEXOT5; //!
   std::vector<ST::SystInfo> m_sysList; //!
   std::map<TString, TTree *> m_trees; //!
   std::map<TString, std::map<TString, std::map< SelectionCriteria, TH1F *>>> m_hists; //!
   std::map<TString, std::map<TString, std::map< SelectionCriteria, TH1F *>>> m_pure_hists; //!
   Analysis::ContentHolder m_content_current; //!
   Analysis::ContentHolder m_content_nominal; //!
 
   LPXKfactorTool* m_kFTool; //!

 

   // things used for output
   std::map<TString, Analysis::OutputEntry> m_cand; //!
   TH1F *m_histoEventCount; //!

   // things used in the analysis
   Int_t m_eventCounter; //!
   Bool_t m_isMC; //!
   Bool_t m_nominalDone; //! was getObjects called at least one time for this event?
   Analysis::CutFlowTool m_cutFlow; //!
   TVector2 m_met_nomuon_to_use; //!
   TVector2 m_met_wmuon_to_use; //!
   TVector2 m_met_noelectron_to_use; //!
   TVector2 m_met_nophoton_to_use; //!
   TVector2 m_met_nophcalib_nomuon_to_use; //!
   TVector2 m_met_nophcalib_wmuon_to_use; //!
   Int_t m_n_el_baseline; //!
   Int_t m_n_allmu_bad; //!
   Int_t m_n_mu_baseline; //!
   Int_t m_n_mu_baseline_bad; //!
   Int_t m_n_ph_baseline; //!
   Float_t m_ht; //!

   // methods used in the analysis
   virtual EL::StatusCode getObjects(ST::SystInfo &systInfo, Analysis::ContentHolder &content);
   virtual EL::StatusCode getLastCutPassed(ST::SystInfo &systInfo, Analysis::ContentHolder &content, SelectionCriteria crit, DileptonCuts::CutID &last);
   virtual Bool_t PassedCut(Bool_t ispassed, DileptonCuts::CutID thiscut,  DileptonCuts::CutID& lastcut);
   virtual EL::StatusCode analyzeEvent(ST::SystInfo &systInfo, Analysis::OutputEntry &cand);
   virtual EL::StatusCode getMET(std::shared_ptr<xAOD::MissingETContainer> &met, std::shared_ptr<xAOD::MissingETAuxContainer> &metAux, xAOD::JetContainer* jet, xAOD::ElectronContainer* el, xAOD::MuonContainer* mu, xAOD::PhotonContainer* ph, Bool_t doTST, Bool_t doJVT, xAOD::IParticleContainer *invis);
   virtual EL::StatusCode getTrackMET(std::shared_ptr<xAOD::MissingETContainer> &met, std::shared_ptr<xAOD::MissingETAuxContainer> &metAux, xAOD::JetContainer* jet, xAOD::ElectronContainer* el, xAOD::MuonContainer* mu);
   virtual void defineAllSystHists(std::map<TString, std::map<TString, std::map< SelectionCriteria, TH1F *>>> & hists, TString systName, TString treeName, TString extra = "");
   virtual void defineSystHists(std::map<TString, std::map<TString, std::map< SelectionCriteria, TH1F *>>> & hists, TString param, TString systName, TString treeName, TString xtitle, TString extra);
   virtual Bool_t  passRegion(SelectionCriteria crit, Analysis::OutputEntry cand);
   virtual Float_t histWeightRegion(SelectionCriteria crit, Analysis::OutputEntry cand);
   virtual Float_t histEntryRegion(SelectionCriteria crit, Analysis::OutputEntry cand, TString param);
   virtual Int_t isTrueW(const xAOD::TruthParticle * particle);
   virtual Bool_t isSherpaVJets(Int_t mcid);

   // this is a standard constructor
   DileptonNTUPMaker();

   // these are the functions inherited from Algorithm
   virtual EL::StatusCode setupJob(EL::Job& job);
   virtual EL::StatusCode fileExecute();
   virtual EL::StatusCode histInitialize();
   virtual EL::StatusCode changeInput(bool firstFile);
   virtual EL::StatusCode initialize();
   virtual EL::StatusCode execute();
   virtual EL::StatusCode postExecute();
   virtual EL::StatusCode finalize();
   virtual EL::StatusCode histFinalize();

   // this is needed to distribute the algorithm to the workers
   ClassDef(DileptonNTUPMaker, 1);
};

#endif
