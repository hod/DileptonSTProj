#ifndef __Analysis_OutputJet__
#define __Analysis_OutputJet__

#include <DileptonST/OutputObject.h>
#include <xAODJet/Jet.h>

namespace Analysis {
   class OutputJet : public OutputObject {
   public:
      typedef enum {
         BAD = 0,
         LOOSE = 1,
         TIGHT = 2
      } JetCleaningLevel;

      std::vector<Float_t> weight;
      std::vector<Float_t> pt;
      std::vector<Float_t> eta;
      std::vector<Float_t> phi;
      std::vector<Float_t> m;
      std::vector<Float_t> raw_pt;
      std::vector<Float_t> raw_eta;
      std::vector<Float_t> raw_phi;
      std::vector<Float_t> raw_m;
      std::vector<Float_t> timing;
      std::vector<Float_t> emfrac;
      std::vector<Float_t> fmax;
      std::vector<Float_t> hecf;
      std::vector<Float_t> hecq;
      std::vector<Float_t> larq;
      std::vector<Float_t> avglarq;
      std::vector<Float_t> fch;
      std::vector<Int_t>   NumTrkPt500;
      std::vector<Float_t> SumPtTrkPt500;
      std::vector<Float_t> negE;
      std::vector<Float_t> lambda;
      std::vector<Float_t> lambda2;
      std::vector<Float_t> jvtxf;
      std::vector<Float_t> MV2c00_discriminant;
      std::vector<Float_t> MV2c10_discriminant;
      std::vector<Float_t> MV2c20_discriminant;
      std::vector<Int_t> fmaxi;
      std::vector<Int_t> isbjet;
      std::vector<Int_t> isbjet_loose;
      std::vector<Float_t> jvt;
      std::vector<Float_t> met_nomuon_dphi;
      std::vector<Float_t> met_wmuon_dphi;
      std::vector<Float_t> met_noelectron_dphi;
      std::vector<Float_t> met_nophoton_dphi;

      std::vector<Int_t> cleaning;

      std::vector<Int_t> PartonTruthLabelID;
      std::vector<Int_t> ConeTruthLabelID;
      std::vector<Float_t> TruthLabelDeltaR_B;
      std::vector<Float_t> TruthLabelDeltaR_C;
      std::vector<Float_t> TruthLabelDeltaR_T;

   public:
      OutputJet(Bool_t doTrim = kFALSE);
      ~OutputJet();
      void reset();
      void attachToTree(TTree *tree);
      void add(const xAOD::Jet &input);
   };
}

#endif

