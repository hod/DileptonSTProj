#ifndef __Analysis_OutputElectron__
#define __Analysis_OutputElectron__

#include <DileptonST/OutputObject.h>
#include <xAODEgamma/Electron.h>

namespace Analysis {
   class OutputElectron : public OutputObject {
   public:

      std::vector<Float_t> SF;
      std::vector<Float_t> SF_iso;
      std::vector<Float_t> SF_trigger;
      std::vector<Float_t> SF_tot;
      std::vector<Float_t> SF_tot_isotight;
      std::vector<Float_t> charge;
      std::vector<Float_t> pt;
      std::vector<Float_t> eta;
      std::vector<Float_t> phi;
      std::vector<Float_t> m;
      std::vector<Float_t> e;
      std::vector<Float_t> id_pt;
      std::vector<Float_t> id_eta;
      std::vector<Float_t> id_phi;
      std::vector<Float_t> id_m;
      std::vector<Float_t> cl_pt;
      std::vector<Float_t> cl_eta;
      std::vector<Float_t> cl_etaBE2;
      std::vector<Float_t> cl_phi;
      std::vector<Float_t> cl_m;
      std::vector<Float_t> ptcone20;
      std::vector<Float_t> ptvarcone20;
      std::vector<Float_t> etcone20;
      std::vector<Float_t> topoetcone20;
      std::vector<Float_t> ptcone30;
      std::vector<Float_t> ptvarcone30;
      std::vector<Float_t> etcone30;
      std::vector<Float_t> topoetcone30;
      std::vector<Float_t> ptcone40;
      std::vector<Float_t> ptvarcone40;
      std::vector<Float_t> etcone40;
      std::vector<Float_t> topoetcone40;
      std::vector<Float_t> d0;
      std::vector<Float_t> d0sig;
      std::vector<Float_t> z0;
      std::vector<Float_t> z0sig;
      std::vector<Float_t> demaxs1;
      std::vector<Float_t> fside;
      std::vector<Float_t> weta2;
      std::vector<Float_t> ws3;
      std::vector<Float_t> eratio;
      std::vector<Float_t> reta;
      std::vector<Float_t> rphi;
      std::vector<Float_t> time_cl;
      std::vector<Float_t> time_maxEcell;
      std::vector<Float_t> truth_pt;
      std::vector<Float_t> truth_eta;
      std::vector<Float_t> truth_phi;
      std::vector<Float_t> truth_E;
      std::vector<Int_t> isGoodOQ;
      std::vector<Int_t> passLHID;
      std::vector<Int_t> author;
      std::vector<Int_t> isConv;
      std::vector<Int_t> passChID;
      std::vector<Float_t> ecisBDT;
      std::vector<Int_t> truth_matched;
      std::vector<Int_t> truth_mothertype;
      std::vector<Int_t> truth_status;
      std::vector<Int_t> truth_type;
      std::vector<Int_t> truth_typebkg;
      std::vector<Int_t> truth_origin;
      std::vector<Int_t> truth_originbkg;
      std::vector<Int_t> isotool_pass_loosetrackonly;
      std::vector<Int_t> isotool_pass_loose;
      std::vector<Int_t> isotool_pass_gradient;
      std::vector<Int_t> isotool_pass_gradientloose;
      std::vector<Int_t> isotool_pass_fixedcuttight;
      std::vector<Int_t> isotool_pass_fixedcuttighttrackonly;
      std::vector<Float_t> met_nomuon_dphi;
      std::vector<Float_t> met_wmuon_dphi;
      std::vector<Float_t> met_noelectron_dphi;

   public:
      OutputElectron(Bool_t doTrim = kFALSE);
      ~OutputElectron();
      void reset();
      void attachToTree(TTree *tree);
      void add(const xAOD::Electron &input);
   };
}

#endif

