#!/bin/sh

DATE=`date +%d%m%Y`

source build/$CMTCONFIG/setup.sh

cd run/

#python ../source/DileptonST/python/monojetSubmit.py -w local -m 10000000 --doPreOR -n test_Zprime_ee_official -d /afs/cern.ch/user/h/hod/data/mc16_13TeV/derived/Zprime_Chi_2TeV_ee_EXOT0_official
#python ../source/DileptonST/python/monojetSubmit.py -w local -m 10000000 --doPreOR -n test_DYmumu_PowhegPythia_official -d /eos/user/h/hod/EXOT0/mc16_13TeV/PowhegPythia_DYmumu_2000M2250
#python ../source/DileptonST/python/monojetSubmit.py -w local -m 10000000 --doPreOR -n test_DYee_PowhegPythia_official -d /eos/user/h/hod/EXOT0/mc16_13TeV/PowhegPythia_DYee_1250M1500
#python ../source/DileptonST/python/monojetSubmit.py -w local -m 10000000 --doPreOR -n test_Zprime_mumu_PowhegPythia_official -d /eos/user/h/hod/EXOT0/mc16_13TeV/new/PowhegPythia_Zprime_mumu
python ../source/DileptonST/python/monojetSubmit.py -w local -m 10000000 --doPreOR -n test_Zprime_ee_PowhegPythia_official -d /eos/user/h/hod/EXOT0/mc16_13TeV/new/PowhegPythia_Zprime_ee

#python ../source/DileptonST/python/monojetSubmit.py -w local -m 10000000 --doPreOR -n test_asg  -d /eos/user/h/hod/ASGTEST/

####################### GRID
#python ../source/DileptonST/python/monojetSubmit.py -w prun -u hod -l ../source/DileptonST/data/samples/mc16_signals_EXOT0.txt -n SIG${DATE}v1 --doPreOR
#python ../source/DileptonST/python/monojetSubmit.py -w prun -u hod -l ../source/DileptonST/data/samples/mc16_backgrounds_EXOT0.txt -n BKG${DATE}v1 --doPreOR
