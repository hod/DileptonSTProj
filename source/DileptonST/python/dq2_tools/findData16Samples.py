## Script for choosing the most recent tag of a data16 sample
## Takes as argument the GRL

import sys
import os
import re
import subprocess
import re

if len(sys.argv)<2:
    print """
Usage: 
  python %prog [grl file name]

"""

inputfile = sys.argv[1]
filetext = open(inputfile,'r').read()

startseq = "<Metadata Name=\"RunList\">"
endseq   = "</Metadata>"

if startseq not in filetext or endseq not in filetext:
  raise RuntimeError('Could not find signal list')

startindex = filetext.find(startseq)+len(startseq)
endindex   = filetext.find(endseq,startindex)

filehead = filetext[:startindex]
filetail = filetext[endindex:]
runs  = filetext[startindex:endindex]
runs = runs.split(',')

print runs

samples_to_use = []

scope = "data15_13TeV"
deriv = "EXOT5"
tags= 'f|_m|_p'
tag_repr = 'p2950'


for run in runs:
    #    if int(run) < 305380: continue
    print run
    results = subprocess.check_output('rucio list-dids {scope}.*{run}*physics_Main*{deriv}.*{tag_repr}*/  --filter type=container'.format(scope=scope, run=run, deriv=deriv, tag_repr=tag_repr), shell=True).split('\n')
    #results = subprocess.check_output('rucio list-dids data16_13TeV.*{run}*physics_Main*EXOT5.*p2840*/  --filter type=container'.format(run=run), shell=True).split('\n')

    temp_list_to_sort = []
    
    # search for all results for a given run
    for result in results:
      print result
      result = re.search('{scope}:(.+?) '.format(scope=scope), result)
      if result:
        sample = result.group(1)
        dset_parts = re.split(r'.{tags}'.format(tags=tags), sample) # split based on or of ".f" , "_m", "_p"
        print dset_parts
        temp_list_to_sort_item = { 'dset' : dset_parts[0], 'ftag': int(dset_parts[1]), 'mtag': int(dset_parts[2]), 'ptag': int(dset_parts[3]) }
        temp_list_to_sort.append(temp_list_to_sort_item)    
        
    # now find the best item
    if len(temp_list_to_sort) > 0 :     
      best_item = temp_list_to_sort[0]
    
      if len(temp_list_to_sort) > 1 : 
        # first find the max ftag    
        for item in temp_list_to_sort:
          if item['ftag'] >= best_item['ftag']:
            best_item['ftag'] = item['ftag']
        
        for item in temp_list_to_sort:  
          if item['ftag'] == best_item['ftag']:
            if item['mtag'] >= best_item['mtag']:
              best_item['mtag'] = item['mtag'] 
    
        for item in temp_list_to_sort:  
          if item['ftag'] == best_item['ftag'] and item['mtag'] == best_item['mtag']:
            if item['ptag'] >= best_item['ptag']:
              best_item['ptag'] = item['ptag']
      
      #print best_item 
      sample_to_use = "{}.f{}_m{}_p{}".format(best_item['dset'], best_item['ftag'], best_item['mtag'], best_item['ptag'] )  
      print sample_to_use

    else: # found no good item
      sample_to_use = '#missing run {run}'.format(run=run)
      print sample_to_use

    samples_to_use.append(sample_to_use) 

for sample in samples_to_use:
  print sample

if len(samples_to_use) != len(runs):
  "ERROR: query missed some runs"

