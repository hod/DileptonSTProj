////////////////////////////////
//
// BRUTE FORCE HEMISPHERES/MEGA-JETS
//
// Code written by Christopher Rogan <crogan@cern.ch>, 06-08-14
//
////////////////////////////////
//
#include <DileptonST/HemisphereCalculation.h>
#include "xAODJet/JetContainer.h"

using namespace std;

// function takes a vector of TLorentzVectors (the jets you want to re-cluster into
// two hemispheres/mega-jets) and returns a vector of TLorentzVectors that will have
// two entries corresponding to your two hemispheres/mega-jets
// NOTE: the input vector myjets must have at least to jets in it for this to
// work!
vector<TLorentzVector> RazorAnalysis::GetHemi(vector<TLorentzVector> myjets)
{
   vector<TLorentzVector> mynewjets;
   TLorentzVector j1, j2;
   //bool foundGood = false;
   int N_comb = 1;
   for (UInt_t  i = 0; i < myjets.size(); i++) {
      N_comb *= 2;
   }
   double M_min = -1.;
   int j_count;

   for (int i = 1; i < N_comb - 1; i++) {
      TLorentzVector j_temp1, j_temp2;
      int itemp = i;
      j_count = N_comb / 2;
      int count = 0;
      while (j_count > 0) {
         if (itemp / j_count == 1) {
            j_temp1 += myjets[count];
         } else {
            j_temp2 += myjets[count];
         }
         itemp -= j_count * (itemp / j_count);
         j_count /= 2;
         count++;
      }
      double M_temp = j_temp1.M2() + j_temp2.M2();

      // smallest mass
      if (M_temp < M_min || M_min < 0) {
         M_min = M_temp;
         j1 = j_temp1;
         j2 = j_temp2;
      }
   }
   if (j2.Pt() > j1.Pt()) {
      TLorentzVector temp = j1;
      j1 = j2;
      j2 = temp;
   }
   mynewjets.push_back(j1);
   mynewjets.push_back(j2);

   return mynewjets;
}

vector<TLorentzVector> RazorAnalysis::GetHemi(const xAOD::JetContainer *myjets)
{
   vector<TLorentzVector> tmp;

   for (auto jet : *myjets) {
      tmp.push_back(jet->p4());
   }

   return GetHemi(tmp);
}

vector<TLorentzVector> RazorAnalysis::GetHemi(const xAOD::JetContainer &myjets)
{
   vector<TLorentzVector> tmp;

   for (auto jet : myjets) {
      tmp.push_back(jet->p4());
   }

   return GetHemi(tmp);
}
