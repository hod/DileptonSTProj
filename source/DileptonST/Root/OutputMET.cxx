#include "DileptonST/OutputMET.h"

#include <TTree.h>

Analysis::OutputMET::OutputMET(Bool_t doTrim) : Analysis::OutputObject::OutputObject(doTrim)
{
   reset();
}

Analysis::OutputMET::~OutputMET()
{
}

void Analysis::OutputMET::reset()
{
   et = -9999;
   etx = -9999;
   ety = -9999;
   sumet = -9999;
   phi = -9999;

   return;
}

void Analysis::OutputMET::attachToTree(TTree *tree)
{
   const TString prefix = name() + "_";

   tree->Branch(prefix + "et", &et);
   tree->Branch(prefix + "etx", &etx);
   tree->Branch(prefix + "ety", &ety);

   if (!doTrim()) {

      tree->Branch(prefix + "sumet", &sumet);
      tree->Branch(prefix + "phi", &phi);
   }

   return;
}

void Analysis::OutputMET::add(const xAOD::MissingET &input)
{
   et = input.met();
   etx = input.mpx();
   ety = input.mpy();
   sumet = input.sumet();
   phi = input.phi();
}
