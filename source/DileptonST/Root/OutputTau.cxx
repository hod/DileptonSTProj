// Local include(s):
#include "DileptonST/OutputTau.h"

// ROOT include(s):
#include <TTree.h>

Analysis::OutputTau::OutputTau(bool doTrim) : Analysis::OutputObject(doTrim)
{
   reset();
}

void Analysis::OutputTau::reset()
{
   pt.clear();
   eta.clear();
   phi.clear();

   idtool_pass_loose.clear();
   idtool_pass_medium.clear();
   idtool_pass_tight.clear();

   tau_multiplicity = 0;
   medium_multiplicity = 0;
   tight_multiplicity = 0;
}

void Analysis::OutputTau::attachToTree(TTree *tree)
{
   const TString prefix = name() + "_";

   if (write()) {

      tree->Branch(prefix + "pt", &pt);
      tree->Branch(prefix + "eta", &eta);
      tree->Branch(prefix + "phi", &phi);

      tree->Branch(prefix + "idtool_pass_loose", &idtool_pass_loose);
      tree->Branch(prefix + "idtool_pass_medium", &idtool_pass_medium);
      tree->Branch(prefix + "idtool_pass_tight", &idtool_pass_tight);

      tree->Branch(prefix + "multiplicity", &tau_multiplicity);
      tree->Branch(prefix + "medium_multiplicity", &medium_multiplicity);
      tree->Branch(prefix + "tight_multiplicity", &tight_multiplicity);
   }
}

void Analysis::OutputTau::add(const xAOD::TauJet &input)
{
   pt.push_back(input.pt());
   eta.push_back(input.eta());
   phi.push_back(input.phi());

   static SG::AuxElement::Accessor<int> acc_is_loose("is_loose");
   static SG::AuxElement::Accessor<int> acc_is_medium("is_medium");
   static SG::AuxElement::Accessor<int> acc_is_tight("is_tight");
   idtool_pass_loose.push_back(acc_is_loose(input));
   idtool_pass_medium.push_back(acc_is_medium(input));
   idtool_pass_tight.push_back(acc_is_tight(input));

   tau_multiplicity++;

   if (acc_is_medium(input))

      medium_multiplicity++;

   if (acc_is_tight(input))

      tight_multiplicity++;
}
