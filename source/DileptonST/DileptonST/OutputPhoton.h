#ifndef __Analysis_OutputPhoton__
#define __Analysis_OutputPhoton__

#include <DileptonST/OutputObject.h>
#include <xAODEgamma/Photon.h>

namespace Analysis {
   class OutputPhoton : public OutputObject {
   public:

      std::vector<Float_t> SF;
      std::vector<Float_t> SF_iso;
      std::vector<Float_t> pt;
      std::vector<Float_t> eta;
      std::vector<Float_t> phi;
      std::vector<Float_t> m;
      std::vector<Float_t> ptcone20;
      std::vector<Float_t> ptvarcone20;
      std::vector<Float_t> etcone20;
      std::vector<Float_t> topoetcone20;
      std::vector<Float_t> ptcone30;
      std::vector<Float_t> ptvarcone30;
      std::vector<Float_t> etcone30;
      std::vector<Float_t> topoetcone30;
      std::vector<Float_t> ptcone40;
      std::vector<Float_t> ptvarcone40;
      std::vector<Float_t> etcone40;
      std::vector<Float_t> topoetcone40;
      std::vector<uint32_t> OQ; //!
      std::vector<Float_t> eta2;
      std::vector<Float_t> DeltaE;
      std::vector<Float_t> Rphi;
      std::vector<Float_t> weta2;
      std::vector<Float_t> fracs1;
      std::vector<Float_t> weta1;
      std::vector<Float_t> emaxs1;
      std::vector<Float_t> f1;
      std::vector<Float_t> Reta;
      std::vector<Float_t> wtots1;
      std::vector<Float_t> Eratio;
      std::vector<Float_t> Rhad;
      std::vector<Float_t> Rhad1;
      std::vector<Float_t> e277;
      std::vector<Float_t> deltae;
      std::vector<Int_t> author;
      std::vector<Int_t> isConv;
      /*
      std::vector<Float_t> time_cl;
      std::vector<Float_t> time_maxEcell;
      std::vector<Float_t> truth_pt;
      std::vector<Float_t> truth_eta;
      std::vector<Float_t> truth_phi;
      std::vector<Float_t> truth_E;
      std::vector<Int_t> truth_matched;
      std::vector<Int_t> truth_mothertype;
      std::vector<Int_t> truth_status;
      std::vector<Int_t> truth_type;
      std::vector<Int_t> truth_typebkg;
      std::vector<Int_t> truth_origin;
      std::vector<Int_t> truth_originbkg;
      */
      std::vector<Int_t> isTight;
      std::vector<Int_t> isEM;
      std::vector<Int_t> isotool_pass_fixedcuttightcaloonly;
      std::vector<Int_t> isotool_pass_fixedcuttight;
      std::vector<Int_t> isotool_pass_fixedcutloose;
      std::vector<Float_t> met_nomuon_dphi;
      std::vector<Float_t> met_wmuon_dphi;
      std::vector<Float_t> met_nophoton_dphi;

   public:
      OutputPhoton(Bool_t doTrim = kFALSE);
      ~OutputPhoton();
      void reset();
      void attachToTree(TTree *tree);
      void add(const xAOD::Photon &input);
   };
}

#endif

