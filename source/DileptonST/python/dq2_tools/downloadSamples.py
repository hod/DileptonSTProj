# Script for downloading DileptonST grid outputs locally
# Prints to stdout DSIDs for which the download was incomplete or not successful in the chosen group of samples
# G.Gustavino, M.G. Ratti
# Usage: python downloadSamples.py -h

import sys
import os
import re
import subprocess
from optparse import OptionParser

parser = OptionParser()
parser.add_option("--version", type=str,  help="minitree version, e.g. MJ211", default="MJ211")
parser.add_option("--outdir", type=str, help="directory where to download the files", default="test")
parser.add_option("--user", type=str, help="grid user who submitted the MJA jobs, e.g. mratti", default="mratti")
parser.add_option("--what", type=str, help="what to downaload, e.g. BKG or SIG or DATA or TRUTH1 (grid sample name must match this string)")

(options, args) = parser.parse_args()

# specify MC samples which will be used
bkg_samples = { 
 # new sherpa W/Z+jets
 'Zee' : range(364114,364127 + 1),
 'Zmumu' : range(364100,364113 + 1),
 'Ztautau' : range(364128,364141 + 1),
 'Znunu' : range(364142,364155 + 1),
 'Wenu' : range(364170,364183 + 1),
 'Wmunu' : range(364156,364169 + 1),
 'Wtaunu' : range(364184,364197 + 1),
 # new drell yan
 'Zummu_DY': range(364198, 364203+1),
 'Zee_DY': range(364204, 364209+1),
 'Ztautau_DY': range(364210, 364215+1),
 # old sherpa W/Z+jets below
 'Zee' : range(363388, 363411 + 1), 
 'Wenu' : range(363460, 363483 + 1),
 'Wmunu' : range(363436, 363459 + 1),
 'Wtaunu' : range(363331, 363354 + 1),
 'Zmumu' : range(363364, 363387 + 1),
 'Ztautau': range(363102, 363122 + 1),
 'Znunu' : range(363412, 363435 + 1),
 # old drell yan
 #'Zee_DY': range(361468, 361475 + 1),
 #'Zmumu_DY':  range(361476, 361483 + 1),
 #'Ztautau_DY': range(363361, 363363 + 1) + range(361484, 361491 + 1),
 # MGPy8 samples below
 'Zee': range(363147, 363170 + 1),
 'Zmumu': range(363123, 363146 + 1),
 'Wenu': range(363600, 363623 + 1),
 'Wmunu': range(363624, 363647 + 1),
 'Wtaunu': range(363648, 363671 + 1),

 # newest diboson
 'VV' :  range(363490, 363494+1) + range(363355, 363360+1) + [363489] + range(361069,361073) + [361077],   # newest recommended diboson samples

 # old diboson powheg
 # 'VV' : range(361600, 361611 + 1),  # diboson powheg
 # old diboson sherpa
 # 'VV' : range(361063,361068+1) + range(361091,361097+1), 
 'top' : [410000,410011,410012,410013,410014,410025,410026],
 'photon' : range(361039, 361062 + 1),
 'Vgamma' : range(301890, 301910 + 1) + [301535,301536,304776],
#'ttgamma' : [410082],
 'multijet' : range(361020, 361032 + 1),

   }

data_samples = {
    'data15' : [276262,276329,276336,276416,276511,276689,276778,276790,276952,276954,278880,278912,278968,279169,279259,279279,279284,279345,279515,279598,279685,279813,279867,279928,279932,279984,280231,280273,280319,280368,280423,280464,280500,280520,280614,280673,280753,280853,280862,280950,280977,281070,281074,281075,281317,281385,281411,282625,282631,282712,282784,282992,283074,283155,283270,283429,283608,283780,284006,284154,284213,284285,284420,284427,284484], 
  'data16' :  [297730,298595,298609,298633,298687,298690,298771,298773,298862,298967,299055,299144,299147,299184,299243,299584,300279,300345,300415,300418,300487,300540,300571,300600,300655,300687,300784,300800,300863,300908,301912,301918,301932,301973,302053,302137,302265,302269,302300,302347,302380,302391,302393,302737,302831,302872,302919,302925,302956,303007,303079,303201,303208,303264,303266,303291,303304,303338,303421,303499,303560,303638,303832,303846,303892,303943,304006,304008,304128,304178,304198,304211,304243,304308,304337,304409,304431,304494,305380,305543,305571,305618,305671,305674,305723],
   'data16_2' : [305727,305735,305777,305811,305920,306269,306278,306310,306384,306419,306442,306448,306451,307126,307195,307259,307306,307354,307358,307394,307454,307514,307539,307569,307601,307619,307656,307710,307716,307732,307861,307935,308047,308084,309375,309390,309440,309516,309640,309674,309759,310015,310247,310249,310341,310370,310405,310468,310473,310634,310691,310738,310809,310863,310872,310969,311071,311170,311244,311287,311321,311365,311402,311473,311481] 
   }
   
sig_samples = { 
  'dmA': range(305853,306079), 
  'dmT_reco': range(307866,307901+1) + range(307920,307925+1) + range(307944,307955+1) + range(307974,307997+1),
  'dmT_truth': range(307902,307919+1) + range(307926,307943+1) + range(307956,307973+1),
  'dmP': range(301436, 301481+1),
  'ADD': range(303496, 303500+1),
  'squark': range(371850, 371870+1),
  'bb': range(387003, 387132+1),
  'tt': range(387775, 387856+1)
 } 

truth_samples = {
  'dmT_truth': range(307902,307919+1) + range(307926,307943+1) + range(307956,307973+1),
  'dmT_reco': range(307866,307901+1) + range(307920,307925+1) + range(307944,307955+1) + range(307974,307997+1), 
  #'DMA_truth': 
} 
   
missing_minitrees = []
missing_hists = []

if 'BKG' in options.what:
  samples_to_use = bkg_samples
elif 'SIG' in options.what:
  samples_to_use = sig_samples
elif 'DATA' in options.what:
  samples_to_use = data_samples
elif 'TRUTH1' in options.what:
  samples_to_use = truth_samples
else:
 raise RuntimeError('option not supported')


for groupname in samples_to_use:

  for channel_number in samples_to_use[groupname]:
    command_trees = 'rucio get user.{user}.*{version}*{type}*{run}*minitrees.root --dir {outdir}'.format(user=options.user,version=options.version,type=options.what,run=channel_number, outdir=options.outdir)
    command_hists = 'rucio get user.{user}.*{version}*{type}*{run}*hist --dir {outdir}'.format(user=options.user,version=options.version,type=options.what,run=channel_number, outdir=options.outdir)
    check_trees = subprocess.call(command_trees, shell=True)
    check_hists = subprocess.call(command_hists, shell=True)
    
    if check_trees==0 and check_hists==0 :
      print "Check ok for trees and hists"
      # you have called the downolad once with return success, 
      # you can call again to check if the files were actually downoladed 
      minitrees_results = subprocess.check_output(command_trees, shell=True).split('\n')
      hists_results = subprocess.check_output(command_trees, shell=True).split('\n')
      tag_line = 'Files that cannot be downloaded :'
      check_line = 'Files that cannot be downloaded :             0'
      failed_minitree = False
      failed_hist = False

      for line in minitrees_results:
        if tag_line in line and check_line not in line:
          failed_minitree = True

      for line in hists_results:
        if tag_line in line and check_line not in line:
          failed_hist = True

      if failed_minitree:
        missing_minitrees.append(mc_channel_number)

      if failed_hist:
        missing_hists.append(channel_number)
    
    else: 
      missing_minitrees.append(channel_number)
      missing_hists.append(channel_number)
      print "Check not ok for trees and hists"
      print "Channel " + str(channel_number) + "  will be missing"  

print "Missing minitrees: ", missing_minitrees
print "Missing hists: ", missing_hists
