// Container for all those variables which are event-related
// and are too complex to read automatically from xAOD
// (unlike e.g. jets)

#ifndef __Analysis_OutputEvent__
#define __Analysis_OutputEvent__

#include <DileptonST/OutputObject.h>
#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <map>

namespace Analysis {
   class OutputEvent : public OutputObject {
   public:
      // event variables
      Int_t passTSTCleaning;
      Int_t run;
      ULong64_t event;
      Int_t lbn;
      Int_t bcid;
      Int_t last; // referred to SR
      Int_t selected; // referred to SR
      Int_t year;
      Float_t averageIntPerXing;
      Float_t corAverageIntPerXing;
      Float_t kF_weight;
      Float_t xsec;
      Float_t geneff;
      Float_t pu_weight;
      Float_t pu_hash;
      Float_t sh22_weight;
      Float_t mconly_weight;
      std::vector<Float_t> mconly_weights;
      Float_t btag_weight;
      Float_t jvt_all_weight;
      Float_t jvt_weight;
      Float_t pdf_x1;
      Float_t pdf_x2;
      Float_t pdf_pdf1;
      Float_t pdf_pdf2;
      Float_t pdf_scale;
      std::map<TString, Int_t> trigger;
      Int_t trigger_matched_electron;
      Int_t trigger_matched_muon;
      Int_t trigger_matched_HLT_e60_lhmedium;
      Int_t trigger_matched_HLT_e120_lhloose;
      Int_t trigger_matched_HLT_e24_lhmedium_L1EM18VH;
      Int_t trigger_matched_HLT_e24_lhmedium_L1EM20VH;
      Int_t trigger_pass;
      Int_t hfor;
      Int_t n_vx; // DEPRECATED
      Int_t n_ph;
      Int_t n_ph_tight;
      Int_t n_ph_baseline;
      Int_t n_ph_baseline_tight;
      Int_t n_jet;
      Int_t n_jet_preor;
      Int_t n_mu_preor;
      Int_t n_el_preor;
      Int_t n_ph_preor;
      Int_t n_bjet; // DEPRECATED ?
      Int_t n_el;
      Int_t n_el_baseline;
      Int_t n_mu;
      Int_t n_allmu_bad;
      Int_t n_mu_baseline;
      Int_t n_mu_baseline_bad;
      Int_t pdf_id1;
      Int_t pdf_id2;
      Int_t bb_decision;
      Int_t flag_bib;
      Int_t flag_bib_raw;
      Int_t flag_sct;
      Int_t flag_core;

      // composite observables (involve >1 objects)
      Float_t shatR;
      Float_t gaminvR;
      Float_t gaminvRp1;
      Float_t dphi_BETA_R;
      Float_t dphi_J1_J2_R;
      Float_t gamma_Rp1;
      Float_t costhetaR;
      Float_t dphi_R_Rp1;
      Float_t mdeltaR;
      Float_t cosptR;
      Float_t costhetaRp1;
      Float_t munu_mT;
      Float_t enu_mT;
      Float_t jj_m;
      Float_t mumu_m;
      Float_t mumu_pt;
      Float_t mumu_eta;
      Float_t mumu_phi;
      Float_t ee_m;
      Float_t ee_pt;
      Float_t ee_eta;
      Float_t ee_phi;

      Int_t n_jet_truth;
      Float_t truth_jet1_pt;
      Float_t truth_jet1_eta;
      Float_t truth_jet1_phi;
      Float_t truth_jet1_m;
      Float_t truth_jet2_pt;
      Float_t truth_jet2_eta;
      Float_t truth_jet2_phi;
      Float_t truth_jet2_m;

      Float_t truth_ph1_pt;
      Float_t truth_ph1_eta;
      Float_t truth_ph1_phi;
      Int_t truth_ph1_type;

      Float_t truth_V_pt;
      Float_t truth_V_eta;
      Float_t truth_V_phi;
      Float_t truth_V_m;

      std::vector<Float_t> truth_mu_pt;
      std::vector<Float_t> truth_mu_eta;
      std::vector<Float_t> truth_mu_phi;
      std::vector<Float_t> truth_mu_m;
      std::vector<Int_t> truth_mu_status;

      Int_t n_truthTop;
      std::vector <Int_t> truth_W_decay;
      std::vector < Float_t> newSH_weight;
      Float_t GenFiltMet;
      Float_t mu_SF_tot;

      Float_t evsf_baseline_nominal_EL;
      Float_t evsf_baseline_nominal_MU;
      Float_t evsf_baseline_nominal_PH;
      std::map<TString, Float_t> evsf_baseline_syst_EL;
      std::map<TString, Float_t> evsf_baseline_syst_MU;
      std::map<TString, Float_t> evsf_baseline_syst_PH;

      std::map<TString, Float_t> weights;

   public:
      OutputEvent(Bool_t doTrim = kFALSE);
      ~OutputEvent();
      void reset();
      void attachToTree(TTree *tree);

      void setTriggers(const std::vector<TString> &trigs); // define interesting triggers
      void setWeights(const std::vector<TString> &names); // define manually-added event weights (e.g. V+jets reweight)
   };
}

#endif

