# Know your grid job status, easily!
# Valerio Ippolito - Harvard University

import json
import os
import re

# task info:
# [u'termcondition', u'walltimeunit', u'statechangetime', u'campaign', u'transuses', u'site', u'processingtype', u'vo', u'reqid', u'frozentime', u'iointensity', u'ramcount', u'taskname', u'workdiskcount', u'cloud', u'workinggroup', u'ticketid', u'workqueue_id', u'prodsourcelabel', u'workdiskunit', u'corecount', u'oldstatus', u'cputimeunit', u'ttcrequested', u'totev', u'transhome', u'progress', u'currentpriority', u'lockedby', u'lockedtime', u'username', u'ramunit', u'ticketsystemtype', u'cpuefficiency', u'totevrem', u'basewalltime', u'splitrule', u'nucleus', u'cputime', u'errordialog', u'parent_tid', u'superstatus', u'failurerate', u'transpath', u'walltime', u'endtime', u'jeditaskid', u'outdiskunit', u'outdiskcount', u'modificationtime', u'countrygroup', u'status', u'architecture', u'starttime', u'tasktype', u'dsinfo', u'iointensityunit', u'creationdate', u'taskpriority', u'eventservice']

def getInfo(outputname, days, taskid=None):
  # asterisk is added at the end of the outputname
  print 'Will look for jobs from the last {days} days with output:'.format(days=days)
  print '    {outputname}*'.format(outputname=outputname)

  # needs to be run over lxplus
  os.system('cern-get-sso-cookie -u https://bigpanda.cern.ch/ -o bigpanda.cookie.txt')
  #os.system('curl -b bigpanda.cookie.txt -H \'Accept: application/json\' -H \'Content-Type: application/json\' "https://bigpanda.cern.ch/tasks/?taskname={outputname}*&days={days}&json" > TMP_KNOWYOURJOBS'.format(outputname=outputname, days=days))
  additional = ''
  if taskid != None:
    additional = '&jeditaskid={taskid}'.format(taskid=taskid)
  additional = '%s&display_limit=3000&limit=3000' % additional
  os.system('curl -b bigpanda.cookie.txt -H \'Accept: application/json\' -H \'Content-Type: application/json\' "https://bigpanda.cern.ch/tasks/?taskname={outputname}*&days={days}{additional}&json" > TMP_KNOWYOURJOBS'.format(outputname=outputname, days=days, additional=additional))

  with open('TMP_KNOWYOURJOBS') as fp:
    return json.load(fp)


def groupByStatus(outputname, days):
  all_tasks = getInfo(outputname, days)

  groups = {}

  for task in all_tasks:
    status = task[u'status']

    if status not in groups: groups[status] = []
    groups[status].append(task)

  return groups

def getGroupStats(groups):
  taskids = {}
  missing_outputs = {}

  for status, tasks in groups.items():
    if status != u'done':
      taskids[status] = map(lambda x: x[u'jeditaskid'], tasks)
      missing_outputs[status] = map(lambda x: '%s (%s%%)' % (x[u'taskname'], x[u'dsinfo'][u'pctfinished']), tasks)
  result = (taskids, missing_outputs)
  return result

def getSampleNames(groups):
  missing_dnames = []
  for status, tasks in groups.items():
    if status != u'done':
      for task in tasks:
        detailed = getInfo('user', 20, task[u'jeditaskid'])
        dname = detailed[0][u'datasets'][0]['containername']
        missing_dnames.append(dname) 
        print dname
  return missing_dnames

if __name__ == '__main__':
  expression = 'user.mratti.MJ236.BKG.20GeV.*'

  groups = groupByStatus(expression, days=10)

  (taskids, missing_outputs) = getGroupStats(groups)

  print ''
  print '### list of tasks which are not marked as "done"'
  for status, names in missing_outputs.items():
    print status
    for name in sorted(names):  
      print '   {name}'.format(name=name)
    
  print ''
  print '### corresponding IDs'
  for status, tasks in taskids.items():
    print status,tasks
  
  print ''
  print '### corresponding DIDs'  
  for dname in getSampleNames(groups): 
    print dname  
 

