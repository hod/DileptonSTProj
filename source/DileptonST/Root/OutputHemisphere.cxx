#include "DileptonST/OutputHemisphere.h"

#include <TTree.h>
#include <TLorentzVector.h>

Analysis::OutputHemisphere::OutputHemisphere(Bool_t doTrim) : Analysis::OutputObject::OutputObject(doTrim)
{
   reset();
}

Analysis::OutputHemisphere::~OutputHemisphere()
{
}

void Analysis::OutputHemisphere::reset()
{
   n = 0;

   pt.clear();
   eta.clear();
   phi.clear();
   m.clear();

   return;
}

void Analysis::OutputHemisphere::attachToTree(TTree *tree)
{
   const TString prefix = name() + "_";

   if (!doTrim()) {
      tree->Branch(prefix + "n", &n);
      tree->Branch(prefix + "pt", &pt);
      tree->Branch(prefix + "eta", &eta);
      tree->Branch(prefix + "phi", &phi);
      tree->Branch(prefix + "m", &m);
   }

   return;
}

void Analysis::OutputHemisphere::add(const TLorentzVector &input)
{
   n++;

   pt.push_back(input.Pt());
   eta.push_back(input.Eta());
   phi.push_back(input.Phi());
   m.push_back(input.M());
}
