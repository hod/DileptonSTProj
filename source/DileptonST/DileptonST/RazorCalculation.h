#ifndef __Analysis_RazorCalculation_h__
#define __Analysis_RazorCalculation_h__

#include <Rtypes.h>

class TLorentzVector;
class TVector2;

namespace RazorAnalysis {
   class RazorVariables {
   public:
      Double_t shatR;
      Double_t gaminvR;
      Double_t gaminvRp1;
      Double_t dphi_BETA_R;
      Double_t dphi_J1_J2_R;
      Double_t gamma_Rp1;
      Double_t costhetaR;
      Double_t dphi_R_Rp1;
      Double_t mdeltaR;
      Double_t cosptR;
      Double_t costhetaRp1;

   };

   RazorVariables GetRazorVariables(const TLorentzVector &jet1, const TLorentzVector &jet2, const TVector2 &met);
}

#endif
