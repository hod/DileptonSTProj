#ifndef __Analysis_OutputEntry__
#define __Analysis_OutputEntry__

#include <DileptonST/OutputEvent.h>
#include <DileptonST/OutputMET.h>
#include <DileptonST/OutputMuon.h>
#include <DileptonST/OutputElectron.h>
#include <DileptonST/OutputJet.h>
#include <DileptonST/OutputPhoton.h>
#include <DileptonST/OutputHemisphere.h>
#include <DileptonST/OutputTau.h>

#include <TString.h>
#include <map>

namespace Analysis {
   class OutputEntry {
   private:
      TTree *m_attachedTree;
      Bool_t m_doTrim;

   public:
      OutputEvent evt;
      std::map<TString, OutputMET> met;
      std::map<TString, OutputMuon> mu;
      std::map<TString, OutputElectron> el;
      std::map<TString, OutputJet> jet;
      std::map<TString, OutputPhoton> ph;
      std::map<TString, OutputHemisphere> hem;
      std::map<TString, OutputTau> tau;


   public:
      OutputEntry();
      ~OutputEntry();
      void reset();
      void setDoTrim(Bool_t val);
      void attachToTree(TTree *tree);
      void save();
   };
}

#endif
