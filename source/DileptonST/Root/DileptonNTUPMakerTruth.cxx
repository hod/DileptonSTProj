// standard C++ headers
#include <algorithm>
#include <bitset>

// ROOT generic
#include <TSystem.h>
#include <TTree.h>
#include <TTreeFormula.h>
#include <TH1F.h>
#include <TFile.h>

// event loop
#include <EventLoop/Job.h>
//#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

// my code
#include <DileptonST/errorcheck.h>
#include <DileptonST/DileptonNTUPMakerTruth.h>
#include <DileptonST/HemisphereCalculation.h>
#include <DileptonST/RazorCalculation.h>

// EDM
#include <xAODRootAccess/Init.h>
#include <xAODRootAccess/TEvent.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODTruth/TruthEventContainer.h>
#include <xAODTruth/TruthEvent.h>
#include <xAODMissingET/MissingETContainer.h>
#include <xAODCutFlow/CutBookkeeper.h>
#include <xAODCutFlow/CutBookkeeperContainer.h>

// utils
#include <DileptonST/Common.h>
#include "xAODRootAccess/tools/TFileAccessTracer.h"

// this is needed to distribute the algorithm to the workers
ClassImp(DileptonNTUPMakerTruth)


DileptonNTUPMakerTruth :: DileptonNTUPMakerTruth() :
   m_determinedDerivation(kFALSE),
   m_isEXOT5(kFALSE),
   m_tree(nullptr),
   m_histoEventCount(nullptr)
{
   // Here you put any code for the base initialization of variables,
   // e.g. initialize all pointers to 0.  Note that you should only put
   // the most basic initialization here, since this method will be
   // called on both the submission and the worker node.  Most of your
   // initialization code will go into histInitialize() and
   // initialize().

   // temporary fix to avoid Base,2.4.21 issues (https://its.cern.ch/jira/browse/ATLASG-809)
   xAOD::TFileAccessTracer::enableDataSubmission(kFALSE);
}

EL::StatusCode DileptonNTUPMakerTruth :: setupJob(EL::Job& job)
{
   // Here you put code that sets up the job on the submission object
   // so that it is ready to work with your algorithm, e.g. you can
   // request the D3PDReader service or add output files.  Any code you
   // put here could instead also go into the submission script.  The
   // sole advantage of putting it here is that it gets automatically
   // activated/deactivated when you add/remove the algorithm from your
   // job, which may or may not be of value to you.

   job.useXAOD();

   xAOD::Init("DileptonNTUPMakerTruth").ignore();   // call before opening first file

   // add output stream (i.e. files in the data-XXX directory)
   EL::OutputStream output("minitrees");
   job.outputAdd(output);

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode DileptonNTUPMakerTruth :: histInitialize()
{
   // Here you do everything that needs to be done at the very
   // beginning on each worker node, e.g. create histograms and output
   // trees.  This method gets called before any input files are
   // connected.
   Info("histInitialize", "Initializing cut flow histogram");

   TString cf_name("SignalRegion");

   Info("histInitialize", "Name will be %s", cf_name.Data());
   m_cutFlow = Analysis::CutFlowTool(cf_name);
   m_cutFlow.addCut("processed");
   m_cutFlow.addCut("GRL");
   m_cutFlow.addCut("cleaning");
   m_cutFlow.addCut("vertex");
   m_cutFlow.addCut("MET_cleaning");

   Info("histInitialize", "Initializing event count histogram (derivation-proof)");
   m_histoEventCount = new TH1F("histoEventCount", "event count (derivation-proof, only MC weight if any)", 100, 0, 100);
   m_histoEventCount->Fill("initial_weighted", 0);
   m_histoEventCount->Fill("initial_raw", 0);

   wk()->addOutput(m_histoEventCount);


   return EL::StatusCode::SUCCESS;
}



EL::StatusCode DileptonNTUPMakerTruth :: fileExecute()
{
   // Here you do everything that needs to be done exactly once for every
   // single file, e.g. collect a list of all lumi-blocks processed

   const char *APP_NAME = "DileptonNTUPMakerTruth:fileExecute";

   xAOD::TEvent *event = wk()->xaodEvent();

   TTree *MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
   if (!MetaData) {
      Error(APP_NAME, "MetaData not found!");
      return EL::StatusCode::FAILURE;
   }
   MetaData->LoadTree(0);

   const Bool_t isDerivation = !MetaData->GetBranch("StreamAOD");
   //const Bool_t shouldDoThis = (wk()->metaData()->castString("isData") == "NO"); // m_isMC not filled yet
   const Bool_t shouldDoThis = kFALSE; //EMMA

   //get sum of event weights in derivation skims
   if (isDerivation && shouldDoThis) {

      // check for corruption
      const xAOD::CutBookkeeperContainer* incompleteCBK = nullptr;
      if (!event->retrieveMetaInput(incompleteCBK, "IncompleteCutBookkeepers").isSuccess()) {
         Error(APP_NAME, "Failed to retrieve IncompleteCutBookkeepers from MetaData! Exiting.");
         return EL::StatusCode::FAILURE;
      }
      if (incompleteCBK->size() != 0) {
         Error("initializeEvent()", "Found incomplete Bookkeepers! Check file for corruption.");
         return EL::StatusCode::FAILURE;
      }

      // set pointers & retrieve the bookkeeper container
      const xAOD::CutBookkeeperContainer* bookkeepers = nullptr;
      if (!event->retrieveMetaInput(bookkeepers, "CutBookkeepers").isSuccess()) {
         Error(APP_NAME, "Failed to retrieve CutBookkeepers from MetaData");
         return EL::StatusCode::FAILURE;
      }
      const xAOD::CutBookkeeper* event_bookkeeper = nullptr;

      // find the max cycle where input stream is StreamAOD and the name is AllExecutedEvents
      int maxCycle = -1;
      for (auto cbk : *bookkeepers) {
         if (cbk->inputStream() == "StreamAOD" && cbk->name() == "AllExecutedEvents" && cbk->cycle() > maxCycle) {
            maxCycle = cbk->cycle();
            event_bookkeeper = cbk;
         }
      }
      // if the right & proper bookkeeper is found, read info
      if (event_bookkeeper) {
         m_histoEventCount->Fill("initial_weighted", event_bookkeeper->sumOfEventWeights());
         m_histoEventCount->Fill("initial_raw", event_bookkeeper->nAcceptedEvents());
      }

   } // derivation

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode DileptonNTUPMakerTruth :: changeInput(bool firstFile)
{
   // Here you do everything you need to do when we change input files,
   // e.g. resetting branch addresses on trees.  If you are using
   // D3PDReader or a similar service this method is not needed.
   if(false) std::cout << firstFile << std::endl; // avoid compilation warnings

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode DileptonNTUPMakerTruth :: initialize()
{
   // Here you do everything that you need to do after the first input
   // file has been connected and before the first event is processed,
   // e.g. create additional histograms based on which variables are
   // available in the input files.  You can also create all of your
   // histograms and trees in here, but be aware that this method
   // doesn't get called if no events are processed.  So any objects
   // you create here won't be available in the output if you have no
   // input events.

   const char *APP_NAME = "DileptonNTUPMakerTruth::initialize";

   //Info(APP_NAME, "Called with");

   xAOD::TEvent *event = wk()->xaodEvent();


   ////////////////////////
   // Counters
   ////////////////////////
   m_eventCounter = 0;

   // as a check, let's see the number of events in our xAOD
   Info(APP_NAME, "Number of events = %lli", event->getEntries());  // print long long int


   ////////////////////////
   // Output tree
   ////////////////////////
   m_cand.reset();

   TFile *thisFile = wk()->getOutputFile("minitrees");

   const TString treeName = "nominal";

   Info(APP_NAME, "Creating TTree named \"%s\"", treeName.Data());

   m_tree = new TTree(treeName, treeName);
   m_tree->SetDirectory(thisFile);

   m_cand.attachToTree(m_tree, ""); // we use empty prefix

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode DileptonNTUPMakerTruth :: execute()
{
   // Here you do everything that needs to be done on every single
   // events, e.g. read input variables, apply cuts, and fill
   // histograms and trees.  This is where most of your actual analysis
   // code will go.
   //
   const char *APP_NAME = "DileptonNTUPMakerTruth::execute";

   if ((m_eventCounter % 100) == 0) Info(APP_NAME, "Event number = %i", m_eventCounter);
   m_eventCounter++;

   analyzeEvent();

   // clear content
   m_cand.reset();
   SafeDelete(m_muons.first);
   SafeDelete(m_muons.second);
   SafeDelete(m_electrons.first);
   SafeDelete(m_electrons.second);
   SafeDelete(m_jets.first);
   SafeDelete(m_jets.second);

   return EL::StatusCode::SUCCESS;
}


EL::StatusCode DileptonNTUPMakerTruth :: analyzeEvent()
{
   const char *APP_NAME = "DileptonNTUPMakerTruth::analyzeEvent";

   xAOD::TEvent *event = wk()->xaodEvent();

   DileptonCuts::CutID last;
   if (getLastCutPassed(DileptonNTUPMakerTruth::SR, last) != EL::StatusCode::SUCCESS) {
      Error(APP_NAME, "getLastCutPassed failed");
      return EL::StatusCode::FAILURE;
   }

   const xAOD::EventInfo *eventInfo(nullptr);
   if (!event->retrieve(eventInfo, "EventInfo").isSuccess()) {
      Error(APP_NAME, "Failed to retrieve event info collection");
      return EL::StatusCode::FAILURE;
   }


   const xAOD::MissingETContainer *met_truth(nullptr);

   if (!event->retrieve(met_truth, "MET_Truth").isSuccess()) {    // retrieve arguments: container type, container key
      Error(APP_NAME, "Failed to retrieve MET_Truth container");
      return EL::StatusCode::FAILURE;
   }

   //m_met_nomuon_to_use = TVector2((*met_truth)["NonInt"]->mpx(), (*met_truth)["NonInt"]->mpy());
   m_met_wmuon_to_use = TVector2(0, 0); // TODO: implement
   m_met_noelectron_to_use = TVector2(0, 0); // TODO: implement

   // fill cutflow
   m_cutFlow.addCutCounter(last, eventInfo->mcEventWeight()); // TODO: add also pileup

   // save tree
   const Bool_t saveMe = kTRUE;

   if (saveMe) {
      m_cand.reset();

      m_cand.run = eventInfo->mcChannelNumber();
      m_cand.event = eventInfo->eventNumber();
      m_cand.lbn = eventInfo->lumiBlock();
      m_cand.bcid = eventInfo->bcid();
      m_cand.last = last;
      m_cand.n_jet = m_goodJets.size(); // after OR
      m_cand.n_el = m_goodElectrons.size(); // counts only veto leptons
      m_cand.n_mu = m_goodMuons.size(); // counts only veto leptons

      // float
      m_cand.averageIntPerXing = eventInfo->averageInteractionsPerCrossing();
      m_cand.pu_weight = 1.0; // pileup part to be implemented yet
      m_cand.pu_weight *= eventInfo->mcEventWeight();
      m_cand.mconly_weight = eventInfo->mcEventWeight();
      m_cand.mconly_weights = eventInfo->mcEventWeights();
      m_cand.overlap_weight = 1.0;

      //////////////////
      // pdf variables
      //////////////////
      const xAOD::TruthEventContainer *truthE(nullptr);
      if (!event->retrieve(truthE, "TruthEvents").isSuccess()) {
         Error(APP_NAME, "Failed to retrieve Truth container");
      } else {
         xAOD::TruthEventContainer::const_iterator truthE_itr = truthE->begin();
         (*truthE_itr)->pdfInfoParameter(m_cand.pdf_id1, xAOD::TruthEvent::PDGID1);
         (*truthE_itr)->pdfInfoParameter(m_cand.pdf_id2, xAOD::TruthEvent::PDGID2);
         (*truthE_itr)->pdfInfoParameter(m_cand.pdf_x1, xAOD::TruthEvent::X1);
         (*truthE_itr)->pdfInfoParameter(m_cand.pdf_x2, xAOD::TruthEvent::X2);
         (*truthE_itr)->pdfInfoParameter(m_cand.pdf_pdf1, xAOD::TruthEvent::PDF1);
         (*truthE_itr)->pdfInfoParameter(m_cand.pdf_pdf2, xAOD::TruthEvent::PDF2);
         //(*truthE_itr)->pdfInfoParameter(m_cand.pdf_scale, xAOD::TruthEvent::Q);
      }

      // truth leading pt
      if (getPhaseSpaceWeight(m_cand.overlap_weight) != EL::StatusCode::SUCCESS) {
         Error(APP_NAME, "Error in getPhaseSpaceWeight");
         return EL::StatusCode::FAILURE;
      }



      //////////////////
      // composite variables
      //////////////////
      TVector2 the_met(m_met_nomuon_to_use);

      if (m_goodMuons.size() > 0) {
         const xAOD::TruthParticle *thisMuon = m_goodMuons[0];
         TVector2 the_muon_component(thisMuon->p4().Vect().XYvector());
         TVector2 the_muon_met = the_met - the_muon_component;
         m_cand.munu_mT = TMath::Sqrt(2.0 * thisMuon->pt() * the_muon_met.Mod() * (1 - TMath::Cos(thisMuon->p4().Vect().XYvector().DeltaPhi(the_muon_met))));
         if (m_goodMuons.size() > 1) {
            TLorentzVector v_2mu = thisMuon->p4() + m_goodMuons[1]->p4();
            m_cand.mumu_pt = v_2mu.Pt();
            m_cand.mumu_eta = v_2mu.Eta();
            m_cand.mumu_phi = v_2mu.Phi();
            m_cand.mumu_m = v_2mu.M();
         } // at least two vetoed m_muons
      } // at least a vetoed muon
      if (m_goodJets.size() > 1) {
         TLorentzVector v_2jet = m_goodJets[0]->p4() + m_goodJets[1]->p4();
         m_cand.jj_m = v_2jet.M();

         std::vector<TLorentzVector> hemi = RazorAnalysis::GetHemi(m_goodJets);
         m_cand.hem1_pt = hemi[0].Pt();
         m_cand.hem1_eta = hemi[0].Eta();
         m_cand.hem1_phi = hemi[0].Phi();
         m_cand.hem1_m = hemi[0].M();
         m_cand.hem2_pt = hemi[1].Pt();
         m_cand.hem2_eta = hemi[1].Eta();
         m_cand.hem2_phi = hemi[1].Phi();
         m_cand.hem2_m = hemi[1].M();

         RazorAnalysis::RazorVariables raz = RazorAnalysis::GetRazorVariables(hemi[0], hemi[1], the_met);
         m_cand.shatR = raz.shatR;
         m_cand.gaminvR = raz.gaminvR;
         m_cand.gaminvRp1 = raz.gaminvRp1;
         m_cand.dphi_BETA_R = raz.dphi_BETA_R;
         m_cand.dphi_J1_J2_R = raz.dphi_J1_J2_R;
         m_cand.gamma_Rp1 = raz.gamma_Rp1;
         m_cand.costhetaR = raz.costhetaR;
         m_cand.dphi_R_Rp1 = raz.dphi_R_Rp1;
         m_cand.mdeltaR = raz.mdeltaR;
         m_cand.cosptR = raz.cosptR;
         m_cand.costhetaRp1 = raz.costhetaRp1;
      }
      if (m_goodElectrons.size() > 0) {
         TVector2 the_electron_met(the_met);
         m_cand.enu_mT = TMath::Sqrt(2.0 * m_goodElectrons[0]->pt() * the_electron_met.Mod() * (1 - TMath::Cos(m_goodElectrons[0]->p4().Vect().XYvector().DeltaPhi(the_electron_met))));
         if (m_goodElectrons.size() > 1) {
            TLorentzVector v_2el = m_goodElectrons[0]->p4() + m_goodElectrons[1]->p4();
            m_cand.ee_pt = v_2el.Pt();
            m_cand.ee_eta = v_2el.Eta();
            m_cand.ee_phi = v_2el.Phi();
            m_cand.ee_m = v_2el.M();
         } // at least two vetoed m_electrons
      } // at least a vetoed electron

      getTruthVectors(m_cand.truth_dm_pt, m_cand.truth_dm_q, m_cand.truth_dmjet_q);


      //////////////////
      // MET variables
      //////////////////
      m_cand.met_truth_etx = m_met_nomuon_to_use.X();
      m_cand.met_truth_ety = m_met_nomuon_to_use.Y();
      m_cand.met_truth_et  = m_met_nomuon_to_use.Mod();
      m_cand.met_truth_sumet = (*met_truth)["NonInt"]->sumet();

      //////////////////
      // m_muons
      //////////////////
      for (auto thisMuon : m_goodMuons) {
         m_cand.mu_charge.push_back(thisMuon->charge());
         m_cand.mu_pt.push_back(thisMuon->pt());
         m_cand.mu_eta.push_back(thisMuon->eta());
         m_cand.mu_phi.push_back(thisMuon->phi());
         m_cand.mu_m.push_back(thisMuon->m());
         m_cand.mu_met_truth_dphi.push_back(thisMuon->p4().Vect().XYvector().DeltaPhi(m_met_nomuon_to_use));
      }

      //////////////////
      // m_electrons
      //////////////////
      for (auto thisElectron : m_goodElectrons) {
         m_cand.el_charge.push_back(thisElectron->charge());
         m_cand.el_pt.push_back(thisElectron->pt());
         m_cand.el_eta.push_back(thisElectron->eta());
         m_cand.el_phi.push_back(thisElectron->phi());
         m_cand.el_m.push_back(thisElectron->m());
         m_cand.el_met_truth_dphi.push_back(thisElectron->p4().Vect().XYvector().DeltaPhi(m_met_nomuon_to_use));
      }

      /////////////////////////////
      // selected m_jets
      // + compute b-jet multiplicity
      ////////////////////////////
      for (auto thisJet : m_goodJets) {
         m_cand.jet_pt.push_back(thisJet->pt());
         m_cand.jet_eta.push_back(thisJet->eta());
         m_cand.jet_phi.push_back(thisJet->phi());
         m_cand.jet_m.push_back(thisJet->m());
         m_cand.jet_met_truth_dphi.push_back(thisJet->p4().Vect().XYvector().DeltaPhi(m_met_nomuon_to_use));

         static SG::AuxElement::ConstAccessor<int> acc_PartonTruthLabelID("PartonTruthLabelID");
         static SG::AuxElement::ConstAccessor<int> acc_ConeTruthLabelID("ConeTruthLabelID");
         static SG::AuxElement::ConstAccessor<float> acc_TruthLabelDeltaR_B("TruthLabelDeltaR_B");
         static SG::AuxElement::ConstAccessor<float> acc_TruthLabelDeltaR_C("TruthLabelDeltaR_C");
         static SG::AuxElement::ConstAccessor<float> acc_TruthLabelDeltaR_T("TruthLabelDeltaR_T");
         m_cand.jet_PartonTruthLabelID.push_back(acc_PartonTruthLabelID(*thisJet));
         m_cand.jet_ConeTruthLabelID.push_back(acc_ConeTruthLabelID(*thisJet));
         m_cand.jet_TruthLabelDeltaR_B.push_back(acc_TruthLabelDeltaR_B(*thisJet));
         m_cand.jet_TruthLabelDeltaR_C.push_back(acc_TruthLabelDeltaR_C(*thisJet));
         m_cand.jet_TruthLabelDeltaR_T.push_back(acc_TruthLabelDeltaR_T(*thisJet));
      }
      m_tree->Fill();
   } // event to be saved

   return EL::StatusCode::SUCCESS;
}


EL::StatusCode DileptonNTUPMakerTruth :: postExecute()
{
   // Here you do everything that needs to be done after the main event
   // processing.  This is typically very rare, particularly in user
   // code.  It is mainly used in implementing the NTupleSvc.
   return EL::StatusCode::SUCCESS;
}



EL::StatusCode DileptonNTUPMakerTruth :: finalize()
{
   // This method is the mirror image of initialize(), meaning it gets
   // called after the last event has been processed on the worker node
   // and allows you to finish up any objects you created in
   // initialize() before they are written to disk.  This is actually
   // fairly rare, since this happens separately for each worker node.
   // Most of the time you want to do your post-processing on the
   // submission node after all your histogram outputs have been
   // merged.  This is different from histFinalize() in that it only
   // gets called on worker nodes that processed input events.

   const char *APP_NAME = "DileptonNTUPMakerTruth::finalize";

   Info(APP_NAME, "Number of processed events = %i", m_eventCounter);

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode DileptonNTUPMakerTruth :: histFinalize()
{
   // This method is the mirror image of histInitialize(), meaning it
   // gets called after the last event has been processed on the worker
   // node and allows you to finish up any objects you created in
   // histInitialize() before they are written to disk.  This is
   // actually fairly rare, since this happens separately for each
   // worker node.  Most of the time you want to do your
   // post-processing on the submission node after all your histogram
   // outputs have been merged.  This is different from finalize() in
   // that it gets called on all worker nodes regardless of whether
   // they processed input events.
   //

   m_cutFlow.printUnw();
   m_cutFlow.print();

   TH1F *cflow_hist = m_cutFlow.createTH1F();
   TH1F *cflow_hist_unw = m_cutFlow.createTH1Fraw();
   wk()->addOutput(cflow_hist);
   wk()->addOutput(cflow_hist_unw);

   return EL::StatusCode::SUCCESS;
}

EL::StatusCode DileptonNTUPMakerTruth::getObjects()
{
   const char *APP_NAME = "DileptonNTUPMakerTruth::getObjects";

   // THIS is the time-expensive part

   xAOD::TEvent *event = wk()->xaodEvent();

   // we clear good object vectors
   //
   // NOTE: we have to use the argument to clear, otherwise the xAOD framework will take ownership of the
   // pointers fed to these vectors, with a DISASTER as a result
   m_goodMuons.clear(SG::VIEW_ELEMENTS);
   m_goodElectrons.clear(SG::VIEW_ELEMENTS);
   m_goodJets.clear(SG::VIEW_ELEMENTS);

   ///////////
   // Muons
   ///////////
   m_allMuons.clear(SG::VIEW_ELEMENTS);

   const xAOD::TruthParticleContainer *const_muons = nullptr;

   if (!m_determinedDerivation) {
      Info(APP_NAME, "Determining derivation type");
      m_isEXOT5 = event->retrieve(const_muons, "EXOT5TruthMuons").isSuccess();
      m_determinedDerivation = kTRUE;
      Info(APP_NAME, "Is it EXOT5? (will trigger access to dedicated truth electron and muon containers): %s", (m_isEXOT5) ? "YES" : "NO");
   }
   const TString mu_container = (m_isEXOT5) ? "EXOT5TruthMuons" : "TruthMuons";
   if (!event->retrieve(const_muons, mu_container.Data()).isSuccess()) {    // retrieve arguments: container type, container key
      Error(APP_NAME, "Failed to retrieve Muons container");
      return EL::StatusCode::FAILURE;
   }

   m_muons = xAOD::shallowCopyContainer(*const_muons);

   for (auto thisMuon : *m_muons.first) {
      if (thisMuon->pt() > 10000 && TMath::Abs(thisMuon->eta()) < 2.5) m_allMuons.push_back(thisMuon);
   }

   m_allMuons.sort(&DileptonST::comparePt);

   //
   ///////////
   // Electrons
   ///////////
   m_allElectrons.clear(SG::VIEW_ELEMENTS);

   const xAOD::TruthParticleContainer *const_electrons = nullptr;

   if (!m_determinedDerivation) {
      Error(APP_NAME, "Wrong logic: is/isn't EXOT5 should have been determined in the muon part!");
   }
   const TString el_container = (m_isEXOT5) ? "EXOT5TruthElectrons" : "TruthElectrons";
   if (!event->retrieve(const_electrons, el_container.Data()).isSuccess()) {    // retrieve arguments: container type, container key
      Error(APP_NAME, "Failed to retrieve Electrons container");
      return EL::StatusCode::FAILURE;
   }

   m_electrons = xAOD::shallowCopyContainer(*const_electrons);

   for (auto thisElectron : *m_electrons.first) {
      if (thisElectron->pt() > 20000 && TMath::Abs(thisElectron->eta()) < 2.47) m_allElectrons.push_back(thisElectron);
   }

   m_allElectrons.sort(&DileptonST::comparePt);

   ///////////
   // Jets
   ///////////
   m_allJets.clear(SG::VIEW_ELEMENTS);

   const xAOD::JetContainer *const_jets = nullptr;

   if (!event->retrieve(const_jets, "AntiKt4TruthJets").isSuccess()) {    // retrieve arguments: container type, container key
      Error(APP_NAME, "Failed to retrieve Jets container");
      return EL::StatusCode::FAILURE;
   }

   m_jets = xAOD::shallowCopyContainer(*const_jets);

   for (auto thisJet : *m_jets.first) {
      if (thisJet->pt() > 10000 && TMath::Abs(thisJet->eta()) < 2.8) m_allJets.push_back(thisJet);
   }

   m_allJets.sort(&DileptonST::comparePt);


   ///////////
   // Overlap removal and good object selection
   ///////////
   //
   // TODO: implement overlap removal


   for (auto thisJet : m_allJets) {
      if (thisJet->pt() > 30000 && TMath::Abs(thisJet->eta()) < 2.8) {

         // overlap removal
         /*bool passOR = true;
         for (auto thisElectron : m_allElectrons){
            double dphi = thisElectron->phi() - thisJet->phi();
            double deta = thisElectron->eta() - thisJet->eta();
            double dr = sqrt(dphi*dphi + deta*deta);
            if (dr < 0.2) passOR = false;
         }

         if (passOR)*/
         m_goodJets.push_back(thisJet);
      }
   }

   /*for (auto thisMuon : m_allMuons) {
      bool passOR = true;
     for (auto thisJet : m_goodJets){
        double dphi = thisMuon->phi() - thisJet->phi();
        double deta = thisMuon->eta() - thisJet->eta();
        double dr = sqrt(dphi*dphi + deta*deta);
        if (dr < 0.4) passOR = false;
     }
     if (passOR)
         m_goodMuons.push_back(thisMuon);

   }*/

   /*for (auto thisElectron : m_allElectrons) {
     bool passOR = true;
     for (auto thisJet : m_goodJets){
        double dphi = thisElectron->phi() - thisJet->phi();
        double deta = thisElectron->eta() - thisJet->eta();
        double dr = sqrt(dphi*dphi + deta*deta);
        if (dr > 0.2 && dr < 0.4) passOR = false;
     }
     if (passOR)
        m_goodElectrons.push_back(thisElectron);

   }*/

   return EL::StatusCode::SUCCESS;
}

EL::StatusCode DileptonNTUPMakerTruth::getLastCutPassed(SelectionCriteria crit, DileptonCuts::CutID &last)
{
   const char *APP_NAME = "DileptonNTUPMakerTruth::getLastCutPassed";
   if(false) std::cout <<  crit << std::endl; // avoid compilation warnings

   xAOD::TEvent *event = wk()->xaodEvent();

   const xAOD::EventInfo *eventInfo(nullptr);

   if (!event->retrieve(eventInfo, "EventInfo").isSuccess()) {
      Error(APP_NAME, "Failed to retrieve event info collection");
      return EL::StatusCode::FAILURE;
   }

   last = DileptonCuts::GRL;

   // cleaning
   if ((eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) ||
       (eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) ||
       (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18))) {
      return EL::StatusCode::SUCCESS;
   } else {
      last = DileptonCuts::cleaning;
   }


   /*
   ///////////////////
   // trigger
   ///////////////////
   // unavailable in truth...
   last = DileptonCuts::trigger;
   */

   ///////////////////
   // vertex
   ///////////////////
   // unavailable in truth...
   last = DileptonCuts::vertex;


   ///////////////////
   // get objects
   ///////////////////

   if (getObjects() != EL::StatusCode::SUCCESS) {
      Error(APP_NAME, "Error in getObjects");
      return EL::StatusCode::FAILURE;
   }

   const xAOD::MissingETContainer *met_truth(nullptr);

   if (!event->retrieve(met_truth, "MET_Truth").isSuccess()) {    // retrieve arguments: container type, container key
      Error(APP_NAME, "Failed to retrieve MET_Truth container");
      return EL::StatusCode::FAILURE;
   }

   m_met_nomuon_to_use = TVector2((*met_truth)["NonInt"]->mpx(), (*met_truth)["NonInt"]->mpy());

   ///////////////////
   // MET cleaning
   ///////////////////
   // unavailable in truth...
   last = DileptonCuts::MET_cleaning;


   /*
   ///////////////////
   // BCH cleaning
   ///////////////////
   // unavailable in truth...
   last = DileptonCuts::BCH_cleaning;
   */

   return EL::StatusCode::SUCCESS;
}

EL::StatusCode DileptonNTUPMakerTruth::getPhaseSpaceWeight(Float_t &result)
{
   //const char *APP_NAME = "DileptonNTUPMakerTruth::getPhaseSpaceWeight";

   // TODO: implement

   result = 1;

   return EL::StatusCode::SUCCESS;
}

EL::StatusCode DileptonNTUPMakerTruth::getTruthVectors(Float_t &truth_dm_pt, Float_t &truth_dm_q, Float_t &truth_dmjet_q)
{
   xAOD::TEvent *event = wk()->xaodEvent();

   const char *APP_NAME = "DileptonNTUPMakerTruth::getTruthVectors";
   const xAOD::TruthParticleContainer *truthP(nullptr);
   CHECK(event->retrieve(truthP, "TruthParticles"));

   UInt_t n_done(0);
   TLorentzVector p_one, p_two, leadP;
   for (auto thisParticle : *truthP) {
      if (thisParticle->status() != 1) continue;
      if (TMath::Abs(thisParticle->pdgId()) == 1000022) {
         leadP = (leadP.Pt() > thisParticle->pt()) ? leadP : thisParticle->p4();
         if (n_done == 0) {
            p_one = thisParticle->p4();
            n_done++;
         } else if (n_done >= 1) {
            p_two = thisParticle->p4();
            n_done++;
            break;
         }
      }
   }

   truth_dm_pt = (p_one + p_two).Pt();
   truth_dm_q = (p_one + p_two).M();
   truth_dmjet_q = (p_one + p_two + leadP).M();

   return EL::StatusCode::SUCCESS;
}
