# script to count the total size of a given set of datasets
# Valerio Ippolito - Harvard University

# http://ddm-build.cern.ch/ddm/build/dq2clients-stable/doc/api/
import dq2.clientapi.DQ2 as theDQ2module
theDQ2api = theDQ2module.DQ2()

fulllist = [
  'user.ggustavi.MJ118*minitree*'
]

grand_total_in_Gb = 0
for query in fulllist:
  print 'Detecting full size of %s:' % query

  d_in_query = None # list of either datasets or containers
  if '*' in query:
    # it's a wildcard -> get list of containers (or datasets, depending on the query)
    searchmetadata = {'name' : query}
    d_in_query = theDQ2api.listDatasets2(metaDataAttributes=searchmetadata, long=False, all=False, p=None, rpp=None)
    print '  Expanded into:\n%s' % '\n'.join(d_in_query)
    del searchmetadata
  else:
    # it's a single object, either a dataset or a container
    d_in_query = [query]
    
  for d in d_in_query:
    # prepare a list of datasets; it's either the dataset itself
    # or the content of the container (depending if it's a dataset
    # or a container)
    dsets = None
    if d[-1] == '/':
      # it's a container
      dsets = theDQ2api.listDatasetsInContainer(d)
    else:
      # it's a dataset
      dsets = [d]

    # now we have a proper list of datasets (not containers, not wilcards)
    # retrieve the sum of the dataset sizes of all the datasets
    # to be considered
    result = 0
    for dset in dsets:
      result += theDQ2api.getDatasetSize(dset)
    result_in_Gb = float(result) / (1024*1024*1024)
    grand_total_in_Gb += result_in_Gb
    print '     %.3f Gb (%d byte out of %d datasets)' % (result_in_Gb, result, len(dsets))
    del dsets
    del result
    del result_in_Gb
  del d_in_query

print ''
print 'Grand total is %.3f Gb (%.3f Tb)' % (grand_total_in_Gb, grand_total_in_Gb / 1024)
