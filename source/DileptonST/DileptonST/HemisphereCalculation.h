#ifndef __Analysis_Hemisphere_Calculation_h__
#define __Analysis_Hemisphere_Calculation_h__

#include <TLorentzVector.h>
#include "xAODJet/JetContainer.h"

using namespace std;


namespace RazorAnalysis {
   vector<TLorentzVector> GetHemi(vector<TLorentzVector> myjets);
   vector<TLorentzVector> GetHemi(const xAOD::JetContainer *myjets);
   vector<TLorentzVector> GetHemi(const xAOD::JetContainer &myjets);
}

#endif
