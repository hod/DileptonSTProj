#include "DileptonST/OutputElectron.h"

#include <TTree.h>
#include <xAODEgamma/EgammaxAODHelpers.h>

Analysis::OutputElectron::OutputElectron(Bool_t doTrim) : Analysis::OutputObject::OutputObject(doTrim)
{
   reset();
}

Analysis::OutputElectron::~OutputElectron()
{
}

void Analysis::OutputElectron::reset()
{

   SF.clear();
   SF_iso.clear();
   SF_trigger.clear();
   SF_tot.clear();
   SF_tot_isotight.clear();
   charge.clear();
   pt.clear();
   eta.clear();
   phi.clear();
   m.clear();
   e.clear();
   isGoodOQ.clear();
   passLHID.clear();
   id_pt.clear();
   id_eta.clear();
   id_phi.clear();
   id_m.clear();
   cl_pt.clear();
   cl_eta.clear();
   cl_etaBE2.clear();
   cl_phi.clear();
   cl_m.clear();
   ptcone20.clear();
   ptvarcone20.clear();
   etcone20.clear();
   topoetcone20.clear();
   ptcone30.clear();
   ptvarcone30.clear();
   etcone30.clear();
   topoetcone30.clear();
   ptcone40.clear();
   ptvarcone40.clear();
   etcone40.clear();
   topoetcone40.clear();
   d0.clear();
   d0sig.clear();
   z0.clear();
   z0sig.clear();
   demaxs1.clear();
   fside.clear();
   weta2.clear();
   ws3.clear();
   eratio.clear();
   reta.clear();
   rphi.clear();
   time_cl.clear();
   time_maxEcell.clear();
   truth_pt.clear();
   truth_eta.clear();
   truth_phi.clear();
   truth_E.clear();
   author.clear();
   isConv.clear();
   passChID.clear();
   ecisBDT.clear();
   truth_matched.clear();
   truth_mothertype.clear();
   truth_status.clear();
   truth_type.clear();
   truth_typebkg.clear();
   truth_origin.clear();
   truth_originbkg.clear();
   isotool_pass_loosetrackonly.clear();
   isotool_pass_loose.clear();
   isotool_pass_gradient.clear();
   isotool_pass_gradientloose.clear();
   isotool_pass_fixedcuttight.clear();
   isotool_pass_fixedcuttighttrackonly.clear();
   met_nomuon_dphi.clear();
   met_wmuon_dphi.clear();
   met_noelectron_dphi.clear();

   return;
}

void Analysis::OutputElectron::attachToTree(TTree *tree)
{
   const TString prefix = name() + "_";

   if (write()) {
      tree->Branch(prefix + "pt", &pt);
      tree->Branch(prefix + "eta", &eta);
      tree->Branch(prefix + "phi", &phi);
      tree->Branch(prefix + "SF", &SF);
      tree->Branch(prefix + "SF_iso", &SF_iso);
      tree->Branch(prefix + "SF_trigger", &SF_trigger);
      tree->Branch(prefix + "SF_tot", &SF_tot);
      tree->Branch(prefix + "SF_tot_isotight", &SF_tot_isotight);
      tree->Branch(prefix + "isotool_pass_loosetrackonly", &isotool_pass_loosetrackonly);
      tree->Branch(prefix + "isotool_pass_fixedcuttighttrackonly", &isotool_pass_fixedcuttighttrackonly);
      tree->Branch(prefix + "isotool_pass_fixedcuttight", &isotool_pass_fixedcuttight);

      if (!doTrim()) {
         tree->Branch(prefix + "m", &m);
         tree->Branch(prefix + "e", &e);
         tree->Branch(prefix + "isGoodOQ", &isGoodOQ);
         tree->Branch(prefix + "passLHID", &passLHID);
         tree->Branch(prefix + "charge", &charge);
         tree->Branch(prefix + "id_pt", &id_pt);
         tree->Branch(prefix + "id_eta", &id_eta);
         tree->Branch(prefix + "id_phi", &id_phi);
         tree->Branch(prefix + "id_m", &id_m);
         tree->Branch(prefix + "cl_pt", &cl_pt);
         tree->Branch(prefix + "cl_eta", &cl_eta);
         tree->Branch(prefix + "cl_etaBE2", &cl_etaBE2);
         tree->Branch(prefix + "cl_phi", &cl_phi);
         tree->Branch(prefix + "cl_m", &cl_m);
         tree->Branch(prefix + "ptcone20", &ptcone20);
         tree->Branch(prefix + "ptvarcone20", &ptvarcone20);
         tree->Branch(prefix + "etcone20", &etcone20);
         tree->Branch(prefix + "topoetcone20", &topoetcone20);
         tree->Branch(prefix + "ptcone30", &ptcone30);
         tree->Branch(prefix + "ptvarcone30", &ptvarcone30);
         tree->Branch(prefix + "etcone30", &etcone30);
         tree->Branch(prefix + "topoetcone30", &topoetcone30);
         tree->Branch(prefix + "ptcone40", &ptcone40);
         tree->Branch(prefix + "ptvarcone40", &ptvarcone40);
         tree->Branch(prefix + "etcone40", &etcone40);
         tree->Branch(prefix + "topoetcone40", &topoetcone40);
         tree->Branch(prefix + "d0", &d0);
         tree->Branch(prefix + "d0sig", &d0sig);
         tree->Branch(prefix + "z0", &z0);
         tree->Branch(prefix + "z0sig", &z0sig);
         tree->Branch(prefix + "demaxs1", &demaxs1);
         tree->Branch(prefix + "fside", &fside);
         tree->Branch(prefix + "weta2", &weta2);
         tree->Branch(prefix + "ws3", &ws3);
         tree->Branch(prefix + "eratio", &eratio);
         tree->Branch(prefix + "reta", &reta);
         tree->Branch(prefix + "rphi", &rphi);
         tree->Branch(prefix + "time_cl", &time_cl);
         tree->Branch(prefix + "time_maxEcell", &time_maxEcell);
         tree->Branch(prefix + "truth_pt", &truth_pt);
         tree->Branch(prefix + "truth_eta", &truth_eta);
         tree->Branch(prefix + "truth_phi", &truth_phi);
         tree->Branch(prefix + "truth_E", &truth_E);
         tree->Branch(prefix + "author", &author);
         tree->Branch(prefix + "isConv", &isConv);
         tree->Branch(prefix + "passChID", &passChID);
         tree->Branch(prefix + "ecisBDT", &ecisBDT);
         tree->Branch(prefix + "truth_matched", &truth_matched);
         tree->Branch(prefix + "truth_mothertype", &truth_mothertype);
         tree->Branch(prefix + "truth_status", &truth_status);
         tree->Branch(prefix + "truth_type", &truth_type);
         tree->Branch(prefix + "truth_typebkg", &truth_typebkg);
         tree->Branch(prefix + "truth_origin", &truth_origin);
         tree->Branch(prefix + "truth_originbkg", &truth_originbkg);

         tree->Branch(prefix + "isotool_pass_loose", &isotool_pass_loose);
         tree->Branch(prefix + "isotool_pass_gradient", &isotool_pass_gradient);
         tree->Branch(prefix + "isotool_pass_gradientloose", &isotool_pass_gradientloose);
         tree->Branch(prefix + "met_nomuon_dphi", &met_nomuon_dphi);
         tree->Branch(prefix + "met_wmuon_dphi", &met_wmuon_dphi);
         tree->Branch(prefix + "met_noelectron_dphi", &met_noelectron_dphi);
      }
   }

   return;
}

void Analysis::OutputElectron::add(const xAOD::Electron &input)
{

   author.push_back(input.author());
   isConv.push_back(xAOD::EgammaHelpers::isConvertedPhoton(&input));
   charge.push_back(input.charge());
   pt.push_back(input.pt());
   eta.push_back(input.eta());
   phi.push_back(input.phi());
   m.push_back(input.m());
   e.push_back(input.e());
   isGoodOQ.push_back( input.isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON) );
   static SG::AuxElement::ConstAccessor<char> acc_passSignalID("passSignalID");
   try {
      passLHID.push_back(acc_passSignalID(input));
   } catch (SG::ExcBadAuxVar) {
      passLHID.push_back(-999);
   }

   static SG::AuxElement::ConstAccessor<char> acc_passChID("passChID");
   try {
      passChID.push_back(acc_passChID(input));
   } catch (SG::ExcBadAuxVar) {
      passChID.push_back(-999);
   }

   /*
   // why there's no accessor in ST?
   static SG::AuxElement::Decorator<double> dec_ecisBDT("ecisBDT");
   try {
      ecisBDT.push_back(dec_ecisBDT(input));
   } catch (SG::ExcBadAuxVar) {
      ecisBDT.push_back(-999);
   }
   */

   const xAOD::TrackParticle *thisTrack = input.trackParticle();
   id_pt.push_back(thisTrack->pt());
   id_eta.push_back(thisTrack->eta());
   id_phi.push_back(thisTrack->phi());
   id_m.push_back(thisTrack->m());
   cl_pt.push_back(input.caloCluster()->pt());
   cl_eta.push_back(input.caloCluster()->eta());
   cl_etaBE2.push_back(input.caloCluster()->etaBE(2)); // to be used for detector-related cuts, e.g. track
   cl_phi.push_back(input.caloCluster()->phi());
   cl_m.push_back(input.caloCluster()->m());

   static SG::AuxElement::ConstAccessor<float> acc_new_d0("new_d0");
   static SG::AuxElement::ConstAccessor<float> acc_new_d0sig("new_d0sig");
   static SG::AuxElement::ConstAccessor<float> acc_new_z0("new_z0");
   static SG::AuxElement::ConstAccessor<float> acc_new_z0sig("new_z0sig");
   try {
      d0.push_back(acc_new_d0(input));
      d0sig.push_back(acc_new_d0sig(input));
      z0.push_back(acc_new_z0(input));
      z0sig.push_back(acc_new_z0sig(input));
   } catch (SG::ExcBadAuxVar) {
      d0.push_back(-9999);
      d0sig.push_back(-9999);
      z0.push_back(-9999);
      z0sig.push_back(-9999);
   }

   demaxs1.push_back(-9999); // TODO: implement or remove
   fside.push_back(-9999); // TODO: implement or remove
   weta2.push_back(-9999); // TODO: implement or remove
   ws3.push_back(-9999); // TODO: implement or remove
   eratio.push_back(-9999);  // TODO: implement or remove
   reta.push_back(-9999);  // TODO: implement or remove
   rphi.push_back(-9999);  // TODO: implement or remove
   time_cl.push_back(-9999); // TODO: implement or remove
   time_maxEcell.push_back(-9999);  // TODO: implement or remove
   truth_pt.push_back(-9999);  // TODO: implement or remove
   truth_eta.push_back(-9999);  // TODO: implement or remove
   truth_phi.push_back(-9999);  // TODO: implement or remove
   truth_E.push_back(-9999);  // TODO: implement or remove

   // isolation variables
   Float_t tmp_ptcone20(-9999);
   Float_t tmp_ptvarcone20(-9999);
   Float_t tmp_etcone20(-9999);
   Float_t tmp_topoetcone20(-9999);
   Float_t tmp_ptcone30(-9999);
   Float_t tmp_ptvarcone30(-9999);
   Float_t tmp_etcone30(-9999);
   Float_t tmp_topoetcone30(-9999);
   Float_t tmp_ptcone40(-9999);
   Float_t tmp_ptvarcone40(-9999);
   Float_t tmp_etcone40(-9999);
   Float_t tmp_topoetcone40(-9999);
   input.isolationValue(tmp_ptcone20, xAOD::Iso::IsolationType::ptcone20);
   input.isolationValue(tmp_ptvarcone20, xAOD::Iso::IsolationType::ptvarcone20);
   input.isolationValue(tmp_etcone20, xAOD::Iso::IsolationType::etcone20);
   input.isolationValue(tmp_topoetcone20, xAOD::Iso::IsolationType::topoetcone20);
   input.isolationValue(tmp_ptcone30, xAOD::Iso::IsolationType::ptcone30);
   input.isolationValue(tmp_ptvarcone30, xAOD::Iso::IsolationType::ptvarcone30);
   input.isolationValue(tmp_etcone30, xAOD::Iso::IsolationType::etcone30);
   input.isolationValue(tmp_topoetcone30, xAOD::Iso::IsolationType::topoetcone30);
   input.isolationValue(tmp_ptcone40, xAOD::Iso::IsolationType::ptcone40);
   input.isolationValue(tmp_ptvarcone40, xAOD::Iso::IsolationType::ptvarcone40);
   input.isolationValue(tmp_etcone40, xAOD::Iso::IsolationType::etcone40);
   input.isolationValue(tmp_topoetcone40, xAOD::Iso::IsolationType::topoetcone40);
   ptcone20.push_back(tmp_ptcone20);
   ptvarcone20.push_back(tmp_ptvarcone20);
   etcone20.push_back(tmp_etcone20);
   topoetcone20.push_back(tmp_topoetcone20);
   ptcone30.push_back(tmp_ptcone30);
   ptvarcone30.push_back(tmp_ptvarcone30);
   etcone30.push_back(tmp_etcone30);
   topoetcone30.push_back(tmp_topoetcone30);
   ptcone40.push_back(tmp_ptcone40);
   ptvarcone40.push_back(tmp_ptvarcone40);
   etcone40.push_back(tmp_etcone40);
   topoetcone40.push_back(tmp_topoetcone40);

   // decorations we define in our analysis code
   static SG::AuxElement::ConstAccessor<char> acc_pass_loosetrackonly("pass_loosetrackonly");
   static SG::AuxElement::ConstAccessor<char> acc_pass_loose("pass_loose");
   static SG::AuxElement::ConstAccessor<char> acc_pass_gradient("pass_gradient");
   static SG::AuxElement::ConstAccessor<char> acc_pass_gradientloose("pass_gradientloose");
   static SG::AuxElement::ConstAccessor<char> acc_pass_fixedcuttight("pass_fixedcuttight");
   static SG::AuxElement::ConstAccessor<char> acc_pass_fixedcuttighttrackonly("pass_fixedcuttighttrackonly");
   try {
      isotool_pass_loosetrackonly.push_back(acc_pass_loosetrackonly(input));
      isotool_pass_loose.push_back(acc_pass_loose(input));
      isotool_pass_gradient.push_back(acc_pass_gradient(input));
      isotool_pass_gradientloose.push_back(acc_pass_gradientloose(input));
      isotool_pass_fixedcuttight.push_back(acc_pass_fixedcuttight(input));
      isotool_pass_fixedcuttighttrackonly.push_back(acc_pass_fixedcuttighttrackonly(input));
   } catch (SG::ExcBadAuxVar) {
      isotool_pass_loosetrackonly.push_back(-9999);
      isotool_pass_loose.push_back(-9999);
      isotool_pass_gradient.push_back(-9999);
      isotool_pass_gradientloose.push_back(-9999);
      isotool_pass_fixedcuttight.push_back(-9999);
      isotool_pass_fixedcuttighttrackonly.push_back(-9999);
   }

   // dphi's
   static SG::AuxElement::ConstAccessor<float> acc_new_met_nomuon_dphi("new_met_nomuon_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_wmuon_dphi("new_met_wmuon_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_noelectron_dphi("new_met_noelectron_dphi");
   try {
      met_nomuon_dphi.push_back(acc_new_met_nomuon_dphi(input));
      met_wmuon_dphi.push_back(acc_new_met_wmuon_dphi(input));
      met_noelectron_dphi.push_back(acc_new_met_noelectron_dphi(input));
   } catch (SG::ExcBadAuxVar) {
      met_nomuon_dphi.push_back(-9999);
      met_wmuon_dphi.push_back(-9999);
      met_noelectron_dphi.push_back(-9999);
   }


   // decorations from SUSYTools
   static SG::AuxElement::ConstAccessor<float> acc_lep_SF("lep_SF");
   static SG::AuxElement::ConstAccessor<float> acc_lep_iso_SF("lep_iso_SF");
   static SG::AuxElement::ConstAccessor<float> acc_lep_trigger_SF("lep_trigger_SF");
   static SG::AuxElement::ConstAccessor<float> acc_lep_tot_SF("lep_tot_SF"); //Fra
   static SG::AuxElement::ConstAccessor<float> acc_lep_tot_isotight_SF("lep_tot_isotight_SF");
   Float_t tmp_lep_SF(-9999);
   Float_t tmp_lep_iso_SF(-9999);
   Float_t tmp_lep_trigger_SF(-9999);
   Float_t tmp_lep_tot_SF(-9999);
   Float_t tmp_lep_tot_isotight_SF(-9999);
   try {
      tmp_lep_SF = acc_lep_SF(input);
      tmp_lep_iso_SF = acc_lep_iso_SF(input);
      tmp_lep_trigger_SF = acc_lep_trigger_SF(input);
      tmp_lep_tot_SF =  acc_lep_tot_SF(input);
      tmp_lep_tot_isotight_SF =  acc_lep_tot_isotight_SF(input);
   } catch (SG::ExcBadAuxVar) {
      tmp_lep_SF = 1.0;
      tmp_lep_iso_SF = 1.0;
      tmp_lep_trigger_SF = 1.0;
      tmp_lep_tot_SF = 1.0;
      tmp_lep_tot_isotight_SF = 1.0;
   }
   SF.push_back(tmp_lep_SF);
   SF_iso.push_back(tmp_lep_iso_SF);
   SF_trigger.push_back(tmp_lep_trigger_SF);
   SF_tot.push_back(tmp_lep_tot_SF);
   SF_tot_isotight.push_back(tmp_lep_tot_isotight_SF);

   return;
}
