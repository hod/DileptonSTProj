# Valerio Ippolito - Harvard University

Code to compute systematic uncertainties (using PowhegControl event weights when available) due to:
  1) PDF (inter-PDF aka test different central values for PDF sets)
        -> does not use event weights; relies on privately produced samples
  2) PDF (intra-PDF aka evaluate the intrinsic NNPDF uncertainty running on the 100 sets)
        -> uses event weights; relies on official production
  3) SCALE (renormalisation and factorisation scales)
        -> uses event weights; relies on official production
  4) TUNE (tune variations)
        -> does not use event weights; relies on privately produced samples

Usage:
   - update readTruth.C whenever you want to change the definition of acceptance, the MET binning,
     the input location, the expected number of variations, and in general whenever you run over
     either of the two sources (private vs official production)
   - run it twice, once for private once for official production:
        root -b -q DileptonST/python/systutils/readTruth.C+
   - redirect the output onto "DMsyst_privateprod.py" and "DMsyst_officialprod.py" and remove manually
     the non-python printout at the beginning
   - run getSyst.py to get the final tables and plots
        python DileptonST/python/systutils/getSyst.py
