// ROOT generic
#include <TH1F.h>

// event loop
#include <EventLoop/Worker.h>

// my code
#include <DileptonST/errorcheck.h>
#include <DileptonST/DileptonNTUPMaker.h>

// utils
//#include <DileptonST/Common.h>


void DileptonNTUPMaker :: defineAllSystHists(std::map<TString, std::map<TString, std::map< SelectionCriteria, TH1F *>>> & hists, TString systName, TString treeName, TString extra)
{

   defineSystHists(hists, "met",      systName, treeName, "E_{T}^{miss} [GeV]",   extra);
   if (doAllHists) {
      defineSystHists(hists, "mt",       systName, treeName, "Transverse Mass [GeV]", extra);
      defineSystHists(hists, "mumum",    systName, treeName, "Dilepton Mass [GeV]",  extra);
      defineSystHists(hists, "eem",    systName, treeName, "Dilepton Mass [GeV]",  extra);
      defineSystHists(hists, "mu1pt",    systName, treeName, "p_{T}^{#mu(1)} [GeV]",    extra);
      defineSystHists(hists, "mu2pt",    systName, treeName, "p_{T}^{#mu(2)} [GeV]",    extra);
      defineSystHists(hists, "jetpt",    systName, treeName, "p_{T}^{jet(1)} [GeV]",    extra);
      defineSystHists(hists, "jetphi",   systName, treeName, "#Phi_{jet}",           extra);
      defineSystHists(hists, "el1pt",    systName, treeName, "p_{T}^{e(1)} [GeV]",      extra);
      defineSystHists(hists, "el2pt",    systName, treeName, "p_{T}^{e(2)} [GeV]",      extra);
      defineSystHists(hists, "njets",    systName, treeName, "n_{jet}",              extra);
      defineSystHists(hists, "ph1pt",    systName, treeName, "p_{T}^{#gamma(1)} [GeV]",    extra);
      defineSystHists(hists, "metjetphi", systName, treeName, "|#Delta#Phi(jets,E_{T}^{miss})|_{min}", extra);
      defineSystHists(hists, "jeteta", systName, treeName, "#eta_{jet}", extra);
   }
}

void DileptonNTUPMaker :: defineSystHists(std::map<TString, std::map<TString, std::map< SelectionCriteria, TH1F *>>> & hists, TString param, TString systName, TString treeName, TString xtitle, TString extra)
{
   //define binning
   TH1F* temp_hist;
   //arrays for irregular binning
   Float_t bins_mT[] = { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 165, 180, 200, 225, 250, 300 };
   Float_t bins_mumu_m[] = { 0, 10, 20, 30, 40, 50, 60, 70, 80, 85, 90, 95, 100, 110, 120, 130, 140, 150, 165, 180, 200, 225, 250, 300 };
   Float_t bins_mu_pt[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 165, 180, 200, 225, 250, 300, 400, 600, 1000};
   Float_t bins_el_pt[] = {20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 165, 180, 200, 225, 250, 300, 400, 600, 1000};
   Float_t bins_ph_pt[] = {150, 175, 200, 225, 250, 275, 300, 350, 400, 450, 500, 600, 800, 1000, 1200, 1500, 2000 };
   Float_t bins_jet_pt[] = {150, 175, 200, 225, 250, 275, 300, 350, 400, 450, 500, 600, 800, 1000, 1200, 1500, 2000 };

   //define histograms
   if (param == "jetpt")     temp_hist = new TH1F("temp", "temp", sizeof(bins_jet_pt) / sizeof(Float_t) - 1, bins_jet_pt);
   else if (param == "mt")   temp_hist = new TH1F("temp", "temp", sizeof(bins_mT)     / sizeof(Float_t) - 1, bins_mT);
   else if (param == "mumum")temp_hist = new TH1F("temp", "temp", sizeof(bins_mumu_m)     / sizeof(Float_t) - 1, bins_mumu_m);
   else if (param == "eem")temp_hist = new TH1F("temp", "temp", sizeof(bins_mumu_m)     / sizeof(Float_t) - 1, bins_mumu_m);
   else if (param == "ph1pt") temp_hist = new TH1F("temp", "temp", sizeof(bins_ph_pt)     / sizeof(Float_t) - 1, bins_ph_pt);
   else if (param == "mu1pt" || param == "mu2pt") temp_hist = new TH1F("temp", "temp", sizeof(bins_mu_pt)     / sizeof(Float_t) - 1, bins_mu_pt);
   else if (param == "el1pt" || param == "el2pt") temp_hist = new TH1F("temp", "temp", sizeof(bins_el_pt)     / sizeof(Float_t) - 1, bins_el_pt);
   else if (param == "njets")     temp_hist = new TH1F("temp", "temp", 4, 0.5, 4.5);
   else if (param == "jetphi")    temp_hist = new TH1F("temp", "temp", 30, -TMath::Pi(), TMath::Pi());
   else if (param == "metjetphi") temp_hist = new TH1F("temp", "temp", 30, 0, TMath::Pi());
   else if (param == "jeteta") temp_hist = new TH1F("temp", "temp", 30, -2.4, 2.4);

   //default & met hist
   else  temp_hist = new TH1F("temp", "temp", 300, 0, 3000);

   treeName = param + "_" + treeName;
   if (extra.Length() > 0) treeName = extra + "_" + treeName;

   //signal regions
   if (!(param.Contains("el") || param.Contains("ee") || param.Contains("mu") || param.Contains("ph"))) { // don't include el or mu or ph parameters
      hists[param][systName][SelectionCriteria::VR150]    = (TH1F*)temp_hist->Clone("VR150_" + treeName);
      hists[param][systName][SelectionCriteria::SR]       = (TH1F*)temp_hist->Clone("SR_"    + treeName);
   }

   //photon CR
   if (!(doIgnorePhotons || param.Contains("el") || param.Contains("ee") || param.Contains("mu") || param.Contains("mt")))
      hists[param][systName][SelectionCriteria::CR1ph]       = (TH1F*)temp_hist->Clone("CR1ph_"    + treeName);

   //muon CRs
   if (!(param.Contains("el") || param.Contains("ph") || param.Contains("ee"))) { // exclude el parameters
      if (!(param.Contains("mu2") || param.Contains("mumu"))) {
         hists[param][systName][SelectionCriteria::CR1mubtag]    = (TH1F*)temp_hist->Clone("CR1mubtag_"    + treeName);
         hists[param][systName][SelectionCriteria::CR1mubveto]    = (TH1F*)temp_hist->Clone("CR1mubveto_"    + treeName);
         hists[param][systName][SelectionCriteria::CR1mu]    = (TH1F*)temp_hist->Clone("CR1mu_"    + treeName);
         hists[param][systName][SelectionCriteria::CR1mu150] = (TH1F*)temp_hist->Clone("CR1mu150_" + treeName);
      }
      if (!(param.Contains("mt"))) {
         hists[param][systName][SelectionCriteria::CR2mu]    = (TH1F*)temp_hist->Clone("CR2mu_"    + treeName);
         hists[param][systName][SelectionCriteria::CR2mu150] = (TH1F*)temp_hist->Clone("CR2mu150_" + treeName);
      }
   }

   //electron CRs
   if (!(param.Contains("mu") || param.Contains("ph"))) { // exclude mu parameters
      if (!(param.Contains("el2") || param.Contains("ee"))) {
         hists[param][systName][SelectionCriteria::CR1e]     = (TH1F*)temp_hist->Clone("CR1e_"  + treeName);
         hists[param][systName][SelectionCriteria::CR1e_metnoel]  = (TH1F*)temp_hist->Clone("CR1e_metnoel_"  + treeName);
         hists[param][systName][SelectionCriteria::CR1e150]  = (TH1F*)temp_hist->Clone("CR1e150_"  + treeName);
         hists[param][systName][SelectionCriteria::CR1e_metnoel150]  = (TH1F*)temp_hist->Clone("CR1e_metnoel150_"  + treeName);
      }
      if (!(param.Contains("mt"))) {
         hists[param][systName][SelectionCriteria::CR2e]     = (TH1F*)temp_hist->Clone("CR2e_"     + treeName);
         hists[param][systName][SelectionCriteria::CR2e150]  = (TH1F*)temp_hist->Clone("CR2e150_"  + treeName);
      }
   }

   // format histogrsm & add to output
   for (auto & h : hists[param][systName]) {
      h.second->SetTitle(h.second->GetName());
      h.second->GetXaxis()->SetTitle(xtitle);
      wk()->addOutput(h.second);
   }

   delete temp_hist;
}

bool DileptonNTUPMaker ::passRegion(SelectionCriteria crit, Analysis::OutputEntry cand)
{

   //met definition
   TString metName = "met_nomuon_tst";
   if (crit == CR2e || crit == CR2e150 || crit == CR1e_metnoel || crit == CR1e_metnoel150) metName = "met_noelectron_tst";
   if (crit == CR1ph) metName = "met_nophoton_tst";

   ///////////////////////////////////
   // Cuts common to all regions
   ///////////////////////////////////

   //if (cand.evt.last < DileptonCuts::MET_cleaning) return kFALSE;
   if (cand.evt.last < DileptonCuts::cleaning) return kFALSE;

   const Bool_t is2015 = ((cand.evt.year == 0 && cand.evt.run >= 276262 && cand.evt.run <= 284484) || cand.evt.year == 2015) ? true : false;

   //trigger
   bool pass_trigger = ((cand.evt.trigger["HLT_xe70"] && is2015) || // 2015
                        ((cand.evt.trigger["HLT_xe80_tc_lcw_L1XE50"] || cand.evt.trigger["HLT_xe90_mht_L1XE50"] || cand.evt.trigger["HLT_xe100_mht_L1XE50"] || cand.evt.trigger["HLT_xe110_mht_L1XE50"] || cand.evt.trigger["HLT_xe130_mht_L1XE50"]) && !is2015)); // 2016
   if (crit == CR2e || crit == CR2e150 || crit == CR1e_metnoel || crit == CR1e_metnoel150) {
      pass_trigger = (((cand.evt.trigger["HLT_e24_lhmedium_L1EM20VH"]
                        || cand.evt.trigger["HLT_e60_lhmedium"]
                        || cand.evt.trigger["HLT_e120_lhloose"]) && is2015)//2015
                      || ((cand.evt.trigger["HLT_e26_lhtight_nod0_ivarloose"]
                           || cand.evt.trigger["HLT_e60_lhmedium_nod0"]
                           || cand.evt.trigger["HLT_e140_lhloose_nod0"]) && !is2015)); // 2016
   } else if (crit == CR1ph) {
      pass_trigger = ((cand.evt.trigger["HLT_g140_loose"] && is2015) || ((cand.evt.trigger["HLT_g140_loose"] || cand.evt.trigger["HLT_g160_loose"]) && !is2015));
   }

   if (!pass_trigger) return kFALSE;


   // removed: TST bug fix

   //leading pt cut & cleaning
   if (cand.jet["jet"].pt[0] / 1000 <= 250) return kFALSE;
   if (TMath::Abs(cand.jet["jet"].eta[0]) >= 2.4) return kFALSE;
   if (cand.jet["jet"].fch[0] / cand.jet["jet"].fmax[0] < 0.1) return kFALSE;

   // jet multiplicity
   if (cand.evt.n_jet > 4) return kFALSE;

   // jet/met overlap
   for (Int_t j = 0; j < cand.evt.n_jet; j++) {
      Float_t dphi = cand.jet["jet"].met_nomuon_dphi[j];
      if (metName.Contains("noelectron")) dphi = cand.jet["jet"].met_noelectron_dphi[j];
      if (cand.evt.n_ph > 0 and  metName.Contains("nophoton")) dphi = TVector2::Phi_mpi_pi(cand.jet["jet"].phi[j] - cand.ph["ph"].phi[0]);    //cand.jet["jet"].met_nophoton_dphi[j];
      if (TMath::Abs(dphi) <=  0.4) return kFALSE;
   }

   ///////////////////////////////////
   // MET cut
   ///////////////////////////////////

   switch (crit) {
      case SR:
      case CR1mu:
      case CR1mubveto:
      case CR1mubtag:
      case CR2mu:
      case CR1e:
      case CR1e_metnoel:
      case CR2e:
         if (cand.met[metName].et / 1000 <= 250) return kFALSE;
         break;
      case CR1ph:
         if (cand.evt.n_ph < 1 || cand.ph["ph"].pt[0] / 1000 <= 250) return kFALSE;
         break;
      case VR150:
      case CR1mu150:
      case CR2mu150:
      case CR1e150:
      case CR1e_metnoel150:
      case CR2e150:
         if (cand.met[metName].et / 1000 <= 150 || cand.met[metName].et / 1000 > 250) return kFALSE;
         break;
      default:
         throw std::logic_error(TString::Format("Unexpected selection criteria %d", (Int_t)crit).Data());
         return kFALSE;
   }

   ///////////////////////////////////
   // Photon multiplicity (and other) cuts
   ///////////////////////////////////

   if (!doIgnorePhotons) {
      if (crit == CR1ph) {
         if (cand.evt.n_ph != 1 || cand.evt.n_ph_baseline != 1)  return kFALSE; // 1 good photon
         if (!cand.ph["ph"].isotool_pass_fixedcuttight[0]) return kFALSE;
      } /*
     else {
     if (cand.evt.n_ph_baseline != 0) return kFALSE; // 0 baseline photons
     }
   */
   }

   ///////////////////////////////////
   // Lepton multiplicity (and other) cuts
   ///////////////////////////////////

   if (crit == SR || crit == VR150 || crit == CR1ph) {
      if (cand.evt.n_el_baseline != 0 || cand.evt.n_mu_baseline != 0) return kFALSE;  // 0 baseline leptons
   } else if (crit == CR1mu || crit == CR1mu150 || crit == CR1mubveto || crit == CR1mubtag) {
      if (cand.evt.n_el_baseline != 0 || cand.evt.n_mu != 1 || cand.evt.n_mu_baseline != 1) return kFALSE; // one good muon
      if (cand.evt.munu_mT / 1000 <= 30 || cand.evt.munu_mT / 1000 >= 100) return kFALSE; // munu_mT cut
      if (crit == CR1mubveto && cand.evt.n_bjet > 0) return kFALSE; // bveto
      if (crit == CR1mubtag && cand.evt.n_bjet == 0) return kFALSE; // btag
   } else if (crit == CR2mu || crit == CR2mu150) {
      if (cand.evt.n_el_baseline != 0 || cand.evt.n_mu != 2 || cand.evt.n_mu_baseline != 2) return kFALSE; // two good muons
      if (cand.evt.mumu_m / 1000 <= 66 || cand.evt.mumu_m / 1000 >= 116) return kFALSE;
   } else if (crit == CR1e || crit == CR1e150) {
      if (cand.evt.n_el != 1 || cand.evt.n_el_baseline != 1 || cand.evt.n_mu_baseline != 0) return kFALSE; // one good electron
      if (!cand.el["el"].isotool_pass_loosetrackonly[0]) return kFALSE; //isolation
   } else if (crit == CR1e_metnoel || crit == CR1e_metnoel150) {
      if (cand.evt.n_el != 1 || cand.evt.n_el_baseline != 1 || cand.evt.n_mu_baseline != 0) return kFALSE;
      if (cand.el["el"].pt[0] / 1000 <= 30) return kFALSE; // leading electron at plateau
      if (TMath::Abs(cand.el["el"].cl_etaBE2[0]) >= 1.37 && TMath::Abs(cand.el["el"].cl_etaBE2[0]) <= 1.52) return kFALSE; // exclude crack (based on detector eta at 2nd sampling)
      if (!cand.el["el"].isotool_pass_fixedcuttight[0]) return kFALSE; //isolation
      if (cand.evt.enu_mT / 1000 <= 30 || cand.evt.enu_mT / 1000 >= 100) return kFALSE; // enu_mT cut
      if (cand.met["met_nomuon_tst"].et / 1000 <= 70) return kFALSE; // MET cut
      if (cand.met["met_wmuon_tst"].et / 1000 / TMath::Sqrt(m_ht / 1000.) <= 5.) return kFALSE; // "significance" cut
      // using the same mT cut as CR1mu
   } else if (crit == CR2e || crit == CR2e150) {
      if (cand.evt.n_el != 2 || cand.evt.n_el_baseline != 2 || cand.evt.n_mu_baseline != 0) return kFALSE; // two good electrons
      if (cand.el["el"].pt[0] / 1000 <= 30 || cand.el["el"].pt[1] / 1000 <= 30) return kFALSE;
      if (cand.evt.ee_m / 1000 <= 66 || cand.evt.ee_m / 1000 >= 116) return kFALSE;
   }

   return kTRUE;
}

Float_t DileptonNTUPMaker ::histEntryRegion(SelectionCriteria crit, Analysis::OutputEntry cand, TString param)
{
   if (param == "met") {
      if (crit == CR1ph) {
         // we are brave, and define "MET(CR1ph) := pT(leading photon)"
         if (cand.ph["ph"].pt.size() > 0) return cand.ph["ph"].pt[0] / 1000;
      } else {
         TString metName = "met_nomuon_tst";
         if (crit == CR2e || crit == CR2e150 || crit == CR1e_metnoel || crit == CR1e_metnoel150)  metName = "met_noelectron_tst";
         return cand.met[metName].et / 1000;
      }
   } else if (param == "mt" || param == "mumum" || param == "eem") {
      if (crit == CR1e || crit == CR1e_metnoel)  return cand.evt.enu_mT / 1000;
      if (crit == CR1mu || crit == CR1mubveto || crit == CR1mubtag) return cand.evt.munu_mT / 1000;
      if (crit == CR2e) return cand.evt.ee_m / 1000;
      if (crit == CR2mu) return cand.evt.mumu_m / 1000;
   } else if (param == "metjetphi") {
      Float_t phimin = 9999;
      if (crit == CR2e || crit == CR1e_metnoel) {
         for (auto dphi : cand.jet["jet"].met_noelectron_dphi)
            if (fabs(dphi) < phimin) phimin = fabs(dphi);
      } else if (crit == CR1ph) {
         for (auto dphi : cand.jet["jet"].met_nophoton_dphi)
            if (fabs(dphi) < phimin) phimin = fabs(dphi);
      } else {
         for (auto dphi : cand.jet["jet"].met_nomuon_dphi)
            if (fabs(dphi) < phimin) phimin = fabs(dphi);
      }
      return phimin;
   } else if (param == "jetpt")return cand.jet["jet"].pt[0] / 1000;
   else if (param == "jetphi") return cand.jet["jet"].phi[0];
   else if (param == "jeteta") return cand.jet["jet"].eta[0];
   else if (param == "ph1pt") {
      if (cand.ph["ph"].pt.size() > 0) return cand.ph["ph"].pt[0] / 1000;
   } else if (param == "mu1pt") {
      if (cand.mu["mu"].pt.size() > 0) return cand.mu["mu"].pt[0] / 1000;
   } else if (param == "mu2pt") {
      if (cand.mu["mu"].pt.size() > 1) return cand.mu["mu"].pt[1] / 1000;
   } else if (param == "el1pt") {
      if (cand.el["el"].pt.size() > 0) return cand.el["el"].pt[0] / 1000;
   } else if (param == "el2pt") {
      if (cand.el["el"].pt.size() > 1) return cand.el["el"].pt[1] / 1000;
   } else if (param == "njets") return cand.evt.n_jet;

   return -9999;

}

Float_t DileptonNTUPMaker ::histWeightRegion(SelectionCriteria crit, Analysis::OutputEntry cand)
{
   // applies object-related SFs (do not add event weights, like mconly_weight, here!)

   switch (crit) {
      case SR:
         return cand.evt.jvt_weight;
      case CR1mu:
      case CR1mu150:
         return cand.mu["mu"].SF[0] * cand.evt.jvt_weight;
      case CR1mubtag:
      case CR1mubveto:
         return cand.mu["mu"].SF[0] * cand.evt.btag_weight * cand.evt.jvt_weight;
      case CR2mu:
      case CR2mu150:
         return cand.mu["mu"].SF[0] * cand.mu["mu"].SF[1] * cand.evt.jvt_weight;
      case CR1e:
      case CR1e150:
         return cand.el["el"].SF_iso[0] * cand.evt.jvt_weight;
      case CR1e_metnoel:
         return cand.el["el"].SF_tot_isotight[0] * cand.evt.jvt_weight;
      case CR1e_metnoel150:
         return cand.el["el"].SF_tot[0] * cand.evt.jvt_weight;
      case CR2e:
      case CR2e150:
         return cand.el["el"].SF_trigger[0] * cand.el["el"].SF_trigger[1] * cand.evt.jvt_weight;
      case CR1ph:
         return cand.ph["ph"].SF[0] * cand.ph["ph"].SF_iso[0] * cand.evt.jvt_weight;

      default:
         return 1;
   }
   return 1;
}

