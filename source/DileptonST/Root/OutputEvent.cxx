#include "DileptonST/OutputEvent.h"

#include <TTree.h>
#include <TError.h>

Analysis::OutputEvent::OutputEvent(Bool_t doTrim) : Analysis::OutputObject::OutputObject(doTrim)
{
   reset();
}

Analysis::OutputEvent::~OutputEvent()
{
}

void Analysis::OutputEvent::reset()
{
   passTSTCleaning = 0;
   run = -9999;
   event = 0;
   lbn = -9999;
   bcid = -9999;
   last = -9999;
   year = -9999;
   averageIntPerXing = -9999;
   corAverageIntPerXing = -9999;
   kF_weight = 1.0;
   xsec      = 1.0;
   geneff    = 1.0;
   pu_weight = 1.0;
   pu_hash = -9999;
   sh22_weight = 1.0;
   mconly_weight = 1.0;
   mconly_weights.clear();
   btag_weight = 1.0;
   jvt_weight = 1.0;
   jvt_all_weight = 1.0;
   pdf_x1 = -9999;
   pdf_x2 = -9999;
   pdf_pdf1 = -9999;
   pdf_pdf2 = -9999;
   pdf_scale = -9999;
   mu_SF_tot = 1.0;
   for (auto &kv : trigger) {
      kv.second = -9999;
   }

   trigger_matched_electron = 0;
   trigger_matched_muon = 0;

   trigger_matched_HLT_e60_lhmedium = 0;
   trigger_matched_HLT_e120_lhloose = 0;
   trigger_matched_HLT_e24_lhmedium_L1EM18VH = 0;
   trigger_matched_HLT_e24_lhmedium_L1EM20VH = 0;
   trigger_pass = -9999;
   hfor = -9999;
   n_vx = -9999;
   n_ph = -9999;
   n_ph_tight = -9999;
   n_ph_baseline = -9999;
   n_ph_baseline_tight = -9999;
   n_jet = -9999;
   n_jet_preor = -9999;
   n_mu_preor = -9999;
   n_el_preor = -9999;
   n_ph_preor = -9999;
   n_bjet = -9999;
   n_el = -9999;
   n_el_baseline = -9999;
   n_mu = -9999;
   n_allmu_bad = -9999;
   n_mu_baseline = -9999;
   n_mu_baseline_bad = -9999;
   pdf_id1 = -9999;
   pdf_id2 = -9999;
   bb_decision = -9999;
   flag_bib = 0;
   flag_bib_raw = 0;
   flag_sct = 0;
   flag_core = 0;

   shatR = -9999;
   gaminvR = -9999;
   gaminvRp1 = -9999;
   dphi_BETA_R = -9999;
   dphi_J1_J2_R = -9999;
   gamma_Rp1 = -9999;
   costhetaR = -9999;
   dphi_R_Rp1 = -9999;
   mdeltaR = -9999;
   cosptR = -9999;
   costhetaRp1 = -9999;
   munu_mT = -9999;
   enu_mT = -9999;
   jj_m = -9999;
   mumu_m = -9999;
   mumu_pt = -9999;
   mumu_eta = -9999;
   mumu_phi = -9999;
   ee_m = -9999;
   ee_pt = -9999;
   ee_eta = -9999;
   ee_phi = -9999;

   n_jet_truth = -9999;
   truth_jet1_pt = -9999;
   truth_jet1_eta = -9999;
   truth_jet1_phi = -9999;
   truth_jet1_m = -9999;
   truth_jet2_pt = -9999;
   truth_jet2_eta = -9999;
   truth_jet2_phi = -9999;
   truth_jet2_m = -9999;

   truth_ph1_pt = -9999;
   truth_ph1_eta = -9999;
   truth_ph1_phi = -9999;
   truth_ph1_type = -9999;

   truth_V_pt = -9999;
   truth_V_eta = -9999;
   truth_V_phi = -9999;
   truth_V_m = -9999;

   truth_mu_pt.clear();
   truth_mu_eta.clear();
   truth_mu_phi.clear();
   truth_mu_m.clear();
   truth_mu_status.clear();

   truth_W_decay.clear();
   n_truthTop = -9999;
   GenFiltMet = -9999;
   newSH_weight.clear();

   evsf_baseline_nominal_EL = -9999;
   evsf_baseline_nominal_MU = -9999;
   evsf_baseline_nominal_PH = -9999;

   for (auto &kv : evsf_baseline_syst_EL) {
      kv.second = -9999;
   }

   for (auto &kv : evsf_baseline_syst_MU) {
      kv.second = -9999;
   }

   for (auto &kv : evsf_baseline_syst_PH) {
      kv.second = -9999;
   }

   for (auto &kv : weights) {
      kv.second = -9999;
   }

   return;
}

void Analysis::OutputEvent::attachToTree(TTree *tree)
{
   const TString prefix = (name() != "") ? name() + "_" : ""; // no prefix by default

   tree->Branch(prefix + "run", &run);
   tree->Branch(prefix + "event", &event);
   tree->Branch(prefix + "last", &last);
   tree->Branch(prefix + "year", &year);
   tree->Branch(prefix + "n_jet", &n_jet);
   tree->Branch(prefix + "n_jet_preor", &n_jet_preor);
   tree->Branch(prefix + "n_mu_preor", &n_mu_preor);
   tree->Branch(prefix + "n_el_preor", &n_el_preor);
   tree->Branch(prefix + "n_ph_preor", &n_ph_preor);
   tree->Branch(prefix + "n_bjet", &n_bjet);
   tree->Branch(prefix + "n_el", &n_el);
   tree->Branch(prefix + "n_el_baseline", &n_el_baseline);
   tree->Branch(prefix + "n_mu_baseline", &n_mu_baseline);
   tree->Branch(prefix + "n_mu_baseline_bad", &n_mu_baseline_bad);
   tree->Branch(prefix + "n_allmu_bad", &n_allmu_bad);
   tree->Branch(prefix + "n_mu", &n_mu);
   tree->Branch(prefix + "munu_mT", &munu_mT);
   tree->Branch(prefix + "enu_mT", &enu_mT);
   tree->Branch(prefix + "mumu_m", &mumu_m);
   tree->Branch(prefix + "ee_m", &ee_m);
   // TODO: add back overlap_weight when needed
   tree->Branch(prefix + "sh22_weight", &sh22_weight);
   tree->Branch(prefix + "mconly_weight", &mconly_weight);
   tree->Branch(prefix + "mconly_weights", &mconly_weights);
   tree->Branch(prefix + "kF_weight", &kF_weight);
   tree->Branch(prefix + "xsec", &xsec);
   tree->Branch(prefix + "geneff", &geneff);
   tree->Branch(prefix + "pu_weight", &pu_weight);
   tree->Branch(prefix + "btag_weight", &btag_weight);
   tree->Branch(prefix + "jvt_weight", &jvt_weight);
   tree->Branch(prefix + "jvt_all_weight", &jvt_all_weight);

   //mono-b
   tree->Branch(prefix + "truth_W_decay", &truth_W_decay);
   tree->Branch(prefix + "GenFiltMet", &GenFiltMet);
   tree->Branch(prefix + "newSH_weight", &newSH_weight);
   tree->Branch(prefix + "n_truthTop", &n_truthTop);
   tree->Branch(prefix + "passTSTCleaning", &passTSTCleaning);
   tree->Branch(prefix + "averageIntPerXing", &averageIntPerXing);
   tree->Branch(prefix + "corAverageIntPerXing", &corAverageIntPerXing);
   tree->Branch(prefix + "n_vx", &n_vx);
   tree->Branch(prefix + "pu_hash", &pu_hash);
   tree->Branch(prefix + "mu_SF_tot", &mu_SF_tot);
   tree->Branch(prefix + "trigger_matched_electron", &trigger_matched_electron);
   tree->Branch(prefix + "trigger_matched_muon", &trigger_matched_muon);


   for (auto &kv : trigger) {
      const TString trigName = kv.first;
      if (trigName == "HLT_e60_lhmedium"
          || trigName == "HLT_e120_lhloose"
          || trigName == "HLT_e24_lhmedium_L1EM20VH" // 2015
          || trigName == "HLT_e24_lhtight_nod0_ivarloose"
          || trigName == "HLT_e26_lhtight_nod0_ivarloose"
          || trigName == "HLT_e60_lhmedium_nod0"
          || trigName == "HLT_e140_lhloose_nod0"
          || trigName == "HLT_g140_loose" // 2016
          || trigName == "HLT_e24_lhmedium_L1EM20VH"
          || trigName == "HLT_e24_lhmedium_nod0_L1EM20VH"
          || trigName == "HLT_e24_lhmedium_L1EM20VHI"
          || trigName == "HLT_e26_lhtight_ivarloose"
          || trigName == "HLT_e60_lhmedium"
          || trigName == "HLT_e60_medium"
          || trigName == "HLT_2e17_lhloose"
          || trigName == "HLT_2e17_lhvloose"
          || trigName == "HLT_2e17_lhvloose_nod0"
          || trigName == "HLT_mu20_ivarloose_L1MU15"
          || trigName == "HLT_mu24_ivarmedium"
          || trigName == "HLT_mu26_imedium"
          || trigName == "HLT_mu26_ivarmedium"
          || trigName == "HLT_mu20_2mu4noL1"
          || trigName == "HLT_mu22_mu8noL1"
          || trigName == "HLT_mu40"
          || trigName == "HLT_mu50"
          || trigName == "HLT_2mu14"
         )
         tree->Branch(prefix + "trigger_" + trigName, &kv.second);
   }

   for (auto &kv : weights) {
      tree->Branch(prefix + "weight_" + kv.first, &kv.second);
   }


   if (!doTrim()) {
      tree->Branch(prefix + "trigger_pass", &trigger_pass);
      tree->Branch(prefix + "trigger_matched_HLT_e60_lhmedium", &trigger_matched_HLT_e60_lhmedium);
      tree->Branch(prefix + "trigger_matched_HLT_e120_lhloose", &trigger_matched_HLT_e120_lhloose);
      tree->Branch(prefix + "trigger_matched_HLT_e24_lhmedium_L1EM18VH", &trigger_matched_HLT_e24_lhmedium_L1EM18VH);
      tree->Branch(prefix + "trigger_matched_HLT_e24_lhmedium_L1EM20VH", &trigger_matched_HLT_e24_lhmedium_L1EM20VH);
      tree->Branch(prefix + "lbn", &lbn);
      tree->Branch(prefix + "bcid", &bcid);
      tree->Branch(prefix + "pdf_x1", &pdf_x1);
      tree->Branch(prefix + "pdf_x2", &pdf_x2);
      tree->Branch(prefix + "pdf_pdf1", &pdf_pdf1);
      tree->Branch(prefix + "pdf_pdf2", &pdf_pdf2);
      tree->Branch(prefix + "pdf_scale", &pdf_scale);
      tree->Branch(prefix + "flag_bib", &flag_bib);
      tree->Branch(prefix + "flag_bib_raw", &flag_bib_raw);
      tree->Branch(prefix + "flag_sct", &flag_sct);
      tree->Branch(prefix + "flag_core", &flag_core);
      for (auto &kv : trigger) {
         const TString trigName = kv.first;
         if (!(trigName == "HLT_xe70"
               || trigName == "HLT_e60_lhmedium"
               || trigName == "HLT_e120_lhloose"
               || trigName == "HLT_e24_lhmedium_L1EM20VH" // 2015
               || trigName == "HLT_xe80_tc_lcw_L1XE50"
               || trigName == "HLT_xe90_mht_L1XE50"
               || trigName == "HLT_xe100_mht_L1XE50"
               || trigName == "HLT_xe110_mht_L1XE50"
               || trigName == "HLT_xe130_mht_L1XE50"
               || trigName == "HLT_xe120_L1XE50"
               || trigName == "HLT_xe110_L1XE50"
               || trigName == "HLT_noalg_L1J400"
               || trigName == "HLT_e24_lhtight_nod0_ivarloose"
               || trigName == "HLT_e26_lhtight_nod0_ivarloose"
               || trigName == "HLT_e60_lhmedium_nod0"
               || trigName == "HLT_e140_lhloose_nod0"
               || trigName == "HLT_g140_loose")) // 2016
            tree->Branch(prefix + "trigger_" + trigName, &kv.second);
      }
      tree->Branch(prefix + "hfor", &hfor);
      tree->Branch(prefix + "n_ph", &n_ph);
      tree->Branch(prefix + "n_ph_tight", &n_ph_tight);
      tree->Branch(prefix + "n_ph_baseline", &n_ph_baseline);
      tree->Branch(prefix + "n_ph_baseline_tight", &n_ph_baseline_tight);
      tree->Branch(prefix + "pdf_id1", &pdf_id1);
      tree->Branch(prefix + "pdf_id2", &pdf_id2);
      tree->Branch(prefix + "bb_decision", &bb_decision);

      tree->Branch(prefix + "shatR", &shatR);
      tree->Branch(prefix + "gaminvR", &gaminvR);
      tree->Branch(prefix + "gaminvRp1", &gaminvRp1);
      tree->Branch(prefix + "dphi_BETA_R", &dphi_BETA_R);
      tree->Branch(prefix + "dphi_J1_J2_R", &dphi_J1_J2_R);
      tree->Branch(prefix + "gamma_Rp1", &gamma_Rp1);
      tree->Branch(prefix + "costhetaR", &costhetaR);
      tree->Branch(prefix + "dphi_R_Rp1", &dphi_R_Rp1);
      tree->Branch(prefix + "mdeltaR", &mdeltaR);
      tree->Branch(prefix + "cosptR", &cosptR);
      tree->Branch(prefix + "costhetaRp1", &costhetaRp1);
      tree->Branch(prefix + "jj_m", &jj_m);
      tree->Branch(prefix + "mumu_pt", &mumu_pt);
      tree->Branch(prefix + "mumu_eta", &mumu_eta);
      tree->Branch(prefix + "mumu_phi", &mumu_phi);
      tree->Branch(prefix + "ee_pt", &ee_pt);
      tree->Branch(prefix + "ee_eta", &ee_eta);
      tree->Branch(prefix + "ee_phi", &ee_phi);

      tree->Branch(prefix + "n_jet_truth", &n_jet_truth);
      tree->Branch(prefix + "truth_jet1_pt", &truth_jet1_pt);
      tree->Branch(prefix + "truth_jet1_eta", &truth_jet1_eta);
      tree->Branch(prefix + "truth_jet1_phi", &truth_jet1_phi);
      tree->Branch(prefix + "truth_jet1_m", &truth_jet1_m);
      tree->Branch(prefix + "truth_jet2_pt", &truth_jet2_pt);
      tree->Branch(prefix + "truth_jet2_eta", &truth_jet2_eta);
      tree->Branch(prefix + "truth_jet2_phi", &truth_jet2_phi);
      tree->Branch(prefix + "truth_jet2_m", &truth_jet2_m);

      tree->Branch(prefix + "truth_ph1_pt", &truth_ph1_pt);
      tree->Branch(prefix + "truth_ph1_eta", &truth_ph1_eta);
      tree->Branch(prefix + "truth_ph1_phi", &truth_ph1_phi);
      tree->Branch(prefix + "truth_ph1_type", &truth_ph1_type);

      tree->Branch(prefix + "truth_V_pt", &truth_V_pt);
      tree->Branch(prefix + "truth_V_eta", &truth_V_eta);
      tree->Branch(prefix + "truth_V_phi", &truth_V_phi);
      tree->Branch(prefix + "truth_V_m", &truth_V_m);

      tree->Branch(prefix + "truth_mu_pt", &truth_mu_pt);
      tree->Branch(prefix + "truth_mu_eta", &truth_mu_eta);
      tree->Branch(prefix + "truth_mu_phi", &truth_mu_phi);
      tree->Branch(prefix + "truth_mu_m", &truth_mu_m);
      tree->Branch(prefix + "truth_mu_status", &truth_mu_status);

      tree->Branch(prefix + "evsf_baseline_nominal_EL", &evsf_baseline_nominal_EL);
      tree->Branch(prefix + "evsf_baseline_nominal_MU", &evsf_baseline_nominal_MU);
      tree->Branch(prefix + "evsf_baseline_nominal_PH", &evsf_baseline_nominal_PH);

      for (auto &kv : evsf_baseline_syst_EL) {
         tree->Branch(prefix + "evsf_baseline0_syst_" + kv.first, &kv.second);
      }
      for (auto &kv : evsf_baseline_syst_MU) {
         tree->Branch(prefix + "evsf_baseline0_syst_" + kv.first, &kv.second);
      }
      for (auto &kv : evsf_baseline_syst_PH) {
         tree->Branch(prefix + "evsf_baseline0_syst_" + kv.first, &kv.second);
      }


   }

   return;
}

void Analysis::OutputEvent::setTriggers(const std::vector<TString> &trigs)
{
   for (auto &trigName : trigs) {
      trigger[trigName] = 0;
      //Info("OutputEvent", "adding trigger %s", trigName.Data());
   }
   trigger_pass = 0;
}

void Analysis::OutputEvent::setWeights(const std::vector<TString> &names)
{
   for (auto &weightName : names) {
      weights[weightName] = 0;
      //Info("OutputEvent", "adding weight %s", weightName.Data());
   }
}

