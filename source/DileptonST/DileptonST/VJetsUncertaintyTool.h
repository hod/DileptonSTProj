#ifndef VJETS_UNCERTAINTY_TOOL_H
#define VJETS_UNCERTAINTY_TOOL_H

// author: Remi Zaidan

#include <TString.h>
#include <map>
#include <vector>

class TH1;

namespace DileptonST {
   class VJetsUncertaintyTool {

   public:

      VJetsUncertaintyTool();
      ~VJetsUncertaintyTool();

      void setInputFileName(TString fname);
      void applyEWCorrection(bool doApply, TString processes = "eej,evj,vvj,aj");
      void applyQCDCorrection(bool doApply, TString processes = "eej,evj,vvj,aj");
      int initialize();

      const std::vector<TString> &getAllVariationNames();

      double getCorrection(int mcChannelNumber, double pTV, TString variation = "Nominal");

   private:

      bool m_initialized;
      TString m_inputName;
      std::map<TString, TH1*> m_histoMap;
      std::vector<TString> m_variations;
      std::map<TString, bool> m_applyEWCorrection;
      std::map<TString, bool> m_applyQCDCorrection;
   };
}

#endif
