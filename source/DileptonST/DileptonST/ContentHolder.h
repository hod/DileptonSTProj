#ifndef DileptonST_ContentHolder_H
#define DileptonST_ContentHolder_H

#include <memory>
#include <iostream> // needed by bug in ShallowCopy header
#ifndef __CINT__
#include "xAODCore/ShallowCopy.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#endif

namespace Analysis {
   class ContentHolder {
      // a very simple class containing pointers to xAOD containers etc.
      // this is used for dealing wisely with systematic variations
      // (each systematics corresponds to a list of pointers to containers,
      // but not all of them are to be updated w.r.t. nominal)
      //
      // the class owns all objects EXCEPT jets
   public:
      Bool_t doMuons;
      Bool_t doElectrons;
      Bool_t doJets;
      Bool_t doPhotons;
      Bool_t doOverlapRemoval;
      Bool_t doMET;
      bool doTaus;

      Bool_t ownJets;
#ifndef __CINT__
      const xAOD::EventInfo* eventInfo; //! // we do not own it!!!
      const xAOD::VertexContainer* vertices; //! // we do not own it!!!
      xAOD::MuonContainer* muons; //!
      xAOD::ShallowAuxContainer* muonsAux; //!
      xAOD::ElectronContainer* electrons; //!
      xAOD::ShallowAuxContainer* electronsAux; //!
      xAOD::JetContainer* jets; //!
      xAOD::ShallowAuxContainer* jetsAux; //!
      xAOD::PhotonContainer* photons; //!
      xAOD::ShallowAuxContainer* photonsAux; //!
      xAOD::TauJetContainer* taus; //!
      xAOD::ShallowAuxContainer* tausAux; //!
      std::shared_ptr<xAOD::MissingETContainer> met_nophcalib_cst_nomuon; //!
      std::shared_ptr<xAOD::MissingETAuxContainer> met_nophcalib_cst_nomuonAux; //!
      std::shared_ptr<xAOD::MissingETContainer> met_nophcalib_tst_nomuon; //!
      std::shared_ptr<xAOD::MissingETAuxContainer> met_nophcalib_tst_nomuonAux; //!
      std::shared_ptr<xAOD::MissingETContainer> met_nophcalib_cst_wmuon; //!
      std::shared_ptr<xAOD::MissingETAuxContainer> met_nophcalib_cst_wmuonAux; //!
      std::shared_ptr<xAOD::MissingETContainer> met_nophcalib_tst_wmuon; //!
      std::shared_ptr<xAOD::MissingETAuxContainer> met_nophcalib_tst_wmuonAux; //!
      std::shared_ptr<xAOD::MissingETContainer> met_nophcalib_cst_noelectron; //!
      std::shared_ptr<xAOD::MissingETAuxContainer> met_nophcalib_cst_noelectronAux; //!
      std::shared_ptr<xAOD::MissingETContainer> met_nophcalib_tst_noelectron; //!
      std::shared_ptr<xAOD::MissingETAuxContainer> met_nophcalib_tst_noelectronAux; //!
      std::shared_ptr<xAOD::MissingETContainer> met_cst_nomuon; //!
      std::shared_ptr<xAOD::MissingETAuxContainer> met_cst_nomuonAux; //!
      std::shared_ptr<xAOD::MissingETContainer> met_tst_nomuon; //!
      std::shared_ptr<xAOD::MissingETAuxContainer> met_tst_nomuonAux; //!
      std::shared_ptr<xAOD::MissingETContainer> met_cst_wmuon; //!
      std::shared_ptr<xAOD::MissingETAuxContainer> met_cst_wmuonAux; //!
      std::shared_ptr<xAOD::MissingETContainer> met_tst_wmuon; //!
      std::shared_ptr<xAOD::MissingETAuxContainer> met_tst_wmuonAux; //!
      std::shared_ptr<xAOD::MissingETContainer> met_cst_noelectron; //!
      std::shared_ptr<xAOD::MissingETAuxContainer> met_cst_noelectronAux; //!
      std::shared_ptr<xAOD::MissingETContainer> met_tst_noelectron; //!
      std::shared_ptr<xAOD::MissingETAuxContainer> met_tst_noelectronAux; //!
      std::shared_ptr<xAOD::MissingETContainer> met_cst_nophoton; //!
      std::shared_ptr<xAOD::MissingETAuxContainer> met_cst_nophotonAux; //!
      std::shared_ptr<xAOD::MissingETContainer> met_tst_nophoton; //!
      std::shared_ptr<xAOD::MissingETAuxContainer> met_tst_nophotonAux; //!
      std::shared_ptr<xAOD::MissingETContainer> met_track; //!
      std::shared_ptr<xAOD::MissingETAuxContainer> met_trackAux; //!
      const xAOD::MissingETContainer* met_truth; //! // we do not own it!!!

      // good objects
      xAOD::MuonContainer allMuons; //!
      xAOD::MuonContainer baselineMuons; //!
      xAOD::MuonContainer goodMuons; //!
      xAOD::ElectronContainer allElectrons; //!
      xAOD::ElectronContainer baselineElectrons; //!
      xAOD::ElectronContainer goodElectrons; //!
      xAOD::JetContainer allJets; //!
      xAOD::JetContainer goodJets; //!
      xAOD::PhotonContainer allPhotons; //!
      xAOD::PhotonContainer baselinePhotons; //!
      xAOD::PhotonContainer goodPhotons; //!
      xAOD::TauJetContainer allTaus; //!
      xAOD::TauJetContainer baselineTaus; //!
      xAOD::TauJetContainer goodTaus; //!
#endif

      void deleteStuff();
   };
}
#endif
