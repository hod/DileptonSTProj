#include <TSystem.h>
#include <TPRegexp.h>

#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopGrid/PrunDriver.h"
#include "EventLoop/ProofDriver.h"

#include "SampleHandler/Sample.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/DiskListLocal.h"
#include "SampleHandler/DiskListEOS.h"

#include "xAODRootAccess/Init.h"

#include "DileptonST/DileptonNTUPMaker.h"

#include <stdexcept>

using namespace std;

int main(int argc, char* argv[])
{
   const TString errMess = TString::Format("%s is DEPRECATED, please use DileptonST/python/monojetSubmit.py instead", argv[0]);
   throw std::logic_error(errMess.Data());

   // Take the submit directory from the input if provided:
   TString submitDir = "submitDir";
   if (argc > 1) submitDir = argv[ 1 ];

   // Possibly run on the grid over a given sample
   const Bool_t gridSamples(argc > 2);

   // Set up the job for xAOD access:
   xAOD::Init().ignore();

   // Construct the samples to run on:
   SH::SampleHandler sh;

   if (!gridSamples) {
      //const char* inputFilePath = gSystem->ExpandPathName("$ALRB_TutorialData/r5591");  // using $ALRB_TutorialData previously defined
      //SH::DiskListLocal list(inputFilePath);
      //SH::scanDir(sh, list, "AOD.01494882._113691.pool.root.1");  // specifying one particular file for testing
      //SH::scanDir(sh, "/tmp/vippolit/mc14_8TeV.167760.Sherpa_CT10_ZnunuMassiveCBPt0_CVetoBVeto.merge.AOD.e1587_s1933_s1911_r5591_r5625_tid01512523_00");
      //SH::scanDir(sh, "/n/atlasfs/atlasdata/vippolit/xAOD/signal");
      //SH::scanDir(sh, "/tmp/vippolit/eos/atlas/user/v/vippolit/DM_production");
      //SH::readFileList(sh, "D5_pt150_13TeV", "input_D5_pt150.txt");
      //SH::readFileList(sh, "D5_pt400_13TeV", "input_D5_pt400.txt");
      //SH::readFileList(sh, "D5_pt600_13TeV", "input_D5_pt600.txt");
      //SH::readFileList(sh, "test", "input_test.txt");
      //SH::scanDir(sh, "/tmp/vippolit/files_to_test");
      //SH::scanDir(sh, "/tmp/vippolit/sampleT_new");
//    SH::readFileList(sh, "data_SUSY9", "test_data_SUSY9.txt");
      SH::readFileList(sh, "mc_EXOT5", "test_mc_EXOT5.txt");
      SH::readFileList(sh, "mc_SUSY1", "test_mc_SUSY1.txt");
//    SH::DiskListEOS list("/eos/atlas/user/v/vippolit/DM_production", "root://eosatlas//eos/atlas/user/v/vippolit/DM_production");
//    SH::scanDir(sh, list);
   } else {
      for (Int_t i = 2; i < argc; i++) {
         SH::scanRucio(sh, argv[i]);
      }
   }

   // Set the name of the input TTree. It's always "CollectionTree"
   // for xAOD files.
   sh.setMetaString("nc_tree", "CollectionTree");

   // Print what we found:
   sh.print();

   // Create an EventLoop job:
   EL::Job job;
   job.sampleHandler(sh);

   // Add our analysis to the job:
   DileptonNTUPMaker* alg = new DileptonNTUPMaker();
   alg->doSystematics = (!submitDir.Contains("noSyst"));
   alg->ptSkim = 150000;
   alg->ptSkimForSyst = 150000;
   alg->metSkim = 150000;
   alg->metSkimForSyst = 150000;
   job.algsAdd(alg);

   // set sample options
   sh.setMetaString("isData", "NO");
   sh.setMetaString("isAFII", "NO");

   for (UInt_t i = 0; i < sh.size(); i++) {
      SH::Sample *sample = sh.at(i);
      TString samplename(sample->name());

      // isData
      Bool_t isData(kFALSE);
      //TPRegexp re_data("data\\d\\d");
      TPRegexp re_data("data");
      if (samplename.Contains(re_data)) {
         cout << "Sample " << samplename << " is recognized as DATA" << endl;
         sample->meta()->setString("isData", "YES");
         isData = kTRUE;
      } else {
         cout << "Sample " << samplename << " is recognized as SIMULATION" << endl;
         sample->meta()->setString("isData", "NO");
         isData = kFALSE;
      }

      // isAFII
      TPRegexp re_af2("a\\d\\d\\d");
      if (samplename.Contains(re_af2) || samplename.Contains("af2")) {
         cout << "Sample " << samplename << " is recognized as AFII" << endl;
         if (isData) throw std::logic_error(TString::Format("Data and atlas fast are orthogonal things, please doublecheck %s", argv[0]).Data());
         sample->meta()->setString("isAFII", "YES");
      } else {
         cout << "Sample " << samplename << " is recognized as FULLSIM" << endl;
         sample->meta()->setString("isAFII", "NO");
      }

   }

   // set access mode
   job.options()->setString(EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_branch);

   // limit number of events
   //job.options()->setDouble(EL::Job::optMaxEvents, 200);

   // Run the job using the local/direct driver:
   if (!gridSamples) {
      EL::DirectDriver driver;
      //EL::ProofDriver driver;
      //driver.numWorkers = 8;

      driver.submit(job, submitDir.Data());
   } else {
      EL::PrunDriver driver;
      driver.options()->setDouble("nc_skipScout", 1);
      driver.options()->setString("nc_outputSampleName", ("user.vippolit." + submitDir + ".%in:name[1]%.%in:name[2]%.%in:name[3]%.%in:name[5]%").Data());
      driver.submitOnly(job, submitDir.Data());
   }

   return 0;
}

