#!/bin/sh

### -------> OBSOLETE
### check the release number here:
### https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisRelease
### OBSOLETE -------->

### see some instructions
### https://atlassoftwaredocs.web.cern.ch/ABtutorial/release_setup/

setupATLAS

mkdir -p source build run

cd source

#asetup 21.2.5,AnalysisBase
asetup 21.2,AnalysisBase,latest

cd -

source build/$CMTCONFIG/setup.sh
