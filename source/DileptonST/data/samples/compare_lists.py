# script to compare dataset lists
# Valerio Ippolito - Harvard University

import re

def getList(fname):
  regexp = re.compile('^\w+\.(\d+).\w+.*')

  result = []

  with open(fname) as f:
    for lineraw in f.readlines():
      line = lineraw.rstrip('\n')

      m = regexp.match(line)

      if m:
        dsid = int(m.group(1))
#       print dsid

        if dsid in result:
          raise RuntimeError('You have two datasets named %d in %s' % (dsid, fname))

        result.append(dsid)

  return result

        


if __name__ == '__main__':

  fnames = [
  'data15_EXOT5_grlCovered.txt',
  'data15_SUSY1_grlCovered.txt',
  'data16_EXOT5_grlCovered.txt',
  'data16_SUSY1_grlCovered.txt',
  'mc15c_backgrounds_EXOT5.txt',
  'mc15c_backgrounds_SUSY1.txt',
  'mc15c_signals_EXOT5.txt',
  'mc15c_signals_SUSY1.txt',
  ]

  dsids = {}
  for fname in fnames:
    dsids[fname] = sorted(getList(fname))

  only_in_SUSY1 = {}
  only_in_EXOT5 = {}

  only_in_SUSY1['data15_grlCovered'] = filter(lambda x: x not in dsids['data15_EXOT5_grlCovered.txt'], dsids['data15_SUSY1_grlCovered.txt'])
  only_in_EXOT5['data15_grlCovered'] = filter(lambda x: x not in dsids['data15_SUSY1_grlCovered.txt'], dsids['data15_EXOT5_grlCovered.txt'])
  only_in_SUSY1['data16_grlCovered'] = filter(lambda x: x not in dsids['data16_EXOT5_grlCovered.txt'], dsids['data16_SUSY1_grlCovered.txt'])
  only_in_EXOT5['data16_grlCovered'] = filter(lambda x: x not in dsids['data16_SUSY1_grlCovered.txt'], dsids['data16_EXOT5_grlCovered.txt'])
  only_in_EXOT5['mc15c_signals'] = filter(lambda x: x not in dsids['mc15c_signals_SUSY1.txt'], dsids['mc15c_signals_EXOT5.txt'])
  only_in_SUSY1['mc15c_signals'] = filter(lambda x: x not in dsids['mc15c_signals_EXOT5.txt'], dsids['mc15c_signals_SUSY1.txt'])
  only_in_EXOT5['mc15c_backgrounds'] = filter(lambda x: x not in dsids['mc15c_backgrounds_SUSY1.txt'], dsids['mc15c_backgrounds_EXOT5.txt'])
  only_in_SUSY1['mc15c_backgrounds'] = filter(lambda x: x not in dsids['mc15c_backgrounds_EXOT5.txt'], dsids['mc15c_backgrounds_SUSY1.txt'])

  print '\n\nOnly in SUSY1:'
  for where in only_in_SUSY1:
    wherePrinted = False
    for dsid in only_in_SUSY1[where]:
      if not wherePrinted:
        print where
        wherePrinted = True
      print '  ', dsid

  print '\n\nOnly in EXOT5:'
  for where in only_in_EXOT5:
    wherePrinted = False
    for dsid in only_in_EXOT5[where]:
      if not wherePrinted:
        print where
        wherePrinted = True
      print '  ', dsid
