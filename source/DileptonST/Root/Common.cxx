#include "CxxUtils/fpcompare.h"
#include "DileptonST/Common.h"
#include "xAODBase/IParticle.h"

namespace DileptonST {
   Bool_t comparePt(const xAOD::IParticle *a, const xAOD::IParticle *b)
   {
      return CxxUtils::fpcompare::greater(a->pt(), b->pt());
   }
}
