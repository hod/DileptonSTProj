#!/bin/env python

# submission script for DileptonST jobs
# Valerio Ippolito - Harvard University

import re

def configureSamples(sh):
  from ROOT import SH
  # configure SampleHandler metadata
  sh.setMetaString('nc_tree', 'CollectionTree')
  sh.setMetaString('isAFII', 'NO')
  sh.setMetaString('isData', 'NO')

  # now, make sensible exceptions (letting us know about whether
  # SampleHandler believes these are 8/13 TeV AFII/FULLSIM samples)
  regexps = { 'isAFII' : re.compile('a\d\d\d'),
              'isData': re.compile('data'),
  }

  for isample in xrange(sh.size()):
    sample = sh.at(isample)

    for opt,this_re in regexps.items():
      state = 'NO'
      if this_re.search(sample.name()):
        state = 'YES'

      sample.setMetaString(opt, state)
      print 'Sample {name} has {opt} set to {val}'.format(name=sample.name(), opt=opt, val=state)


if __name__ == '__main__':
  allowed_drivers = ['local', 'prun', 'proof', 'slurm']
  allowed_algos = ['DileptonNTUPMaker', 'DileptonNTUPMakerTruth']

  from argparse import ArgumentParser
  import os
  parser = ArgumentParser(description='launch DileptonST jobs', add_help=True)

  # submission
  parser.add_argument('-w', '--where', type=str, dest='where', choices=allowed_drivers, help='driver to be used (local, prun, proof, slurm)', metavar='where')
  parser.add_argument('-t', '--inputList', type=str, dest='txt', help='comma-separated list of txt files, one per sample, containing file paths', metavar='list', default='')
  parser.add_argument('-g', '--inputDatasets', type=str, dest='dq2', help='comma-separated list of input DQ2 datasets', metavar='list', default='')
  parser.add_argument('-l', '--inputDatasetLists', type=str, dest='dq2list', help='comma-separated list of txt files containing one DQ2 sample per line (empty lines or lines starting with # are ignored)', metavar='list', default='')
  parser.add_argument('-d', '--inputDirs', type=str, dest='dir', help='comma-separated list of input directories (they will be scanned for samples, one per subdirectory)', metavar='list', default='')
  parser.add_argument('-n', '--name', type=str, dest='submitDir', help='name of the local directory to use for output (and, if applicable, prefix of the output DQ2 datasets)', metavar='submitDir', default='submitDir')
  parser.add_argument('-a', '--algo', type=str, dest='algoName', choices=allowed_algos, help='algorithm name (e.g. DileptonNTUPMaker)', metavar='algoName', default='DileptonNTUPMaker')
  parser.add_argument('-m', '--nEvtsMax', type=int, dest='nEvtsMax', help='maximum number of events to process', metavar='nEvtsMax', default=-1)
  parser.add_argument('-s', '--nSkipEvts', type=int, dest='nSkipEvts', help='skip number of events', metavar='nSkipEvts', default=-1)
  parser.add_argument('-u', '--user', type=str, dest='userName', help='username for grid jobs', metavar='userName', default=os.environ['USER'])
  parser.add_argument('-rs', '--replicationSite', type=str, dest='replicationSite', help='name of disk where to replicate output of grid jobs', metavar='replicationSite', default=None)
  #parser.add_argument('-f', '--additional-flags', type=str, dest='additionalFlags', help='additional commands for prun / batch submission', metavar='cmd', default='--bexec="unset ROOT_TTREECACHE_SIZE"')

  # algo
  parser.add_argument('--doSyst', dest='doSystematics', help='also do systematic variations', action='store_true', default=False)
  parser.add_argument('--doAllHists', dest='doAllHists', help='make all the distribution plots', action='store_true', default=False)
  parser.add_argument('--doIgnorePhotons', dest='doIgnorePhotons', help='ignore the photon definitions', action='store_true', default=False)
  parser.add_argument('--doSystTrees', dest='doSystTrees', help='write the systematic TTrees', action='store_true', default=False)
  parser.add_argument('--dirtyJets', dest='dirtyJets', help='no Loose cleaning', action='store_true', default=False)
  parser.add_argument('--doPreOR', dest='doPreOR', help='save also objects before OR (big ntuples!)', action='store_true', default=False)
  parser.add_argument('--configFile', type=str, dest='configFile', help='name of the config file to use', metavar='configFile', default='SUSYTools_Dilepton.config')
  parser.add_argument('--ptSkim', type=int, dest='ptSkim', help='leading jet pt skim (nominal tree), in MeV', metavar='cut', default=150000)
  parser.add_argument('--ptSkimForSyst', type=int, dest='ptSkimForSyst', help='leading jet pt skim (systematics), in MeV', metavar='cut', default=150000)
  parser.add_argument('--metSkim', type=int, dest='metSkim', help='MET skim (nominal tree), in MeV', metavar='cut', default=150000)
  parser.add_argument('--metSkimForSyst', type=int, dest='metSkimForSyst', help='MET skim (systematics), in MeV', metavar='cut', default=150000)
  parser.add_argument('--phptSkim', type=int, dest='phptSkim', help='leading photon pt skim (nominal tree), in MeV', metavar='cut', default=150000)
  parser.add_argument('--phptSkimForSyst', type=int, dest='phptSkimForSyst', help='leading photon pt skim (systematics), in MeV', metavar='cut', default=150000)

  options = parser.parse_args()

  import ROOT
  ROOT.gROOT.SetBatch(True)
  ROOT.gROOT.ProcessLine(ROOT.gSystem.ExpandPathName('.x $ROOTCOREDIR/scripts/load_packages.C'))
  from ROOT import xAOD
  from ROOT import SH
  from ROOT import EL

  ### checks
  if (options.where == None):
    parser.error('Empty driver')
  if (options.userName == ''):
    parser.error('Empty user name')
  if (options.txt == '' and options.dq2 == '' and options.dq2list == '' and options.dir == ''): 
    parser.error('Empty input list')

  ### set up the job for xAOD access
  xAOD.Init().ignore()

  ### crash on unchecked status codes
  #ROOT.xAOD.TReturnCode.enableFailure()

  ###
  # determine input samples
  sh = SH.SampleHandler()

  getlist = lambda x: filter(lambda y: y != '', x.replace(' ', '').split(','))

  txts = getlist(options.txt)
  for txt in txts:
    sampleName = ''.join(txt.split('/')[-1].split('.')[:-1]) # /a/b/c/d.txt -> d
    print 'adding txt ',sampleName
    SH.readFileList(sh, sampleName, txt)

  dq2s = getlist(options.dq2)
  for dq2 in dq2s:
    print 'adding dq2 ',dq2
    SH.scanRucio(sh, dq2)

  dq2lists = getlist(options.dq2list)
  for dq2list in dq2lists:
    print 'adding dq2list ',dq2list
    for dq2raw in open(dq2list).readlines():
      dq2 = dq2raw.rstrip('\n').replace(' ', '') # remove spaces from line
      if dq2.startswith('#') == False and dq2 != '': # ignore comments / separators
        SH.scanRucio(sh, dq2)

  dirs = getlist(options.dir)
  for dir in dirs:
    print 'adding dir ',dir
    SH.scanDir(sh, dir)

  configureSamples(sh)
  sh.setMetaString("nc_grid_filter", "*AOD*");
    
  # print SampleHandler object (need the getattr trick to access the method 'print')
  getattr(sh, 'print')()


  ###
  # create an EventLoop job
  job = EL.Job()
  job.sampleHandler(sh)

  # add the algorithm we asked to the job
  # (we use getattr to make it possible to use any
  # algorithm, e.g. DileptonNTUPMakerTruth, DileptonNTUPMaker...)
  alg = getattr(ROOT, options.algoName)()

  # set algorithm options
  alg.configFile = options.configFile
  alg.doSystematics = options.doSystematics
  alg.doAllHists = options.doAllHists
  alg.doSystTrees = options.doSystTrees
  alg.doIgnorePhotons = options.doIgnorePhotons
  alg.dirtyJets = options.dirtyJets
  alg.doPreOR = options.doPreOR
  alg.ptSkim = options.ptSkim
  alg.ptSkimForSyst = options.ptSkimForSyst
  alg.metSkim = options.metSkim
  alg.metSkimForSyst = options.metSkimForSyst
  alg.phptSkim = options.phptSkim
  alg.phptSkimForSyst = options.phptSkimForSyst
  job.algsAdd(alg)

  # make sure we can read trigger decision
  job.options().setString(EL.Job.optXaodAccessMode, EL.Job.optXaodAccessMode_class)

  # limit number of events
  if options.nEvtsMax > 0:
    job.options().setDouble(EL.Job.optMaxEvents, options.nEvtsMax);
    print 'Jobs will process maximum {n} events'.format(n=options.nEvtsMax)

  # limit number of events
  if options.nSkipEvts > 0:
    job.options().setDouble(EL.Job.optSkipEvents, options.nSkipEvts);
    print 'Jobs will skip {n} events'.format(n=options.nSkipEvts)

  ###
  # run
  driver = None
  submitDir = options.submitDir
  if (options.where == 'local'):
#   if options.additionalFlags != None:
#     raise RuntimeError('Additional flags not supported for DirectDriver')
    driver = EL.DirectDriver()
    driver.submit(job, submitDir)
  elif (options.where == 'prun'):
    out = EL.OutputStream("outputLabel", "xAOD")
    out.options().setString(EL.OutputStream.optMergeCmd, "XAOD_ACCESSTRACER_FRACTION=0 xAODMerge -b") 
    dset_name_mask = 'user.{user}.{tag}.%in:name[1]%.%in:name[2]%.%in:name[3]%'.format(user=options.userName, tag=submitDir)
    print dset_name_mask, len(dset_name_mask)
    #if len(dset_name_mask > )
    driver = EL.PrunDriver()
    #driver.options().setDouble('nc_skipScout', 1) 
    driver.options().setString(EL.Job.optGridNGBPerJob, '5')
    driver.options().setString('nc_optGridNfilesPerJob', '5')
    driver.options().setString('nc_outputSampleName', dset_name_mask)
    if options.replicationSite !=None: 
      driver.options().setString('nc_destSE', options.replicationSite)
#   if options.additionalFlags != None:
#     driver.options().setString(EL.Job.optSubmitFlags, options.additionalFlags)
    driver.submitOnly(job, submitDir)
  elif (options.where == 'proof'):
    driver = EL.ProofDriver()
    driver.numWorkers = 8
#   if options.additionalFlags != None:
#     driver.options().setString(EL.Job.optSubmitFlags, options.additionalFlags)
    driver.submit(job, submitDir)
  elif (options.where == 'slurm'):
    driver = EL.SlurmDriver()
    driver.SetJobName('mj')
    driver.SetAccount('hepl')
    driver.SetPartition('pleiades')
    driver.SetRunTime('0-10:00') # 10h
    driver.SetMemory('2000')
    driver.SetConstrain('amd|intel')
    driver.submit(job, submitDir)
  else:
    raise RuntimeError('Unrecognized driver option {opt}'.format(opt=options.where))
