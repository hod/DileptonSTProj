#include "DileptonST/OutputTruth.h"

#include <TTree.h>

Analysis::OutputTruth::OutputTruth()
{
   reset();
}

Analysis::OutputTruth::~OutputTruth()
{
}

void Analysis::OutputTruth::reset()
{
   //////////
   // event variables
   //////////
   pu_hash = -9999;
   averageIntPerXing = -9999;
   pu_weight = -9999;
   mconly_weight = -9999;
   mconly_weights.clear();
   overlap_weight = -9999;
   pdf_x1 = -9999;
   pdf_x2 = -9999;
   pdf_pdf1 = -9999;
   pdf_pdf2 = -9999;
   pdf_scale = -9999;
   //////////
   // composite observables (involve >1 objects)
   //////////
   shatR = -9999;
   gaminvR = -9999;
   gaminvRp1 = -9999;
   dphi_BETA_R = -9999;
   dphi_J1_J2_R = -9999;
   gamma_Rp1 = -9999;
   costhetaR = -9999;
   dphi_R_Rp1 = -9999;
   mdeltaR = -9999;
   cosptR = -9999;
   costhetaRp1 = -9999;
   munu_mT = -9999;
   enu_mT = -9999;
   jj_m = -9999;
   mumu_m = -9999;
   mumu_pt = -9999;
   mumu_eta = -9999;
   mumu_phi = -9999;
   ee_m = -9999;
   ee_pt = -9999;
   ee_eta = -9999;
   ee_phi = -9999;
   truth_dm_pt = -9999;
   truth_dm_q = -9999;
   truth_dmjet_q = -9999;
   //////////
   // met
   //////////
   met_truth_et = -9999;
   met_truth_etx = -9999;
   met_truth_ety = -9999;
   met_truth_sumet = -9999;
   //////////
   // muons
   //////////
   mu_charge.clear();
   mu_pt.clear();
   mu_eta.clear();
   mu_phi.clear();
   mu_m.clear();
   mu_met_truth_dphi.clear();
   //////////
   // vetoed electrons
   //////////
   el_charge.clear();
   el_pt.clear();
   el_eta.clear();
   el_phi.clear();
   el_m.clear();
   el_met_truth_dphi.clear();
   //////////
   // jets
   //////////
   jet_pt.clear();
   jet_eta.clear();
   jet_phi.clear();
   jet_m.clear();
   jet_met_truth_dphi.clear();
   jet_PartonTruthLabelID.clear();
   jet_ConeTruthLabelID.clear();
   jet_TruthLabelDeltaR_B.clear();
   jet_TruthLabelDeltaR_C.clear();
   jet_TruthLabelDeltaR_T.clear();
   //////////
   // hemispheres, or mega-jets
   //////////
   hem1_pt = -9999;
   hem1_eta = -9999;
   hem1_phi = -9999;
   hem1_m = -9999;
   //
   hem2_pt = -9999;
   hem2_eta = -9999;
   hem2_phi = -9999;
   hem2_m = -9999;

   //////////
   // SR/CR flags
   //////////
   last = -9999; // referred to SR
   //////////
   // event variables
   //////////
   run = -9999;
   event = -9999;
   lbn = -9999;
   bcid = -9999;
   n_jet = -9999; // with good objects
   n_el = -9999; // with good objects
   n_mu = -9999; // with good objects
   pdf_id1 = -9999;
   pdf_id2 = -9999;
}

void Analysis::OutputTruth::attachToTree(TTree *tree, TString prefix)
{
   if (prefix != "") prefix = prefix + "_"; // add _ if needed

   //////////
   // event variables
   //////////
   tree->Branch(prefix + "pu_hash", &pu_hash);
   tree->Branch(prefix + "averageIntPerXing", &averageIntPerXing);
   tree->Branch(prefix + "pu_weight", &pu_weight);
   tree->Branch(prefix + "mconly_weight", &mconly_weight);
   tree->Branch(prefix + "mconly_weights", &mconly_weights);
   tree->Branch(prefix + "overlap_weight", &overlap_weight);
   tree->Branch(prefix + "pdf_x1", &pdf_x1);
   tree->Branch(prefix + "pdf_x2", &pdf_x2);
   tree->Branch(prefix + "pdf_pdf1", &pdf_pdf1);
   tree->Branch(prefix + "pdf_pdf2", &pdf_pdf2);
   tree->Branch(prefix + "pdf_scale", &pdf_scale);
   //////////
   // composite observables (involve >1 objects)
   //////////
   tree->Branch(prefix + "shatR", &shatR);
   tree->Branch(prefix + "gaminvR", &gaminvR);
   tree->Branch(prefix + "gaminvRp1", &gaminvRp1);
   tree->Branch(prefix + "dphi_BETA_R", &dphi_BETA_R);
   tree->Branch(prefix + "dphi_J1_J2_R", &dphi_J1_J2_R);
   tree->Branch(prefix + "gamma_Rp1", &gamma_Rp1);
   tree->Branch(prefix + "costhetaR", &costhetaR);
   tree->Branch(prefix + "dphi_R_Rp1", &dphi_R_Rp1);
   tree->Branch(prefix + "mdeltaR", &mdeltaR);
   tree->Branch(prefix + "cosptR", &cosptR);
   tree->Branch(prefix + "costhetaRp1", &costhetaRp1);
   tree->Branch(prefix + "munu_mT", &munu_mT);
   tree->Branch(prefix + "enu_mT", &enu_mT);
   tree->Branch(prefix + "jj_m", &jj_m);
   tree->Branch(prefix + "mumu_m", &mumu_m);
   tree->Branch(prefix + "mumu_pt", &mumu_pt);
   tree->Branch(prefix + "mumu_eta", &mumu_eta);
   tree->Branch(prefix + "mumu_phi", &mumu_phi);
   tree->Branch(prefix + "ee_m", &ee_m);
   tree->Branch(prefix + "ee_pt", &ee_pt);
   tree->Branch(prefix + "ee_eta", &ee_eta);
   tree->Branch(prefix + "ee_phi", &ee_phi);
   tree->Branch(prefix + "truth_dm_pt", &truth_dm_pt);
   tree->Branch(prefix + "truth_dm_q", &truth_dm_q);
   tree->Branch(prefix + "truth_dmjet_q", &truth_dmjet_q);
   //////////
   // met
   //////////
   tree->Branch(prefix + "met_truth_et", &met_truth_et);
   tree->Branch(prefix + "met_truth_etx", &met_truth_etx);
   tree->Branch(prefix + "met_truth_ety", &met_truth_ety);
   tree->Branch(prefix + "met_truth_sumet", &met_truth_sumet);
   //////////
   // muons
   //////////
   tree->Branch(prefix + "mu_charge", &mu_charge);
   tree->Branch(prefix + "mu_pt", &mu_pt);
   tree->Branch(prefix + "mu_eta", &mu_eta);
   tree->Branch(prefix + "mu_phi", &mu_phi);
   tree->Branch(prefix + "mu_m", &mu_m);
   tree->Branch(prefix + "mu_met_truth_dphi", &mu_met_truth_dphi);
   //////////
   // vetoed electrons
   //////////
   tree->Branch(prefix + "el_charge", &el_charge);
   tree->Branch(prefix + "el_pt", &el_pt);
   tree->Branch(prefix + "el_eta", &el_eta);
   tree->Branch(prefix + "el_phi", &el_phi);
   tree->Branch(prefix + "el_m", &el_m);
   tree->Branch(prefix + "el_met_truth_dphi", &el_met_truth_dphi);
   //////////
   // jets
   //////////
   tree->Branch(prefix + "jet_pt", &jet_pt);
   tree->Branch(prefix + "jet_eta", &jet_eta);
   tree->Branch(prefix + "jet_phi", &jet_phi);
   tree->Branch(prefix + "jet_m", &jet_m);
   tree->Branch(prefix + "jet_met_truth_dphi", &jet_met_truth_dphi);
   tree->Branch(prefix + "jet_PartonTruthLabelID", &jet_PartonTruthLabelID);
   tree->Branch(prefix + "jet_ConeTruthLabelID", &jet_ConeTruthLabelID);
   tree->Branch(prefix + "jet_TruthLabelDeltaR_B", &jet_TruthLabelDeltaR_B);
   tree->Branch(prefix + "jet_TruthLabelDeltaR_C", &jet_TruthLabelDeltaR_C);
   tree->Branch(prefix + "jet_TruthLabelDeltaR_T", &jet_TruthLabelDeltaR_T);
   //////////
   // hemispheres, or mega-jets
   //////////
   tree->Branch(prefix + "hem1_pt", &hem1_pt);
   tree->Branch(prefix + "hem1_eta", &hem1_eta);
   tree->Branch(prefix + "hem1_phi", &hem1_phi);
   tree->Branch(prefix + "hem1_m", &hem1_m);
   //
   tree->Branch(prefix + "hem2_pt", &hem2_pt);
   tree->Branch(prefix + "hem2_eta", &hem2_eta);
   tree->Branch(prefix + "hem2_phi", &hem2_phi);
   tree->Branch(prefix + "hem2_m", &hem2_m);

   //////////
   // SR/CR flags
   //////////
   tree->Branch(prefix + "last", &last); // referred to SR
   //////////
   // event variables
   //////////
   tree->Branch(prefix + "run", &run);
   tree->Branch(prefix + "event", &event);
   tree->Branch(prefix + "lbn", &lbn);
   tree->Branch(prefix + "bcid", &bcid);
   tree->Branch(prefix + "n_jet", &n_jet); // with good objects
   tree->Branch(prefix + "n_el", &n_el); // with good objects
   tree->Branch(prefix + "n_mu", &n_mu); // with good objects
   tree->Branch(prefix + "pdf_id1", &pdf_id1);
   tree->Branch(prefix + "pdf_id2", &pdf_id2);
}
