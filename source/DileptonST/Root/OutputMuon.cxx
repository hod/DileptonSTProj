#include "DileptonST/OutputMuon.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include <TTree.h>

Analysis::OutputMuon::OutputMuon(Bool_t doTrim) : Analysis::OutputObject::OutputObject(doTrim)
{
   reset();
}

Analysis::OutputMuon::~OutputMuon()
{
}

void Analysis::OutputMuon::reset()
{
   charge.clear();
   SF.clear();
   SF_iso.clear();
   pt.clear();
   ptErr.clear();
   eta.clear();
   phi.clear();
   m.clear();
   e.clear();
   id_pt.clear();
   id_ptErr.clear();
   id_eta.clear();
   id_phi.clear();
   id_m.clear();
   id_qop.clear();
   id_qoperr.clear();
   ms_pt.clear();
   ms_ptErr.clear();
   ms_eta.clear();
   ms_phi.clear();
   ms_m.clear();
   ms_qop.clear();
   ms_qoperr.clear();
   me_pt.clear();
   me_ptErr.clear();
   me_eta.clear();
   me_phi.clear();
   me_m.clear();
   me_qop.clear();
   me_qoperr.clear();
   mse_pt.clear();
   mse_ptErr.clear();
   mse_eta.clear();
   mse_phi.clear();
   mse_m.clear();
   mse_qop.clear();
   mse_qoperr.clear();
   ptcone20.clear();
   ptvarcone20.clear();
   etcone20.clear();
   topoetcone20.clear();
   ptcone30.clear();
   ptvarcone30.clear();
   etcone30.clear();
   topoetcone30.clear();
   ptcone40.clear();
   ptvarcone40.clear();
   etcone40.clear();
   topoetcone40.clear();
   d0.clear();
   d0sig.clear();
   z0.clear();
   z0sig.clear();
   author.clear();
   quality.clear();
   isSA.clear();
   isST.clear();
   isCB.clear();
   isTight.clear();
   isMedium.clear();
   isLoose.clear();
   isVeryLoose.clear();
   isHighPt.clear();
   isBad.clear();
   isBadHighPt.clear();
   finalFitPt.clear();
   finalFitPtErr.clear();
   finalFitEta.clear();
   finalFitPhi.clear();
   finalFitQOverP.clear();
   finalFitQOverPErr.clear();
   truthEta.clear();
   truthPhi.clear();
   truthPt.clear();
   truthTheta.clear();
   truthQOverP.clear();
   isotool_pass_loosetrackonly.clear();
   isotool_pass_loose.clear();
   isotool_pass_gradient.clear();
   isotool_pass_gradientloose.clear();
   // isotool_pass_fixedcuttight.clear();
   isotool_pass_fixedcuttighttrackonly.clear();
   met_nomuon_dphi.clear();
   met_wmuon_dphi.clear();

   truth_origin.clear();
   truth_type.clear();


   // TODO!!! NOAM -->
   muonType.clear();
   momentumBalanceSignificance.clear();
   CaloLRLikelihood.clear();
   CaloMuonIDTag.clear();
   numberOfPrecisionLayers.clear();
   numberOfPrecisionHoleLayers.clear();
   numberOfGoodPrecisionLayers.clear();

   innerSmallHits.clear();
   innerLargeHits.clear();
   innerSmallHoles.clear();
   innerLargeHoles.clear();
   middleSmallHoles.clear();
   middleLargeHoles.clear();
   outerSmallHoles.clear();
   outerLargeHoles.clear();
   extendedSmallHoles.clear();
   extendedLargeHoles.clear();
   innerClosePrecisionHits.clear();
   middleClosePrecisionHits.clear();
   outerClosePrecisionHits.clear();
   extendedClosePrecisionHits.clear();
   innerOutBoundsPrecisionHits.clear();
   middleOutBoundsPrecisionHits.clear();
   outerOutBoundsPrecisionHits.clear();
   extendedOutBoundsPrecisionHits.clear();
   combinedTrackOutBoundsPrecisionHits.clear();
   middleLargeHits.clear();
   middleSmallHits.clear();
   outerLargeHits.clear();
   outerSmallHits.clear();
   extendedSmallHits.clear();
   extendedLargeHits.clear();

   numberOfPixelHits.clear();
   numberOfPixelDeadSensors.clear();
   numberOfSCTHits.clear();
   numberOfSCTDeadSensors.clear();
   numberOfPixelHoles.clear();
   numberOfSCTHoles.clear();
   numberOfTRTHits.clear();
   numberOfTRTOutliers.clear();
   primarytrketa.clear();
   numberOfInnermostPixelLayerHits.clear();
   cbtrketa.clear();

   // cscEtaHits.clear();
   // cscUnspoiledEtaHits.clear();

   spectrometerFieldIntegral.clear();
   scatteringCurvatureSignificance.clear();
   scatteringNeighbourSignificance.clear();
   allAuthors.clear();
   numberOfPhiLayers.clear();
   numberOfPhiHoleLayers.clear();
   numberOfTriggerEtaLayers.clear();
   numberOfTriggerEtaHoleLayers.clear();
   nUnspoiledCscHits.clear();
   FSR_CandidateEnergy.clear();
   EnergyLoss.clear();
   EnergyLossSigma.clear();
   ParamEnergyLoss.clear();
   ParamEnergyLossSigmaPlus.clear();
   ParamEnergyLossSigmaMinus.clear();
   MeasEnergyLoss.clear();
   MeasEnergyLossSigma.clear();
   msInnerMatchChi2.clear();
   msInnerMatchDOF.clear();
   energyLossType.clear();
   // primarySector.clear();
   // secondarySector.clear();
   phiLayer1Hits.clear();
   phiLayer2Hits.clear();
   phiLayer3Hits.clear();
   phiLayer4Hits.clear();
   etaLayer1Hits.clear();
   etaLayer2Hits.clear();
   etaLayer3Hits.clear();
   etaLayer4Hits.clear();
   phiLayer1Holes.clear();
   phiLayer2Holes.clear();
   phiLayer3Holes.clear();
   phiLayer4Holes.clear();
   etaLayer1Holes.clear();
   etaLayer2Holes.clear();
   etaLayer3Holes.clear();
   etaLayer4Holes.clear();
   phiLayer1RPCHits.clear();
   phiLayer2RPCHits.clear();
   phiLayer3RPCHits.clear();
   // phiLayer4RPCHits.clear();
   etaLayer1RPCHits.clear();
   etaLayer2RPCHits.clear();
   etaLayer3RPCHits.clear();
   // etaLayer4RPCHits.clear();
   phiLayer1RPCHoles.clear();
   phiLayer2RPCHoles.clear();
   phiLayer3RPCHoles.clear();
   // phiLayer4RPCHoles.clear();
   etaLayer1RPCHoles.clear();
   etaLayer2RPCHoles.clear();
   etaLayer3RPCHoles.clear();
   // etaLayer4RPCHoles.clear();
   phiLayer1TGCHits.clear();
   phiLayer2TGCHits.clear();
   phiLayer3TGCHits.clear();
   phiLayer4TGCHits.clear();
   etaLayer1TGCHits .clear();
   etaLayer2TGCHits.clear();
   etaLayer3TGCHits.clear();
   etaLayer4TGCHits.clear();
   phiLayer1TGCHoles.clear();
   phiLayer2TGCHoles.clear();
   phiLayer3TGCHoles.clear();
   phiLayer4TGCHoles.clear();
   etaLayer1TGCHoles.clear();
   etaLayer2TGCHoles.clear();
   etaLayer3TGCHoles.clear();
   etaLayer4TGCHoles.clear();
   segmentDeltaEta.clear();
   segmentDeltaPhi.clear();
   segmentChi2OverDoF.clear();
   t0.clear();
   beta.clear();
   // annBarrel.clear();
   // annEndCap.clear();
   // innAngle.clear();
   // midAngle.clear();
   meanDeltaADCCountsMDT.clear();
   msOuterMatchDOF.clear();


   return;
}

void Analysis::OutputMuon::attachToTree(TTree *tree)
{
   const TString prefix = name() + "_";

   if (write()) {
      tree->Branch(prefix + "pt", &pt);
      tree->Branch(prefix + "ptErr", &ptErr);
      tree->Branch(prefix + "eta", &eta);
      tree->Branch(prefix + "phi", &phi);
      tree->Branch(prefix + "SF", &SF);
      tree->Branch(prefix + "SF_iso", &SF_iso);
      tree->Branch(prefix + "isotool_pass_loosetrackonly", &isotool_pass_loosetrackonly);
      tree->Branch(prefix + "isotool_pass_fixedcuttighttrackonly", &isotool_pass_fixedcuttighttrackonly);

      //Mono-b
      tree->Branch(prefix + "m", &m);
      tree->Branch(prefix + "e", &e);
      tree->Branch(prefix + "d0", &d0);
      tree->Branch(prefix + "d0sig", &d0sig);
      tree->Branch(prefix + "z0", &z0);
      tree->Branch(prefix + "z0sig", &z0sig);

      if (!doTrim()) {

         tree->Branch(prefix + "charge", &charge);
         tree->Branch(prefix + "id_pt", &id_pt);
         tree->Branch(prefix + "id_ptErr", &id_ptErr);
         tree->Branch(prefix + "id_eta", &id_eta);
         tree->Branch(prefix + "id_phi", &id_phi);
         tree->Branch(prefix + "id_m", &id_m);
         tree->Branch(prefix + "id_qop", &id_qop);
         tree->Branch(prefix + "id_qoperr", &id_qoperr);
         tree->Branch(prefix + "ms_pt", &ms_pt);
         tree->Branch(prefix + "ms_ptErr", &ms_ptErr);
         tree->Branch(prefix + "ms_eta", &ms_eta);
         tree->Branch(prefix + "ms_phi", &ms_phi);
         tree->Branch(prefix + "ms_m", &ms_m);
         tree->Branch(prefix + "ms_qop", &ms_qop);
         tree->Branch(prefix + "ms_qoperr", &ms_qoperr);
         tree->Branch(prefix + "me_pt", &me_pt);
         tree->Branch(prefix + "me_ptErr", &me_ptErr);
         tree->Branch(prefix + "me_eta", &me_eta);
         tree->Branch(prefix + "me_phi", &me_phi);
         tree->Branch(prefix + "me_m", &me_m);
         tree->Branch(prefix + "me_qop", &me_qop);
         tree->Branch(prefix + "me_qoperr", &me_qoperr);
         tree->Branch(prefix + "mse_pt", &mse_pt);
         tree->Branch(prefix + "mse_ptErr", &mse_ptErr);
         tree->Branch(prefix + "mse_eta", &mse_eta);
         tree->Branch(prefix + "mse_phi", &mse_phi);
         tree->Branch(prefix + "mse_m", &mse_m);
         tree->Branch(prefix + "mse_qop", &mse_qop);
         tree->Branch(prefix + "mse_qoperr", &mse_qoperr);
         tree->Branch(prefix + "ptcone20", &ptcone20);
         tree->Branch(prefix + "ptvarcone20", &ptvarcone20);
         tree->Branch(prefix + "etcone20", &etcone20);
         tree->Branch(prefix + "topoetcone20", &topoetcone20);
         tree->Branch(prefix + "ptcone30", &ptcone30);
         tree->Branch(prefix + "ptvarcone30", &ptvarcone30);
         tree->Branch(prefix + "etcone30", &etcone30);
         tree->Branch(prefix + "topoetcone30", &topoetcone30);
         tree->Branch(prefix + "ptcone40", &ptcone40);
         tree->Branch(prefix + "ptvarcone40", &ptvarcone40);
         tree->Branch(prefix + "etcone40", &etcone40);
         tree->Branch(prefix + "topoetcone40", &topoetcone40);
         tree->Branch(prefix + "author", &author);
         tree->Branch(prefix + "quality", &quality);
         tree->Branch(prefix + "isSA", &isSA);
         tree->Branch(prefix + "isST", &isST);
         tree->Branch(prefix + "isCB", &isCB);
         tree->Branch(prefix + "isTight", &isTight);
         tree->Branch(prefix + "isMedium", &isMedium);
         tree->Branch(prefix + "isLoose", &isLoose);
         tree->Branch(prefix + "isVeryLoose", &isVeryLoose);
         tree->Branch(prefix + "isHighPt", &isHighPt);
         tree->Branch(prefix + "isBad", &isBad);
         tree->Branch(prefix + "isBadHighPt", &isBadHighPt);
         tree->Branch(prefix + "finalFitPt", &finalFitPt);
         tree->Branch(prefix + "finalFitPtErr", &finalFitPtErr);
         tree->Branch(prefix + "finalFitEta", &finalFitEta);
         tree->Branch(prefix + "finalFitPhi", &finalFitPhi);
         tree->Branch(prefix + "finalFitQOverP", &finalFitQOverP);
         tree->Branch(prefix + "finalFitQOverPErr", &finalFitQOverPErr);
         tree->Branch(prefix + "truthEta", &truthEta);
	 tree->Branch(prefix + "truthPhi", &truthPhi);
	 tree->Branch(prefix + "truthPt", &truthPt);
	 tree->Branch(prefix + "truthTheta", &truthTheta);
	 tree->Branch(prefix + "truthQOverP", &truthQOverP);

         tree->Branch(prefix + "isotool_pass_loose", &isotool_pass_loose);
         tree->Branch(prefix + "isotool_pass_gradient", &isotool_pass_gradient);
         tree->Branch(prefix + "isotool_pass_gradientloose", &isotool_pass_gradientloose);
         // tree->Branch(prefix + "isotool_pass_fixedcuttight", &isotool_pass_fixedcuttight);
         tree->Branch(prefix + "met_nomuon_dphi", &met_nomuon_dphi);
         tree->Branch(prefix + "met_wmuon_dphi", &met_wmuon_dphi);

         tree->Branch(prefix + "truth_origin", &truth_origin);
         tree->Branch(prefix + "truth_type", &truth_type);

         
         tree->Branch(prefix + "muonType", &muonType);
         tree->Branch(prefix + "momentumBalanceSignificance", &momentumBalanceSignificance);
         tree->Branch(prefix + "CaloLRLikelihood", &CaloLRLikelihood);
         tree->Branch(prefix + "CaloMuonIDTag", &CaloMuonIDTag);
         tree->Branch(prefix + "numberOfPrecisionLayers", &numberOfPrecisionLayers);
         tree->Branch(prefix + "numberOfPrecisionHoleLayers", &numberOfPrecisionHoleLayers);
         tree->Branch(prefix + "numberOfGoodPrecisionLayers", &numberOfGoodPrecisionLayers);

         tree->Branch(prefix + "innerSmallHits", &innerSmallHits);
         tree->Branch(prefix + "innerLargeHits", &innerLargeHits);
         tree->Branch(prefix + "innerSmallHoles", &innerSmallHoles);
         tree->Branch(prefix + "innerLargeHoles", &innerLargeHoles);
         tree->Branch(prefix + "middleSmallHoles", &middleSmallHoles);
         tree->Branch(prefix + "middleLargeHoles", &middleLargeHoles);
         tree->Branch(prefix + "outerSmallHoles", &outerSmallHoles);
         tree->Branch(prefix + "outerLargeHoles", &outerLargeHoles);
         tree->Branch(prefix + "extendedSmallHoles", &extendedSmallHoles);
         tree->Branch(prefix + "extendedLargeHoles", &extendedLargeHoles);
         tree->Branch(prefix + "innerClosePrecisionHits", &innerClosePrecisionHits);
         tree->Branch(prefix + "middleClosePrecisionHits", &middleClosePrecisionHits);
         tree->Branch(prefix + "outerClosePrecisionHits", &outerClosePrecisionHits);
         tree->Branch(prefix + "extendedClosePrecisionHits", &extendedClosePrecisionHits);
         tree->Branch(prefix + "innerOutBoundsPrecisionHits", &innerOutBoundsPrecisionHits);
         tree->Branch(prefix + "middleOutBoundsPrecisionHits", &middleOutBoundsPrecisionHits);
         tree->Branch(prefix + "outerOutBoundsPrecisionHits", &outerOutBoundsPrecisionHits);
         tree->Branch(prefix + "extendedOutBoundsPrecisionHits", &extendedOutBoundsPrecisionHits);
         tree->Branch(prefix + "combinedTrackOutBoundsPrecisionHits", &combinedTrackOutBoundsPrecisionHits);
         tree->Branch(prefix + "middleLargeHits", &middleLargeHits);
         tree->Branch(prefix + "middleSmallHits", &middleSmallHits);
         tree->Branch(prefix + "outerLargeHits", &outerLargeHits);
         tree->Branch(prefix + "outerSmallHits", &outerSmallHits);
         tree->Branch(prefix + "extendedSmallHits", &extendedSmallHits);
         tree->Branch(prefix + "extendedLargeHits", &extendedLargeHits);

         tree->Branch(prefix + "numberOfPixelHits", &numberOfPixelHits);
         tree->Branch(prefix + "numberOfPixelDeadSensors", &numberOfPixelDeadSensors);
         tree->Branch(prefix + "numberOfSCTHits", &numberOfSCTHits);
         tree->Branch(prefix + "numberOfSCTDeadSensors", &numberOfSCTDeadSensors);
         tree->Branch(prefix + "numberOfPixelHoles", &numberOfPixelHoles);
         tree->Branch(prefix + "numberOfSCTHoles", &numberOfSCTHoles);
         tree->Branch(prefix + "numberOfTRTHits", &numberOfTRTHits);
         tree->Branch(prefix + "numberOfTRTOutliers", &numberOfTRTOutliers);
         tree->Branch(prefix + "primarytrketa", &primarytrketa);
         tree->Branch(prefix + "numberOfInnermostPixelLayerHits", &numberOfInnermostPixelLayerHits);
         tree->Branch(prefix + "cbtrketa", &cbtrketa);
        

         // tree->Branch(prefix + "cscEtaHits", &cscEtaHits);
         // tree->Branch(prefix + "cscUnspoiledEtaHits", &cscUnspoiledEtaHits);
         tree->Branch(prefix + "spectrometerFieldIntegral", &spectrometerFieldIntegral);
         tree->Branch(prefix + "scatteringCurvatureSignificance", &scatteringCurvatureSignificance);
         tree->Branch(prefix + "scatteringNeighbourSignificance", &scatteringNeighbourSignificance);
         tree->Branch(prefix + "allAuthors", &allAuthors);
         tree->Branch(prefix + "numberOfPhiLayers", &numberOfPhiLayers);
         tree->Branch(prefix + "numberOfPhiHoleLayers", &numberOfPhiHoleLayers);
         tree->Branch(prefix + "numberOfTriggerEtaLayers", &numberOfTriggerEtaLayers);
         tree->Branch(prefix + "numberOfTriggerEtaHoleLayers", &numberOfTriggerEtaHoleLayers);
         tree->Branch(prefix + "nUnspoiledCscHits", &nUnspoiledCscHits);
         tree->Branch(prefix + "FSR_CandidateEnergy", &FSR_CandidateEnergy);
         tree->Branch(prefix + "EnergyLoss", &EnergyLoss);
         tree->Branch(prefix + "EnergyLossSigma", &EnergyLossSigma);
         tree->Branch(prefix + "ParamEnergyLoss", &ParamEnergyLoss);
         tree->Branch(prefix + "ParamEnergyLossSigmaPlus", &ParamEnergyLossSigmaPlus);
         tree->Branch(prefix + "ParamEnergyLossSigmaMinus", &ParamEnergyLossSigmaMinus);
         tree->Branch(prefix + "MeasEnergyLoss", &MeasEnergyLoss);
         tree->Branch(prefix + "MeasEnergyLossSigma", &MeasEnergyLossSigma);
         tree->Branch(prefix + "msInnerMatchChi2", &msInnerMatchChi2);
         tree->Branch(prefix + "msInnerMatchDOF", &msInnerMatchDOF);
         tree->Branch(prefix + "energyLossType", &energyLossType);
         // tree->Branch(prefix + "primarySector", &primarySector);
         // tree->Branch(prefix + "secondarySector", &secondarySector);
         tree->Branch(prefix + "phiLayer1Hits", &phiLayer1Hits);
         tree->Branch(prefix + "phiLayer2Hits", &phiLayer2Hits);
         tree->Branch(prefix + "phiLayer3Hits", &phiLayer3Hits);
         tree->Branch(prefix + "phiLayer4Hits", &phiLayer4Hits);
         tree->Branch(prefix + "etaLayer1Hits", &etaLayer1Hits);
         tree->Branch(prefix + "etaLayer2Hits", &etaLayer2Hits);
         tree->Branch(prefix + "etaLayer3Hits", &etaLayer3Hits);
         tree->Branch(prefix + "etaLayer4Hits", &etaLayer4Hits);
         tree->Branch(prefix + "phiLayer1Holes", &phiLayer1Holes);
         tree->Branch(prefix + "phiLayer2Holes", &phiLayer2Holes);
         tree->Branch(prefix + "phiLayer3Holes", &phiLayer3Holes);
         tree->Branch(prefix + "phiLayer4Holes", &phiLayer4Holes);
         tree->Branch(prefix + "etaLayer1Holes", &etaLayer1Holes);
         tree->Branch(prefix + "etaLayer2Holes", &etaLayer2Holes);
         tree->Branch(prefix + "etaLayer3Holes", &etaLayer3Holes);
         tree->Branch(prefix + "etaLayer4Holes", &etaLayer4Holes);
         tree->Branch(prefix + "phiLayer1RPCHits", &phiLayer1RPCHits);
         tree->Branch(prefix + "phiLayer2RPCHits", &phiLayer2RPCHits);
         tree->Branch(prefix + "phiLayer3RPCHits", &phiLayer3RPCHits);
         // tree->Branch(prefix + "phiLayer4RPCHits", &phiLayer4RPCHits);
         tree->Branch(prefix + "etaLayer1RPCHits", &etaLayer1RPCHits);
         tree->Branch(prefix + "etaLayer2RPCHits", &etaLayer2RPCHits);
         tree->Branch(prefix + "etaLayer3RPCHits", &etaLayer3RPCHits);
         // tree->Branch(prefix + "etaLayer4RPCHits", &etaLayer4RPCHits);
         tree->Branch(prefix + "phiLayer1RPCHoles", &phiLayer1RPCHoles);
         tree->Branch(prefix + "phiLayer2RPCHoles", &phiLayer2RPCHoles);
         tree->Branch(prefix + "phiLayer3RPCHoles", &phiLayer3RPCHoles);
         // tree->Branch(prefix + "phiLayer4RPCHoles", &phiLayer4RPCHoles);
         tree->Branch(prefix + "etaLayer1RPCHoles", &etaLayer1RPCHoles);
         tree->Branch(prefix + "etaLayer2RPCHoles", &etaLayer2RPCHoles);
         tree->Branch(prefix + "etaLayer3RPCHoles", &etaLayer3RPCHoles);
         // tree->Branch(prefix + "etaLayer4RPCHoles", &etaLayer4RPCHoles);
         tree->Branch(prefix + "phiLayer1TGCHits", &phiLayer1TGCHits);
         tree->Branch(prefix + "phiLayer2TGCHits", &phiLayer2TGCHits);
         tree->Branch(prefix + "phiLayer3TGCHits", &phiLayer3TGCHits);
         tree->Branch(prefix + "phiLayer4TGCHits", &phiLayer4TGCHits);
         tree->Branch(prefix + "etaLayer1TGCHits", &etaLayer1TGCHits);
         tree->Branch(prefix + "etaLayer2TGCHits", &etaLayer2TGCHits);
         tree->Branch(prefix + "etaLayer3TGCHits", &etaLayer3TGCHits);
         tree->Branch(prefix + "etaLayer4TGCHits", &etaLayer4TGCHits);
         tree->Branch(prefix + "phiLayer1TGCHoles", &phiLayer1TGCHoles);
         tree->Branch(prefix + "phiLayer2TGCHoles", &phiLayer2TGCHoles);
         tree->Branch(prefix + "phiLayer3TGCHoles", &phiLayer3TGCHoles);
         tree->Branch(prefix + "phiLayer4TGCHoles", &phiLayer4TGCHoles);
         tree->Branch(prefix + "etaLayer1TGCHoles", &etaLayer1TGCHoles);
         tree->Branch(prefix + "etaLayer2TGCHoles", &etaLayer2TGCHoles);
         tree->Branch(prefix + "etaLayer3TGCHoles", &etaLayer3TGCHoles);
         tree->Branch(prefix + "etaLayer4TGCHoles", &etaLayer4TGCHoles);
         tree->Branch(prefix + "segmentDeltaEta", &segmentDeltaEta);
         tree->Branch(prefix + "segmentDeltaPhi", &segmentDeltaPhi);
         tree->Branch(prefix + "segmentChi2OverDoF", &segmentChi2OverDoF);
         tree->Branch(prefix + "t0", &t0);
         tree->Branch(prefix + "beta", &beta);
         // tree->Branch(prefix + "annBarrel", &annBarrel);
         // tree->Branch(prefix + "annEndCap", &annEndCap);
         // tree->Branch(prefix + "innAngle", &innAngle);
         // tree->Branch(prefix + "midAngle", &midAngle);
         tree->Branch(prefix + "meanDeltaADCCountsMDT", &meanDeltaADCCountsMDT);
         tree->Branch(prefix + "msOuterMatchDOF", &msOuterMatchDOF);
      }
   }

   return;
}

void Analysis::OutputMuon::add(const xAOD::Muon &input)
{
   // TODO!!! NOAM -->
   uint8_t ui_tmpval = 0;

   author.push_back((Int_t)input.author());
   quality.push_back(input.quality());
   isSA.push_back((input.muonType() == xAOD::Muon::MuonType::MuonStandAlone));
   isST.push_back((input.muonType() == xAOD::Muon::MuonType::SegmentTagged));
   isCB.push_back((input.muonType() == xAOD::Muon::MuonType::Combined));
   isTight.push_back((input.quality() == xAOD::Muon::Tight));
   isMedium.push_back((input.quality() == xAOD::Muon::Medium));
   isLoose.push_back((input.quality() == xAOD::Muon::Loose));
   isVeryLoose.push_back((input.quality() == xAOD::Muon::VeryLoose));
   charge.push_back(input.charge());
   pt.push_back(input.pt());
   ElementLink<xAOD::TrackParticleContainer> cblink = input.auxdata<ElementLink<xAOD::TrackParticleContainer> >("combinedTrackParticleLink");
   if(cblink.isValid())
   {
      const xAOD::TrackParticle* cbTrack = (*cblink);
      bool iscov = xAOD::TrackingHelpers::hasValidCovQoverP(cbTrack);
      if(cbTrack && iscov) ptErr.push_back( xAOD::TrackingHelpers::pTErr( cbTrack ) );
      else                 ptErr.push_back( -999 );
   }
   else ptErr.push_back( -999 );
   eta.push_back(input.eta());
   phi.push_back(input.phi());
   m.push_back(input.m());
   e.push_back(input.e());
   const xAOD::TrackParticle *thisTrack = input.trackParticle(xAOD::Muon::InnerDetectorTrackParticle);
   if (thisTrack) {
      id_pt.push_back(thisTrack->pt());
      bool iscov = xAOD::TrackingHelpers::hasValidCovQoverP(thisTrack);
      if(iscov) id_ptErr.push_back( xAOD::TrackingHelpers::pTErr(thisTrack) );
      else      id_ptErr.push_back( -999. );
      id_eta.push_back(thisTrack->eta());
      id_phi.push_back(thisTrack->phi());
      id_theta.push_back(thisTrack->theta());
      id_m.push_back(thisTrack->m());
      id_qop.push_back(thisTrack->qOverP());
      id_qoperr.push_back(sqrt(thisTrack->definingParametersCovMatrix()(4,4)));
   }
   else {
      id_pt.push_back(-999);
      id_ptErr.push_back(-999);
      id_eta.push_back(-999);
      id_phi.push_back(-999);
      id_theta.push_back(-999);
      id_m.push_back(-999);
      id_qop.push_back(-999);
      id_qoperr.push_back(-999);
   }
   thisTrack = input.trackParticle(xAOD::Muon::MuonSpectrometerTrackParticle);
   if (thisTrack) {
      ms_pt.push_back(thisTrack->pt());
      bool iscov = xAOD::TrackingHelpers::hasValidCovQoverP(thisTrack);
      if(iscov) ms_ptErr.push_back( xAOD::TrackingHelpers::pTErr(thisTrack) );
      else      ms_ptErr.push_back( -999. );
      ms_eta.push_back(thisTrack->eta());
      ms_phi.push_back(thisTrack->phi());
      ms_theta.push_back(thisTrack->theta());
      ms_m.push_back(thisTrack->m());
      ms_qop.push_back(thisTrack->qOverP());
      ms_qoperr.push_back(sqrt(thisTrack->definingParametersCovMatrix()(4,4)));
   }
   else {
      ms_pt.push_back(-999);
      ms_ptErr.push_back(-999);
      ms_eta.push_back(-999);
      ms_phi.push_back(-999);
      ms_theta.push_back(-999);
      ms_m.push_back(-999);
      ms_qop.push_back(-999);
      ms_qoperr.push_back(-999);
   }
   thisTrack = input.trackParticle(xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle);
   if (thisTrack) {
      me_pt.push_back(thisTrack->pt());
      bool iscov = xAOD::TrackingHelpers::hasValidCovQoverP(thisTrack);
      if(iscov) me_ptErr.push_back( xAOD::TrackingHelpers::pTErr(thisTrack) );
      else      me_ptErr.push_back( -999. );
      me_eta.push_back(thisTrack->eta());
      me_phi.push_back(thisTrack->phi());
      me_theta.push_back(thisTrack->theta());
      me_m.push_back(thisTrack->m());
      me_qop.push_back(thisTrack->qOverP());
      me_qoperr.push_back(sqrt(thisTrack->definingParametersCovMatrix()(4,4)));
   }
   else {
      me_pt.push_back(-999);
      me_ptErr.push_back(-999);
      me_eta.push_back(-999);
      me_phi.push_back(-999);
      me_theta.push_back(-999);
      me_m.push_back(-999);
      me_qop.push_back(-999);
      me_qoperr.push_back(-999);
   }
   thisTrack = input.trackParticle(xAOD::Muon::MSOnlyExtrapolatedMuonSpectrometerTrackParticle);
   if (thisTrack) {
      mse_pt.push_back(thisTrack->pt());
      bool iscov = xAOD::TrackingHelpers::hasValidCovQoverP(thisTrack);
      if(iscov) mse_ptErr.push_back( xAOD::TrackingHelpers::pTErr(thisTrack) );
      else      mse_ptErr.push_back( -999. );
      mse_eta.push_back(thisTrack->eta());
      mse_phi.push_back(thisTrack->phi());
      mse_theta.push_back(thisTrack->theta());
      mse_m.push_back(thisTrack->m());
      mse_qop.push_back(thisTrack->qOverP());
      mse_qoperr.push_back(sqrt(thisTrack->definingParametersCovMatrix()(4,4)));
   }
   else {
      mse_pt.push_back(-999);
      mse_ptErr.push_back(-999);
      mse_eta.push_back(-999);
      mse_phi.push_back(-999);
      mse_theta.push_back(-999);
      mse_m.push_back(-999);
      mse_qop.push_back(-999);
      mse_qoperr.push_back(-999);
   }


   // decorations from SUSYTools
   static SG::AuxElement::ConstAccessor<char> acc_passedHighPtCuts("passedHighPtCuts");
   static SG::AuxElement::ConstAccessor<char> acc_isBad("bad");
   static SG::AuxElement::ConstAccessor<char> acc_isBadHighPt("bad_highPt");
   Int_t tmp_passedHighPtCuts(-999);
   Int_t tmp_isBad(-999);
   Int_t tmp_isBadHighPt(-999);
   try {
      tmp_passedHighPtCuts = acc_passedHighPtCuts(input);
      tmp_isBad            = acc_isBad(input);
      tmp_isBadHighPt      = acc_isBadHighPt(input);
   } catch (SG::ExcBadAuxVar) {
      tmp_passedHighPtCuts = -1;
      tmp_isBad            = -1;
      tmp_isBadHighPt      = -1;
   }
   isHighPt.push_back(tmp_passedHighPtCuts);
   isBad.push_back(tmp_isBad);
   isBadHighPt.push_back(tmp_isBadHighPt);

   ////////// Momentum from combined fit plus smearing (no stat comb)
   double muonFinalFitPt = -999;
   double muonFinalFitPtErr = -999;
   double muonFinalFitEta = -999;
   double muonFinalFitPhi = -999;
   double muonFinalFitQOverP = -999;
   double muonFinalFitQOverPErr = -999;
   if( input.quality() != 3  && input.primaryTrackParticle()) {
    const xAOD::TrackParticle* muTP = input.primaryTrackParticle();
    muonFinalFitPt = input.pt();
    muonFinalFitEta = input.eta();
    muonFinalFitPhi = input.phi();
    AmgVector(5) pars = muTP->definingParameters();;
    muonFinalFitQOverP = input.charge()*TMath::Sin(pars[3])/input.pt();
    AmgSymMatrix(5) cov = muTP->definingParametersCovMatrix();     
    muonFinalFitQOverPErr = sqrt(cov(4,4));
    if(xAOD::TrackingHelpers::hasValidCovQoverP(muTP) && muTP->definingParametersCovMatrixVec().size()>=15) muonFinalFitPtErr = xAOD::TrackingHelpers::pTErr(muTP);
   }
   finalFitPt.push_back( muonFinalFitPt );
   finalFitPtErr.push_back( muonFinalFitPtErr );
   finalFitEta.push_back( muonFinalFitEta );
   finalFitPhi.push_back( muonFinalFitPhi );
   finalFitQOverP.push_back( muonFinalFitQOverP );
   finalFitQOverPErr.push_back( muonFinalFitQOverPErr );


   const xAOD::TrackParticle* ptrk = input.primaryTrackParticle();
   if(ptrk)
   {
      ui_tmpval = 0; ptrk->summaryValue(ui_tmpval, xAOD::SummaryType::numberOfPixelHits); numberOfPixelHits.push_back( (int)ui_tmpval );
      ui_tmpval = 0; ptrk->summaryValue(ui_tmpval, xAOD::SummaryType::numberOfPixelDeadSensors); numberOfPixelDeadSensors.push_back( (int)ui_tmpval );
      ui_tmpval = 0; ptrk->summaryValue(ui_tmpval, xAOD::SummaryType::numberOfSCTHits); numberOfSCTHits.push_back( (int)ui_tmpval );
      ui_tmpval = 0; ptrk->summaryValue(ui_tmpval, xAOD::SummaryType::numberOfSCTDeadSensors); numberOfSCTDeadSensors.push_back( (int)ui_tmpval );
      ui_tmpval = 0; ptrk->summaryValue(ui_tmpval, xAOD::SummaryType::numberOfPixelHoles); numberOfPixelHoles.push_back( (int)ui_tmpval );
      ui_tmpval = 0; ptrk->summaryValue(ui_tmpval, xAOD::SummaryType::numberOfSCTHoles); numberOfSCTHoles.push_back( (int)ui_tmpval );
      ui_tmpval = 0; ptrk->summaryValue(ui_tmpval, xAOD::SummaryType::numberOfTRTHits); numberOfTRTHits.push_back( (int)ui_tmpval );
      ui_tmpval = 0; ptrk->summaryValue(ui_tmpval, xAOD::SummaryType::numberOfTRTOutliers); numberOfTRTOutliers.push_back( (int)ui_tmpval );
      primarytrketa.push_back( ptrk->eta() );
   }
   else
   {
      numberOfPixelHits.push_back(-999);
      numberOfPixelDeadSensors.push_back(-999);
      numberOfSCTHits.push_back(-999);
      numberOfSCTDeadSensors.push_back(-999);
      numberOfPixelHoles.push_back(-999);
      numberOfSCTHoles.push_back(-999);
      numberOfTRTHits.push_back(-999);
      numberOfTRTOutliers.push_back(-999);
      primarytrketa.push_back(-999);
   }
   const xAOD::TrackParticle* cbtrk = input.trackParticle( xAOD::Muon::CombinedTrackParticle );
   if(cbtrk)
   {
      ui_tmpval = 0; cbtrk->summaryValue(ui_tmpval, xAOD::SummaryType::numberOfInnermostPixelLayerHits); numberOfInnermostPixelLayerHits.push_back( (int)ui_tmpval );
      cbtrketa.push_back( cbtrk->eta() );
   }
   else
   {
      numberOfInnermostPixelLayerHits.push_back(-999);
      cbtrketa.push_back(-999);
   }


    //////// Momentum from truth
   float muonTruthPt = -999.;
   float muonTruthEta = -999.;
   float muonTruthPhi = -999.;
   float muonTruthTheta = -999.;
   float muonTruthQOverP = -999.;
   if(input.isAvailable<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink")) {
     ElementLink<xAOD::TruthParticleContainer> link = input.auxdata<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink");
     if(link.isValid())
     {
        const xAOD::TruthParticle* truthmu = (*link);
        if(truthmu) {
           muonTruthPt = truthmu->pt();
           muonTruthEta = truthmu->eta();
           muonTruthPhi = truthmu->phi();
           muonTruthTheta = truthmu->p4().Theta();
           muonTruthQOverP = truthmu->charge()/truthmu->p4().P();
        }
     }
   }
   truthEta.push_back(muonTruthEta);
   truthPhi.push_back(muonTruthPhi);
   truthPt.push_back(muonTruthPt);
   truthTheta.push_back(muonTruthTheta);
   truthQOverP.push_back(muonTruthQOverP);


   Float_t tmp_ptcone20(-9999);
   Float_t tmp_ptcone30(-9999);
   Float_t tmp_ptcone40(-9999);
   Float_t tmp_etcone20(-9999);
   Float_t tmp_etcone30(-9999);
   Float_t tmp_etcone40(-9999);
   Float_t tmp_topoetcone20(-9999);
   Float_t tmp_topoetcone30(-9999);
   Float_t tmp_topoetcone40(-9999);
   input.isolation(tmp_ptcone20, xAOD::Iso::IsolationType::ptcone20);
   input.isolation(tmp_ptcone30, xAOD::Iso::IsolationType::ptcone30);
   input.isolation(tmp_ptcone40, xAOD::Iso::IsolationType::ptcone40);
   input.isolation(tmp_etcone20, xAOD::Iso::IsolationType::etcone20);
   input.isolation(tmp_etcone30, xAOD::Iso::IsolationType::etcone30);
   input.isolation(tmp_etcone40, xAOD::Iso::IsolationType::etcone40);
   input.isolation(tmp_topoetcone20, xAOD::Iso::IsolationType::topoetcone20);
   input.isolation(tmp_topoetcone30, xAOD::Iso::IsolationType::topoetcone30);
   input.isolation(tmp_topoetcone40, xAOD::Iso::IsolationType::topoetcone40);
   ptcone20.push_back(tmp_ptcone20);
   ptcone30.push_back(tmp_ptcone30);
   ptcone40.push_back(tmp_ptcone40);
   etcone20.push_back(tmp_etcone20);
   etcone30.push_back(tmp_etcone30);
   etcone40.push_back(tmp_etcone40);
   topoetcone20.push_back(tmp_topoetcone20);
   topoetcone30.push_back(tmp_topoetcone30);
   topoetcone40.push_back(tmp_topoetcone40);


   static SG::AuxElement::ConstAccessor<float> acc_ptvarcone20("ptvarcone20");
   static SG::AuxElement::ConstAccessor<float> acc_ptvarcone30("ptvarcone30");
   static SG::AuxElement::ConstAccessor<float> acc_ptvarcone40("ptvarcone40");
   if (acc_ptvarcone20.isAvailable(input)) ptvarcone20.push_back(acc_ptvarcone20(input));
   if (acc_ptvarcone30.isAvailable(input)) ptvarcone30.push_back(acc_ptvarcone30(input));
   if (acc_ptvarcone40.isAvailable(input)) ptvarcone40.push_back(acc_ptvarcone40(input));

   thisTrack = input.primaryTrackParticle();

   static SG::AuxElement::ConstAccessor<float> acc_new_d0("new_d0");
   static SG::AuxElement::ConstAccessor<float> acc_new_d0sig("new_d0sig");
   static SG::AuxElement::ConstAccessor<float> acc_new_z0("new_z0");
   static SG::AuxElement::ConstAccessor<float> acc_new_z0sig("new_z0sig");
   try {
      d0.push_back(acc_new_d0(input));
      d0sig.push_back(acc_new_d0sig(input));
      z0.push_back(acc_new_z0(input));
      z0sig.push_back(acc_new_z0sig(input));
   } catch (SG::ExcBadAuxVar) {
      d0.push_back(-9999);
      d0sig.push_back(-9999);
      z0.push_back(-9999);
      z0sig.push_back(-9999);
   }


   // decorations we define in our analysis code
   static SG::AuxElement::ConstAccessor<char> acc_pass_loosetrackonly("pass_loosetrackonly");
   static SG::AuxElement::ConstAccessor<char> acc_pass_loose("pass_loose");
   static SG::AuxElement::ConstAccessor<char> acc_pass_gradient("pass_gradient");
   static SG::AuxElement::ConstAccessor<char> acc_pass_gradientloose("pass_gradientloose");
   static SG::AuxElement::ConstAccessor<char> acc_pass_fixedcuttight("pass_fixedcuttight");
   static SG::AuxElement::ConstAccessor<char> acc_pass_fixedcuttighttrackonly("pass_fixedcuttighttrackonly");
   try {
      isotool_pass_loosetrackonly.push_back(acc_pass_loosetrackonly(input));
      isotool_pass_loose.push_back(acc_pass_loose(input));
      isotool_pass_gradient.push_back(acc_pass_gradient(input));
      isotool_pass_gradientloose.push_back(acc_pass_gradientloose(input));
      // isotool_pass_fixedcuttight.push_back(acc_pass_fixedcuttight(input));
      isotool_pass_fixedcuttighttrackonly.push_back(acc_pass_fixedcuttighttrackonly(input));
   } catch (SG::ExcBadAuxVar) {
      isotool_pass_loosetrackonly.push_back(-9999);
      isotool_pass_loose.push_back(-9999);
      isotool_pass_gradient.push_back(-9999);
      isotool_pass_gradientloose.push_back(-9999);
      // isotool_pass_fixedcuttight.push_back(-9999);
      isotool_pass_fixedcuttighttrackonly.push_back(-9999);
   }

   // dphi's
   static SG::AuxElement::ConstAccessor<float> acc_new_met_nomuon_dphi("new_met_nomuon_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_wmuon_dphi("new_met_wmuon_dphi");
   try {
      met_nomuon_dphi.push_back(acc_new_met_nomuon_dphi(input));
      met_wmuon_dphi.push_back(acc_new_met_wmuon_dphi(input));
   } catch (SG::ExcBadAuxVar) {
      met_nomuon_dphi.push_back(-9999);
      met_wmuon_dphi.push_back(-9999);
   }

   // decorations from SUSYTools
   static SG::AuxElement::ConstAccessor<float> acc_lep_SF("lep_SF");
   static SG::AuxElement::ConstAccessor<float> acc_lep_iso_SF("lep_iso_SF");
   Float_t tmp_lep_SF(-9999);
   Float_t tmp_lep_iso_SF(-9999);
   try {
      tmp_lep_SF = acc_lep_SF(input);
      tmp_lep_iso_SF = acc_lep_iso_SF(input);
   } catch (SG::ExcBadAuxVar) {
      tmp_lep_SF = 1.0;
      tmp_lep_iso_SF = 1.0;
   }
   SF.push_back(tmp_lep_SF);
   SF_iso.push_back(tmp_lep_iso_SF);


   // MCTruthClassifier
   // from https://twiki.cern.ch/twiki/bin/view/Atlas/XAODMuon#How_to_retrieve_truth_type_and_o

   Int_t tp_type(-9999), tp_origin(-9999);

   static SG::AuxElement::ConstAccessor<int> acc_truthType("truthType");
   static SG::AuxElement::ConstAccessor<int> acc_truthOrigin("truthOrigin");

   try {
     tp_type = acc_truthType(input);
     tp_origin = acc_truthOrigin(input);
   } catch (SG::ExcBadAuxVar) {
     tp_type = -9999;
     tp_origin = -9999;
   }

   truth_type.push_back(tp_type);
   truth_origin.push_back(tp_origin);

   muonType.push_back( input.muonType() );
   allAuthors.push_back( input.allAuthors() );
   energyLossType.push_back( input.energyLossType() );

   momentumBalanceSignificance.push_back( input.floatParameter(xAOD::Muon::momentumBalanceSignificance) );
   scatteringCurvatureSignificance.push_back( input.floatParameter(xAOD::Muon::scatteringCurvatureSignificance) );
   scatteringNeighbourSignificance.push_back( input.floatParameter(xAOD::Muon::scatteringNeighbourSignificance) );
   CaloLRLikelihood.push_back( input.floatParameter(xAOD::Muon::CaloLRLikelihood) );
   CaloMuonIDTag.push_back( input.intParameter(xAOD::Muon::CaloMuonIDTag) );
   spectrometerFieldIntegral.push_back( input.floatParameter(xAOD::Muon::spectrometerFieldIntegral) );
   FSR_CandidateEnergy.push_back( input.floatParameter(xAOD::Muon::FSR_CandidateEnergy) );
   EnergyLoss.push_back( input.floatParameter(xAOD::Muon::EnergyLoss) );
   EnergyLossSigma.push_back( input.floatParameter(xAOD::Muon::EnergyLossSigma) );
   ParamEnergyLoss.push_back( input.floatParameter(xAOD::Muon::ParamEnergyLoss) );
   ParamEnergyLossSigmaPlus.push_back( input.floatParameter(xAOD::Muon::ParamEnergyLossSigmaPlus) );
   ParamEnergyLossSigmaMinus.push_back( input.floatParameter(xAOD::Muon::ParamEnergyLossSigmaMinus) );
   MeasEnergyLoss.push_back( input.floatParameter(xAOD::Muon::MeasEnergyLoss) );
   MeasEnergyLossSigma.push_back( input.floatParameter(xAOD::Muon::MeasEnergyLossSigma) );
   msInnerMatchChi2.push_back( input.floatParameter(xAOD::Muon::msInnerMatchChi2) );
   msInnerMatchDOF.push_back( input.intParameter(xAOD::Muon::msInnerMatchDOF) );
   segmentDeltaEta.push_back( input.floatParameter(xAOD::Muon::segmentDeltaEta) );
   segmentDeltaPhi.push_back( input.floatParameter(xAOD::Muon::segmentDeltaPhi) );
   segmentChi2OverDoF.push_back( input.floatParameter(xAOD::Muon::segmentChi2OverDoF) );
   t0.push_back( input.floatParameter(xAOD::Muon::t0) );
   beta.push_back( input.floatParameter(xAOD::Muon::beta) );
   meanDeltaADCCountsMDT.push_back( input.floatParameter(xAOD::Muon::meanDeltaADCCountsMDT) );
   msOuterMatchDOF.push_back( input.intParameter(xAOD::Muon::msOuterMatchDOF) );


   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::innerSmallHits); innerSmallHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::innerLargeHits);  innerLargeHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::innerSmallHoles);  innerSmallHoles.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::innerLargeHoles);  innerLargeHoles.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::middleSmallHoles);  middleSmallHoles.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::middleLargeHoles);  middleLargeHoles.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::outerSmallHoles);  outerSmallHoles.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::outerLargeHoles);  outerLargeHoles.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::extendedSmallHoles);  extendedSmallHoles.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::extendedLargeHoles);  extendedLargeHoles.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::innerClosePrecisionHits);  innerClosePrecisionHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::middleClosePrecisionHits);  middleClosePrecisionHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::outerClosePrecisionHits);  outerClosePrecisionHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::extendedClosePrecisionHits);  extendedClosePrecisionHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::innerOutBoundsPrecisionHits);  innerOutBoundsPrecisionHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::middleOutBoundsPrecisionHits);  middleOutBoundsPrecisionHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::outerOutBoundsPrecisionHits);  outerOutBoundsPrecisionHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::extendedOutBoundsPrecisionHits);  extendedOutBoundsPrecisionHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::combinedTrackOutBoundsPrecisionHits);  combinedTrackOutBoundsPrecisionHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::middleLargeHits);  middleLargeHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::middleSmallHits);  middleSmallHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::middleSmallHits);  outerLargeHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::outerSmallHits);  outerSmallHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::extendedSmallHits);  extendedSmallHits.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval, xAOD::MuonSummaryType::extendedLargeHits);  extendedLargeHits.push_back( (int)ui_tmpval );

   // cscEtaHits.push_back( input.isAvailable<int>("cscEtaHits") ? input.auxdata<int>("cscEtaHits") : -999 );
   // cscUnspoiledEtaHits.push_back( input.isAvailable<int>("cscUnspoiledEtaHits") ? input.auxdata<int>("cscUnspoiledEtaHits") : -999 );

   nUnspoiledCscHits.push_back( input.isAvailable<int>("nUnspoiledCscHits") ? input.auxdata<int>("nUnspoiledCscHits") : -999 );

   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::SummaryType::numberOfPrecisionLayers);      numberOfPrecisionLayers.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::SummaryType::numberOfGoodPrecisionLayers);  numberOfGoodPrecisionLayers.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::SummaryType::numberOfPrecisionHoleLayers);  numberOfPrecisionHoleLayers.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::SummaryType::numberOfPhiLayers);            numberOfPhiLayers.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::SummaryType::numberOfPhiHoleLayers);        numberOfPhiHoleLayers.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::SummaryType::numberOfTriggerEtaLayers);     numberOfTriggerEtaLayers.push_back( (int)ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::SummaryType::numberOfTriggerEtaHoleLayers); numberOfTriggerEtaHoleLayers.push_back( (int)ui_tmpval );

   // primarySector.push_back( input.primarySector() );
   // secondarySector.push_back( input.secondarySector() );

   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer1Hits); phiLayer1Hits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer2Hits); phiLayer2Hits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer3Hits); phiLayer3Hits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer4Hits); phiLayer4Hits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer1Hits); etaLayer1Hits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer2Hits); etaLayer2Hits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer3Hits); etaLayer3Hits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer4Hits); etaLayer4Hits.push_back( ui_tmpval );
                                                      
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer1Holes);  phiLayer1Holes.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer2Holes);  phiLayer2Holes.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer3Holes);  phiLayer3Holes.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer4Holes);  phiLayer4Holes.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer1Holes);  etaLayer1Holes.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer2Holes);  etaLayer2Holes.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer3Holes);  etaLayer3Holes.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer4Holes);  etaLayer4Holes.push_back( ui_tmpval );
                                                      
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer1RPCHits);  phiLayer1RPCHits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer2RPCHits);  phiLayer2RPCHits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer3RPCHits);  phiLayer3RPCHits.push_back( ui_tmpval );
   // ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer4RPCHits);  phiLayer4RPCHits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer1RPCHits);  etaLayer1RPCHits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer2RPCHits);  etaLayer2RPCHits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer3RPCHits);  etaLayer3RPCHits.push_back( ui_tmpval );
   // ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer4RPCHits);  etaLayer4RPCHits.push_back( ui_tmpval );
                                                      
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer1RPCHoles);  phiLayer1RPCHoles.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer2RPCHoles);  phiLayer2RPCHoles.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer3RPCHoles);  phiLayer3RPCHoles.push_back( ui_tmpval );
   // ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer4RPCHoles);  phiLayer4RPCHoles.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer1RPCHoles);  etaLayer1RPCHoles.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer2RPCHoles);  etaLayer2RPCHoles.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer3RPCHoles);  etaLayer3RPCHoles.push_back( ui_tmpval );
   // ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer4RPCHoles);  etaLayer4RPCHoles.push_back( ui_tmpval );
                                                      
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer1TGCHits);  phiLayer1TGCHits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer2TGCHits);  phiLayer2TGCHits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer3TGCHits);  phiLayer3TGCHits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer4TGCHits);  phiLayer4TGCHits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer1TGCHits);  etaLayer1TGCHits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer2TGCHits);  etaLayer2TGCHits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer3TGCHits);  etaLayer3TGCHits.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer4TGCHits);  etaLayer4TGCHits.push_back( ui_tmpval );
                                                      
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer1TGCHoles);  phiLayer1TGCHoles.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer2TGCHoles);  phiLayer2TGCHoles.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer3TGCHoles);  phiLayer3TGCHoles.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::phiLayer4TGCHoles);  phiLayer4TGCHoles.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer1TGCHoles);  etaLayer1TGCHoles.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer2TGCHoles);  etaLayer2TGCHoles.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer3TGCHoles);  etaLayer3TGCHoles.push_back( ui_tmpval );
   ui_tmpval = 0; input.summaryValue(ui_tmpval ,xAOD::etaLayer4TGCHoles);  etaLayer4TGCHoles.push_back( ui_tmpval );

   // annBarrel.push_back( input.annBarrel() );
   // annEndCap.push_back( input.annEndCap() );
   // innAngle.push_back( input.innAngle() );
   // midAngle.push_back( input.midAngle() );

   return;
}
