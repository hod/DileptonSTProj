#ifndef __Analysis_OutputHemisphere__
#define __Analysis_OutputHemisphere__

#include <DileptonST/OutputObject.h>

class TLorentzVector;

namespace Analysis {
   class OutputHemisphere : public OutputObject {
   public:
      Int_t n;

      std::vector<Float_t> pt;
      std::vector<Float_t> eta;
      std::vector<Float_t> phi;
      std::vector<Float_t> m;

   public:
      OutputHemisphere(Bool_t doTrim = kFALSE);
      ~OutputHemisphere();
      void reset();
      void attachToTree(TTree *tree);

      void add(const TLorentzVector &input);
   };
}

#endif

