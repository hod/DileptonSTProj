// Interface class for OutputXXX classes
// Valerio Ippolito - Harvard University

#ifndef __Analysis_OutputObject__
#define __Analysis_OutputObject__

#include <TString.h>

class TTree;

namespace Analysis {
   class OutputObject {
   private:
      TString m_name;
      Bool_t m_doTrim;
      Bool_t m_write;

   public:

      inline OutputObject(Bool_t doTrim = kFALSE)
      {
         m_doTrim = doTrim;
         m_write = true;
      }

      inline TString name()
      {
         return m_name;
      }

      inline Bool_t doTrim()
      {
         return m_doTrim;
      }

      inline Bool_t write()
      {
         return m_write;
      }

      inline void setName(TString val)
      {
         m_name = val;
      }

      inline void setDoTrim(Bool_t val)
      {
         m_doTrim = val;
      }

      inline void setWrite(Bool_t val)
      {
         m_write = val;
      }


   public:
      inline virtual ~OutputObject() = 0;
      virtual void reset() = 0;
      virtual void attachToTree(TTree *tree) = 0;
   };
}

Analysis::OutputObject::~OutputObject()
{
}

#endif
