#include "DileptonST/VJetsUncertaintyTool.h"

#include <TH1.h>
#include <TFile.h>
#include <TObjArray.h>

#include <iostream>

using namespace DileptonST;

TH1 *getHisto(TDirectory *fIn, TString hname);


VJetsUncertaintyTool::VJetsUncertaintyTool()
{
   m_initialized = false;
   m_inputName = "correcctions_vjets.root";
   m_histoMap.clear();
   m_variations.clear();
   m_applyEWCorrection["eej"] = true;
   m_applyQCDCorrection["eej"] = true;
   m_applyEWCorrection["evj"] = true;
   m_applyQCDCorrection["evj"] = true;
   m_applyEWCorrection["vvj"] = true;
   m_applyQCDCorrection["vvj"] = true;
   m_applyEWCorrection["aj"] = true;
   m_applyQCDCorrection["aj"] = true;
}

VJetsUncertaintyTool::~VJetsUncertaintyTool()
{

   for (std::map<TString, TH1*>::iterator itr = m_histoMap.begin(); itr != m_histoMap.end(); itr++) {
      delete itr->second;
      itr->second = 0;
   }
   m_histoMap.clear();
   m_variations.clear();
}


void VJetsUncertaintyTool::setInputFileName(TString fname)
{
   m_inputName = fname;
}

void VJetsUncertaintyTool::applyEWCorrection(bool doApply, TString processes)
{

   TObjArray *tokens = processes.Tokenize(",");
   for (int iTok = 0; iTok < tokens->GetSize(); iTok++) {
      if (!tokens->At(iTok)) break;
      TString process = tokens->At(iTok)->GetName();
      m_applyEWCorrection[process] = doApply;
   }
}

void VJetsUncertaintyTool::applyQCDCorrection(bool doApply, TString processes)
{

   TObjArray *tokens = processes.Tokenize(",");
   for (int iTok = 0; iTok < tokens->GetSize(); iTok++) {
      if (!tokens->At(iTok)) break;
      TString process = tokens->At(iTok)->GetName();
      m_applyQCDCorrection[process] = doApply;
   }
}

int VJetsUncertaintyTool::initialize()
{

   std::vector<TString> variations = { "Nominal",
                                       "vjets_d1kappa_EW_High", "vjets_d1kappa_EW_Low",
                                       "vjets_d2kappa_EW_High", "vjets_d2kappa_EW_Low",
                                       "vjets_d3kappa_EW_High", "vjets_d3kappa_EW_Low",
                                       "vjets_d1K_NLO_High", "vjets_d1K_NLO_Low",
                                       "vjets_d2K_NLO_High", "vjets_d2K_NLO_Low",
                                       "vjets_d3K_NLO_High", "vjets_d3K_NLO_Low",
                                       "vjets_dK_NLO_mix_High", "vjets_dK_NLO_mix_Low",
                                       "vjets_dK_NLO_fix_High", "vjets_dK_NLO_fix_Low"
                                     };

   m_variations = variations;

   TFile *fIn = TFile::Open(m_inputName, "READ");

   std::vector<TString> processes = {"eej", "evj", "vvj", "aj"};

   for (unsigned int iP = 0; iP < processes.size(); iP++) {

      TString process = processes[iP];

      TH1 *hMC = getHisto(fIn, process + "_mc");
      if (!hMC) return 1;

      TH1 *hSLO = getHisto(fIn, process + "_pTV_LO");
      TH1 *hKNLO = getHisto(fIn, process + "_pTV_K_NLO");
      TH1 *hd1KNLO = getHisto(fIn, process + "_pTV_d1K_NLO");
      TH1 *hd2KNLO = getHisto(fIn, process + "_pTV_d2K_NLO");
      TH1 *hd3KNLO = getHisto(fIn, process + "_pTV_d3K_NLO");
      TH1 *hkEW = getHisto(fIn, process + "_pTV_kappa_EW");
      TH1 *hd1kEW = getHisto(fIn, process + "_pTV_d1kappa_EW");
      TH1 *hd2kEW = getHisto(fIn, process + "_pTV_d2kappa_EW");
      TH1 *hd3kEW = getHisto(fIn, process + "_pTV_d3kappa_EW");
      TH1 *hdKNLOmix = getHisto(fIn, process + "_pTV_dK_NLO_mix");
      TH1 *hKNLOfix = 0;
      if (process == "aj") {
         hKNLOfix = getHisto(fIn, process + "_pTV_K_NLO_fix");
      }

      for (unsigned int iVar = 0; iVar < m_variations.size(); iVar++) {

         TString variation = m_variations[iVar];

         double e[8] = {0};
         if (variation == "vjets_d1kappa_EW_High") e[0] = 1;
         if (variation == "vjets_d1kappa_EW_Low") e[0] = -1;
         if (variation == "vjets_d2kappa_EW_High") e[1] = 1;
         if (variation == "vjets_d2kappa_EW_Low") e[1] = -1;
         if (variation == "vjets_d3kappa_EW_High") e[2] = 1;
         if (variation == "vjets_d3kappa_EW_Low") e[2] = -1;
         if (variation == "vjets_d1K_NLO_High") e[3] = 1;
         if (variation == "vjets_d1K_NLO_Low") e[3] = -1;
         if (variation == "vjets_d2K_NLO_High") e[4] = 1;
         if (variation == "vjets_d2K_NLO_Low") e[4] = -1;
         if (variation == "vjets_d3K_NLO_High") e[5] = 1;
         if (variation == "vjets_d3K_NLO_Low") e[5] = -1;
         if (variation == "vjets_dK_NLO_mix_High") e[6] = 1;
         if (variation == "vjets_dK_NLO_mix_Low") e[6] = -1;
         if (variation == "vjets_dK_NLO_fix_High") e[7] = 1;
         if (variation == "vjets_dK_NLO_fix_Low") e[7] = -1;

         TH1 *hVar = (TH1*)hMC->Clone(process + "_" + variation);
         hVar->SetDirectory(0);
         hVar->Reset();

         for (int bin = 1; bin <= hMC->GetNbinsX() + 1; bin++) {

            double kappa_EW = hkEW->GetBinContent(bin);
            double d1kappa_EW = hd1kEW->GetBinContent(bin);
            double d2kappa_EW = hd2kEW->GetBinContent(bin);
            double d3kappa_EW = hd3kEW->GetBinContent(bin);
            double K_NLO = hKNLO->GetBinContent(bin);
            double K_NNLO = K_NLO * 0.9725;
            double d1K_NLO = hd1KNLO->GetBinContent(bin);
            double d2K_NLO = hd2KNLO->GetBinContent(bin);
            double d3K_NLO = hd3KNLO->GetBinContent(bin);
            double dK_NLO_mix = hdKNLOmix->GetBinContent(bin);
            double K_NLO_fix = K_NLO;
            if (hKNLOfix) {
               K_NLO_fix = hKNLOfix->GetBinContent(bin);
            }
            double dK_NLO_fix = fabs(K_NLO_fix - K_NLO);

            double sLO = hSLO->GetBinContent(bin);

            double e_kappa_EW = kappa_EW + e[0] * d1kappa_EW + e[1] * d2kappa_EW + e[2] * d3kappa_EW;
            double e_K_NNLO = K_NNLO + e[3] * d1K_NLO + e[4] * d2K_NLO + e[5] * d3K_NLO + e[7] * dK_NLO_fix;
            double e_K = e_K_NNLO * (1 + e_kappa_EW) + e[6] * dK_NLO_mix;

            double sNLO = e_K * sLO;

            if (!m_applyEWCorrection[process]) {
               sNLO /= (1 + kappa_EW);
            }

            if (!m_applyQCDCorrection[process]) {
               sNLO *= hMC->GetBinContent(bin) / (K_NNLO * sLO) ;
            }

            hVar->SetBinContent(bin, sNLO);
         }
         hVar->Divide(hMC);
         m_histoMap[hVar->GetName()] = hVar;
      }
   }

   fIn->Clear();
   fIn->Close();
   delete fIn;

   m_initialized = true;
   return 0;
}


const std::vector<TString> &VJetsUncertaintyTool::getAllVariationNames()
{

   if (!m_initialized) {
      std::cout << "ERROR: VJetsUncertaintyTool not initialized" << std::endl;
      return m_variations;
   }

   return m_variations;
}

double VJetsUncertaintyTool::getCorrection(int mcChannelNumber, double pTV, TString variation)
{

   if (!m_initialized) {
      std::cout << "ERROR: VJetsUncertaintyTool not initialized" << std::endl;
      return 1;
   }

   TString process = "";
   // Sherpa 2.2.0
   if (363102 <= mcChannelNumber && mcChannelNumber <= 363122) process = "eej"; // Ztt
   if (363361 <= mcChannelNumber && mcChannelNumber <= 363363) process = "eej"; // Ztt
   if (363364 <= mcChannelNumber && mcChannelNumber <= 363387) process = "eej"; // Zuu
   if (363388 <= mcChannelNumber && mcChannelNumber <= 363411) process = "eej"; // Zee
   if (363412 <= mcChannelNumber && mcChannelNumber <= 363435) process = "vvj"; // Zvv
   if (363436 <= mcChannelNumber && mcChannelNumber <= 363459) process = "evj"; // Wuv
   if (363460 <= mcChannelNumber && mcChannelNumber <= 363483) process = "evj"; // Wev
   if (363331 <= mcChannelNumber && mcChannelNumber <= 363354) process = "evj"; // Wtv
   // Sherpa 2.2.1
   if (364100 <= mcChannelNumber && mcChannelNumber <= 364113) process = "eej"; // Zuu
   if (364114 <= mcChannelNumber && mcChannelNumber <= 364127) process = "eej"; // Zee
   if (364128 <= mcChannelNumber && mcChannelNumber <= 364141) process = "eej"; // Ztt
   if (364142 <= mcChannelNumber && mcChannelNumber <= 364155) process = "vvj"; // Zvv
   if (364156 <= mcChannelNumber && mcChannelNumber <= 364169) process = "evj"; // Wuv
   if (364170 <= mcChannelNumber && mcChannelNumber <= 364183) process = "evj"; // Wev
   if (364184 <= mcChannelNumber && mcChannelNumber <= 364197) process = "evj"; // Wtv
   // Single photon
   if (361039 <= mcChannelNumber && mcChannelNumber <= 361062) process = "aj"; // Wtv

   if (process == "") return 1.;

   std::map<TString, TH1*>::iterator itr = m_histoMap.find(process + "_" + variation);
   if (itr == m_histoMap.end()) {
      std::cout << "Error: unknown variation: " << variation << std::endl;
      return 1;
   }

   TH1 *hCorr = itr->second;
   if (pTV < hCorr->GetXaxis()->GetXmin()) return 1;
   if (pTV >= hCorr->GetXaxis()->GetXmax()) return 1;
   int bin = hCorr->FindBin(pTV);

   return hCorr->GetBinContent(bin);
}

TH1 *getHisto(TDirectory *fIn, TString hname)
{

   TH1 *h = 0;

   if (!fIn || fIn->IsZombie()) {
      return h;
   }

   TObject *obj = fIn->Get(hname);
   if (!obj) {
      std::cout << "Error: object of name " << hname << " was not found in file " << fIn->GetName() << std::endl;
      return h;
   }

   h = (TH1*)obj->Clone();

   return h;
}

