# code to interpret the two dictionaries of DM systematics obtained from private and official production
# Valerio Ippolito - Harvard University

# input dictionaries have the following structure:
#  evts_sel["DMP_tloop_1_10_g1"]["nominal"]["generated"] = 8307.52
#  evts_sel["DMP_tloop_1_10_g1"]["nominal"]["250"] = 256.8

import math
import re

def getVariations(evts, ref='nominal', allowed=[], xsecreg='generated'):
  # input: yield in sample, variation, region
  # output: list of relative differences wrt nominal in sample, region, computed only for allowed variations
  #       - for the cross-section region, the quantity of interest is the difference between variation yield
  #         and nominal, divided by nominal (-> xsec effect)
  #       - for the other regions, it is instead the difference between the ratios between the variation yield
  #         and the overall yield, divided by the nominal version of this ratio (-> acc effect)
  reldiffs = {}
  for sample in evts.keys():
    reldiffs[sample] = {}
    for variation in evts[sample].keys():
      if variation == ref: continue # skip nominal
      goAhead = False
      for allow in allowed:
        if allow in variation: goAhead = True
      if not goAhead: continue # keep only what we asked for

      for region in evts[sample][variation].keys(): 
        if region not in reldiffs[sample].keys():
          reldiffs[sample][region] = [] # a list, so that we can check we have a given number of entries

        if region == xsecreg:
          # xsec
          num = evts[sample][variation][region]
          den = evts[sample][ref][region]
        else:
          # acc
          num = evts[sample][variation][region]/evts[sample][variation][xsecreg]
          den = evts[sample][ref][region]/evts[sample][ref][xsecreg]
        if den == 0:
          print 'WARNING: (sample, variation, region) = (%s, %s, %s) has zero denominator for %s - setting 0%% uncertainty ' % (sample, variation, region, ref)
          reldiffs[sample][region].append(0.0)
        else:
          reldiffs[sample][region].append(num / den - 1.0)
  return reldiffs


def getIntraPDF(evts, ref='nominal', N=100):
  # NNPDF prescription (https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PdfRecommendations#Standard_deviation)
  # input: yield in sample, variation, region
  # output: relative difference wrt nominal in sample, region
  reldiffs = getVariations(evts, ref, allowed=['NNPDF_VAR'], xsecreg='generated')

  result = {}
  for sample in reldiffs.keys():
    result[sample] = {}
    for region in reldiffs[sample].keys():
      if len(reldiffs[sample][region]) != N:
        raise RuntimeError('Unable to find all variations')

      result[sample][region] = math.sqrt(1./float(N) * sum(map(lambda x: pow(x, 2.), reldiffs[sample][region])))

  return result
      
def getInterPDF(evts, ref='nominal', N=2):
  # worst difference between NNPDF, MMHT and CT14 (https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PdfRecommendations#Differences_between_PDF_sets
  # input: yield in sample, variation, region
  # output: relative difference wrt nominal in sample, region
  reldiffs = getVariations(evts, ref, allowed=['MMHT', 'CT14'], xsecreg='generated')

  result = {}
  for sample in reldiffs.keys():
    result[sample] = {}
    for region in reldiffs[sample].keys():
      if len(reldiffs[sample][region]) != N:
        raise RuntimeError('Unable to find all variations')

      result[sample][region] = max(map(abs, reldiffs[sample][region])) # take max variation

  return result

def getScale(evts, ref='nominal', N=6):
  # worst difference between alternative scale variations (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/JDMSignalUncertainties)
  # input: yield in sample, variation, region
  # output: relative difference wrt nominal in sample, region
  reldiffs = getVariations(evts, ref, allowed=['SCALE_VAR'], xsecreg='generated')

  result = {}
  for sample in reldiffs.keys():
    result[sample] = {}
    for region in reldiffs[sample].keys():
      if len(reldiffs[sample][region]) != N:
        raise RuntimeError('Unable to find all variations')

      result[sample][region] = max(map(abs, reldiffs[sample][region])) # take max variation

  return result

def getTune(evts, ref='nominal', N=10):
  # worst difference between alternative MC tunes (https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCTuningRecommendations)
  # input: yield in sample, variation, region
  # output: relative difference wrt nominal in sample, region
  reldiffs = getVariations(evts, ref, allowed=['Var'], xsecreg='generated')

  result = {}
  for sample in reldiffs.keys():
    result[sample] = {}
    for region in reldiffs[sample].keys():
      if len(reldiffs[sample][region]) != N:
        raise RuntimeError('Unable to find all variations')

      result[sample][region] = max(map(abs, reldiffs[sample][region])) # take max variation

  return result

def getMasses(sample):
  # return (process, x,y) = (process, mMed, mDM)
  regexp = re.compile('(DM\w+)_(\d+)_(\d+)')

  m = regexp.match(sample)

  if m:
    process = m.group(1)
    mDM = m.group(2)
    mMed = m.group(3)
    return (process, mMed, mDM)
  else:
    print sample
    raise RuntimeError('Unable to parse')

if __name__ == '__main__':
  folder  = 'DileptonST/python/systutils'

  import sys
  sys.path.append(folder)

  import DMsyst_privateprod as private # tune, inter-PDF
  import DMsyst_officialprod as official # intra-PDF, scale

  # prepare syst variations
  syst = {}
  syst['intraPDF'] = getIntraPDF(official.evts_sel)
  syst['interPDF'] = getInterPDF(private.evts_sel)
  syst['scale'] = getScale(official.evts_sel)
  syst['tune'] = getTune(private.evts_sel)

  # print them assuming interPDF includes every region and sample
  good_kind = 'interPDF'
  print '\n\n'
  print 'Systematic variations'
  print '-----------------------------------------------------'
  for sample in sorted(syst[good_kind].keys(), key=getMasses):
      (process, mMed, mDM) = getMasses(sample)
      print '  %s mMed=%s GeV mDM=%s GeV' % (process, mMed, mDM)

      print '                %s' % ('    '.join(syst.keys()))

      xsec_effects = [] # in %
      for kind in syst.keys():
        if sample in syst[kind].keys():
          xsec_effects.append('%+.2f%%' % (syst[kind][sample]['generated'] * 100))
        else:
          xsec_effects.append('  NA  ')
      print '       xsec:      %s' % ('    '.join(xsec_effects))

      regions = sorted(filter(lambda x: x != 'generated', syst[good_kind][sample].keys()), key=float)
      for region in regions:
        region_effects = [] # in %
        for kind in syst.keys():
          if sample in syst[kind].keys():
            region_effects.append('%+.2f%%' % (syst[kind][sample][region] * 100.))
          else:
            region_effects.append(' NA ')
        print '       acc(%s): %s' % (region, '    '.join(region_effects))
