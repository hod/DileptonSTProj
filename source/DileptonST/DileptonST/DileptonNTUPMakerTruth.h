#ifndef DileptonST_DileptonNTUPMakerTruth_H
#define DileptonST_DileptonNTUPMakerTruth_H

#include <iostream>

#include <EventLoop/Algorithm.h>
#include <EventLoop/StatusCode.h>

#include <xAODCore/ShallowCopy.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODJet/JetContainer.h>

#include <DileptonST/Enums.h>
#include <DileptonST/OutputTruth.h>
#include <DileptonST/CutFlowTool.h>

class DileptonNTUPMakerTruth : public EL::Algorithm {
public:
   typedef enum {
      SR = 0,
      CR_Wmunu,
      CR_Wenu,
   } SelectionCriteria;


   // put your configuration variables here as public variables.
   // that way they can be set directly from CINT and python.
public:
   TString outputName;




   // variables that don't get filled at submission time should be
   // protected from being send from the submission node to the worker
   // node (done by the //!)
public:
   // things used to access xAOD information
   Bool_t m_determinedDerivation; //!
   Bool_t m_isEXOT5; //!
   std::pair<xAOD::TruthParticleContainer*, xAOD::ShallowAuxContainer*> m_muons; //!
   xAOD::TruthParticleContainer m_allMuons; //!
   xAOD::TruthParticleContainer m_goodMuons; //!

   std::pair<xAOD::TruthParticleContainer*, xAOD::ShallowAuxContainer*> m_electrons; //!
   xAOD::TruthParticleContainer m_allElectrons; //!
   xAOD::TruthParticleContainer m_goodElectrons; //!

   std::pair<xAOD::JetContainer*, xAOD::ShallowAuxContainer*> m_jets; //!
   xAOD::JetContainer m_allJets; //!
   xAOD::JetContainer m_goodJets; //!

   // things used for output
   TTree * m_tree; //!
   Analysis::OutputTruth m_cand; //!
   TH1F *m_histoEventCount; //!

   // things used in the analysis
   Int_t m_eventCounter; //!
   Analysis::CutFlowTool m_cutFlow; //!
   TVector2 m_met_nomuon_to_use; //!
   TVector2 m_met_wmuon_to_use; //!
   TVector2 m_met_noelectron_to_use; //!

   // methods used in the analysis
   virtual EL::StatusCode getObjects();
   virtual EL::StatusCode getLastCutPassed(SelectionCriteria crit, DileptonCuts::CutID &last);
   virtual EL::StatusCode getPhaseSpaceWeight(Float_t &result);
   virtual EL::StatusCode getTruthVectors(Float_t &truth_dm_pt, Float_t &truth_dm_q, Float_t &truth_dmjet_q);
   virtual EL::StatusCode analyzeEvent();


   // this is a standard constructor
   DileptonNTUPMakerTruth();

   // these are the functions inherited from Algorithm
   virtual EL::StatusCode setupJob(EL::Job& job);
   virtual EL::StatusCode fileExecute();
   virtual EL::StatusCode histInitialize();
   virtual EL::StatusCode changeInput(bool firstFile);
   virtual EL::StatusCode initialize();
   virtual EL::StatusCode execute();
   virtual EL::StatusCode postExecute();
   virtual EL::StatusCode finalize();
   virtual EL::StatusCode histFinalize();

   // this is needed to distribute the algorithm to the workers
   ClassDef(DileptonNTUPMakerTruth, 1);
};

#endif
