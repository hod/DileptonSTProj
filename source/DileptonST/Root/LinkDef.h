#include <DileptonST/DileptonNTUPMaker.h>
#include <DileptonST/DileptonNTUPMakerTruth.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__

#pragma link C++ class DileptonNTUPMaker+;
#pragma link C++ class OutputEntry+;
#pragma link C++ class CutFlowTool+;
#pragma link C++ class DileptonNTUPMaker::ContentHolder+;
#pragma link C++ class DileptonNTUPMakerTruth+;

#endif
